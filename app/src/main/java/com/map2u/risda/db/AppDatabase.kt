package com.map2u.risda.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.map2u.risda.util.StorageUtils
import java.io.File

@Database(entities = [Dummy::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    companion object {

        const val DB_NAME = "risda.db"

        @Volatile
        private var database: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase = database ?: buildDatabase(context)

        @Synchronized
        fun buildDatabase(context: Context): AppDatabase {
            val path = getDbPath()
            val file = File(path)
            if (!file.exists())
                file.mkdirs()

            if (database == null) {
                database = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "$path/$DB_NAME"
                ).build()
            }
            return database!!
        }

        private fun getDbPath(): String = "${StorageUtils.getUserStoragePath()}/db"

        fun getDbFile(): File = File(getDbPath(), DB_NAME)
    }
}