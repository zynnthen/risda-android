package com.map2u.risda.db

object DbConstants {

    const val EXCEPTION_TABLE_NAME = "exception"

    const val APPOINTMENT_TABLE_NAME = "appointment"
    const val TS_APPLICATION_TABLE_NAME = "tsApplication"
    const val LOT_DIMILIKI_TABLE_NAME = "lotDimiliki"
    const val PARTICIPANT_TABLE_NAME = "participant"
    const val KUMPULAN_TS_TABLE_NAME = "kumpulanTS"
    const val FARM_BUDGET_ITEM_TABLE_NAME = "farmBudgetItem"
    const val FORM_TABLE_NAME = "form"
    const val ADD_TS18_FORM_TABLE_NAME = "addTS18Form"
    const val ADD_TS18_FORM_IMAGE_TABLE_NAME = "addTS18FormImage"

    const val ADD_TS18A_FORM_TABLE_NAME = "addTS18AForm"
    const val ADD_TS18A_FORM_IMAGE_TABLE_NAME = "addTS18AFormImage"

    const val tkhTJanji = "tkhTJanji"
    const val masaTJanji = "masaTJanji"
    const val tkhKemaskini = "tkhKemaskini"
    const val tkhLawat = "tkhLawat"
    const val tkhLulus = "tkhLulus"

    const val signPegawai = "signPegawai"
    const val signPerakuan = "signPerakuan"

    const val TS_MOHON_ID = "tsMohonId"
    const val TS_MOHON_ID_PK = "tsMohonIdPk"
    const val NO_SIRI_TEMUJANJI = "noSiriTemujanji"
    const val NO_SIRI_TEMUJANJI_PK = "noSiriTemujanjiPk"
    const val TS_PENDEKATAN = "tsPendekatan"
    const val TS_PENDEKATAN_PK = "tsPendekatanPk"
    const val JENIS_LK = "jenisLk"
    const val JENIS_LK_PK = "jenisLkPk"
    const val KUMPULAN_ID = "kumpulanId"
    const val KUMPULAN_ID_PK = "kumpulanIdPk"
    const val NAMA_KUMPULAN = "namaKumpulan"
    const val NAMA_KUMPULAN_PK = "namaKumpulanPk"
    const val PT_CODE = "ptCode"
    const val PT_CODE_PK = "ptCodePk"

    const val STATUS_TEMUJANJI = "statusTJanji"

    const val TARIKH_TEMUJANJI = "tkhTJanji"

    val TABLE_NAMES = arrayOf(
        APPOINTMENT_TABLE_NAME,
        TS_APPLICATION_TABLE_NAME,
        LOT_DIMILIKI_TABLE_NAME,
        PARTICIPANT_TABLE_NAME,
        KUMPULAN_TS_TABLE_NAME,
        FARM_BUDGET_ITEM_TABLE_NAME,
        FORM_TABLE_NAME,
        ADD_TS18_FORM_TABLE_NAME,
        ADD_TS18_FORM_IMAGE_TABLE_NAME,
        ADD_TS18A_FORM_TABLE_NAME,
        ADD_TS18A_FORM_IMAGE_TABLE_NAME
    )

    val APPOINTMENT_TABLE_COLUMN_NAMES = arrayOf(
        "noSiriTemujanji",
        "ptCode",
        "moduleOwner",
        "tsPendekatan",
        "jenisLK",
        "tsMohonId",
        "kumpulanId",
        "kumpulanClaim",
        "tkhTJanji",
        "masaTJanji",
        "tempatTJanji",
        "statusTJanji",
        "pegKemaskini",
        "tkhKemaskini",
        "ansuranNo"
    )

    val TS_APPLICATION_TABLE_COLUMN_NAMES = arrayOf(
        "ptCode",
        "tsMohonNo",
        "tsJenis",
        "pemilikLot",
        "idESpek",
        "icNoPemohon",
        "namaPemohon",
        "sexPemohon",
        "racePemohon",
        "address1",
        "address2",
        "address3",
        "poskod",
        "bandar",
        "negeri",
        "daerah",
        "mukim",
        "seksyen",
        "kampung",
        "dun",
        "parlimen",
        "noPhoneMobile",
        "noPhoneHouse",
        "noPhoneOffice",
        "email",
        "bankAccNo",
        "bankName",
        "bankSCode",
        "tsPendekatan",
        "tsAgensi",
        "tsAktiviti",
        "tsProgram",
        "tsRMK",
        "asalPemohon",
        "noSmbTso",
        "tkhTerima",
        "tkhLulus",
        "pegLulus",
        "ptrimaBantuan",
        "statusPmohon",
        "statusAlasan",
        "statusAktif",
        "statusPerluTS18",
        "kodPASemasa",
        "kodObjSTunai",
        "kodObjSStok",
        "kodPaBerturut",
        "kodObjBTunai",
        "kodObjBStok",
        "pegKemaskini",
        "tkhKemaskini",
        "noSiriTs",
        "dtSiriTs",
        "warganegara",
        "bayarKepada",
        "swkStatus",
        "swkKod",
        "tsKategori",
        "kumpulanId",
        "tsMohonId",
        "dob_Pmohon",
    )

    val LOT_DIMILIKI_TABLE_COLUMN_NAMES = arrayOf(
        "lotMilikId",
        "ptCode",
        "tsMohonId",
        "icNoMilik",
        "noLithosheet",
        "noGeran",
        "noGeranLama",
        "noLot",
        "noLotLama",
        "bhgnMilikan",
        "luasDiambil",
        "tkhDiambil",
        "syaratKhas",
        "butirPajak",
        "butirKaveat",
        "catatan",
        "negeri",
        "daerah",
        "mukim",
        "seksyen",
        "kampung",
        "dun",
        "parlimen",
        "replant",
        "syaratNyata",
        "tsCrop",
        "tsCropSub",
        "tsBulan",
        "tsTahun",
        "luasLot",
        "luasTs",
        "pegKemaskini",
        "tkhKKemaskini",
        "carianRasmi",
        "tarikhPecahan",
        "sumber",
        "logUpi",
        "kodUpi",
        "tarafTanah"
    )

    val PARTICIPANT_TABLE_COLUMN_NAMES = arrayOf(
        "temujanjiPID",
        "noSiriTemujanji",
        "kumpulanID",
        "status",
        "lejer",
        "tsmohonID",
    )

    val KUMPULAN_TS_TABLE_COLUMN_NAMES = arrayOf(
        "kumpulanId",
        "namaKumpulan",
    )

    val FARM_BUDGET_TABLE_COLUMN_NAMES = arrayOf(
        "ptCode",
        "ansuranNo",
        "katKerja",
        "kerjaStokKod",
        "stokItem",
        "jenisStok",
        "perihalStokItem",
        "ukuran",
        "kodKerjaId",
        "kodKerjaDesc",
        "noSiriTemujanji"
    )

    // todo store documents for offline mode
    val FORM_TABLE_COLUMN_NAMES = arrayOf(
        "ptCode",
        "tsMohonId",
        "statusLulus",
        "pegPemeriksa",
        "pegLulus",
        "pegKemaskini",
        "noSiriTemujanji",
        "noKpPpk",
        "jenisLK",
        "jawPemeriksa",
        "catatan",
        "tkhLulus",
        "tkhLawat",
        "tkhKemaskini",
        "bilLawatan",
        "noSiriLB",
        "modulOwner",
        "tsMohonLBSiri",
        "ansuranNo",
        "perimeter",
        "tebangJentera1",
        "tebangJentera2",
        "jarakLubang1",
        "jarakLubang2",
        "jarakLubang3",
        "luasTanam",
        "tkhTanam",
        "jenisBenih",
        "klonBenih",
        "benihLuar",
        "sahKlon",
        "gapSenggara",
        "gapBaja",
        "gapCantasan",
        "jenisCantasan",
        "gapGalakDahan",
        "jenisGalakDahan",
        "kpbKontan",
        "kpbKontanSelenggara",
        "getahPkkHek",
        "tlPkkHidup",
        "sulaman",
        "ukurLilit",
        "kawalPerosak",
        "rumpaiSenggara",
        "rumpaiTdkSenggara",
        "tkhBerhasil",
        "kenyataanLain",
        "planTanah",
        "hadirSendiri",
        "luasBerjaya",
        "statusTolakKIV",
        "kumpulanId",
        "namaKumpulan",
        "kumpNoClaim",
        "jarakTanaman",
        "bekalAwal",
        "statusR7",
        "klonMutu",
        "resitBenih",
        "tapakSemaian",
        "benihSawit",
        "luasKontan",
        "signPegawai",
        "signPerakuan",
        "namaPerakuan",
        "noKpPerakuan",
        "hadir",
        "latitude",
        "longitude",
//        "tsApplication",
//        "documents",
        "activities",
        "ts18e",
        "mlawatMula",
        "mlawatTamat"
    )

    val ADD_FORM_TS18_COLUMN_NAMES = arrayOf(
        "alasanLulus",
        "alasanNo",
        "bantuanLain",
        "bilLawatan",
        "blAgensi",
        "blCrop",
        "blLuasHabisByr",
        "blLuasLulus",
        "blNoTs",
        "buktiLot",
        "buktiLotTs",
        "bukuLesen",
        "catatan",
        "catatanLulus",
        "gambarSatelit",
        "getahTua",
        "hasilK500",
        "jawPegLulus",
        "jawPemeriksa",
        "jenisLK",
        "kulitTorehan",
        "lainLain",
        "lesenGetah",
        "luasGetahTua",
        "luasHutan",
        "luasLain",
        "luasLainLain",
        "luasLulus",
        "luasRumah",
        "luasTanamanLain",
        "mlawatMula",
        "mlawatTamat",
        "namaPerakuan",
        "noKpPerakuan",
        "noKpPpk",
        "noSiriLK",
        "noSiriTemujanji",
        "pegKemaskini",
        "pegLulus",
        "pegPemeriksa",
        "pelanTanah",
        "pkHadir",
        "pokokK300",
        "ptCode",
        "sebabLainPRN",
        "signPegawai",
        "signPerakuan",
        "statusLulus",
        "suratAgensi",
        "tkhKemaskini",
        "tkhLawat",
        "tkhLulus",
        "tsMohonId",
        "tunggulGetah",
        "umurPokok",
        "latitude",
        "longitude",
        "hadir"
    )

    val ADD_FORM_TS18_IMAGE_COLUMN_NAMES = arrayOf(
        "noSiriTemujanji",
        "filePath"
    )

    val ADD_FORM_TS18A_COLUMN_NAMES = arrayOf(
        "activities",
        "ansuranNo",
        "bekalAwal",
        "benihLuar",
        "benihSawit",
        "bilLawatan",
        "catatan",
        "gapBaja",
        "gapCantasan",
        "gapGalakDahan",
        "gapSenggara",
        "getahPkkHek",
        "hadirSendiri",
        "jarakLubang1",
        "jarakLubang2",
        "jarakLubang3",
        "jarakTanaman",
        "jawPemeriksa",
        "jenisBenih",
        "jenisCantasan",
        "jenisGalakDahan",
        "jenisLK",
        "kawalPerosak",
        "kenyataanLain",
        "klonBenih",
        "klonMutu",
        "kpbKontan",
        "kpbKontanSelenggara",
        "kumpNoClaim",
        "kumpulanId",
        "luasBerjaya",
        "luasKontan",
        "luasTanam",
        "mlawatMula",
        "mlawatTamat",
        "modulOwner",
        "namaKumpulan",
        "namaPerakuan",
        "noKpPerakuan",
        "noKpPpk",
        "noSiriLB",
        "noSiriTemujanji",
        "pegKemaskini",
        "pegLulus",
        "pegPemeriksa",
        "perimeter",
        "planTanah",
        "ptCode",
        "resitBenih",
        "rumpaiSenggara",
        "rumpaiTdkSenggara",
        "sahKlon",
        "signPegawai",
        "signPerakuan",
        "statusLulus",
        "statusR7",
        "statusTolakKIV",
        "sulaman",
        "tapakSemaian",
        "tebangJentera1",
        "tebangJentera2",
        "tkhBerhasil",
        "tkhKemaskini",
        "tkhLawat",
        "tkhLulus",
        "tkhTanam",
        "tlPkkHidup",
        "tsMohonId",
        "tsMohonLBSiri",
        "ukurLilit",
        "latitude",
        "longitude",
        "hadir"
    )

    val ADD_FORM_TS18A_IMAGE_COLUMN_NAMES = arrayOf(
        "noSiriTemujanji",
        "tsmohonID",
        "filePath"
    )

    //used to replace any columnName contain this symbol"-"
    const val I_AM_DASH = "iamadash"
}