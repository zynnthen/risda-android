package com.map2u.risda.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.DatabaseUtils
import android.database.MatrixCursor
import android.database.sqlite.SQLiteDatabase
import android.text.TextUtils
import android.util.Log
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.sqlite.db.SupportSQLiteOpenHelper
import androidx.sqlite.db.SupportSQLiteQueryBuilder
import com.google.gson.*
import com.map2u.risda.BuildConfig
import com.map2u.risda.model.db.SimpleDColumn
import com.map2u.risda.util.Utils
import java.util.*
import kotlin.collections.ArrayList

class DbHelper {
    constructor(context: Context) {
        mDatabase = AppDatabase.getInstance(context.applicationContext).openHelper
    }

    private var mDatabase: SupportSQLiteOpenHelper?

    private val timestampColumns: HashSet<String> =
        hashSetOf(
            DbConstants.tkhTJanji,
            DbConstants.masaTJanji,
            DbConstants.tkhKemaskini,
            DbConstants.tkhLawat,
            DbConstants.tkhLulus
        )

    private val integerColumns: HashSet<String> =
        hashSetOf(
//            DbConstants.STORER_ID,
//            DbConstants.MODULE_ID,
//            DbConstants.JOB_ID,
//            DbConstants.QTY,
//            DbConstants.SHORT_QTY,
//            DbConstants.GIVE_UP_QTY,
//            DbConstants.DAMAGE_QTY,
//            BASE_QTY,
//            DbConstants.TOTAL_RELEASE_BASE_QTY,
//            DbConstants.BASE_QTY_PER_UOM
        )

    fun createTable(tableName: String, columnName: List<String>) {
        createTable(tableName, columnName, Collections.emptyMap())
    }

    fun createTable(
        tableName: String,
        columnName: List<String>,
        defaultValue: Map<String, String>
    ) {
        if (columnName.isNullOrEmpty()) {
            throw IllegalArgumentException("DbHelper, CreateTable(): Column Name list is empty")
        }

        val db = mDatabase?.writableDatabase

        val sb = StringBuilder()
        sb.append("CREATE TABLE IF NOT EXISTS $tableName (")

        // ensure names are unique
        val set: Set<String> = HashSet(columnName)
        val columnNameList: List<String> = set.toList()

        for (i in columnNameList.indices) {
            val column = columnNameList[i]

            sb.append(column)
            if (timestampColumns.contains(column)) {
                sb.append(" INTEGER DEFAULT 0")
            } else {
                sb.append(" TEXT")
                if (defaultValue.containsKey(column))
                    sb.append(" DEFAULT '${defaultValue[column]}'")
            }

            if (i < columnNameList.size - 1)
                sb.append(", ")
        }
        sb.append(");")

        val sql = sb.toString()
        Log.i(TAG, sql)
        db?.execSQL(sql)
    }

    fun deleteTable(tableName: String) {
        Log.i(TAG, "delete table")
        mDatabase?.writableDatabase?.execSQL("DROP TABLE IF EXISTS $tableName")
    }

    fun deleteAllTables() {
        Log.i(TAG, "delete all tables")
        val array = DbConstants.TABLE_NAMES
        array.forEach {
            mDatabase?.writableDatabase?.execSQL("DROP TABLE IF EXISTS $it")
        }
    }

    fun batchInsert(tableName: String, batch: List<List<SimpleDColumn>>) {
        batchInsert(tableName, batch, Collections.emptyMap())
    }

    fun batchInsert(
        tableName: String,
        batch: List<List<SimpleDColumn>>,
        duplicateValueForKey: Map<String, String>
    ) {
        Log.i(TAG, "Batch Insertion starts for table name: $tableName")
        val db = mDatabase?.writableDatabase ?: return

        val set: Set<String> = getTableColumnName(db, tableName)

        val size = batch.size
        db.beginTransaction()
        try {
            var cv: ContentValues? = null

            for (i in 0 until size) {
                val productColumn = batch[i]

                if (cv == null)
                    cv = ContentValues(productColumn.size)

                //add values to column
                productColumn.forEach { column ->
                    val dColId = column.DColId
                    if (set.contains(dColId))
                        cv.put(dColId, column.DColValue?.trim())
                    else
                        Log.w(TAG, "Table $tableName does not have Column $dColId. Dropping associated value ${column.DColValue}")

                    if (duplicateValueForKey.containsKey(dColId)) {
                        Log.d(TAG, "duplicating column = $dColId, value = ${column.DColValue} into ${duplicateValueForKey[dColId]}")
                        cv.put(duplicateValueForKey[dColId], column.DColValue?.trim())
                    }
                }
                Log.i(TAG, cv.toString())
                db.insert(tableName, SQLiteDatabase.CONFLICT_REPLACE, cv)
                cv.clear()
            }
            db.setTransactionSuccessful()
        } finally {
            db.endTransaction()
        }
    }

    fun getRawQuery(tableName: String, rawQuery: String, strings: Array<String>?): Cursor? {
        Log.d(TAG + " RAWQUERY", rawQuery)
        if (isTableExist(tableName))
            return mDatabase?.readableDatabase?.query(rawQuery, strings)
        return MatrixCursor(arrayOf())
    }

    fun isTableExist(tableName: String): Boolean =
        isTableExist(mDatabase?.readableDatabase, tableName)

    fun updateValue(
        tableName: String,
        dColId: String,
        dColValue: String,
        whereClause: String?,
        whereArgs: Array<String>?
    ): Int? {
        val cv = ContentValues(1)
        if (integerColumns.contains(dColId))
            cv.put(dColId, dColValue.toLong())
        else
            cv.put(dColId, dColValue)

        return mDatabase?.writableDatabase?.update(
            tableName,
            SQLiteDatabase.CONFLICT_REPLACE,
            cv,
            whereClause + "=?",
            whereArgs
        )
    }

    fun updateListOfAppointments(
        tableName: String,
        dColId: String,
        dColValue: String,
        whereClause: String?,
        whereArgs: Array<Long>?
    ): Int? {
        val cv = ContentValues(1)
        if (integerColumns.contains(dColId))
            cv.put(dColId, dColValue.toLong())
        else
            cv.put(dColId, dColValue)

        return mDatabase?.writableDatabase?.update(
            tableName,
            SQLiteDatabase.CONFLICT_REPLACE,
            cv,
            whereClause + "<?",
            whereArgs
        )
    }

    fun insert(tableName: String, scanItem: List<SimpleDColumn>) {
        Log.i(TAG, "Insertion starts for table name: $tableName")
        val db = mDatabase?.writableDatabase ?: return

        val size = scanItem.size
        db.beginTransaction()

        try {
            val tableColumnNames = getTableColumnName(db, tableName)
            val cv = ContentValues(size)
            scanItem.forEach { item ->
                val dColId = Utils.escapeString(item.DColId)
                if (tableColumnNames.contains(dColId))
                    cv.put(dColId, item.DColValue)
                else
                    Log.w(TAG, "Table $tableName does not have Column ${item.DColId}. Dropping associated value ${item.DColValue}")
            }
            db.insert(tableName, SQLiteDatabase.CONFLICT_REPLACE, cv)
            db.setTransactionSuccessful()
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            db.endTransaction()
        }
        Log.i(TAG, "Insertion complete for table name: $tableName")
    }

    companion object {
        val TAG = DbHelper::class.java.simpleName!!

        private var helper: DbHelper? = null

        @Synchronized
        fun getHelper(context: Context): DbHelper {
            if (helper == null)
                helper = DbHelper(context)
            return helper!!
        }

        fun getRowCount(
            db: SupportSQLiteDatabase?,
            tableName: String,
            distinct: Boolean,
            columnName: String?,
            selection: String?,
            selectionArgs: Array<String>?
        ): Long {
            if (!isTableExist(db, tableName)) return 0

            val builder =
                SupportSQLiteQueryBuilder.builder(tableName).selection(selection, selectionArgs)
            if (distinct) builder.distinct()
            if (!TextUtils.isEmpty(columnName)) builder.columns(arrayOf(columnName))

            var count: Long = 0
            db?.query(builder.create()).use { cursor ->
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        count = cursor.count.toLong()
                    }
                }
            }
            return count
        }

        fun findExactItem(
            db: SupportSQLiteDatabase?,
            tableName: String,
            columnName: String,
            targetValue: String
        ): Map<String, String> {
            Log.i(TAG, "finding item %s = %s $columnName $targetValue")

            val queryBuilder = SupportSQLiteQueryBuilder.builder(tableName).selection(
                "$columnName=?",
                arrayOf(targetValue)
            ).limit("1")

            var lists = listOf<Map<String, String>>()
            db?.query(queryBuilder.create()).use { cursor ->
                if (BuildConfig.DEBUG) {
                    DatabaseUtils.dumpCursor(cursor)
                }
                lists = convertCursorMap(cursor)
            }

            return if (lists.isEmpty()) HashMap() else lists[0]
        }

        fun convertCursorMap(cursor: Cursor?): List<Map<String, String>> {
            if (cursor == null || cursor.count == 0) return Collections.emptyList()

            val list: MutableList<Map<String, String>> =
                ArrayList(cursor.count)
            if (cursor.moveToFirst()) {
                val columnCount = cursor.columnCount
                val columnNames = cursor.columnNames
                do {
                    val row: MutableMap<String, String> =
                        HashMap(columnCount)
                    for (i in 0 until columnCount) {
                        row[columnNames[i]] = cursor.getString(i)
                    }
                    list.add(row)
                } while (cursor.moveToNext())
            }

            return list
        }

        fun isTableExist(db: SupportSQLiteDatabase?, tableName: String): Boolean {
            if (db == null) return false
            val query = "SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ?"
            var count = 0
            db.query(query, arrayOf("table", tableName)).use { cursor ->
                if (cursor.moveToFirst()) {
                    count = cursor.getInt(0)
                }
            }
            return count > 0
        }

        private fun getTableColumnName(db: SupportSQLiteDatabase?, tableName: String): Set<String> {
            if (db != null && !isTableExist(db, tableName)) return Collections.emptySet()

            val query = SupportSQLiteQueryBuilder.builder(tableName).limit("0").create()
            val set: MutableSet<String> = mutableSetOf()
            db!!.query(query).use { cursor ->
                val columnNames: Array<String> = cursor.columnNames
                Log.i(TAG, "$tableName = ${columnNames.contentToString()}")

                set.addAll(columnNames)
            }
            return Collections.unmodifiableSet(set)
        }

        fun convert(cursor: Cursor?): List<JsonObject>? {
            if (cursor == null) return null

            val columns = cursor.columnNames
            val size = columns.size
            val list: ArrayList<JsonObject> = arrayListOf()
            val gson = Utils.getGson()
            try {
                while (cursor.moveToNext()) {
                    val jsonObject = JsonObject()
                    for (i in 0 until size) {
                        val value = cursor.getString(i) ?: JsonNull.INSTANCE
                        jsonObject.add(Utils.revertEscapeString(columns[i]), gson?.toJsonTree(value))
                    }
                    list.add(jsonObject)
                }

            } catch (e: JsonParseException) {
                e.printStackTrace()
            } catch (e: JsonIOException) {
                e.printStackTrace()
            } catch (e: JsonSyntaxException) {
                e.printStackTrace()
            }
            return list
        }

        fun getDateWhereClause(columnName: String?): String? = "strftime(\'%Y-%m-%d\', $columnName)"

        fun getDateTimeWhereClause(columnName: String?): String? =
            "strftime(\'%Y-%m-%d %H:%M:%S\', $columnName)"

        fun insertAsJSON(helper: DbHelper?, tableName: String?, array: JsonArray?) {
            if (helper == null || TextUtils.isEmpty(tableName) || array == null) return

            try {
                val allRows: ArrayList<List<SimpleDColumn>> = ArrayList(array.size())
                for (i in 0 until array.size()) {
                    val simpleDColumnList = arrayListOf<SimpleDColumn>()
                    val jsonObject = array.get(i).asJsonObject
                    val sets = jsonObject.entrySet()
                    sets.forEach { set ->
                        var value = if (set.value == null) null else set.value.toString()
                        if (value is String) {
                            value = value.replace("""[$"]""".toRegex(), "").trim()
                        }
                        simpleDColumnList.add(
                            SimpleDColumn(
                                set.key,
                                value
                            )
                        )
                    }
                    allRows.add(simpleDColumnList)
                }

                helper.batchInsert(tableName!!, allRows)
            } catch (e: JsonParseException) {
                e.printStackTrace()
            }
        }

    }
}