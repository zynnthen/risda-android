package com.map2u.risda.db

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * A dummy class to allow Room to compile
 */
@Entity
class Dummy {
    @PrimaryKey(autoGenerate = true)
    var _id: Long? = null
}