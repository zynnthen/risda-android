package com.map2u.risda.model.appointment

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class AppointmentItem (
    @SerializedName("noSiriTemujanji")
    @Expose
    var noSiriTemujanji: String? = null,

    @SerializedName("ptCode")
    @Expose
    var ptCode: String? = null,

    @SerializedName("moduleOwner")
    @Expose
    var moduleOwner: String? = null,

    @SerializedName("tsPendekatan")
    @Expose
    var tsPendekatan: String? = null,

    @SerializedName("jenisLK")
    @Expose
    var jenisLK: String? = null,

    @SerializedName("tsMohonId")
    @Expose
    var tsMohonId: String? = null,

    @SerializedName("kumpulanId")
    @Expose
    var kumpulanId: String? = null,

    @SerializedName("kumpulanClaim")
    @Expose
    var kumpulanClaim: String? = null,

    @SerializedName("tkhTJanji")
    @Expose
    var tkhTJanji: Long? = null,

    @SerializedName("masaTJanji")
    @Expose
    var masaTJanji: Long? = null,

    @SerializedName("tempatTJanji")
    @Expose
    var tempatTJanji: String? = null,

    @SerializedName("statusTJanji")
    @Expose
    var statusTJanji: String? = null,

    @SerializedName("pegKemaskini")
    @Expose
    var pegKemaskini: String? = null,

    @SerializedName("tkhKemaskini")
    @Expose
    var tkhKemaskini: Long? = null,

    @SerializedName("ansuranNo")
    @Expose
    var ansuranNo: String? = null,

    @SerializedName("tsApplication")
    @Expose
    var tsApplication: TsApplication? = null,

    @SerializedName("forms")
    @Expose
    var formList: ArrayList<Form>? = null,

    @SerializedName("participants")
    @Expose
    var participantList: ArrayList<Participant>? = null,

    @SerializedName("farmBudgetItems")
    @Expose
    var farmBudgetItems: ArrayList<FarmBudgetItem>? = null
) : Parcelable {

    val getAnsuranNo get() = if(ansuranNo.isNullOrEmpty()) { "TIADA" } else { ansuranNo }
    val getTsMohonId get() = if(tsMohonId.isNullOrEmpty()) { "-" } else { tsMohonId!!.trim() }
}
