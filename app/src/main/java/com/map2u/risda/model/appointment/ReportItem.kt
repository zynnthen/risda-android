package com.map2u.risda.model.appointment

class ReportItem (
    var appointmentItem: AppointmentItem? = null,
    var form: Form? = null,
    var participant: Participant? = null
)