package com.map2u.risda.model.appointment

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class LotDimiliki(
    @SerializedName("lotMilikId")
    @Expose
    var lotMilikId: String? = null,

    @SerializedName("ptCode")
    @Expose
    var ptCode: String? = null,

    @SerializedName("tsMohonId")
    @Expose
    var tsMohonId: String? = null,

    @SerializedName("icNoMilik")
    @Expose
    var icNoMilik: String? = null,

    @SerializedName("noLithosheet")
    @Expose
    var noLithosheet: String? = null,

    @SerializedName("noGeran")
    @Expose
    var noGeran: String? = null,

    @SerializedName("noGeranLama")
    @Expose
    var noGeranLama: String? = null,

    @SerializedName("noLot")
    @Expose
    var noLot: String? = null,

    @SerializedName("noLotLama")
    @Expose
    var noLotLama: String? = null,

    @SerializedName("bhgnMilikan")
    @Expose
    var bhgnMilikan: Double? = null,

    @SerializedName("luasDiambil")
    @Expose
    var luasDiambil: Double? = null,

    @SerializedName("tkhDiambil")
    @Expose
    var tkhDiambil: Long? = null,

    @SerializedName("syaratKhas")
    @Expose
    var syaratKhas: String? = null,

    @SerializedName("butirPajak")
    @Expose
    var butirPajak: String? = null,

    @SerializedName("butirKaveat")
    @Expose
    var butirKaveat: String? = null,

    @SerializedName("catatan")
    @Expose
    var catatan: String? = null,

    @SerializedName("negeri")
    @Expose
    var negeri: String? = null,

    @SerializedName("daerah")
    @Expose
    var daerah: String? = null,

    @SerializedName("mukim")
    @Expose
    var mukim: String? = null,

    @SerializedName("seksyen")
    @Expose
    var seksyen: String? = null,

    @SerializedName("kampung")
    @Expose
    var kampung: String? = null,

    @SerializedName("dun")
    @Expose
    var dun: String? = null,

    @SerializedName("parlimen")
    @Expose
    var parlimen: String? = null,

    @SerializedName("replant")
    @Expose
    var replant: String? = null,

    @SerializedName("syaratNyata")
    @Expose
    var syaratNyata: String? = null,

    @SerializedName("tsCrop")
    @Expose
    var tsCrop: String? = null,

    @SerializedName("tsCropSub")
    @Expose
    var tsCropSub: String? = null,

    @SerializedName("tsBulan")
    @Expose
    var tsBulan: String? = null,

    @SerializedName("tsTahun")
    @Expose
    var tsTahun: String? = null,

    @SerializedName("luasLot")
    @Expose
    var luasLot: Double? = null,

    @SerializedName("luasTs")
    @Expose
    var luasTs: Double? = null,

    @SerializedName("pegKemaskini")
    @Expose
    var pegKemaskini: String? = null,

    @SerializedName("tkhKKemaskini")
    @Expose
    var tkhKKemaskini: Long? = null,

    @SerializedName("carianRasmi")
    @Expose
    var carianRasmi: String? = null,

    @SerializedName("tarikhPecahan")
    @Expose
    var tarikhPecahan: Long? = null,

    @SerializedName("sumber")
    @Expose
    var sumber: String? = null,

    @SerializedName("logUpi")
    @Expose
    var logUpi: String? = null,

    @SerializedName("kodUpi")
    @Expose
    var kodUpi: String? = null,

    @SerializedName("tarafTanah")
    @Expose
    var tarafTanah: String? = null
): Parcelable