package com.map2u.risda.model.appointment.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.map2u.risda.model.appointment.AppointmentItem

data class ListOfAppointmentResponse(
    @SerializedName("result")
    @Expose
    var result: String? = null,

    @SerializedName("message")
    @Expose
    var message: String? = null,

    @SerializedName("data")
    @Expose
    var datums: ArrayList<AppointmentItem>? = null
)