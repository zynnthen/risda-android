package com.map2u.risda.model.appointment

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Participant(
    @SerializedName("kumpulanID")
    @Expose
    var kumpulanID: String? = null,

    @SerializedName("kumpulanTS")
    @Expose
    var kumpulanTS: KumpulanTS? = null,

    @SerializedName("noSiriTemujanji")
    @Expose
    var noSiriTemujanji: String? = null,

    @SerializedName("status")
    @Expose
    var status: String? = null,

    @SerializedName("temujanjiPID")
    @Expose
    var temujanjiPID: Long? = null,

    @SerializedName("tsApplication")
    @Expose
    var tsApplication: TsApplication? = null,

    @SerializedName("tsmohonID")
    @Expose
    var tsmohonID: String? = null,

    @SerializedName("lejer")
    @Expose
    var lejer: List<String>? = null

) : Parcelable {
}