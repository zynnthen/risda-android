package com.map2u.risda.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ErrorResponse (
    @SerializedName("timestamp")
    @Expose
    var timestamp: Long? = null,

    @SerializedName("status")
    @Expose
    var status: Int? = null,

    @SerializedName("error")
    @Expose
    var error: String? = null,

    @SerializedName("message")
    @Expose
    var message: String? = null,

    @SerializedName("path")
    @Expose
    var path: String? = null
)