package com.map2u.risda.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class FarmBudgetActivity(
    @SerializedName("id")
    @Expose
    var id: Int? = null,

    @SerializedName("jenisCaj")
    @Expose
    var jenisCaj: Int? = null,

    @SerializedName("katKerja")
    @Expose
    var katKerja: String? = null,

    @SerializedName("kerjaStokKod")
    @Expose
    var kerjaStokKod: String? = null,

    @SerializedName("noSiriLB")
    @Expose
    var noSiriLB: String? = null,

    @SerializedName("noSiriTemujanji")
    @Expose
    var noSiriTemujanji: String? = null,

    @SerializedName("status")
    @Expose
    var status: Int = 1,

    @SerializedName("stokItem")
    @Expose
    var stokItem: String? = null
): Parcelable