package com.map2u.risda.model.appointment

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.map2u.risda.model.Item
import kotlinx.parcelize.Parcelize

@Parcelize
data class Ts18eCompleted(
    @SerializedName("noSiriLE") @Expose var noSiriLE: String? = null,
    @SerializedName("catatan") @Expose var catatan: String? = null,
    @SerializedName("ptCode") @Expose var ptCode: String? = null,
    @SerializedName("noSiriLawatan") @Expose var noSiriLawatan: String? = null,
    @SerializedName("jenisLE") @Expose var jenisLE: String? = null,
    @SerializedName("tsMohonId") @Expose var tsMohonId: String? = null,
    @SerializedName("luasLulus") @Expose var luasLulus: String? = null,
    @SerializedName("statusLulus") @Expose var statusLulus: String? = null,
    @SerializedName("pegLulus") @Expose var pegLulus: String? = null,
    @SerializedName("signPegawai") @Expose var signPegawai: String? = null,
    @SerializedName("latitude") @Expose var latitude: Double? = null,
    @SerializedName("longitude") @Expose var longitude: Double? = null,
    @SerializedName("tkhLulus") @Expose var tkhLulus: Long? = null,
    @SerializedName("tkhLawat") @Expose var tkhLawat: Long? = null,
    @SerializedName("imgPlan") @Expose var imgPlan: String? = null,
    @SerializedName("mlawatTamat") @Expose var mlawatTamat: Long? = null,
    @SerializedName("mlawatMula") @Expose var mlawatMula: Long? = null,

    @SerializedName("items") @Expose var items: ArrayList<Item>? = null,
    @SerializedName("documents") @Expose var documents: ArrayList<Document>? = null
) : Parcelable {

}