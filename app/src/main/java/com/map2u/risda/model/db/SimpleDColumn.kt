package com.map2u.risda.model.db

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SimpleDColumn(
    var DColId: String?,
    var DColValue: String?,
    var prefix: String?
) : Parcelable {

    constructor(DColId: String?, DColValue: String?) : this(DColId, DColValue, null)

    override fun toString(): String {
        return DColValue ?: ""
    }

}