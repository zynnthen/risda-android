package com.map2u.risda.model.form.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdateFormResponse (
    @SerializedName("result")
    @Expose
    var result: String? = null,

    @SerializedName("message")
    @Expose
    var message: String? = null,

    @SerializedName("data")
    @Expose
    var data: String? = null,

    @SerializedName("images")
    @Expose
    var images: List<String>? = null
)