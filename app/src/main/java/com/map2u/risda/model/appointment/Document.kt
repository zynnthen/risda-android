package com.map2u.risda.model.appointment

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Document(
    @SerializedName("id")
    @Expose
    var id: String? = null,

    @SerializedName("noSiri")
    @Expose
    var noSiri: String? = null,

    @SerializedName("docType")
    @Expose
    var docType: String? = null,

    @SerializedName("docName")
    @Expose
    var docName: String? = null
): Parcelable