package com.map2u.risda.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Item(
    @SerializedName("id") @Expose var id: Int? = null,
    @SerializedName("noSiriLE") @Expose var noSiriLE: String? = null,
    @SerializedName("item") @Expose var item: String? = null,
    @SerializedName("catatan") @Expose var catatan: String? = null
): Parcelable