package com.map2u.risda.model.login


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Data class that captures information returning from Auth Service API
 */
data class LoginResponse(
    @SerializedName("result")
    @Expose
    var result: String? = null,

    @SerializedName("message")
    @Expose
    var message: String? = null,

    @SerializedName("data")
    @Expose
    var datum: Datum? = null
) {
    data class Datum(
        @SerializedName("user")
        @Expose
        var user: UserDatum? = null,

        @SerializedName("tokens")
        @Expose
        var tokens: TokenDatum? = null
    ) {
        data class UserDatum(
            @SerializedName("id")
            @Expose
            var id: Int? = null,

            @SerializedName("staffNo")
            @Expose
            var staffNo: String? = null,

            @SerializedName("identity")
            @Expose
            var identity: String? = null,

            @SerializedName("name")
            @Expose
            var name: String? = null,

            @SerializedName("email")
            @Expose
            var email: String? = null,

            @SerializedName("category")
            @Expose
            var category: Int? = null,

            @SerializedName("ptCode")
            @Expose
            var ptCode: String? = null,

            @SerializedName("designation")
            @Expose
            var designation: String? = null,

            @SerializedName("contact")
            @Expose
            var contact: String? = null,

            @SerializedName("userRole")
            @Expose
            var userRole: UserRole? = null
        ) {
            data class UserRole(
                @SerializedName("id")
                @Expose
                var id: Int? = null,

                @SerializedName("role")
                @Expose
                var role: String? = null,

                @SerializedName("categoryId")
                @Expose
                var categoryId: Int? = null,
            )
        }

        data class TokenDatum(
            @SerializedName("idToken")
            @Expose
            var idToken: String? = null,

            @SerializedName("refreshToken")
            @Expose
            var refreshToken: String? = null
        )
    }
}