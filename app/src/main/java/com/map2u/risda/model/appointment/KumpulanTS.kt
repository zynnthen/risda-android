package com.map2u.risda.model.appointment

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class KumpulanTS(
    @SerializedName("kumpulanId")
    @Expose
    var kumpulanId: String? = null,

    @SerializedName("namaKumpulan")
    @Expose
    var namaKumpulan: String? = null
): Parcelable