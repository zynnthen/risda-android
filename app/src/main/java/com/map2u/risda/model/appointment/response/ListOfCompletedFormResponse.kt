package com.map2u.risda.model.appointment.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.map2u.risda.model.appointment.PendingVerify

data class ListOfCompletedFormResponse (
    @SerializedName("result")
    @Expose
    var result: String? = null,

    @SerializedName("message")
    @Expose
    var message: String? = null,

    @SerializedName("data")
    @Expose
    var datums: CompletedFormResponse? = null
) {
    data class CompletedFormResponse (
        @SerializedName("count")
        @Expose
        var count: Int = 0,

        @SerializedName("forms")
        @Expose
        var forms: ArrayList<PendingVerify>? = null
    )
}