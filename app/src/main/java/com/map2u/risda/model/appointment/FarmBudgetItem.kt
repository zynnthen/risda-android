package com.map2u.risda.model.appointment

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class FarmBudgetItem(
    @SerializedName("ptCode")
    @Expose
    var ptCode: String? = null,

    @SerializedName("ansuranNo")
    @Expose
    var ansuranNo: String? = null,

    @SerializedName("katKerja")
    @Expose
    var katKerja: String? = null,

    @SerializedName("kerjaStokKod")
    @Expose
    var kerjaStokKod: String? = null,

    // input pertanian id
    @SerializedName("stokItem")
    @Expose
    var stokItem: String? = null,

    @SerializedName("jenisStok")
    @Expose
    var jenisStok: String? = null,

    @SerializedName("perihalStokItem")
    @Expose
    var perihalStokItem: String? = null,

    @SerializedName("ukuran")
    @Expose
    var ukuran: String? = null,

    // kerja utama id
    @SerializedName("kodKerjaId")
    @Expose
    var kodKerjaId: String? = null,

    @SerializedName("kodKerjaDesc")
    @Expose
    var kodKerjaDesc: String? = null,

    @SerializedName("noSiriTemujanji")
    @Expose
    var noSiriTemujanji: String? = null
): Parcelable