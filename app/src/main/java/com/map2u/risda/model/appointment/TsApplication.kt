package com.map2u.risda.model.appointment

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class TsApplication (
    @SerializedName("ptCode")
    @Expose
    var ptCode: String? = null,

    @SerializedName("tsMohonNo")
    @Expose
    var tsMohonNo: String? = null,

    @SerializedName("tsJenis")
    @Expose
    var tsJenis: String? = null,

    @SerializedName("pemilikLot")
    @Expose
    var pemilikLot: String? = null,

    @SerializedName("idESpek")
    @Expose
    var idESpek: String? = null,

    @SerializedName("icNoPemohon")
    @Expose
    var icNoPemohon: String? = null,

    @SerializedName("namaPemohon")
    @Expose
    var namaPemohon: String? = null,

    @SerializedName("sexPemohon")
    @Expose
    var sexPemohon: String? = null,

    @SerializedName("racePemohon")
    @Expose
    var racePemohon: String? = null,

    @SerializedName("address1")
    @Expose
    var address1: String? = null,

    @SerializedName("address2")
    @Expose
    var address2: String? = null,

    @SerializedName("address3")
    @Expose
    var address3: String? = null,

    @SerializedName("poskod")
    @Expose
    var poskod: String? = null,

    @SerializedName("bandar")
    @Expose
    var bandar: String? = null,

    @SerializedName("negeri")
    @Expose
    var negeri: String? = null,

    @SerializedName("daerah")
    @Expose
    var daerah: String? = null,

    @SerializedName("mukim")
    @Expose
    var mukim: String? = null,

    @SerializedName("seksyen")
    @Expose
    var seksyen: String? = null,

    @SerializedName("kampung")
    @Expose
    var kampung: String? = null,

    @SerializedName("dun")
    @Expose
    var dun: String? = null,

    @SerializedName("parlimen")
    @Expose
    var parlimen: String? = null,

    @SerializedName("noPhoneMobile")
    @Expose
    var noPhoneMobile: String? = null,

    @SerializedName("noPhoneHouse")
    @Expose
    var noPhoneHouse: String? = null,

    @SerializedName("noPhoneOffice")
    @Expose
    var noPhoneOffice: String? = null,

    @SerializedName("email")
    @Expose
    var email: String? = null,

    @SerializedName("bankAccNo")
    @Expose
    var bankAccNo: String? = null,

    @SerializedName("bankName")
    @Expose
    var bankName: String? = null,

    @SerializedName("bankSCode")
    @Expose
    var bankSCode: String? = null,

    @SerializedName("tsPendekatan")
    @Expose
    var tsPendekatan: String? = null,

    @SerializedName("tsAgensi")
    @Expose
    var tsAgensi: String? = null,

    @SerializedName("tsAktiviti")
    @Expose
    var tsAktiviti: String? = null,

    @SerializedName("tsProgram")
    @Expose
    var tsProgram: String? = null,

    @SerializedName("tsRMK")
    @Expose
    var tsRMK: String? = null,

    @SerializedName("asalPemohon")
    @Expose
    var asalPemohon: String? = null,

    @SerializedName("noSmbTso")
    @Expose
    var noSmbTso: String? = null,

    @SerializedName("tkhTerima")
    @Expose
    var tkhTerima: Long? = null,

    @SerializedName("tkhLulus")
    @Expose
    var tkhLulus: Long? = null,

    @SerializedName("pegLulus")
    @Expose
    var pegLulus: String? = null,

    @SerializedName("ptrimaBantuan")
    @Expose
    var ptrimaBantuan: String? = null,

    @SerializedName("statusPmohon")
    @Expose
    var statusPmohon: String? = null,

    @SerializedName("statusAlasan")
    @Expose
    var statusAlasan: String? = null,

    @SerializedName("statusAktif")
    @Expose
    var statusAktif: String? = null,

    @SerializedName("statusPerluTS18")
    @Expose
    var statusPerluTS18: String? = null,

    @SerializedName("kodPASemasa")
    @Expose
    var kodPASemasa: String? = null,

    @SerializedName("kodObjSTunai")
    @Expose
    var kodObjSTunai: String? = null,

    @SerializedName("kodObjSStok")
    @Expose
    var kodObjSStok: String? = null,

    @SerializedName("kodPaBerturut")
    @Expose
    var kodPaBerturut: String? = null,

    @SerializedName("kodObjBTunai")
    @Expose
    var kodObjBTunai: String? = null,

    @SerializedName("kodObjBStok")
    @Expose
    var kodObjBStok: String? = null,

    @SerializedName("pegKemaskini")
    @Expose
    var pegKemaskini: String? = null,

    @SerializedName("tkhKemaskini")
    @Expose
    var tkhKemaskini: Long? = null,

    @SerializedName("noSiriTs")
    @Expose
    var noSiriTs: Int? = null,

    @SerializedName("dtSiriTs")
    @Expose
    var dtSiriTs: String? = null,

    @SerializedName("warganegara")
    @Expose
    var warganegara: String? = null,

    @SerializedName("bayarKepada")
    @Expose
    var bayarKepada: String? = null,

    @SerializedName("swkStatus")
    @Expose
    var swkStatus: String? = null,

    @SerializedName("swkKod")
    @Expose
    var swkKod: String? = null,

    @SerializedName("tsKategori")
    @Expose
    var tsKategori: String? = null,

    @SerializedName("kumpulanId")
    @Expose
    var kumpulanId: String? = null,

    @SerializedName("tsMohonId")
    @Expose
    var tsMohonId: String? = null,

    @SerializedName("dob_Pmohon")
    @Expose
    var dob_Pmohon: Long? = null,

    @SerializedName("lotDimiliki")
    @Expose
    var lotDimilikiList: ArrayList<LotDimiliki>? = null
) : Parcelable {
}