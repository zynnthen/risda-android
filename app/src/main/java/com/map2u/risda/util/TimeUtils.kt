package com.map2u.risda.util

import android.annotation.SuppressLint
import android.util.Log
import java.text.SimpleDateFormat
import java.util.*

object TimeUtils {
    @SuppressLint("SimpleDateFormat")
    fun displayTime(timestamp: Long): String {
        try {
            val sdf = SimpleDateFormat("hh:mm a")
            val netDate = Date(timestamp)

            return sdf.format(netDate)
        } catch (e: Exception) {
            Log.e("TimeUtils displayTime", e.message.toString())
            return ""
        }
    }

    fun displayDate(timestamp: Long): String {
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        val netDate = Date(timestamp)

        return sdf.format(netDate)
    }

    fun displayTodayDate(): String {
        return displayDate(System.currentTimeMillis())
    }

    fun getTimestamp(value: String): Long {
        try {
            val date = SimpleDateFormat("dd-MM-yyyy").parse(value)
            return date.time
        } catch (e: Exception) {
            Log.e("TimeUtils getTimestamp", e.message.toString())
            return getCurrentTimestamp()
        }
    }

    fun getMasaTimestamp(value: String): Long {
        try {
            val date = SimpleDateFormat("HH:mm:ss")
            date.timeZone = TimeZone.getTimeZone("UTC")
            return date.parse(value).time
        } catch (e: Exception) {
            Log.e("TimeUtils getMasaTimestamp", e.message.toString())
            return getCurrentTimestamp()
        }
    }

    fun getCurrentTimestamp(): Long {
        return System.currentTimeMillis()
    }

    fun get1JanTimestamp(): Long {
        val cal = Calendar.getInstance()
        cal[Calendar.HOUR] = 0
        cal[Calendar.MINUTE] = 0
        cal[Calendar.SECOND] = 0
        cal[Calendar.MILLISECOND] = 0

        cal.set(Calendar.MONTH, 0); // 0 = January
        cal.set(Calendar.DAY_OF_MONTH, 1);

        return cal.timeInMillis
    }

    fun get12amTimestamp(): Long {
        val cal = Calendar.getInstance()
        cal[Calendar.HOUR_OF_DAY] = 0
        cal[Calendar.MINUTE] = 0
        cal[Calendar.SECOND] = 0
        cal[Calendar.MILLISECOND] = 0

        return cal.timeInMillis
    }

    fun get1159pmTimestamp(): Long {
        val cal = Calendar.getInstance()
        cal[Calendar.HOUR_OF_DAY] = 23
        cal[Calendar.MINUTE] = 59
        cal[Calendar.SECOND] = 59
        cal[Calendar.MILLISECOND] = 0

        return cal.timeInMillis
    }

    @SuppressLint("SimpleDateFormat")
    fun parseDisplayTimeFormat(value: String) : String {
        try {
            val dateFormatter = SimpleDateFormat("hh:mm aa")
            val date = dateFormatter.parse(value)

            val timeFormatter = SimpleDateFormat("HH:mm:ss")
            return timeFormatter.format(date!!)
        } catch (ex: Exception) {
            ex.message?.let { Log.e("TimeUtils", it) }
        }
        return ""
    }
}