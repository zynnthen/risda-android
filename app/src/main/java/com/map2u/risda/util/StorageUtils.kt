package com.map2u.risda.util

import com.map2u.risda.app.MyApplication
import com.map2u.risda.network.SessionManager
import java.io.File

object StorageUtils {

    private val STORAGE_PATH =
        "${MyApplication.getInstance()!!.getExternalFilesDir(null)?.absolutePath}"

    fun getUserStoragePath(): String =
        "${STORAGE_PATH}/${SessionManager(MyApplication.getInstance()!!.applicationContext).fetchUsername()}"

    fun clearUserStorage() {
        val file = File(getUserStoragePath())
        deleteRecursive(file)
    }

    private fun deleteRecursive(file: File) {
        if (file.isDirectory) {
            val listFiles = file.listFiles()
            if (!listFiles.isNullOrEmpty()) {
                listFiles.forEach { deleteRecursive(it) }
            }
        }
        file.delete()
    }

}
