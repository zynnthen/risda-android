package com.map2u.risda.util

import android.content.Context
import android.os.Environment
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

object PhotoUtils {

    fun createImageFile(context: Context, latitude: Double, longitude: Double): File? {
        // Create an image file name
        val imageFileName = "image${SimpleDateFormat("HHmmss").format(Date())}_${latitude}_${longitude}"
        val storageDir: File = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        val image = File.createTempFile(
            imageFileName,  /* prefix */
            ".png",  /* suffix */
            storageDir /* directory */
        )

        return image
    }
}