package com.map2u.risda.util.display

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.util.Base64
import android.util.Log
import android.view.View
import java.io.ByteArrayOutputStream


object DisplayUtils {
    fun getJenisBorang(
        tempJenisLk: String?,
        tempModuleOwner: String?,
        tempTsPendekatan: String?
    ): String {
        var jenisBorang = ""
        val jenisLk = tempJenisLk?.trim()?.toLowerCase()
        val moduleOwner = tempModuleOwner?.trim()?.toLowerCase()
        val tsPendekatan = tempTsPendekatan?.trim()?.toLowerCase()

        if(
            (jenisLk.equals("kebun", true) || jenisLk.equals("ts18", true))
            && moduleOwner.equals(
                "ts",
                true
            )
        ) {
            when(tsPendekatan) {
                "tsi" -> jenisBorang = "TS18 (Individu)"
                "tsa" -> jenisBorang = "TS18 (Agensi)"
                "tsk" -> jenisBorang = "TS18 (Komersial)"
                "tsb" -> jenisBorang = "TS18 (Berkumpulan)"
            }
        } else if (
            (jenisLk.equals("bayar", true)
                    && moduleOwner.equals(
                "ts",
                true
            ))
            ||
            (jenisLk.equals("ts18a", true)
                    && moduleOwner.equals(
                "tsb",
                true
            ))
            ||
            (jenisLk.equals("ts18a", true)
                    && moduleOwner.equals(
                "ts",
                true
            ))
        ) {
            when(tsPendekatan) {
                "tsi" -> jenisBorang = "TS18A (Individu)"
                "tsa" -> jenisBorang = "TS18A (Agensi)"
                "tsk" -> jenisBorang = "TS18A (Komersial)"
                "tsb" -> jenisBorang = "TS18A (Berkumpulan)"
            }
        }

        return jenisBorang
    }

    fun getModuleOwner(
        jenisBorang: String?
    ): String {
        var moduleOwner = ""
        when(jenisBorang) {
            "TS18A (Individu)" -> moduleOwner = "TSI"
            "TS18A (Agensi)" -> moduleOwner = "TSA"
            "TS18A (Komersial)" -> moduleOwner = "TSK"
            "TS18A (Berkumpulan)" -> moduleOwner = "TSB"
        }

        return moduleOwner
    }

    fun getBase64String(view: View): String {
        // getBitmapFromView
        val bitmap =
            Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)

        // createImageStringFromBitmap
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 30, stream)
        val byteArray: ByteArray = stream.toByteArray()

        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }

    fun getBitmapFromBase64String(encodedImage: String): Bitmap? {

        try {
            val decodedString: ByteArray = Base64.decode(encodedImage, Base64.DEFAULT)
            val byteArray = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

            if(byteArray != null) return byteArray

        }catch (e: Exception) {
            Log.e("DisplayUtils DEFAULT", e.printStackTrace().toString())
        }

        try {
            val image = encodedImage.replace("\\n", "")
            val decodedString = Base64.decode(image, Base64.NO_WRAP)
            val byteArray = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

            return byteArray

        }catch (e: Exception) {
            Log.e("DisplayUtils NO_WRAP", e.printStackTrace().toString())
        }

        return null
    }
}