package com.map2u.risda.util

object StringUtils {
    fun isNullOrEmpty(str: String?): Boolean {
        if (str != null && str.isNotEmpty())
            return false
        return true
    }

    fun convertCamelToSnakeCase(camelCase : String) : String {
        val snakeCase = StringBuilder()
        for(character in camelCase) {
            if(character.isUpperCase()) {
                snakeCase.append("_${character}")
            } else {
                snakeCase.append(character)
            }
        }
        val string = snakeCase.removePrefix("_").toString()
        return string[0].toUpperCase() + string.substring(1)
    }

    fun replaceNullToEmptyString(value: String): String {
        if(value == "null") {
            return ""
        }
        return value
    }
}