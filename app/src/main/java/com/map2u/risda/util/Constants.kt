package com.map2u.risda.util

object Constants {

    const val PAGINATION_LIMIT = 25

    const val SELESAI = "Selesai"
    const val BARU = "Baru"
    const val BATAL = "Batal"

    const val REQUEST_IMAGE_CAPTURE = 1

    const val TARGET_FRAGMENT_REQUEST_CODE = 1

    enum class SearchAction(val action: String) {
        CLEAR_ALL("clear_all"), OK("ok")
    }

    enum class UserRole(val role: Int) {
        KETUA_UNIT(7), // ts18/ts18a/ts18e
        PEGAWAI_PEMERIKSA_KEBUN(8) // ts18/ts18a
    }

    enum class Form(val type: String) {
        TS18("18"),
        TS18A("18A")
    }

    enum class JenisLk(val type: String) {
        BAYAR("Bayar"),
        TS18A("TS18A")
    }

    const val ITEM = "item"
    const val CATATAN = "catatan"

    enum class FarmBudget(val category: String) {
        KERJA_UTAMA("Kerja Utama"),
        INPUT_PERTANIAN("Input Pertanian")
    }
}