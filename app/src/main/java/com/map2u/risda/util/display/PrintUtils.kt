package com.map2u.risda.util.display

import android.bluetooth.BluetoothAdapter

object PrintUtils {
    fun inchToPoint(inch: Float): Int {
        return (inch * 203).toInt()
    }

    fun checkBluetoothStatus(): Boolean {
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled) {
            return false
        }
        return true
    }
}