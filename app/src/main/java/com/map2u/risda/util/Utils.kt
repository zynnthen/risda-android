package com.map2u.risda.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.map2u.risda.app.MyApplication
import com.map2u.risda.db.DbConstants

object Utils {
    private var gson: Gson? = null
    private var nullGson: Gson? = null

    fun hasInternetConnection(application: MyApplication): Boolean {
        val connectivityManager = application.getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val activeNetwork = connectivityManager.activeNetwork ?: return false
            val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
            return when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.activeNetworkInfo?.run {
                return when(type) {
                    ConnectivityManager.TYPE_WIFI -> true
                    ConnectivityManager.TYPE_MOBILE -> true
                    ConnectivityManager.TYPE_ETHERNET -> true
                    else -> false
                }
            }
        }
        return false
    }

    fun getLatitude(activity: Activity): Double {
        val lm = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        if (ActivityCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val LocationGps: Location? =
                lm!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            val LocationNetwork: Location? =
                lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
            val LocationPassive: Location? =
                lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER)
            when {
                LocationGps != null -> {
                    return LocationGps.latitude
                }
                LocationNetwork != null -> {
                    return LocationNetwork.latitude
                }
                LocationPassive != null -> {
                    return LocationPassive.latitude
                }
            }
        }
        return 0.0
    }

    fun getLongitude(activity: Activity): Double {
        val lm = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        if (ActivityCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val LocationGps: Location? =
                lm!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            val LocationNetwork: Location? =
                lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
            val LocationPassive: Location? =
                lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER)
            when {
                LocationGps != null -> {
                    return LocationGps.longitude
                }
                LocationNetwork != null -> {
                    return LocationNetwork.longitude
                }
                LocationPassive != null -> {
                    return LocationPassive.longitude
                }
            }
        }
        return 0.0
    }

    fun getGson(): Gson? {
        if (gson == null)
            gson = GsonBuilder().create()
        return gson
    }

    fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    fun Activity.hideKeyboard() {
        hideKeyboard(currentFocus ?: View(this))
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    /**
     * this method is to convert symbol "-" into "iamdash"
     * symbol "-" is not accepted in sqlite,
     * */
    fun escapeString(ori: String?, isValue: Boolean = false): String? {
        val result = ori?.replace("\"", "")
        if (!isValue) {
            return result?.replace("-", DbConstants.I_AM_DASH)
        }
        return result
    }

    /**
     * revert escape string to respective symbol
     * */
    fun revertEscapeString(ori: String?): String? {
        return ori?.replaceFirst(DbConstants.I_AM_DASH, "-")
    }
}