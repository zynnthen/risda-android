package com.map2u.risda.app

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate

open class MyApplication : Application() {

    companion object {
        private var instance: MyApplication? = null
        fun getInstance(): MyApplication? = instance
    }

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        instance = this
    }
}