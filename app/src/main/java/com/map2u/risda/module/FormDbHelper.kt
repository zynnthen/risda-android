package com.map2u.risda.module

import com.google.gson.JsonObject
import com.map2u.risda.db.DbConstants
import com.map2u.risda.db.DbHelper
import com.map2u.risda.util.Constants

class FormDbHelper : ModuleDbHelper {

    protected var helper: DbHelper? = null

    companion object {

        fun createTable(helper: DbHelper?, columns: List<String>, tableName: String) {
            helper?.createTable(tableName, columns)
        }

        fun getAppointmentTableName(): String {
            return DbConstants.APPOINTMENT_TABLE_NAME
        }

        fun getAddTS18FormTableName(): String {
            return DbConstants.ADD_TS18_FORM_TABLE_NAME
        }

        fun getAddTS18FormImageTableName(): String {
            return DbConstants.ADD_TS18_FORM_IMAGE_TABLE_NAME
        }

        fun getAddTS18AFormTableName(): String {
            return DbConstants.ADD_TS18A_FORM_TABLE_NAME
        }

        fun getAddTS18AFormImageTableName(): String {
            return DbConstants.ADD_TS18A_FORM_IMAGE_TABLE_NAME
        }

        fun updateAppointment(helper: DbHelper?, noSiriTemujanji: String) {
            helper?.updateValue(
                getAppointmentTableName(),
                DbConstants.STATUS_TEMUJANJI,
                Constants.SELESAI,
                DbConstants.NO_SIRI_TEMUJANJI,
                arrayOf(noSiriTemujanji)
            )
        }

        fun updateMissedAppointment(helper: DbHelper?, date: Long) {
            helper?.updateListOfAppointments(
                getAppointmentTableName(),
                DbConstants.STATUS_TEMUJANJI,
                Constants.BATAL,
                DbConstants.TARIKH_TEMUJANJI,
                arrayOf(date)
            )
        }

        fun queryCompletedTs18Forms(helper: DbHelper): List<JsonObject>? {
            val singleFormOriginQuery =
                "SELECT *, " +
                        " group_concat(filePath) as filePaths " +
                        "FROM ${getAddTS18FormTableName()} " +
                        "LEFT JOIN ${getAddTS18FormImageTableName()} " +
                        "on ${getAddTS18FormTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} = ${getAddTS18FormImageTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} " +
                        "GROUP BY addTS18Form.noSiriTemujanji "

            return DbHelper.convert(
                helper.getRawQuery(
                    getAddTS18FormTableName(),
                    singleFormOriginQuery,
                    null
                )
            )
        }

        fun queryCompletedTs18aForms(helper: DbHelper): List<JsonObject>? {
            val singleFormOriginQuery =
                "SELECT *, " +
                        " group_concat(filePath) as filePaths " +
                        "FROM ${getAddTS18AFormTableName()} " +
                        "LEFT JOIN ${getAddTS18AFormImageTableName()} " +
                        "on (${getAddTS18AFormTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} = ${getAddTS18AFormImageTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} " +
                        "and ${getAddTS18AFormTableName()}.tsMohonId = ${getAddTS18AFormImageTableName()}.tsmohonID )" +
                        "GROUP BY addTS18AForm.noSiriTemujanji "

            return DbHelper.convert(
                helper.getRawQuery(
                    getAddTS18AFormTableName(),
                    singleFormOriginQuery,
                    null
                )
            )
        }
    }

    override fun findItem(key: String, value: String, tableName: String): Map<String, String>? {
        TODO("Not yet implemented")
    }

    override fun dropTables() {
        TODO("Not yet implemented")
    }

    override fun hasAllRequiredTables(): Boolean {
        TODO("Not yet implemented")
    }

    /**
     * Checks if a value is existing in SKU/Suggestion/Picking list
     * Only for ScanIn, Putaway, Picking modules.
     * There is no such list for Unloading.
     *
     * @param key
     * @param value
     * @return
     */
    override fun isExistingValue(key: String, value: String, tableName: String): Boolean {
        TODO("Not yet implemented")
    }

    override fun updateValue(
        tableName: String,
        targetColumn: String,
        newValue: String,
        whereClause: String?,
        whereArgs: Array<String>?
    ) {
        helper?.updateValue(
            tableName,
            targetColumn,
            newValue,
            whereClause,
            whereArgs
        )
    }
}