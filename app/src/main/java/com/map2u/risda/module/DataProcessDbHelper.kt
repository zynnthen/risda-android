package com.map2u.risda.module

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.map2u.risda.db.DbConstants
import com.map2u.risda.db.DbHelper
import com.map2u.risda.model.FarmBudgetActivity
import com.map2u.risda.model.appointment.*
import com.map2u.risda.network.RequestBodies

class DataProcessDbHelper : ModuleDbHelper {

    override fun findItem(key: String, value: String, tableName: String): Map<String, String>? {
        TODO("Not yet implemented")
    }

    override fun dropTables() {
        TODO("Not yet implemented")
    }

    override fun hasAllRequiredTables(): Boolean {
        TODO("Not yet implemented")
    }

    /**
     * Checks if a value is existing in SKU/Suggestion/Picking list
     * Only for ScanIn, Putaway, Picking modules.
     * There is no such list for Unloading.
     *
     * @param key
     * @param value
     * @return
     */
    override fun isExistingValue(key: String, value: String, tableName: String): Boolean {
        TODO("Not yet implemented")
    }

    override fun updateValue(
        tableName: String,
        targetColumn: String,
        newValue: String,
        whereClause: String?,
        whereArgs: Array<String>?
    ) {
        TODO("Not yet implemented")
    }

    companion object {

        fun massageAppointmentsData(
            context: Context,
            list: List<JsonObject>
        ): ArrayList<AppointmentItem> {
            val appointmentsData: ArrayList<AppointmentItem> = ArrayList()

            val dbHelper = DbHelper(context)
            val groupBy = list.groupBy { it.get(DbConstants.NO_SIRI_TEMUJANJI_PK) }

            val gson = Gson()
            groupBy.values.forEach {
                val obj = gson.fromJson(it[0].toString(), AppointmentItem::class.java)
                obj.noSiriTemujanji = it[0].get(DbConstants.NO_SIRI_TEMUJANJI_PK).toString()
                    .replace("""[$"]""".toRegex(), "")
                obj.tsPendekatan = it[0].get(DbConstants.TS_PENDEKATAN_PK).toString()
                    .replace("""[$"]""".toRegex(), "")
                obj.jenisLK =
                    it[0].get(DbConstants.JENIS_LK_PK).toString().replace("""[$"]""".toRegex(), "")
                obj.ptCode =
                    it[0].get(DbConstants.PT_CODE_PK).toString().replace("""[$"]""".toRegex(), "")
                obj.tsMohonId = it[0].get(DbConstants.TS_MOHON_ID_PK).toString()
                    .replace("""[$"]""".toRegex(), "")

                if (obj.jenisLK.equals("ts18a", true)) {
                    val participants: ArrayList<Participant> = ArrayList()
                    val forms: ArrayList<Form> = ArrayList()

                    it.forEach { item ->
                        val tempLejer =
                            item.get("lejer").toString().replace("[", "").replace("]", "")
                                .replace("""[$"]""".toRegex(), "")
                        item.remove("lejer")
                        val participant = gson.fromJson(item.toString(), Participant::class.java)
                        participant.tsApplication =
                            gson.fromJson(item.toString(), TsApplication::class.java)
                        participant.tsApplication?.lotDimilikiList = ArrayList()
                        participant.tsApplication?.lotDimilikiList?.add(
                            gson.fromJson(
                                item.toString(),
                                LotDimiliki::class.java
                            )
                        )
                        participant.kumpulanTS =
                            gson.fromJson(item.toString(), KumpulanTS::class.java)
                        participant.kumpulanTS?.kumpulanId =
                            item.get(DbConstants.KUMPULAN_ID_PK).toString()
                                .replace("""[$"]""".toRegex(), "")
                        participant.kumpulanTS?.namaKumpulan =
                            item.get(DbConstants.NAMA_KUMPULAN_PK).toString()
                                .replace("""[$"]""".toRegex(), "")

                        participant.lejer = tempLejer.split(",")
                        participants.add(participant)

                        val tempActivities =
                            item.get("activities").toString().replace("""[$"]""".toRegex(), "")
                        item.remove("activities")

                        val form = gson.fromJson(item.toString(), Form::class.java)

                        if(!item.get("hadir").isJsonNull) {
                            form.hadirSendiri = item.get("hadir").asString
                        }

                        try {
                            val activitiesList =
                                gson.fromJson<ArrayList<FarmBudgetActivity>>(tempActivities, object :
                                    TypeToken<ArrayList<FarmBudgetActivity>>() {}.type)
                            form.activities = activitiesList
                        } catch (e: Exception) {
                            Log.e("DataProcessDbHelper no process", e.message.toString())
                        }

                        if(form.activities == null) {
                            try {
                                val editedActivities = tempActivities
                                    .replace("""[$:]""".toRegex(), ":\"")
                                    .replace("""[$,]""".toRegex(), "\",")
                                    .replace("""[$}]""".toRegex(), "\"}")
                                val activitiesList =
                                    gson.fromJson<ArrayList<FarmBudgetActivity>>(editedActivities, object :
                                        TypeToken<ArrayList<FarmBudgetActivity>>() {}.type)
                                form.activities = activitiesList

                            } catch (e: Exception) {
                                Log.e("DataProcessDbHelper", e.printStackTrace().toString())
                            }
                        }

                        forms.add(form)
                    }
                    obj.participantList = participants
                    obj.formList = forms

                    val farmBudgets =
                        AppointmentDbHelper.queryFarmBudget(dbHelper, obj.noSiriTemujanji)
                    if (farmBudgets != null) {
                        obj.farmBudgetItems = ArrayList()
                        farmBudgets.forEach {
                            val farmBudget =
                                gson.fromJson(it.toString(), FarmBudgetItem::class.java)
                            obj.farmBudgetItems?.add(farmBudget)
                        }
                    }

                } else {
                    val tsApplication = gson.fromJson(it[0].toString(), TsApplication::class.java)
                    tsApplication.lotDimilikiList = ArrayList()
                    tsApplication.lotDimilikiList?.add(
                        gson.fromJson(
                            it[0].toString(),
                            LotDimiliki::class.java
                        )
                    )
                    obj.tsApplication = tsApplication

                    val forms: ArrayList<Form> = ArrayList()
                    try {
                        val form = gson.fromJson(it[0].toString(), Form::class.java)
                        if(!it[0].get("hadir").isJsonNull) {
                            form.hadirSendiri = it[0].get("hadir").asString
                        }

                        /**** Disable activities queries for normal TS18 and TS18A Individual ***/
//                    if (it[0].get("activities") != null && !it[0].get("activities").isJsonNull) {
//                        val tempActivities =
//                            it[0].get("activities").toString().replace("""[$"]""".toRegex(), "")
//                        it[0].remove("activities")
//
//                        val activitiesList =
//                            gson.fromJson<ArrayList<FarmBudgetActivity>>(tempActivities, object :
//                                TypeToken<ArrayList<FarmBudgetActivity>>() {}.type)
//                        form.activities = activitiesList
//                    }

                        forms.add(form)
                    } catch (e: Exception) {
                        Log.e("DataProcessDbHelper", e.stackTraceToString())
                    }

                    obj.formList = ArrayList()
                    obj.formList = forms
                }

                appointmentsData.add(obj)
            }

            return appointmentsData
        }

        fun massageTs18Data(
            list: List<JsonObject>
        ): ArrayList<RequestBodies.AddTS18Form> {
            val datum: ArrayList<RequestBodies.AddTS18Form> = ArrayList()

            val gson = Gson()
            list.forEach{
                val obj = gson.fromJson(it.toString(), RequestBodies.AddTS18Form::class.java)
                datum.add(obj)
            }

            return datum
        }

        fun massageTs18aData(
            list: List<JsonObject>
        ): ArrayList<RequestBodies.AddTS18AForm> {
            val datum: ArrayList<RequestBodies.AddTS18AForm> = ArrayList()

            val gson = Gson()
            list.forEach{
                val tempActivities =
                    it.get("activities").toString().replace("""[$"]""".toRegex(), "")
                it.remove("activities")

                val obj = gson.fromJson(it.toString(), RequestBodies.AddTS18AForm::class.java)
                try {
                    obj.activities =
                        gson.fromJson<JsonArray>(tempActivities, object :
                            TypeToken<JsonArray>() {}.type)
                } catch (e: Exception) {
                    Log.e("DataProcessDbHelper no process", e.message.toString())
                }
                datum.add(obj)
            }

            return datum
        }
    }
}