package com.map2u.risda.module

import android.util.Log
import com.google.gson.JsonObject
import com.map2u.risda.db.DbConstants
import com.map2u.risda.db.DbHelper
import com.map2u.risda.util.Constants

class AppointmentDbHelper : ModuleDbHelper {

    protected var helper: DbHelper? = null

    override fun findItem(key: String, value: String, tableName: String): Map<String, String>? {
        TODO("Not yet implemented")
    }

    override fun dropTables() {
        TODO("Not yet implemented")
    }

    override fun hasAllRequiredTables(): Boolean {
        TODO("Not yet implemented")
    }

    override fun isExistingValue(key: String, value: String, tableName: String): Boolean {
        TODO("Not yet implemented")
    }

    override fun updateValue(
        tableName: String,
        targetColumn: String,
        newValue: String,
        whereClause: String?,
        whereArgs: Array<String>?
    ) {
        TODO("Not yet implemented")
    }

    companion object {

        fun createTable(helper: DbHelper?, columns: List<String>, tableName: String) {
            helper?.deleteTable(tableName)
            helper?.createTable(tableName, columns)
        }

//        fun insert(helper: DbHelper?, item: List<SimpleDColumn>, tableName: String) {
//            helper?.insert(tableName, item)
//        }
//
//        fun delete(helper: DbHelper?, criteria: String, value: String, tableName: String) {
//            helper?.deleteRecord(tableName, "$criteria = ?", value)
//        }

        fun getAppointmentTableName(): String {
            return DbConstants.APPOINTMENT_TABLE_NAME
        }

        fun getTsApplicationTableName(): String {
            return DbConstants.TS_APPLICATION_TABLE_NAME
        }

        fun getLotDimilikiTableName(): String {
            return DbConstants.LOT_DIMILIKI_TABLE_NAME
        }

        fun getParticipantTableName(): String {
            return DbConstants.PARTICIPANT_TABLE_NAME
        }

        fun getKumpulanTSTableName(): String {
            return DbConstants.KUMPULAN_TS_TABLE_NAME
        }

        fun getFarmBudgetTableName(): String {
            return DbConstants.FARM_BUDGET_ITEM_TABLE_NAME
        }

        fun getFormTableName(): String {
            return DbConstants.FORM_TABLE_NAME
        }

        fun getAddTS18FormTableName(): String {
            return DbConstants.ADD_TS18_FORM_TABLE_NAME
        }

        fun getAddTS18FormImageTableName(): String {
            return DbConstants.ADD_TS18_FORM_IMAGE_TABLE_NAME
        }

        fun getAddTS18AFormTableName(): String {
            return DbConstants.ADD_TS18A_FORM_TABLE_NAME
        }

        fun getAddTS18AFormImageTableName(): String {
            return DbConstants.ADD_TS18A_FORM_IMAGE_TABLE_NAME
        }

        fun queryAppointment(
            helper: DbHelper,
            statusTemujanji: String?,
            range: String?,
            date: Long?
        ): List<JsonObject> {

            val list: ArrayList<JsonObject> = ArrayList()

            val singleFormOriginQuery =
                "SELECT ${getAppointmentTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} as '${DbConstants.NO_SIRI_TEMUJANJI_PK}'," +
                        "${getAppointmentTableName()}.${DbConstants.TS_PENDEKATAN} as '${DbConstants.TS_PENDEKATAN_PK}'," +
                        "${getAppointmentTableName()}.${DbConstants.JENIS_LK} as '${DbConstants.JENIS_LK_PK}'," +
                        "${getAppointmentTableName()}.${DbConstants.PT_CODE} as '${DbConstants.PT_CODE_PK}'," +
                        "${getAppointmentTableName()}.${DbConstants.TS_MOHON_ID} as '${DbConstants.TS_MOHON_ID_PK}'," +
                        " * " +
                        "FROM ${getAppointmentTableName()} " +
                        "LEFT JOIN ${getTsApplicationTableName()} " +
                        "on ${getAppointmentTableName()}.${DbConstants.TS_MOHON_ID} = ${getTsApplicationTableName()}.${DbConstants.TS_MOHON_ID} " +
                        "LEFT JOIN ${getLotDimilikiTableName()} " +
                        "on ${getLotDimilikiTableName()}.${DbConstants.TS_MOHON_ID} = ${getTsApplicationTableName()}.${DbConstants.TS_MOHON_ID} " +
                        "LEFT JOIN ${getFormTableName()} " +
                        "on ${getFormTableName()}.${DbConstants.TS_MOHON_ID} = ${getTsApplicationTableName()}.${DbConstants.TS_MOHON_ID} " +
                        "WHERE statusTJanji IN ('${statusTemujanji}') and tkhTJanji $range $date and LOWER(appointment.jenisLK) != 'ts18a' " +
                        "GROUP BY ${getAppointmentTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} " +
                        "ORDER BY appointment.tkhTJanji, appointment.masaTJanji"

            val groupFormOriginQuery =
                "SELECT ${getAppointmentTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} as '${DbConstants.NO_SIRI_TEMUJANJI_PK}'," +
                        "${getAppointmentTableName()}.${DbConstants.TS_PENDEKATAN} as '${DbConstants.TS_PENDEKATAN_PK}'," +
                        "${getAppointmentTableName()}.${DbConstants.JENIS_LK} as '${DbConstants.JENIS_LK_PK}'," +
                        "${getKumpulanTSTableName()}.${DbConstants.KUMPULAN_ID} as '${DbConstants.KUMPULAN_ID_PK}'," +
                        "${getKumpulanTSTableName()}.${DbConstants.NAMA_KUMPULAN} as '${DbConstants.NAMA_KUMPULAN_PK}'," +
                        "${getAppointmentTableName()}.${DbConstants.PT_CODE} as '${DbConstants.PT_CODE_PK}'," +
                        "${getAppointmentTableName()}.${DbConstants.TS_MOHON_ID} as '${DbConstants.TS_MOHON_ID_PK}'," +
                        " * " +
                        "FROM ${getAppointmentTableName()} " +
                        "LEFT JOIN ${getParticipantTableName()} " +
                        "on ${getParticipantTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} = ${getAppointmentTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} " +
                        "LEFT JOIN ${getTsApplicationTableName()} " +
                        "on ${getTsApplicationTableName()}.${DbConstants.TS_MOHON_ID} = ${getParticipantTableName()}.${DbConstants.TS_MOHON_ID} " +
                        "LEFT JOIN ${getLotDimilikiTableName()} " +
                        "on ${getLotDimilikiTableName()}.${DbConstants.TS_MOHON_ID} = ${getTsApplicationTableName()}.${DbConstants.TS_MOHON_ID} " +
                        "LEFT JOIN ${getKumpulanTSTableName()} " +
                        "on ${getKumpulanTSTableName()}.kumpulanId = ${getParticipantTableName()}.kumpulanID " +
                        "LEFT JOIN ${getFormTableName()} " +
                        "on ${getFormTableName()}.${DbConstants.TS_MOHON_ID} = ${getParticipantTableName()}.tsmohonID " +
                        "WHERE statusTJanji IN ('${statusTemujanji}') and tkhTJanji $range $date and LOWER(appointment.jenisLK) = 'ts18a' " +
                        "ORDER BY appointment.tkhTJanji, appointment.masaTJanji"

            DbHelper.convert(
                helper.getRawQuery(
                    getAppointmentTableName(),
                    singleFormOriginQuery,
                    null
                )
            )?.let { list.addAll(it) }

            DbHelper.convert(
                helper.getRawQuery(
                    getAppointmentTableName(),
                    groupFormOriginQuery,
                    null
                )
            )?.let { list.addAll(it) }

            return list.toList()
        }

        fun queryOfflineCompletedAppointment(
            helper: DbHelper,
            statusTemujanji: String?,
            range: String?,
            date: Long?
        ): List<JsonObject> {

            val list: ArrayList<JsonObject> = ArrayList()

            try {
                val singleFormOriginQuery =
                    "SELECT ${getAppointmentTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} as '${DbConstants.NO_SIRI_TEMUJANJI_PK}'," +
                            "${getAppointmentTableName()}.${DbConstants.TS_PENDEKATAN} as '${DbConstants.TS_PENDEKATAN_PK}'," +
                            "${getAppointmentTableName()}.${DbConstants.JENIS_LK} as '${DbConstants.JENIS_LK_PK}'," +
                            "${getAppointmentTableName()}.${DbConstants.PT_CODE} as '${DbConstants.PT_CODE_PK}'," +
                            "${getAppointmentTableName()}.${DbConstants.TS_MOHON_ID} as '${DbConstants.TS_MOHON_ID_PK}'," +
                            " * " +
                            "FROM ${getAppointmentTableName()} " +
                            "LEFT JOIN ${getTsApplicationTableName()} " +
                            "on ${getAppointmentTableName()}.${DbConstants.TS_MOHON_ID} = ${getTsApplicationTableName()}.${DbConstants.TS_MOHON_ID} " +
                            "LEFT JOIN ${getLotDimilikiTableName()} " +
                            "on ${getLotDimilikiTableName()}.${DbConstants.TS_MOHON_ID} = ${getTsApplicationTableName()}.${DbConstants.TS_MOHON_ID} " +
                            "LEFT JOIN ${getAddTS18FormTableName()} " +
                            "on ${getAddTS18FormTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} = ${getAppointmentTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} " +
                            "WHERE statusTJanji IN ('${statusTemujanji}') and tkhLawat $range $date and LOWER(appointment.jenisLK) != 'ts18a' " +
                            "GROUP BY ${getAppointmentTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} " +
                            "ORDER BY appointment.tkhTJanji, appointment.masaTJanji"

                DbHelper.convert(
                    helper.getRawQuery(
                        getAppointmentTableName(),
                        singleFormOriginQuery,
                        null
                    )
                )?.let { list.addAll(it) }

            } catch (e: Exception) {
                Log.e("AppointmentDbHelper", e.message.toString())
            }

            try {
                val singleFormTs18aOriginQuery =
                    "SELECT ${getAppointmentTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} as '${DbConstants.NO_SIRI_TEMUJANJI_PK}'," +
                            "${getAppointmentTableName()}.${DbConstants.TS_PENDEKATAN} as '${DbConstants.TS_PENDEKATAN_PK}'," +
                            "${getAppointmentTableName()}.${DbConstants.JENIS_LK} as '${DbConstants.JENIS_LK_PK}'," +
                            "${getAppointmentTableName()}.${DbConstants.PT_CODE} as '${DbConstants.PT_CODE_PK}'," +
                            "${getAppointmentTableName()}.${DbConstants.TS_MOHON_ID} as '${DbConstants.TS_MOHON_ID_PK}'," +
                            " * " +
                            "FROM ${getAppointmentTableName()} " +
                            "LEFT JOIN ${getTsApplicationTableName()} " +
                            "on ${getAppointmentTableName()}.${DbConstants.TS_MOHON_ID} = ${getTsApplicationTableName()}.${DbConstants.TS_MOHON_ID} " +
                            "LEFT JOIN ${getLotDimilikiTableName()} " +
                            "on ${getLotDimilikiTableName()}.${DbConstants.TS_MOHON_ID} = ${getTsApplicationTableName()}.${DbConstants.TS_MOHON_ID} " +
                            "LEFT JOIN ${getAddTS18AFormTableName()} " +
                            "on ${getAddTS18AFormTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} = ${getAppointmentTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} " +
                            "WHERE statusTJanji IN ('${statusTemujanji}') and tkhLawat $range $date and LOWER(appointment.jenisLK) != 'ts18a' " +
                            "GROUP BY ${getAppointmentTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} " +
                            "ORDER BY appointment.tkhTJanji, appointment.masaTJanji"

                DbHelper.convert(
                    helper.getRawQuery(
                        getAppointmentTableName(),
                        singleFormTs18aOriginQuery,
                        null
                    )
                )?.let { list.addAll(it) }

            } catch (e: Exception) {
                Log.e("AppointmentDbHelper", e.message.toString())
            }

            try {
                val groupFormOriginQuery =
                    "SELECT ${getAppointmentTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} as '${DbConstants.NO_SIRI_TEMUJANJI_PK}'," +
                            "${getAppointmentTableName()}.${DbConstants.TS_PENDEKATAN} as '${DbConstants.TS_PENDEKATAN_PK}'," +
                            "${getAppointmentTableName()}.${DbConstants.JENIS_LK} as '${DbConstants.JENIS_LK_PK}'," +
                            "${getKumpulanTSTableName()}.${DbConstants.KUMPULAN_ID} as '${DbConstants.KUMPULAN_ID_PK}'," +
                            "${getKumpulanTSTableName()}.${DbConstants.NAMA_KUMPULAN} as '${DbConstants.NAMA_KUMPULAN_PK}'," +
                            "${getAddTS18AFormTableName()}.${DbConstants.PT_CODE} as '${DbConstants.PT_CODE_PK}'," +
                            "${getAddTS18AFormTableName()}.${DbConstants.TS_MOHON_ID} as '${DbConstants.TS_MOHON_ID_PK}'," +
                            " * " +
                            "FROM ${getAppointmentTableName()} " +
                            "LEFT JOIN ${getParticipantTableName()} " +
                            "on ${getParticipantTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} = ${getAppointmentTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} " +
                            "LEFT JOIN ${getTsApplicationTableName()} " +
                            "on ${getTsApplicationTableName()}.${DbConstants.TS_MOHON_ID} = ${getParticipantTableName()}.${DbConstants.TS_MOHON_ID} " +
                            "LEFT JOIN ${getLotDimilikiTableName()} " +
                            "on ${getLotDimilikiTableName()}.${DbConstants.TS_MOHON_ID} = ${getTsApplicationTableName()}.${DbConstants.TS_MOHON_ID} " +
                            "LEFT JOIN ${getKumpulanTSTableName()} " +
                            "on ${getKumpulanTSTableName()}.kumpulanId = ${getParticipantTableName()}.kumpulanID " +
                            "LEFT JOIN ${getFormTableName()} " +
                            "on ${getFormTableName()}.${DbConstants.TS_MOHON_ID} = ${getParticipantTableName()}.tsmohonID " +
                            "LEFT JOIN ${getAddTS18AFormTableName()} " +
                            "on ${getAddTS18AFormTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} = ${getAppointmentTableName()}.${DbConstants.NO_SIRI_TEMUJANJI} " +
                            "WHERE statusTJanji IN ('${statusTemujanji}') and ${getAddTS18AFormTableName()}.tkhLawat $range $date and LOWER(appointment.jenisLK) = 'ts18a' " +
                            "ORDER BY appointment.tkhTJanji, appointment.masaTJanji"

                DbHelper.convert(
                    helper.getRawQuery(
                        getAppointmentTableName(),
                        groupFormOriginQuery,
                        null
                    )
                )?.let { list.addAll(it) }

            } catch (e: Exception) {
                Log.e("AppointmentDbHelper", e.message.toString())
            }

            return list.toList()
        }

        fun queryFarmBudget(
            helper: DbHelper,
            noSiriTemujanji: String?
        ): List<JsonObject>? {

            val originQuery =
                "SELECT * " +
                        "FROM ${getFarmBudgetTableName()} " +
                        "WHERE noSiriTemujanji = '${noSiriTemujanji}' "

            return DbHelper.convert(
                helper.getRawQuery(
                    getFarmBudgetTableName(),
                    originQuery,
                    null
                )
            )
        }

        fun queryTs18Image(
            helper: DbHelper,
            noSiriTemujanji: String?
        ): List<JsonObject>? {

            val originQuery =
                "SELECT filePath " +
                        "FROM ${getAddTS18FormImageTableName()} " +
                        "WHERE noSiriTemujanji = '${noSiriTemujanji}' "

            return DbHelper.convert(
                helper.getRawQuery(
                    getAddTS18FormImageTableName(),
                    originQuery,
                    null
                )
            )
        }

        fun queryTs18AImage(
            helper: DbHelper,
            noSiriTemujanji: String?,
            tsmohonID: String?
        ): List<JsonObject>? {

            var originQuery =
                "SELECT filePath " +
                        "FROM ${getAddTS18AFormImageTableName()} " +
                        "WHERE noSiriTemujanji = '${noSiriTemujanji}' AND tsmohonID = '${tsmohonID}' "

            if (tsmohonID.isNullOrEmpty()) {
                originQuery =
                    "SELECT filePath " +
                            "FROM ${getAddTS18AFormImageTableName()} " +
                            "WHERE noSiriTemujanji = '${noSiriTemujanji}' "
            }

            return DbHelper.convert(
                helper.getRawQuery(
                    getAddTS18AFormImageTableName(),
                    originQuery,
                    null
                )
            )
        }

        fun queryNoSiriTemujanji(helper: DbHelper): List<JsonObject>? {
            val originQuery =
                "SELECT ${getAppointmentTableName()}.${DbConstants.NO_SIRI_TEMUJANJI}, statusTJanji " +
                        "FROM ${getAppointmentTableName()} " +
                        "WHERE statusTJanji IN ('${Constants.SELESAI}','${Constants.BATAL}') "

            return DbHelper.convert(
                helper.getRawQuery(
                    getAppointmentTableName(),
                    originQuery,
                    null
                )
            )
        }
    }
}