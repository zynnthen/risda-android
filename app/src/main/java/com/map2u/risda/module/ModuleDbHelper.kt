package com.map2u.risda.module

interface ModuleDbHelper {

    fun findItem(key: String, value: String, tableName:String): Map<String, String>?

    fun dropTables()

    fun hasAllRequiredTables(): Boolean

    /**
     * Checks if a value is existing in SKU/Suggestion/Picking list
     * Only for ScanIn, Putaway, Picking modules.
     * There is no such list for Unloading.
     *
     * @param key
     * @param value
     * @return
     */
    fun isExistingValue(key: String, value: String, tableName: String): Boolean

    fun updateValue(
        tableName:String,
        targetColumn: String,
        newValue: String,
        whereClause: String?,
        whereArgs: Array<String>?
    )
}