package com.map2u.risda.ui.shared

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.map2u.risda.R
import com.map2u.risda.databinding.FragmentSignatureDialogBinding
import com.map2u.risda.view.SignatureView

class SignatureDialogFragment(private val onSignedListener: OnSignedCaptureListener, private val fileName: String) :
    DialogFragment(),
    SignatureView.OnSignedListener {

    private var _binding: FragmentSignatureDialogBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        isCancelable = false
        _binding = FragmentSignatureDialogBinding.inflate(inflater, container, false)
        val view = binding.root

        return view
    }

    override fun getTheme(): Int {
        return R.style.Dialog_App
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonCancel.setOnClickListener { dismiss() }
        binding.buttonClear.setOnClickListener { binding.signatureView.clear() }
        binding.buttonOk.setOnClickListener {
            onSignedListener.onSignatureCaptured(binding.signatureView.getSignatureBitmap(), fileName)
            dismiss()
        }
        binding.signatureView.setOnSignedListener(this)
    }

    override fun onStartSigning() {
    }

    override fun onSigned() {
        binding.buttonOk.isEnabled = true
        binding.buttonClear.isEnabled = true
    }

    override fun onClear() {
        binding.buttonClear.isEnabled = false
        binding.buttonOk.isEnabled = false
    }
}