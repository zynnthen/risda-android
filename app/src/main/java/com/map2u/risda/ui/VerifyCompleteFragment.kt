package com.map2u.risda.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.map2u.risda.R
import com.map2u.risda.adapter.PendingVerifyAdapter
import com.map2u.risda.databinding.FragmentVerifyBinding
import com.map2u.risda.model.appointment.PendingVerify
import com.map2u.risda.model.appointment.TsApplication
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.ui.readonly.ReadOnlyTs18EFragment
import com.map2u.risda.ui.readonly.ReadOnlyTs18aEFragment
import com.map2u.risda.ui.shared.VerifySearchDialogFragment
import com.map2u.risda.util.Constants
import com.map2u.risda.util.Resource
import com.map2u.risda.util.TimeUtils
import com.map2u.risda.util.errorSnack
import com.map2u.risda.viewmodel.VerifyCompleteViewModel
import com.map2u.risda.viewmodel.ViewModelProviderFactory

class VerifyCompleteFragment : Fragment() {

    private lateinit var viewModel: VerifyCompleteViewModel
    lateinit var pendingVerifyAdapter: PendingVerifyAdapter

    private var _binding: FragmentVerifyBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private var pageNumber = 0
    private var isLoading = false

    // This variables is to handle search counts
    private var searchFieldsCount = 0
    lateinit var searchBadgeTextView: TextView

    var tsMohonIdPlaceholder: String? = null
    var tarikhStartPlaceholder: String = TimeUtils.displayDate(TimeUtils.get1JanTimestamp())
    var tarikhEndPlaceholder: String = TimeUtils.displayDate(TimeUtils.getCurrentTimestamp())
    var kpPpkPlaceholder: String? = null
    var jenisBorangPlaceholder: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentVerifyBinding.inflate(inflater, container, false)
        val view = binding.root

        init()

        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun init() {
        pendingVerifyAdapter = PendingVerifyAdapter() { pendingVerify: PendingVerify ->
            navigateToForm(pendingVerify)
        }
        setupViewModel()
        setupRvListener()
    }

    private fun navigateToForm(pendingVerify: PendingVerify) {
        showProgressBar()

        pageNumber = 0

        var tsApplication: TsApplication = pendingVerify.tsApplication!!
        // Pengesahan mode
        if (pendingVerify.jenisLK == "TS18A") {
            activity!!.supportFragmentManager
                .beginTransaction()
                .addToBackStack(tag)
                .replace(
                    R.id.content_frame,
                    ReadOnlyTs18aEFragment.newInstance(pendingVerify, tsApplication),
                    tag
                )
                .commit()

        } else {
            activity!!.supportFragmentManager
                .beginTransaction()
                .addToBackStack(tag)
                .replace(
                    R.id.content_frame,
                    ReadOnlyTs18EFragment.newInstance(pendingVerify),
                    tag
                )
                .commit()
        }
    }

    private fun setupViewModel() {
        val repository = AppRepository()
        val factory = activity?.let { ViewModelProviderFactory(it.application, repository) }
        viewModel = factory?.let { ViewModelProvider(this, it).get(VerifyCompleteViewModel::class.java) }!!

        getForms()
    }

    private fun setupRvListener() {
        binding.rvReports.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE && !isLoading) {
                    getForms()
                }
            }
        })
    }

    private fun getForms() {
        viewModel.getFormsValidation(
            pageNumber,
            start = TimeUtils.getTimestamp(tarikhStartPlaceholder),
            end = TimeUtils.getTimestamp(tarikhEndPlaceholder),
            noKpPpk = kpPpkPlaceholder,
            jenisLK = jenisBorangPlaceholder,
            tsMohonId = tsMohonIdPlaceholder
        )

        viewModel.apiResponse.observe(this, { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        isLoading(false)
                        hideProgressBar()
                        response.data?.let { response ->
                            pendingVerifyAdapter.differ.submitList(response.datums?.forms)
                            binding.rvReports.adapter = pendingVerifyAdapter
                            pageNumber += 1

                            if (pendingVerifyAdapter.itemCount == 0) {
                                binding.textviewNoData.visibility = View.VISIBLE
                            } else {
                                binding.textviewNoData.visibility = View.GONE
                            }
                        }
                    }

                    is Resource.NoConnection -> {
                        isLoading(false)
                        hideProgressBar()
                        // todo query offline storage
                    }

                    is Resource.Error -> {
                        isLoading(false)
                        hideProgressBar()
                        response.message?.let { message ->
                            binding.progressbar.errorSnack(message, Snackbar.LENGTH_SHORT)
                        }
                    }

                    is Resource.Loading -> {
                        isLoading(true)
                        showProgressBar()
                    }
                }
            }
        })
    }

    private fun hideProgressBar() {
        binding.progressbar.visibility = View.GONE
    }

    private fun showProgressBar() {
        binding.progressbar.visibility = View.VISIBLE
        binding.textviewNoData.visibility = View.GONE
    }

    private fun isLoading(state: Boolean) {
        isLoading = state
    }

    /*** Start of search menu ***/
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.report_menu, menu)

        val menuItem = menu.findItem(R.id.search_menu)
        val actionView: View = menuItem.actionView
        searchBadgeTextView = actionView.findViewById(R.id.textview_search_badge)
        setupBadge()

        actionView.setOnClickListener { onOptionsItemSelected(menuItem) }
    }

    private fun setupBadge() {
        if (this::searchBadgeTextView.isInitialized) {
            if (searchFieldsCount == 0) {
                searchBadgeTextView.visibility = View.GONE
            } else {
                searchBadgeTextView.text = searchFieldsCount.toString()
                searchBadgeTextView.visibility = View.VISIBLE
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.search_menu -> {
                onSearchClick()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun onSearchClick() {
        val searchDialogFragment = VerifySearchDialogFragment(
            searchFieldsCount = searchFieldsCount,
            tsMohonIdPlaceholder = tsMohonIdPlaceholder,
            tarikhStartPlaceholder = tarikhStartPlaceholder,
            tarikhEndPlaceholder = tarikhEndPlaceholder,
            kpPpkPlaceholder = kpPpkPlaceholder,
            jenisBorangPlaceholder = jenisBorangPlaceholder
        )
        searchDialogFragment.setTargetFragment(this, Constants.TARGET_FRAGMENT_REQUEST_CODE);
        fragmentManager?.let { searchDialogFragment.show(it, TAG) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) {
            return
        }
        if (requestCode == Constants.TARGET_FRAGMENT_REQUEST_CODE) {
            when (data?.getStringExtra(AppointmentsFragment.EXTRA_SEARCH_ACTION)) {
                Constants.SearchAction.CLEAR_ALL.action -> {
                    pageNumber = 0
                    searchFieldsCount = 0
                    tsMohonIdPlaceholder = null
                    tarikhStartPlaceholder = TimeUtils.displayDate(TimeUtils.get1JanTimestamp())
                    tarikhEndPlaceholder = TimeUtils.displayDate(TimeUtils.getCurrentTimestamp())
                    kpPpkPlaceholder = null
                    jenisBorangPlaceholder = null
                }
                Constants.SearchAction.OK.action -> {
                    searchFieldsCount = data?.getIntExtra("searchFieldsCount", 0)
                    tsMohonIdPlaceholder = data?.getStringExtra("tsMohonIdPlaceholder")
                    tarikhStartPlaceholder = data?.getStringExtra("tarikhStartPlaceholder")!!
                    tarikhEndPlaceholder = data?.getStringExtra("tarikhEndPlaceholder")!!
                    kpPpkPlaceholder = data?.getStringExtra("kpPpkPlaceholder")
                    jenisBorangPlaceholder = data?.getStringExtra("jenisBorangPlaceholder")
                }
            }
            setupBadge()
            getForms()
        }
    }

    companion object {
        val TAG = VerifyCompleteFragment::class.java.simpleName!!
        val EXTRA_SEARCH_ACTION = "search_action"

        @JvmStatic
        fun newInstance() =
            VerifyCompleteFragment().apply {
            }

        fun newIntent(
            action: String,
            searchFieldsCount: Int? = null,
            tsMohonIdPlaceholder: String? = null,
            tarikhStartPlaceholder: String? = null,
            tarikhEndPlaceholder: String? = null,
            kpPpkPlaceholder: String? = null,
            jenisBorangPlaceholder: String? = null
        ): Intent {
            val intent = Intent()
            intent.putExtra(EXTRA_SEARCH_ACTION, action)
            if (action == Constants.SearchAction.OK.action) {
                intent.putExtra("searchFieldsCount", searchFieldsCount)
                intent.putExtra("tsMohonIdPlaceholder", tsMohonIdPlaceholder)
                intent.putExtra("tarikhStartPlaceholder", tarikhStartPlaceholder)
                intent.putExtra("tarikhEndPlaceholder", tarikhEndPlaceholder)
                intent.putExtra("kpPpkPlaceholder", kpPpkPlaceholder)
                intent.putExtra("jenisBorangPlaceholder", jenisBorangPlaceholder)
            }
            return intent
        }
    }
    /*** End of search menu ***/
}