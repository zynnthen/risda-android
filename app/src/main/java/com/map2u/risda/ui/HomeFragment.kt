package com.map2u.risda.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.map2u.risda.R
import com.map2u.risda.app.MyApplication
import com.map2u.risda.databinding.FragmentHomeBinding
import com.map2u.risda.network.SessionManager
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.util.*
import com.map2u.risda.viewmodel.HomeViewModel
import com.map2u.risda.viewmodel.ViewModelProviderFactory
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class HomeFragment : Fragment() {

    lateinit var viewModel: HomeViewModel

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val view = binding.root

        binding.imagebuttonTemujanji.setOnClickListener{
            activity?.title = getString(R.string.senarai_temujanji)
            activity?.supportFragmentManager
                ?.beginTransaction()
                ?.addToBackStack(AppointmentsFragment.TAG)
                ?.replace(R.id.content_frame, AppointmentsFragment.newInstance(), AppointmentsFragment.TAG)
                ?.commit()
        }

        binding.imagebuttonPelanTanah.setOnClickListener{
            val launchIntent =
                activity!!.packageManager.getLaunchIntentForPackage("com.esri.fieldmaps")
            if (launchIntent != null) {
                startActivity(launchIntent)
            } else {
                binding.textviewMenu.errorSnack(
                    getString(R.string.no_argis_app),
                    Snackbar.LENGTH_LONG
                )
            }
        }

        binding.imagebuttonPaparanPeta.setOnClickListener{
            val url =
                "https://gisportal.risda.gov.my/arcgis/apps/opsdashboard/index.html#/97c7352a7daa4552bb2a9e09e19233f1"
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            startActivity(intent)
        }

        binding.imagebuttonCetakan.setOnClickListener{
            activity?.title = getString(R.string.cetakan)
            activity?.supportFragmentManager
                ?.beginTransaction()
                ?.addToBackStack(PrintFragment.TAG)
                ?.replace(R.id.content_frame, PrintFragment.newInstance(), PrintFragment.TAG)
                ?.commit()
        }

        setupViewModel()

        if (Utils.hasInternetConnection(activity!!.application as MyApplication) && viewModel.isSyncRequired()) {
            val noOfTs18 = syncTs18From()
            val noOfTs18a = syncTs18aFrom()
            val noOfAppt = syncAppointmentStatus()
            val totalUpload = noOfTs18 + noOfTs18a + noOfAppt
            if(totalUpload > 0) {
                binding.textviewUploading.visibility = View.VISIBLE
            }
        }

        // This property is to get username
        val sessionManager = context?.let { SessionManager(it) }!!
        binding.textviewMenu.text = getString(R.string.selamat_datang, sessionManager.fetchName())

        return view
    }

    private fun setupViewModel() {
        val repository = AppRepository()
        val factory = activity?.let { ViewModelProviderFactory(it.application, repository) }
        viewModel =
            factory?.let { ViewModelProvider(this, it).get(HomeViewModel::class.java) }!!
    }

    private fun syncTs18From(): Int {
        val numOfForm = viewModel.syncTs18From()

        viewModel.apiResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { response ->
                            if (!response.data.isNullOrEmpty()) {
                                response.data?.let {
                                    Event(saveImage(it, response.images))
                                }
                            } else {
                                binding.textviewUploading.visibility = View.INVISIBLE
                            }
                        }
                    }

                    is Resource.Error -> {
                        binding.textviewUploading.visibility = View.INVISIBLE
                        response.message?.let { message ->
                            binding.textviewUploading.errorSnack(
                                message,
                                Snackbar.LENGTH_SHORT
                            )
                        }
                    }
                }
            }
        })

        return numOfForm
    }

    private fun syncTs18aFrom(): Int {
        val numOfForm = viewModel.syncTs18aFrom()

        viewModel.apiResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { response ->
                            if (!response.data.isNullOrEmpty()) {
                                response.data?.let {
                                    Event(saveImage(it, response.images))
                                }
                            } else {
                                binding.textviewUploading.visibility = View.INVISIBLE
                            }
                        }
                    }

                    is Resource.Error -> {
                        binding.textviewUploading.visibility = View.INVISIBLE
                        response.message?.let { message ->
                            binding.textviewUploading.errorSnack(
                                message,
                                Snackbar.LENGTH_SHORT
                            )
                        }
                    }
                }
            }
        })

        return numOfForm
    }

    private fun saveImage(noSeri: String? = null, filePaths: List<String>?) {
        if (filePaths?.isEmpty() == true) {
            return
        }

        filePaths?.forEachIndexed { index, it ->
                val imageFile = File(it) // Create a file using the absolute path of the image
                val reqBody: RequestBody =
                    RequestBody.create("multipart/form-data".toMediaTypeOrNull(), imageFile)
                val latitude = Utils.getLatitude(activity!!).toString().replace('.', ',')
                val longitude = Utils.getLongitude(activity!!).toString().replace('.', ',')
                val pathImage: MultipartBody.Part =
                    MultipartBody.Part.createFormData(
                        "file",
                        "image${index}_${latitude}_${longitude}." + imageFile.extension,
                        reqBody
                    )
                if (!StringUtils.isNullOrEmpty(noSeri)) {
                    viewModel.uploadFile(
                        noSeri!!,
                        pathImage
                    )
                }
        }

        viewModel.apiImageResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                binding.textviewUploading.visibility = View.INVISIBLE
                when (response) {
                    is Resource.Error -> {
                        response.message?.let { message ->
                            binding.textviewUploading.errorSnack(
                                message,
                                Snackbar.LENGTH_SHORT
                            )
                        }
                    }
                }
            }
        })
    }

    private fun syncAppointmentStatus(): Int {
        val numOfAppt = viewModel.syncAppointmentStatus()

        viewModel.apiApptStatusResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { response ->
                            response.message?.let {
                                Event(dropAllTables())
                                binding.textviewMenu.showSnack(
                                    getString(R.string.upload_success),
                                    Snackbar.LENGTH_SHORT
                                )
                            }
                        }
                    }

                    is Resource.Error -> {
                        binding.textviewUploading.visibility = View.INVISIBLE
                        response.message?.let { message ->
                            binding.textviewMenu.errorSnack(
                                message,
                                Snackbar.LENGTH_SHORT
                            )
                        }
                    }
                }
            }
        })

        return numOfAppt
    }

    private fun dropAllTables() {
        viewModel.dropAllTables()
        binding.textviewUploading.visibility = View.INVISIBLE
    }

    companion object {
        val TAG = HomeFragment::class.java.simpleName!!

        @JvmStatic
        fun newInstance() =
            HomeFragment().apply {
            }
    }
}