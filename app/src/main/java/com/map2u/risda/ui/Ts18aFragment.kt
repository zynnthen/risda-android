package com.map2u.risda.ui

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonArray
import com.map2u.risda.BuildConfig
import com.map2u.risda.R
import com.map2u.risda.databinding.FragmentTs18aBinding
import com.map2u.risda.model.FarmBudgetActivity
import com.map2u.risda.model.appointment.AppointmentItem
import com.map2u.risda.model.appointment.Form
import com.map2u.risda.model.appointment.Participant
import com.map2u.risda.model.appointment.TsApplication
import com.map2u.risda.network.RequestBodies
import com.map2u.risda.network.SessionManager
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.ui.shared.OnSignedCaptureListener
import com.map2u.risda.ui.shared.PrinterDialogFragment
import com.map2u.risda.ui.shared.SignatureDialogFragment
import com.map2u.risda.util.*
import com.map2u.risda.util.PhotoUtils.createImageFile
import com.map2u.risda.util.StringUtils.replaceNullToEmptyString
import com.map2u.risda.util.TimeUtils.getMasaTimestamp
import com.map2u.risda.util.TimeUtils.getTimestamp
import com.map2u.risda.util.display.DisplayUtils
import com.map2u.risda.util.display.DisplayUtils.getModuleOwner
import com.map2u.risda.util.display.PrintUtils
import com.map2u.risda.viewmodel.Ts18AViewModel
import com.map2u.risda.viewmodel.ViewModelProviderFactory
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [Ts18aFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Ts18aFragment : Fragment(), OnSignedCaptureListener {
    val ARG_PARAM_APPT_ITEM = "ARG_PARAM_APPT_ITEM"
    val ARG_PARAM_PARTICIPANT = "ARG_PARAM_PARTICIPANT"
    val ARG_PARAM_PARTICIPANT_FORM = "ARG_PARAM_FORM"
    val ARG_PARAM_BORANG_TYPE = "ARG_PARAM_BORANG_TYPE"
    var paramApptItem: AppointmentItem? = null
    var paramParticipant: Participant? = null
    var paramForm: Form? = null
    var paramBorangType: String? = null

    private var _binding: FragmentTs18aBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private lateinit var viewModel: Ts18AViewModel

    // This property is to get username
    private lateinit var sessionManager: SessionManager

    // This property is for image upload
    private val filePaths: ArrayList<String> = ArrayList()

    // This property is to get location
    var longitude: Double = 0.0
    var latitude: Double = 0.0

    lateinit var photoURI: Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            paramApptItem = it.getParcelable(ARG_PARAM_APPT_ITEM)
            paramParticipant = it.getParcelable(ARG_PARAM_PARTICIPANT)
            paramForm = it.getParcelable(ARG_PARAM_PARTICIPANT_FORM)
            paramBorangType = it.getString(ARG_PARAM_BORANG_TYPE)
        }

        latitude = Utils.getLatitude(activity!!)
        longitude = Utils.getLongitude(activity!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTs18aBinding.inflate(inflater, container, false)
        val view = binding.root

        setupViewModel()

        binding.seekBar.max = 6

        binding.sectionFarmBudget.sectionFarmBudget.visibility = View.GONE
        binding.sectionC1.sectionC1.visibility = View.GONE
        binding.sectionC2.sectionC2.visibility = View.GONE
        binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
        binding.sectionD.sectionD.visibility = View.GONE
        binding.sectionF.sectionF.visibility = View.GONE
        binding.sectionG.sectionG.visibility = View.GONE

        binding.sectionC1.checkboxMengemaskini.setOnCheckedChangeListener { v, isChecked ->
            if (isChecked) {
                val launchIntent =
                    context?.packageManager?.getLaunchIntentForPackage("com.esri.fieldmaps")
                if (launchIntent != null) {
                    startActivity(launchIntent)
                } else {
                    binding.textviewTitle.errorSnack(
                        getString(R.string.no_argis_app),
                        Snackbar.LENGTH_LONG
                    )
                }
            }
        }

        binding.sectionC2.checkboxMengemaskini.setOnCheckedChangeListener { v, isChecked ->
            if (isChecked) {
                val launchIntent =
                    context?.packageManager?.getLaunchIntentForPackage("com.esri.fieldmaps")
                if (launchIntent != null) {
                    startActivity(launchIntent)
                } else {
                    binding.textviewTitle.errorSnack(
                        getString(R.string.no_argis_app),
                        Snackbar.LENGTH_LONG
                    )
                }
            }
        }

        /*** Next button listener ***/
        nextButtonListener()

        /*** Previous button listener ***/
        previousButtonListener()

        /*** Camera button listener ***/
        cameraButtonListener()

        /*** Section D default date ***/
        binding.sectionD.edtTarikhLawatan.setText(TimeUtils.displayTodayDate())

        /*** Section G listener ***/
        binding.sectionG.rbPemohonHadirSendiriYa.setOnCheckedChangeListener { v, isChecked ->
            if(isChecked) {
                val participant: Participant? = paramParticipant
                binding.sectionG.edtNama.setText(participant?.tsApplication?.namaPemohon)
                binding.sectionG.edtKpPemohon.setText(participant?.tsApplication?.icNoPemohon)

            } else {
                binding.sectionG.edtNama.setText("")
                binding.sectionG.edtKpPemohon.setText("")
            }
        }

        /*** Sign listener ***/
        binding.sectionD.buttonSignHereD.setOnClickListener { v ->
            val dialogFragment = SignatureDialogFragment(this, "D")
            dialogFragment.show(activity!!.supportFragmentManager, "signature")
        }
        binding.sectionG.buttonSignHereG.setOnClickListener { v ->
            val dialogFragment = SignatureDialogFragment(this, "G")
            dialogFragment.show(activity!!.supportFragmentManager, "signature")
        }

        /*** Save form button listener ***/
        binding.sectionG.buttonSaveG.setOnClickListener { v ->
            val signPerakuan = DisplayUtils.getBase64String(binding.sectionG.imageViewSignG)
            val edtNama = binding.sectionG.edtNama.text.toString()
            val edtKpPemohon = binding.sectionG.edtKpPemohon.text.toString()

            if (signPerakuan.isEmpty() || edtNama.isEmpty() || edtKpPemohon.isEmpty()) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_not_filled),
                    Snackbar.LENGTH_LONG
                )
                return@setOnClickListener
            }

            saveForm()
        }

        /*** Time input picker ***/
        // This property is for Time picker usage
        val myCalendar: Calendar = Calendar.getInstance()
        val timeDari =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                myCalendar.set(Calendar.MINUTE, minute)

                val sdf = SimpleDateFormat("hh:mm a")
                binding.sectionD.edtMasaDari.setText(sdf.format(myCalendar.getTime()))
            }
        binding.sectionD.edtMasaDari.setOnClickListener { v ->
            TimePickerDialog(
                context!!,
                timeDari,
                myCalendar[Calendar.HOUR_OF_DAY],
                myCalendar[Calendar.MINUTE],
                false
            ).show()
        }

        val timeSampai =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                myCalendar.set(Calendar.MINUTE, minute)

                val sdf = SimpleDateFormat("hh:mm a")
                binding.sectionD.edtMasaSampai.setText(sdf.format(myCalendar.getTime()))
            }
        binding.sectionD.edtMasaSampai.setOnClickListener { v ->
            TimePickerDialog(
                context!!,
                timeSampai,
                myCalendar[Calendar.HOUR_OF_DAY],
                myCalendar[Calendar.MINUTE],
                false
            ).show()
        }

        /*** Date input picker ***/
        val myCalendar1: Calendar = Calendar.getInstance()
        val date1 =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                myCalendar1.set(Calendar.YEAR, year)
                myCalendar1.set(Calendar.MONTH, monthOfYear)
                myCalendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val sdf = SimpleDateFormat("dd-MM-yyyy")
                binding.sectionC1.edtTarikhTanam.setText(sdf.format(myCalendar1.getTime()))
            }

        binding.sectionC1.edtTarikhTanam.setOnClickListener { v ->
            DatePickerDialog(
                context!!, date1, myCalendar1[Calendar.YEAR], myCalendar1[Calendar.MONTH],
                myCalendar1[Calendar.DAY_OF_MONTH]
            ).show()
        }

        initAbContent()
        initFarmBudget()

        return view
    }

    private fun setupViewModel() {
        val repository = AppRepository()
        val factory = activity?.let { ViewModelProviderFactory(it.application, repository) }
        viewModel = factory?.let { ViewModelProvider(this, it).get(Ts18AViewModel::class.java) }!!
    }

    private fun saveForm() {
        val form: RequestBodies.AddTS18AForm?
        if (paramForm == null) {
            form = setupRequestBodies("POST")
            viewModel.postTS18AForm(setupRequestBodies("POST"))
        } else {
            form = setupRequestBodies("PUT")
            viewModel.putTS18AForm(setupRequestBodies("PUT"))
        }

        viewModel.apiResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { response ->
                            response.data?.let {
                                Event(saveImage(it))
                            }
                        }
                    }

                    is Resource.NoConnection -> {
                        viewModel.updateAppointmentOffline(form)
                        viewModel.updateImageOffline(
                            noSiriTemujanji = paramApptItem?.noSiriTemujanji!!,
                            tsmohonID = paramParticipant?.tsmohonID!!,
                            filePaths
                        )
                        showPrintAlertDialog()
                    }

                    is Resource.Error -> {
                        response.message?.let { message ->
                            binding.textviewTitle.errorSnack(
                                message,
                                Snackbar.LENGTH_SHORT
                            )
                        }
                    }

                    is Resource.Loading -> {
//                        showProgressBar()
                    }
                }
            }
        })
    }

    private fun saveImage(noSeri: String? = null) {
        if (filePaths.size == 0) {
            showPrintAlertDialog()
            return
        }

        filePaths.forEachIndexed { index, it ->
            val imageFile = File(it) // Create a file using the absolute path of the image
            val reqBody: RequestBody =
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), imageFile)

            val latitude = Utils.getLatitude(activity!!).toString().replace('.', ',')
            val longitude = Utils.getLongitude(activity!!).toString().replace('.', ',')
            val pathImage: MultipartBody.Part =
                MultipartBody.Part.createFormData(
                    "file",
                    "image${index}_${latitude}_${longitude}." + imageFile.extension,
                    reqBody
                )
            if (StringUtils.isNullOrEmpty(noSeri)) {
                viewModel.uploadFile(
                    paramForm?.noSiriLB!!,
                    pathImage
                )
            } else {
                viewModel.uploadFile(
                    noSeri!!,
                    pathImage
                )
            }
        }

        viewModel.apiImageResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { response ->
                            response.message?.let {
                                showPrintAlertDialog()
                            }
                        }
                    }

                    is Resource.Error -> {
                        response.message?.let { message ->
                            binding.textviewTitle.errorSnack(
                                message,
                                Snackbar.LENGTH_SHORT
                            )
                            showPrintAlertDialog()
                        }
                    }
                }
            }
        })
    }

    private fun setupRequestBodies(apiType: String): RequestBodies.AddTS18AForm {
        /*** Section kerja utama, input pertanian ***/
        val activities = JsonArray()
        val childCount = binding.sectionFarmBudget.linearLayoutFarmBudget.childCount
        var v: View?
        for (i in 0 until childCount) {
            v = binding.sectionFarmBudget.linearLayoutFarmBudget.getChildAt(i)
            val kerjaUtamaTextView: TextView? = v.findViewById(R.id.textview_kerjaUtama)
            val inputPertanianTextView: TextView? = v.findViewById(R.id.textview_inputPertanian)
            if (kerjaUtamaTextView != null && kerjaUtamaTextView.visibility == View.VISIBLE) {
                continue
            }
            if (inputPertanianTextView != null && inputPertanianTextView.visibility == View.VISIBLE) {
                continue
            }

            val farmBudgetActivity = FarmBudgetActivity()
            farmBudgetActivity.noSiriTemujanji = paramApptItem?.noSiriTemujanji
            if (apiType == "PUT") {
                farmBudgetActivity.noSiriLB = paramForm!!.noSiriLB
            }

            val kerjaUtamaLinearLayout: LinearLayout = v.findViewById(R.id.ll_kerjaUtama)

            if (kerjaUtamaLinearLayout.visibility == View.VISIBLE) {
                val kerjaUtama: Spinner = v.findViewById(R.id.spinner_kerjaUtama)
                val kerjaStokKod: TextView = v.findViewById(R.id.textview_kerjaStokKod)

                farmBudgetActivity.katKerja = Constants.FarmBudget.KERJA_UTAMA.category
                when (kerjaUtama.selectedItem.toString()) {
                    "Caj" -> farmBudgetActivity.jenisCaj = 0
                    "Laksana" -> farmBudgetActivity.jenisCaj = 1
                    "Tidak", "Sila pilih" -> continue
                }
                farmBudgetActivity.kerjaStokKod = kerjaStokKod.text.toString()

            } else {
                val inputPertanian: Spinner = v.findViewById(R.id.spinner_inputPertanian)
                val stokItem: TextView = v.findViewById(R.id.textview_stokItem)

                farmBudgetActivity.katKerja = Constants.FarmBudget.INPUT_PERTANIAN.category
                when (inputPertanian.selectedItem.toString()) {
                    "Ya" -> farmBudgetActivity.jenisCaj = 1
                    "Tidak", "Sila pilih" -> continue
                }
                farmBudgetActivity.stokItem = stokItem.text.toString()
            }

            activities.add(Utils.getGson()?.toJsonTree(farmBudgetActivity))
        }

        /*** Section C1 ***/
        val radiogroupKerjaMengukur =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupKerjaMengukur.checkedRadioButtonId)?.text.toString())
        val edtLuasKawasanSiapDibersihkan =
            binding.sectionC1.edtLuasKawasanSiapDibersihkan.text.toString().toDoubleOrNull() ?: 0.0
        val spinnerJarakTanamanDanKepadatanPokokHektar = binding.sectionC1.spinnerJarakTanaman.selectedItemPosition.toString()

        val edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih =
            binding.sectionC1.edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih.text.toString()
                .toDoubleOrNull() ?: 0.0
        val edtTarikhTanam =
            binding.sectionC1.edtTarikhTanam.text.toString()
        val radiogroupJenisBenih =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupJenisBenih.checkedRadioButtonId)?.text.toString())
        val radiogroupKelulusanBorangTs29a =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupKelulusanBorangTs29a.checkedRadioButtonId)?.text.toString())
        val radiogroupKlonBermutuTinggi =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupKlonBermutuTinggi.checkedRadioButtonId)?.text.toString())
        val radiogroupResitPembelianBenih =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupResitPembelianBenih.checkedRadioButtonId)?.text.toString())
        val radiogroupSijilPengesahan =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupSijilPengesahan.checkedRadioButtonId)?.text.toString())
        val radiogroupTapakSemaian =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupTapakSemaian.checkedRadioButtonId)?.text.toString())
        val edtBenihSawitNyatakanSumber =
            binding.sectionC1.edtBenihSawitNyatakanSumber.text.toString()
        val radiogroupAdakahTeres =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupAdakahTeres.checkedRadioButtonId)?.text.toString())
        val edtLainLainKenyataan =
            binding.sectionC1.edtLainLainKenyataan.text.toString()

        /*** Section C2 ***/
        val edtLuasTanamanUtamaYangBerjaya =
            binding.sectionC2.edtLuasTanamanUtamaYangBerjaya.text.toString().toDoubleOrNull() ?: 0.0
        val radiogroupAdakahMembaja =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahMembaja.checkedRadioButtonId)?.text.toString())
        val radiogroupAdakahCantasan =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahCantasan.checkedRadioButtonId)?.text.toString())
        val radiogroupJikaYa =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupJikaYa.checkedRadioButtonId)?.text.toString())
        val radiogroupAdakahGalakan =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahGalakan.checkedRadioButtonId)?.text.toString())
        val radiogroupJikaYaNyatakan =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupJikaYaNyatakan.checkedRadioButtonId)?.text.toString())
        val radiogroupAdakahKawalan =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahKawalan.checkedRadioButtonId)?.text.toString())
        val edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang =
            binding.sectionC2.edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang.text.toString()
                .toDoubleOrNull() ?: 0.0
        val edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh =
            binding.sectionC2.edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh.text.toString()
                .toDoubleOrNull() ?: 0.0
        val radiogroupAdakahTeresPelantar =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahTeresPelantar.checkedRadioButtonId)?.text.toString())
        val radiogroupAdakahKpb =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahKpb.checkedRadioButtonId)?.text.toString())
        val edtJikaYaLuasYangDiselenggara =
            binding.sectionC2.edtJikaYaLuasYangDiselenggara.text.toString().toDoubleOrNull() ?: 0.0
        val edtGetah =
            binding.sectionC2.edtGetah.text.toString().toIntOrNull() ?: 0
        val edtTanamanLain =
            binding.sectionC2.edtTanamanLain.text.toString().toIntOrNull() ?: 0
        val edtPurataUkurLilitPokokGetahYang =
            binding.sectionC2.edtPurataUkurLilitPokokGetahYang.text.toString().toDoubleOrNull()
                ?: 0.0
        val edtLainLainKenyataanJikaAda =
            binding.sectionC2.edtLainLainKenyataanJikaAda.text.toString()

        var gapSenggara = radiogroupAdakahTeresPelantar
        var kenyataanLain = edtLainLainKenyataanJikaAda
        if (paramApptItem!!.getAnsuranNo?.toIntOrNull() == 1) {
            gapSenggara = radiogroupAdakahTeres
            kenyataanLain = edtLainLainKenyataan
        }

        /*** Section D ***/
        val edtTarikhLawatan = binding.sectionD.edtTarikhLawatan.text.toString()
        val edtMasaDari = TimeUtils.parseDisplayTimeFormat(binding.sectionD.edtMasaDari.text.toString())
        val edtMasaSampai = TimeUtils.parseDisplayTimeFormat(binding.sectionD.edtMasaSampai.text.toString())
        val signPegawai = DisplayUtils.getBase64String(binding.sectionD.imageViewSignD)

        /*** Section G ***/
        val signPerakuan = DisplayUtils.getBase64String(binding.sectionG.imageViewSignG)
        val edtNama = binding.sectionG.edtNama.text.toString()
        val edtKpPemohon = binding.sectionG.edtKpPemohon.text.toString()
        val radiogroupPemohonHadir =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionG.radiogroupPemohonHadirSendiri.checkedRadioButtonId)?.text.toString())

        val participant: Participant? = paramParticipant
        val tsApplication: TsApplication? = participant?.tsApplication

        sessionManager = context?.let { SessionManager(it) }!!

        return RequestBodies.AddTS18AForm(
            activities = activities,
            ansuranNo = paramApptItem!!.getAnsuranNo.toString(),
            bekalAwal = "",
            benihLuar = radiogroupKelulusanBorangTs29a,
            benihSawit = edtBenihSawitNyatakanSumber,
            bilLawatan = 0,
            catatan = "",
            gapBaja = radiogroupAdakahMembaja,
            gapCantasan = radiogroupAdakahCantasan,
            gapGalakDahan = radiogroupAdakahGalakan,
            gapSenggara = gapSenggara,
            getahPkkHek = edtGetah,
            hadirSendiri = radiogroupPemohonHadir,
            jarakLubang1 = 0.0,
            jarakLubang2 = 0.0,
            jarakLubang3 = 0.0,
            jarakTanaman = spinnerJarakTanamanDanKepadatanPokokHektar,
            jawPemeriksa = sessionManager.fetchDesignation(),
            jenisBenih = radiogroupJenisBenih,
            jenisCantasan = radiogroupJikaYa,
            jenisGalakDahan = radiogroupJikaYaNyatakan,
            jenisLK = Constants.JenisLk.TS18A.type,
            kawalPerosak = radiogroupAdakahKawalan,
            kenyataanLain = kenyataanLain,
            klonBenih = "",
            klonMutu = radiogroupKlonBermutuTinggi,
            kpbKontan = radiogroupAdakahKpb,
            kpbKontanSelenggara = "",
            kumpNoClaim = "",
            kumpulanId = participant!!.kumpulanID!!,
            luasBerjaya = edtLuasTanamanUtamaYangBerjaya,
            luasKontan = edtJikaYaLuasYangDiselenggara,
            luasTanam = edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih,
            mlawatMula = getMasaTimestamp(edtMasaDari),
            mlawatTamat = getMasaTimestamp(edtMasaSampai),
            modulOwner = getModuleOwner(paramBorangType),
            namaKumpulan = participant.kumpulanTS!!.namaKumpulan!!,
            namaPerakuan = edtNama,
            noKpPerakuan = edtKpPemohon,
            noKpPpk = sessionManager.fetchUsername(),
            noSiriLB = if (apiType == "POST") null else paramForm!!.noSiriLB,
            noSiriTemujanji = paramApptItem!!.noSiriTemujanji!!.trim(),
            pegKemaskini = sessionManager.fetchUsername()!!,
            pegLulus = "",
            pegPemeriksa = sessionManager.fetchName(),
            perimeter = radiogroupKerjaMengukur,
            planTanah = "",
            ptCode = paramApptItem!!.ptCode!!.trim(),
            resitBenih = radiogroupResitPembelianBenih,
            rumpaiSenggara = edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang,
            rumpaiTdkSenggara = edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh,
            sahKlon = radiogroupSijilPengesahan,
            signPegawai = signPegawai,
            signPerakuan = signPerakuan,
            statusLulus = "",
            statusR7 = null,
            statusTolakKIV = "",
            sulaman = 0,
            tapakSemaian = radiogroupTapakSemaian,
            tebangJentera1 = edtLuasKawasanSiapDibersihkan,
            tebangJentera2 = 0.0,
            tkhBerhasil = 0,
            tkhKemaskini = 0,
            tkhLawat = getTimestamp(edtTarikhLawatan),
            tkhLulus = 0,
            tkhTanam = getTimestamp(edtTarikhTanam),
            tlPkkHidup = edtTanamanLain,
            tsMohonId = participant.tsmohonID!!,
            tsMohonLBSiri = "",
            ukurLilit = edtPurataUkurLilitPokokGetahYang,
            latitude = latitude,
            longitude = longitude,
            hadir = radiogroupPemohonHadir
        )
    }

    override fun onSignatureCaptured(bitmap: Bitmap, fileName: String) {
        when (fileName) {
            "D" -> {
                binding.sectionD.imageViewSignD.setImageBitmap(bitmap)
            }
            "G" -> {
                binding.sectionG.imageViewSignG.setImageBitmap(bitmap)
            }
        }
    }

    private fun initAbContent() {
        val participant: Participant? = paramParticipant

        val address =
            "${participant?.tsApplication?.address1} ${participant?.tsApplication?.address2} ${participant?.tsApplication?.address3}"
        binding.sectionAb.edtNama.setText(participant?.tsApplication?.namaPemohon)
        binding.sectionAb.edtNoKpPolisTenteraSyarikat.setText(participant?.tsApplication?.icNoPemohon)
        binding.sectionAb.edtAlamat.setText(address)
        binding.sectionAb.edtPoskod.setText(participant?.tsApplication?.poskod)
        binding.sectionAb.edtNoTelRumah.setText(participant?.tsApplication?.noPhoneHouse)
        binding.sectionAb.edtNoTelBimbit.setText(participant?.tsApplication?.noPhoneMobile)
        binding.sectionAb.edtNoPermohonan.setText(participant?.tsmohonID)

        binding.sectionAb.edtDaerah.setText(participant?.tsApplication?.daerah)
        binding.sectionAb.edtParlimen.setText(participant?.tsApplication?.parlimen)
        binding.sectionAb.edtDun.setText(participant?.tsApplication?.dun)
        binding.sectionAb.edtMukim.setText(participant?.tsApplication?.mukim)
        binding.sectionAb.edtKampung.setText(participant?.tsApplication?.kampung)

        binding.sectionAb.edtNoGeran.setText(participant?.tsApplication?.lotDimilikiList?.get(0)?.noGeran.toString())
        binding.sectionAb.edtNoLot.setText(participant?.tsApplication?.lotDimilikiList?.get(0)?.noLot.toString())
        binding.sectionAb.edtJenisTanaman.setText(participant?.tsApplication?.lotDimilikiList?.get(0)?.tsCrop.toString())

        binding.sectionAb.edtLuasKebunHektar.setText(participant?.tsApplication?.lotDimilikiList?.get(0)?.luasLot.toString())
        binding.sectionAb.edtLuasKebunHektarHendakDitanam.setText(participant?.tsApplication?.lotDimilikiList?.get(0)?.luasTs.toString())
    }

    private fun initFarmBudget() {
        val farmBudgets = paramApptItem!!.farmBudgetItems?.sortedByDescending { it.katKerja }

        val inflater =
            activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        if (farmBudgets != null && farmBudgets.isNotEmpty()) {
            var isFirstKerjaUtama = true
            var isFirstInputPertanian = true

            farmBudgets?.forEachIndexed { index, it ->
                val farmBudgetHeaderView: View =
                    inflater.inflate(R.layout.item_farm_budget_header, null)
                val farmBudgetView: View = inflater.inflate(R.layout.item_farm_budget, null)

                val kerjaUtamaTextView =
                    farmBudgetHeaderView.findViewById<TextView>(R.id.textview_kerjaUtama)
                val inputPertanianTextView =
                    farmBudgetHeaderView.findViewById<TextView>(R.id.textview_inputPertanian)
                val inputPertanianDescLinearLayout =
                    farmBudgetHeaderView.findViewById<LinearLayout>(R.id.ll_inputPertanianDesc)

                val kerjaUtamaLinearLayout =
                    farmBudgetView.findViewById<LinearLayout>(R.id.ll_kerjaUtama)
                val questionKerjaUtamaTextView =
                    farmBudgetView.findViewById<TextView>(R.id.textview_question_kerjaUtama)
                val kerjaStokKodTextView =
                    farmBudgetView.findViewById<TextView>(R.id.textview_kerjaStokKod)

                val inputPertanianLinearLayout =
                    farmBudgetView.findViewById<LinearLayout>(R.id.ll_inputPertanian)
                val jenisStokTextView =
                    farmBudgetView.findViewById<TextView>(R.id.textview_jenisStok)
                val perihalStokItemTextView =
                    farmBudgetView.findViewById<TextView>(R.id.textview_perihalStokItem)
                val stokItemTextView = farmBudgetView.findViewById<TextView>(R.id.textview_stokItem)

                farmBudgetView.id = index
                var isFound = false

                if (it.katKerja == Constants.FarmBudget.KERJA_UTAMA.category) {

                    val found =
                        paramParticipant?.lejer?.find { value -> it.kodKerjaId?.trim() == value.trim() }

                    if (found == null) {
                        if (isFirstKerjaUtama) {
                            kerjaUtamaTextView.visibility = View.VISIBLE
                        }

                        questionKerjaUtamaTextView.text = it.kodKerjaDesc
                        kerjaStokKodTextView.text = it.kerjaStokKod
                        kerjaUtamaLinearLayout.visibility = View.VISIBLE
                    } else {
                        isFound = true
                    }
                } else if (it.katKerja == Constants.FarmBudget.INPUT_PERTANIAN.category) {

                    val foundPertanian =
                        paramParticipant?.lejer?.find { value -> it.stokItem?.trim() == value.trim() }
                    if (foundPertanian == null) {
                        if (isFirstInputPertanian) {
                            inputPertanianTextView.visibility = View.VISIBLE
                            inputPertanianDescLinearLayout.visibility = View.VISIBLE
                        }

                        var jenisStok = "Baja" // BJ
                        if (it.jenisStok == "BH") {
                            jenisStok = "Benih"
                        }
                        inputPertanianLinearLayout.visibility = View.VISIBLE
                        jenisStokTextView.text = jenisStok
                        perihalStokItemTextView.text = it.perihalStokItem
                        stokItemTextView.text = it.stokItem
                    } else {
                        isFound = true
                    }
                }

                if (isFirstKerjaUtama && kerjaUtamaTextView.visibility == View.VISIBLE) {
                    isFirstKerjaUtama = false
                    binding.sectionFarmBudget.linearLayoutFarmBudget.addView(farmBudgetHeaderView)
                }
                if (isFirstInputPertanian && inputPertanianTextView.visibility == View.VISIBLE) {
                    isFirstInputPertanian = false
                    binding.sectionFarmBudget.linearLayoutFarmBudget.addView(farmBudgetHeaderView)
                }
                if (!isFound) {
                    binding.sectionFarmBudget.linearLayoutFarmBudget.addView(farmBudgetView)
                }
            }
        }

    }

    private fun nextButtonListener() {
        binding.sectionAb.buttonNextAb.setOnClickListener { v ->
            scrollToTop()
            binding.sectionAb.sectionAb.visibility = View.GONE
            binding.sectionFarmBudget.sectionFarmBudget.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.aktiviti_lawatan)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionFarmBudget.buttonNextFarmBudget.setOnClickListener { v ->
            scrollToTop()
            binding.sectionFarmBudget.sectionFarmBudget.visibility = View.GONE

            if (paramApptItem!!.getAnsuranNo?.toIntOrNull() == 1) {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_pendahuluan_semasa)
                binding.sectionC1.sectionC1.visibility = View.VISIBLE
            } else {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_berturut_ansuran)
                binding.sectionC2.sectionC2.visibility = View.VISIBLE
            }

            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionC1.buttonNextC1.setOnClickListener { v ->
            val edtLuasKawasanSiapDibersihkan =
                binding.sectionC1.edtLuasKawasanSiapDibersihkan.text.toString().toDoubleOrNull() ?: 0.0
            val edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih =
                binding.sectionC1.edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih.text.toString()
                    .toDoubleOrNull() ?: 0.0

            if (edtLuasKawasanSiapDibersihkan < 0.0
                || edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih < 0.0) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_hektar_negative),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            if(!sectionC1Mandatory()) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.mandatory),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionC1.sectionC1.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionC2.buttonNextC2.setOnClickListener { v ->
            val edtLuasTanamanUtamaYangBerjaya =
                binding.sectionC2.edtLuasTanamanUtamaYangBerjaya.text.toString().toDoubleOrNull() ?: 0.0
            val edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang =
                binding.sectionC2.edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang.text.toString()
                    .toDoubleOrNull() ?: 0.0
            val edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh =
                binding.sectionC2.edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh.text.toString()
                    .toDoubleOrNull() ?: 0.0
            val edtJikaYaLuasYangDiselenggara =
                binding.sectionC2.edtJikaYaLuasYangDiselenggara.text.toString().toDoubleOrNull() ?: 0.0
            val edtGetah =
                binding.sectionC2.edtGetah.text.toString().toIntOrNull() ?: 0
            val edtTanamanLain =
                binding.sectionC2.edtTanamanLain.text.toString().toIntOrNull() ?: 0
            val edtPurataUkurLilitPokokGetahYang =
                binding.sectionC2.edtPurataUkurLilitPokokGetahYang.text.toString().toDoubleOrNull()
                    ?: 0.0

            if (edtLuasTanamanUtamaYangBerjaya < 0.0
                || edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang < 0.0
                || edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh < 0.0
                || edtJikaYaLuasYangDiselenggara < 0.0
                || edtGetah < 0.0
                || edtTanamanLain < 0.0
                || edtPurataUkurLilitPokokGetahYang < 0.0) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_hektar_negative),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            if(!sectionC2Mandatory()) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.mandatory),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionC2.sectionC2.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionUploadAttachment.buttonNextUploadAttachment.setOnClickListener { v ->
            if (filePaths.size < 2 && !BuildConfig.DEBUG) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.attachements_desc),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.sectionD.sectionD.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.d_perakuan_pegawai_pemeriksa_kebun)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionD.buttonNextD.setOnClickListener { v ->
            val edtTarikhLawatan = binding.sectionD.edtTarikhLawatan.text.toString()
            val edtMasaDari = binding.sectionD.edtMasaDari.text.toString()
            val edtMasaSampai = binding.sectionD.edtMasaSampai.text.toString()
            val signPegawai = DisplayUtils.getBase64String(binding.sectionD.imageViewSignD)

            if (edtTarikhLawatan.isEmpty() || edtMasaDari.isEmpty() || edtMasaSampai.isEmpty() || signPegawai.isEmpty()) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_not_filled),
                    Snackbar.LENGTH_LONG
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionD.sectionD.visibility = View.GONE
            binding.sectionF.sectionF.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.f_peringatan)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionF.buttonNextF.setOnClickListener { v ->
            if (!binding.sectionF.checkboxSetuju.isChecked) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_not_checked),
                    Snackbar.LENGTH_LONG
                )
                return@setOnClickListener
            }
            scrollToTop()
            binding.sectionF.sectionF.visibility = View.GONE
            binding.sectionG.sectionG.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.g_perakuan_pemohon_wakilnya)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }
    }

    private fun previousButtonListener() {
        binding.sectionFarmBudget.buttonPreviousFarmBudget.setOnClickListener { v ->
            scrollToTop()
            binding.sectionAb.sectionAb.visibility = View.VISIBLE
            binding.sectionFarmBudget.sectionFarmBudget.visibility = View.GONE

            binding.textviewTitle.text = getString(R.string.a_b_profil_pemohon_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionC1.buttonPreviousC1.setOnClickListener { v ->
            scrollToTop()
            binding.sectionFarmBudget.sectionFarmBudget.visibility = View.VISIBLE
            binding.sectionC1.sectionC1.visibility = View.GONE

            binding.textviewTitle.text = getString(R.string.aktiviti_lawatan)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionC2.buttonPreviousC2.setOnClickListener { v ->
            scrollToTop()
            binding.sectionFarmBudget.sectionFarmBudget.visibility = View.VISIBLE
            binding.sectionC2.sectionC2.visibility = View.GONE

            binding.textviewTitle.text =
                getString(R.string.aktiviti_lawatan)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionUploadAttachment.buttonPreviousUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            if (paramApptItem!!.getAnsuranNo?.toIntOrNull() == 1) {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_pendahuluan_semasa)
                binding.sectionC1.sectionC1.visibility = View.VISIBLE
            } else {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_berturut_ansuran)
                binding.sectionC2.sectionC2.visibility = View.VISIBLE
            }

            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionD.buttonPreviousD.setOnClickListener { v ->
            scrollToTop()
            binding.textviewTitle.text =
                getString(R.string.attachments)
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.sectionD.sectionD.visibility = View.GONE
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionF.buttonPreviousF.setOnClickListener { v ->
            scrollToTop()
            binding.sectionD.sectionD.visibility = View.VISIBLE
            binding.sectionF.sectionF.visibility = View.GONE

            binding.textviewTitle.text = getString(R.string.d_perakuan_pegawai_pemeriksa_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionG.buttonPreviousG.setOnClickListener { v ->
            scrollToTop()
            binding.sectionF.sectionF.visibility = View.VISIBLE
            binding.sectionG.sectionG.visibility = View.GONE

            binding.textviewTitle.text = getString(R.string.f_peringatan)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }
    }

    private fun sectionC1Mandatory(): Boolean {
        val radiogroupKerjaMengukur = replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupKerjaMengukur.checkedRadioButtonId)?.text.toString())
        if (radiogroupKerjaMengukur.isEmpty()) return false
        val spinnerJarakTanamanDanKepadatanPokokHektar = binding.sectionC1.spinnerJarakTanaman.selectedItemPosition.toString()
        if (spinnerJarakTanamanDanKepadatanPokokHektar.isEmpty()) return false
        val edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih = binding.sectionC1.edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih.text.toString()
        if (edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih.isEmpty()) return false
        val radiogroupJenisBenih = replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupJenisBenih.checkedRadioButtonId)?.text.toString())
        if (radiogroupJenisBenih.isEmpty()) return false
        val radiogroupKelulusanBorangTs29a = replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupKelulusanBorangTs29a.checkedRadioButtonId)?.text.toString())
        if (radiogroupKelulusanBorangTs29a.isEmpty()) return false
        val radiogroupKlonBermutuTinggi = replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupKlonBermutuTinggi.checkedRadioButtonId)?.text.toString())
        if (radiogroupKlonBermutuTinggi.isEmpty()) return false
        val radiogroupResitPembelianBenih = replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupResitPembelianBenih.checkedRadioButtonId)?.text.toString())
        if (radiogroupResitPembelianBenih.isEmpty()) return false
        val radiogroupSijilPengesahan = replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupSijilPengesahan.checkedRadioButtonId)?.text.toString())
        if (radiogroupSijilPengesahan.isEmpty()) return false
        val radiogroupTapakSemaian = replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupTapakSemaian.checkedRadioButtonId)?.text.toString())
        if (radiogroupTapakSemaian.isEmpty()) return false
        val radiogroupAdakahTeres = replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupAdakahTeres.checkedRadioButtonId)?.text.toString())
        if (radiogroupAdakahTeres.isEmpty()) return false

        if(!binding.sectionC1.checkboxMengemaskini.isChecked) return false

        return true
    }

    private fun sectionC2Mandatory(): Boolean {
        val edtLuasTanamanUtamaYangBerjaya = binding.sectionC2.edtLuasTanamanUtamaYangBerjaya.text.toString()
        if(edtLuasTanamanUtamaYangBerjaya.isEmpty()) return false
        val radiogroupAdakahCantasan = replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahCantasan.checkedRadioButtonId)?.text.toString())
        if (radiogroupAdakahCantasan.isEmpty()) return false
        val radiogroupAdakahMembaja = replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahMembaja.checkedRadioButtonId)?.text.toString())
        if (radiogroupAdakahMembaja.isEmpty()) return false
        val radiogroupAdakahGalakan = replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahGalakan.checkedRadioButtonId)?.text.toString())
        if (radiogroupAdakahGalakan.isEmpty()) return false
        val radiogroupAdakahKawalan = replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahKawalan.checkedRadioButtonId)?.text.toString())
        if (radiogroupAdakahKawalan.isEmpty()) return false
        val edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang = binding.sectionC2.edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang.text.toString()
        if(edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang.isEmpty()) return false
        val edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh = binding.sectionC2.edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh.text.toString()
        if(edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh.isEmpty()) return false
        val edtGetah = binding.sectionC2.edtGetah.text.toString()
        if(edtGetah.isEmpty()) return false
        val edtTanamanLain = binding.sectionC2.edtTanamanLain.text.toString()
        if(edtTanamanLain.isEmpty()) return false
        val edtPurataUkurLilitPokokGetahYang = binding.sectionC2.edtPurataUkurLilitPokokGetahYang.text.toString()
        if(edtPurataUkurLilitPokokGetahYang.isEmpty()) return false

        if(!binding.sectionC2.checkboxMengemaskini.isChecked) return false

        return true
    }

    private fun cameraButtonListener() {
        binding.sectionUploadAttachment.cameraButton.setOnClickListener {
            if (filePaths.size >= 4) {
                // not allow to add more than 4 pictures
                return@setOnClickListener
            }
            try {
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

                // Create the File where the photo should go
                var photoFile: File? = null
                try {
                    photoFile = context?.let { it1 -> createImageFile(it1, latitude, longitude) }
                    // Save a file: path for use with ACTION_VIEW intents
                    photoFile?.let { it1 -> filePaths.add(it1.absolutePath) }
                } catch (e: IOException) {
                    Log.e(TAG, e.printStackTrace().toString())
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    photoURI = FileProvider.getUriForFile(
                        context!!,
                        getString(R.string.authority),
                        photoFile
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, Constants.REQUEST_IMAGE_CAPTURE)
                }
            } catch (e: Exception) {
                Log.e(TAG, e.printStackTrace().toString())
                binding.textviewTitle.errorSnack(
                    e.message.toString(),
                    Snackbar.LENGTH_SHORT
                )
            }
        }
    }

    private fun scrollToTop() {
        binding.scrollview.fullScroll(View.FOCUS_UP)
        binding.scrollview.smoothScrollTo(0, 0)
    }

    private fun showPrintAlertDialog() {
        binding.sectionG.buttonSaveG.isEnabled = false

        if (!PrintUtils.checkBluetoothStatus()) {
            binding.textviewTitle.showSnack(
                getString(R.string.ble_not_enabled),
                Snackbar.LENGTH_SHORT
            )
            return
        }

        val printerDialogFragment =
            PrinterDialogFragment(
                reportItem = null,
                appointmentItem = paramApptItem,
                participant = paramParticipant,
                form = paramForm
            )
        fragmentManager?.let { printerDialogFragment.show(it, Ts18Fragment.TAG) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.REQUEST_IMAGE_CAPTURE) {
            if (resultCode == Activity.RESULT_OK) {

                try {
                    val bitmap =
                        MediaStore.Images.Media.getBitmap(context!!.contentResolver, photoURI);

                    val inflater =
                        activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

                    val attachmentView: View = inflater.inflate(R.layout.item_camera, null)
                    attachmentView.id = filePaths.size

                    val imageView =
                        attachmentView.findViewById<ImageView>(R.id.image_view_attachment)
                    imageView.setImageBitmap(bitmap)
                    val imageName = attachmentView.findViewById<TextView>(R.id.tv_image_name)
                    val imageFileName = "image${SimpleDateFormat("HHmmss").format(Date())}\n${latitude}, ${longitude}"
                    imageName.setText(imageFileName)
                    val deleteButton = attachmentView.findViewById<Button>(R.id.delete_button)
                    deleteButton.tag = filePaths.size
                    deleteButton.setOnClickListener {
                        try {
                            binding.sectionUploadAttachment.linearLayoutAttachment.removeViewAt(it.tag as Int - 1)
                            filePaths.removeAt(it.tag as Int - 1)
                            imageView.setImageBitmap(null)
                        } catch (e: Exception) {
                            Log.e(TAG, e.message.toString())
                        }
                    }
                    binding.sectionUploadAttachment.linearLayoutAttachment.addView(attachmentView)

                } catch (e: IOException) {
                    e.printStackTrace();
                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
                binding.sectionUploadAttachment.cameraButton.errorSnack(
                    getString(R.string.cancelled),
                    Snackbar.LENGTH_SHORT
                )
            }
        }
    }

    companion object {
        val TAG = Ts18aFragment::class.java.simpleName!!

        @JvmStatic
        fun newInstance(
            paramApptItem: AppointmentItem,
            paramParticipant: Participant,
            paramForm: Form?,
            paramBorangType: String?
        ) =
            Ts18aFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM_APPT_ITEM, paramApptItem)
                    putParcelable(ARG_PARAM_PARTICIPANT, paramParticipant)
                    putParcelable(ARG_PARAM_PARTICIPANT_FORM, paramForm)
                    putString(ARG_PARAM_BORANG_TYPE, paramBorangType)
                }
            }

    }
}