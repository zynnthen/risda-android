package com.map2u.risda.ui.readonly

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.map2u.risda.R
import com.map2u.risda.app.MyApplication
import com.map2u.risda.databinding.FragmentTs18Binding
import com.map2u.risda.model.appointment.AppointmentItem
import com.map2u.risda.model.appointment.Form
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.util.Resource
import com.map2u.risda.util.TimeUtils.displayDate
import com.map2u.risda.util.Utils
import com.map2u.risda.util.display.DisplayUtils
import com.map2u.risda.util.display.DisplayUtils.getBitmapFromBase64String
import com.map2u.risda.util.errorSnack
import com.map2u.risda.viewmodel.Ts18ViewModel
import com.map2u.risda.viewmodel.ViewModelProviderFactory
import kotlinx.coroutines.*
import java.io.File
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [Ts18Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ReadOnlyTs18Fragment : Fragment() {
    val ARG_PARAM_APPT_ITEM = "ARG_PARAM_APPT_ITEM"
    val ARG_PARAM_FORM = "ARG_PARAM_FORM"
    var paramApptItem: AppointmentItem? = null
    var paramForm: Form? = null

    private var _binding: FragmentTs18Binding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private lateinit var viewModel: Ts18ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            paramApptItem = it.getParcelable(ARG_PARAM_APPT_ITEM)
            paramForm = it.getParcelable(ARG_PARAM_FORM)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTs18Binding.inflate(inflater, container, false)
        val view = binding.root

        setupViewModel()

        /*** Disable edit text, radio button ***/
        val ll: LinearLayout? = binding.rootLinearLayout
        for (view in ll!!.touchables) {
            if (view is EditText) {
                val editText = view
                editText.setTextColor(context!!.getColor(R.color.colorPrimaryText))
                editText.isEnabled = false
                editText.isFocusable = false
                editText.isFocusableInTouchMode = false
            } else if (view is RadioButton) {
                val radioButton = view
                radioButton.isEnabled = false
                radioButton.buttonTintList =
                    ColorStateList.valueOf(context!!.getColor(R.color.colorPrimaryDark))
                radioButton.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.md_grey_300)))
            }
        }

        binding.sectionC.sectionC.visibility = View.GONE
        binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
        binding.sectionD.sectionD.visibility = View.GONE
        binding.sectionF.sectionF.visibility = View.GONE
        binding.sectionG.sectionG.visibility = View.GONE

        binding.seekBar.setOnTouchListener(OnTouchListener { v, event ->
            // True doesn't propagate the event
            // the user cannot move the indicator
            true
        })

        /*** Next button listener ***/
        nextButtonListener()

        /*** Previous button listener ***/
        previousButtonListener()

        initAbContent()
        initCContent()
        initAttachmentsContent()
        initDContent()
        initFContent()
        initGContent()

        return view
    }

    private fun setupViewModel() {
        val repository = AppRepository()
        val factory = activity?.let { ViewModelProviderFactory(it.application, repository) }
        viewModel = factory?.let { ViewModelProvider(this, it).get(Ts18ViewModel::class.java) }!!
    }

    private fun initAbContent() {
        val address =
            "${paramApptItem?.tsApplication?.address1} ${paramApptItem?.tsApplication?.address2} ${paramApptItem?.tsApplication?.address3}"
        binding.sectionAb.edtNama.setText(paramApptItem?.tsApplication?.namaPemohon)
        binding.sectionAb.edtNoKpPolisTenteraSyarikat.setText(paramApptItem?.tsApplication?.icNoPemohon)
        binding.sectionAb.edtAlamat.setText(address)
        binding.sectionAb.edtPoskod.setText(paramApptItem?.tsApplication?.poskod)
        binding.sectionAb.edtNoTelRumah.setText(paramApptItem?.tsApplication?.noPhoneHouse)
        binding.sectionAb.edtNoTelBimbit.setText(paramApptItem?.tsApplication?.noPhoneMobile)
        binding.sectionAb.edtNoPermohonan.setText(paramApptItem?.tsMohonId)

        binding.sectionAb.edtDaerah.setText(paramApptItem?.tsApplication?.daerah)
        binding.sectionAb.edtParlimen.setText(paramApptItem?.tsApplication?.parlimen)
        binding.sectionAb.edtDun.setText(paramApptItem?.tsApplication?.dun)
        binding.sectionAb.edtMukim.setText(paramApptItem?.tsApplication?.mukim)
        binding.sectionAb.edtKampung.setText(paramApptItem?.tsApplication?.kampung)
        binding.sectionAb.edtNoGeran.setText(paramApptItem?.tsApplication?.lotDimilikiList?.get(0)?.noGeran)
        binding.sectionAb.edtNoLot.setText(paramApptItem?.tsApplication?.lotDimilikiList?.get(0)?.noLot)
        binding.sectionAb.edtLuasKebunHektar.setText(
            paramApptItem?.tsApplication?.lotDimilikiList?.get(
                0
            )?.luasLot.toString()
        )
        binding.sectionAb.edtLuasKebunHendakDitanamSemulaHektar.setText(
            paramApptItem?.tsApplication?.lotDimilikiList?.get(
                0
            )?.luasTs.toString()
        )

        if (paramForm != null) {
            if (paramForm!!.bantuanLain?.trim() == "Ya") {
                binding.sectionAb.rbMenerimaBantuanYa.isChecked = true
            } else if (paramForm!!.bantuanLain?.trim() == "Tidak") {
                binding.sectionAb.rbMenerimaBantuanTidak.isChecked = true
            }

            // jika ya, nyatakan
            binding.sectionAb.edtNamaAgensiPemberiBantuan.setText(
                paramForm!!.blAgensi
            )
            binding.sectionAb.edtLuasDilulus.setText(paramForm!!.blLuasLulus)
            binding.sectionAb.edtJenisTanaman.setText(paramForm!!.blCrop)
        }

    }

    private fun initCContent() {
        if (paramForm != null) {
            binding.sectionC.edtKeluasanTanamanGetahTuaSekarang.setText(paramForm!!.luasGetahTua.toString())
            binding.sectionC.edtTanamanLain.setText(paramForm!!.luasTanamanLain.toString())
            binding.sectionC.edtHutanTanahKosong.setText(paramForm!!.luasHutan.toString())
            binding.sectionC.edtRumahBangunan.setText(paramForm!!.luasRumah.toString())
            binding.sectionC.edtLainLainSilaNyatakan.setText(paramForm!!.lainLain)
            binding.sectionC.edtLainLainSilaNyatakanHek.setText(paramForm!!.luasLainLain.toString())

            if (paramForm!!.tunggulGetah?.trim() == "Ya") {
                binding.sectionC.rbTunggulGetahTuaYa.isChecked = true
            } else if (paramForm!!.tunggulGetah?.trim() == "Tidak") {
                binding.sectionC.rbTunggulGetahTuaTidak.isChecked = true
            }

            if (paramForm!!.bukuLesen?.trim() == "Ya") {
                binding.sectionC.rbBukuLesenGetahYa.isChecked = true
            } else if (paramForm!!.bukuLesen?.trim() == "Tidak") {
                binding.sectionC.rbBukuLesenGetahTidak.isChecked = true
            }

            if (paramForm!!.gambarSatelit?.trim() == "Ya") {
                binding.sectionC.rbGambarSatelitYa.isChecked = true
            } else if (paramForm!!.gambarSatelit?.trim() == "Tidak") {
                binding.sectionC.rbGambarSatelitTidak.isChecked = true
            }

            if (paramForm!!.lesenGetah?.trim() == "Ya") {
                binding.sectionC.rbLesenGetahYa.isChecked = true
            } else if (paramForm!!.lesenGetah?.trim() == "Tidak") {
                binding.sectionC.rbLesenGetahTidak.isChecked = true
            }

            if (paramForm!!.suratAgensi?.trim() == "Ya") {
                binding.sectionC.rbSuratPengesahanYa.isChecked = true
            } else if (paramForm!!.lesenGetah?.trim() == "Tidak") {
                binding.sectionC.rbSuratPengesahanTidak.isChecked = true
            }

            if (paramForm!!.buktiLotTs?.trim() == "Ya") {
                binding.sectionC.rbRekodYangMembukitkanYa.isChecked = true
            } else if (paramForm!!.buktiLotTs?.trim() == "Tidak") {
                binding.sectionC.rbRekodYangMembukitkanTidak.isChecked = true
            }

            if (paramForm!!.buktiLot?.trim() == "Ya") {
                binding.sectionC.rbSilaSertakanYa.isChecked = true
            } else if (paramForm!!.buktiLot?.trim() == "Tidak") {
                binding.sectionC.rbSilaSertakanTidak.isChecked = true
            }

            if (paramForm!!.umurPokok?.trim() == "<20tahun") {
                binding.sectionC.rbUmurPokok20.isChecked = true
            } else if (paramForm!!.umurPokok?.trim() == ">=20tahun") {
                binding.sectionC.rbUmurPokokMore20.isChecked = true
            }

            if (paramForm!!.getahTua?.trim() == "Ekonomik") {
                binding.sectionC.rbKeadaanPokokEkonomik.isChecked = true
            } else if (paramForm!!.getahTua?.trim() == "Tidak ekonomik") {
                binding.sectionC.rbKeadaanPokokTidakEkonomik.isChecked = true
                val color = ColorStateList.valueOf(context!!.getColor(R.color.colorPrimaryDark))

                binding.sectionC.rbHasil500Ya.buttonTintList = color
                binding.sectionC.rbHasil500Tidak.buttonTintList = color
                if (paramForm!!.pokokK300?.trim() == "Ya") {
                    binding.sectionC.rbHasil500Ya.isChecked = true
                } else if (paramForm!!.pokokK300?.trim() == "Tidak") {
                    binding.sectionC.rbHasil500Tidak.isChecked = true
                }

                binding.sectionC.rbHabisKulitYa.buttonTintList = color
                binding.sectionC.rbHabisKulitTidak.buttonTintList = color
                if (paramForm!!.kulitTorehan?.trim() == "Ya") {
                    binding.sectionC.rbHabisKulitYa.isChecked = true
                } else if (paramForm!!.kulitTorehan?.trim() == "Tidak") {
                    binding.sectionC.rbHabisKulitTidak.isChecked = true
                }

                binding.sectionC.rbPokokGetahYa.buttonTintList = color
                binding.sectionC.rbPokokGetahTidak.buttonTintList = color
                if (paramForm!!.hasilK500?.trim() == "Ya") {
                    binding.sectionC.rbPokokGetahYa.isChecked = true
                } else if (paramForm!!.hasilK500?.trim() == "Tidak") {
                    binding.sectionC.rbPokokGetahTidak.isChecked = true
                }

                binding.sectionC.rbLainLainSebabYa.buttonTintList = color
                binding.sectionC.rbLainLainSebabTidak.buttonTintList = color
                if (paramForm!!.sebabLainPRN?.trim() == "Ya") {
                    binding.sectionC.rbLainLainSebabYa.isChecked = true
                } else if (paramForm!!.sebabLainPRN?.trim() == "Tidak") {
                    binding.sectionC.rbLainLainSebabTidak.isChecked = true
                }
            }
        }

        binding.sectionC.checkboxMengemaskini.isChecked = true
        binding.sectionC.checkboxMengemaskini.isClickable = false
    }

    private fun initAttachmentsContent() {
        binding.sectionUploadAttachment.cameraButton.visibility = View.GONE

        if (Utils.hasInternetConnection(activity!!.application as MyApplication)) {
            GlobalScope.launch {
                callApiAndUpdateLiveData()
            }
        } else {
            val filePaths = paramForm?.noSiriTemujanji?.let { viewModel.queryImageOffline(it) }

            filePaths?.forEach{
                val dir = it.substringBeforeLast('/').replace("\"", "")
                val fileName = it.substringAfterLast('/').replace("\"", "")
                val file = File(dir, fileName)
                if (file.exists()) {
                    val bitmap = BitmapFactory.decodeFile(file.absolutePath)

                    val inflater =
                        activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

                    val attachmentView: View = inflater.inflate(R.layout.item_camera, null)
                    val imageView = attachmentView.findViewById<ImageView>(R.id.image_view_attachment)
                    imageView.setImageBitmap(bitmap)

                    val imageName = attachmentView.findViewById<TextView>(R.id.tv_image_name)
                    val name = fileName.replace("_","\n")
                    imageName.text = name

                    val deleteButton = attachmentView.findViewById<Button>(R.id.delete_button)
                    deleteButton.visibility = View.GONE

                    binding.sectionUploadAttachment.linearLayoutAttachment.addView(
                        attachmentView
                    )
                }
            }
        }
    }

    private suspend fun callApiAndUpdateLiveData() {
        paramForm?.documents?.forEach {
            coroutineScope {
                async {
                    val result = it.id?.let { file -> viewModel.getFile(file) }
                }
            }.await()
        }

        lifecycleScope.launch {
            withContext(Dispatchers.Main) {
                viewModel.apiFileResponse.observe(this@ReadOnlyTs18Fragment, { event ->
                    event.getContentIfNotHandled()?.let { response ->
                        when (response) {
                            is Resource.Success -> {
                                val fileName = response.message
                                response.data?.let { response ->
                                    val bitmap =
                                        BitmapFactory.decodeStream(response.byteStream())

                                    val inflater =
                                        activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

                                    val attachmentView: View =
                                        inflater.inflate(R.layout.item_camera, null)
                                    val imageView =
                                        attachmentView.findViewById<ImageView>(R.id.image_view_attachment)
                                    imageView.setImageBitmap(bitmap)

                                    val imageName = attachmentView.findViewById<TextView>(R.id.tv_image_name)
                                    imageName.text = fileName

                                    val deleteButton =
                                        attachmentView.findViewById<Button>(R.id.delete_button)
                                    deleteButton.visibility = View.GONE

                                    binding.sectionUploadAttachment.linearLayoutAttachment.addView(
                                        attachmentView
                                    )
                                }
                            }
                        }
                    }
                })
            }
        }
    }

    private fun initDContent() {
        binding.sectionD.buttonSignHereD.visibility = View.GONE

        binding.sectionD.edtTarikhLawatan.setText(paramForm!!.tkhLawat?.let { displayDate(it) })
        binding.sectionD.edtMasaDari.setText(paramForm!!.mlawatMula!!)
        binding.sectionD.edtMasaSampai.setText(paramForm!!.mlawatTamat)

        binding.sectionD.imageViewSignD.setImageBitmap(paramForm!!.signPegawai?.let {
            getBitmapFromBase64String(
                it
            )
        })
    }

    private fun initFContent() {
        binding.sectionF.checkboxSetuju.isClickable = false
        binding.sectionF.checkboxSetuju.isChecked = true
    }

    private fun initGContent() {
        binding.sectionG.buttonSignHereG.visibility = View.GONE
        binding.sectionG.buttonSaveG.visibility = View.GONE

        if (paramForm!!.hadirSendiri?.trim() == "Ya") {
            binding.sectionG.rbPemohonHadirSendiriYa.isChecked = true
        } else if (paramForm!!.hadirSendiri?.trim() == "Tidak") {
            binding.sectionG.rbPemohonHadirSendiriTidak.isChecked = true
        }

        binding.sectionG.edtNama.setText(paramForm!!.namaPerakuan)
        binding.sectionG.edtKpPemohon.setText(paramForm!!.noKpPerakuan)
        binding.sectionG.imageViewSignG.setImageBitmap(paramForm!!.signPerakuan?.let {
            getBitmapFromBase64String(
                it
            )
        })
    }

    private fun nextButtonListener() {
        binding.sectionAb.buttonAbNextAb.setOnClickListener { v ->
            scrollToTop()
            binding.sectionAb.sectionAb.visibility = View.GONE
            binding.sectionC.sectionC.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.c_laporan_pengesahan_lawatan_kebun)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionC.buttonNextC.setOnClickListener { v ->
            scrollToTop()
            binding.sectionC.sectionC.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionUploadAttachment.buttonNextUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.sectionD.sectionD.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.d_perakuan_pegawai_pemeriksa_kebun)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionD.buttonNextD.setOnClickListener { v ->
            val edtTarikhLawatan = binding.sectionD.edtTarikhLawatan.text.toString()
            val edtMasaDari = binding.sectionD.edtMasaDari.text.toString()
            val edtMasaSampai = binding.sectionD.edtMasaSampai.text.toString()
            val signPegawai = DisplayUtils.getBase64String(binding.sectionD.imageViewSignD)
            if (edtTarikhLawatan.isEmpty() || edtMasaDari.isEmpty() || edtMasaSampai.isEmpty() || signPegawai.isEmpty()) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_not_filled),
                    Snackbar.LENGTH_LONG
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionD.sectionD.visibility = View.GONE
            binding.sectionF.sectionF.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.f_peringatan)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionF.buttonNextF.setOnClickListener { v ->
            if (!binding.sectionF.checkboxSetuju.isChecked) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_not_checked),
                    Snackbar.LENGTH_LONG
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionF.sectionF.visibility = View.GONE
            binding.sectionG.sectionG.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.g_perakuan_pemohon_wakilnya)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }
    }

    private fun previousButtonListener() {
        binding.sectionC.buttonPreviousC.setOnClickListener { v ->
            scrollToTop()
            binding.sectionC.sectionC.visibility = View.GONE
            binding.sectionAb.sectionAb.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.a_b_profil_pemohon_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionUploadAttachment.buttonPreviousUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.sectionC.sectionC.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.c_laporan_pengesahan_lawatan_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionD.buttonPreviousD.setOnClickListener { v ->
            scrollToTop()
            binding.sectionD.sectionD.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionF.buttonPreviousF.setOnClickListener { v ->
            scrollToTop()
            binding.sectionF.sectionF.visibility = View.GONE
            binding.sectionD.sectionD.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.d_perakuan_pegawai_pemeriksa_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionG.buttonPreviousG.setOnClickListener { v ->
            scrollToTop()
            binding.sectionG.sectionG.visibility = View.GONE
            binding.sectionF.sectionF.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.f_peringatan)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }
    }

    private fun scrollToTop() {
        binding.scrollview.fullScroll(View.FOCUS_UP)
        binding.scrollview.smoothScrollTo(0, 0)
    }

    companion object {
        val TAG = ReadOnlyTs18Fragment::class.java.simpleName!!

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param paramApptItem Parameter 1.
         * @return A new instance of fragment Ts18Fragment.
         */
        @JvmStatic
        fun newInstance(paramApptItem: AppointmentItem, paramForm: Form) =
            ReadOnlyTs18Fragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM_APPT_ITEM, paramApptItem)
                    putParcelable(ARG_PARAM_FORM, paramForm)
                }
            }

    }
}