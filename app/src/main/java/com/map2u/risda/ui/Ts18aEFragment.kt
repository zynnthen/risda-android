package com.map2u.risda.ui

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RadioButton
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.map2u.risda.BuildConfig
import com.map2u.risda.R
import com.map2u.risda.databinding.FragmentTs18aEBinding
import com.map2u.risda.model.appointment.PendingVerify
import com.map2u.risda.model.appointment.TsApplication
import com.map2u.risda.network.RequestBodies
import com.map2u.risda.network.SessionManager
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.ui.shared.OnSignedCaptureListener
import com.map2u.risda.ui.shared.SignatureDialogFragment
import com.map2u.risda.util.*
import com.map2u.risda.util.TimeUtils.getTimestamp
import com.map2u.risda.util.display.DisplayUtils
import com.map2u.risda.viewmodel.Ts18EViewModel
import com.map2u.risda.viewmodel.ViewModelProviderFactory
import kotlinx.coroutines.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class Ts18aEFragment : Fragment(), OnSignedCaptureListener {
    val ARG_PARAM_PENDING_VERIFY = "ARG_PARAM_PENDING_VERIFY"
    val ARG_PARAM_TS_APPLICATION = "ARG_PARAM_TS_APPLICATION"

    var paramPendingVerify: PendingVerify? = null
    var paramTsApplication: TsApplication? = null

    private var _binding: FragmentTs18aEBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private lateinit var viewModel: Ts18EViewModel

    // This property is for image upload
    private val filePaths: ArrayList<String> = ArrayList()

    lateinit var photoURI: Uri

    // This property is to get location
    var longitude: Double = 0.0
    var latitude: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            paramPendingVerify = it.getParcelable(ARG_PARAM_PENDING_VERIFY)
            paramTsApplication = it.getParcelable(ARG_PARAM_TS_APPLICATION)
        }

        latitude = Utils.getLatitude(activity!!)
        longitude = Utils.getLongitude(activity!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTs18aEBinding.inflate(inflater, container, false)
        val view = binding.root

        setupViewModel()

        binding.sectionC1.sectionC1.visibility = View.GONE
        binding.sectionC2.sectionC2.visibility = View.GONE
        binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
        binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.GONE
        binding.sectionD.sectionD.visibility = View.GONE
        binding.sectionF.sectionF.visibility = View.GONE

        /*** Next button listener ***/
        nextButtonListener()

        /*** Previous button listener ***/
        previousButtonListener()

        /*** Camera button listener ***/
        cameraButtonListener()

        initAbContent()
        initC1Content()
        initC2Content()
        initAttachmentsContent()
        initDContent()
        initFContent()

        /*** Date input picker ***/
        val myCalendar1: Calendar = Calendar.getInstance()
        val date1 =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                myCalendar1.set(Calendar.YEAR, year)
                myCalendar1.set(Calendar.MONTH, monthOfYear)
                myCalendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val sdf = SimpleDateFormat("dd-MM-yyyy")
                binding.sectionC1.edtTarikhTanam2.setText(sdf.format(myCalendar1.getTime()))
            }

        binding.sectionC1.edtTarikhTanam2.setOnClickListener { v ->
            DatePickerDialog(
                context!!, date1, myCalendar1[Calendar.YEAR], myCalendar1[Calendar.MONTH],
                myCalendar1[Calendar.DAY_OF_MONTH]
            ).show()
        }

        /*** Save form button listener ***/
        binding.sectionF.buttonSaveF.setOnClickListener { v ->
            if (!binding.sectionF.checkboxSetuju.isChecked) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_not_checked),
                    Snackbar.LENGTH_LONG
                )
                return@setOnClickListener
            }

            saveForm()
        }

        return view
    }

    private fun setupViewModel() {
        val repository = AppRepository()
        val factory = activity?.let { ViewModelProviderFactory(it.application, repository) }
        viewModel = factory?.let { ViewModelProvider(this, it).get(Ts18EViewModel::class.java) }!!
    }

    private fun initAbContent() {
        val tsApplication: TsApplication? = paramTsApplication

        val address =
            "${tsApplication?.address1} ${tsApplication?.address2} ${tsApplication?.address3}"
        binding.sectionAb.edtNama.setText(tsApplication?.namaPemohon)
        binding.sectionAb.edtNoKpPolisTenteraSyarikat.setText(tsApplication?.icNoPemohon)
        binding.sectionAb.edtAlamat.setText(address)
        binding.sectionAb.edtPoskod.setText(tsApplication?.poskod)
        binding.sectionAb.edtNoTelRumah.setText(tsApplication?.noPhoneHouse)
        binding.sectionAb.edtNoTelBimbit.setText(tsApplication?.noPhoneMobile)
        binding.sectionAb.edtNoPermohonan.setText(tsApplication?.tsMohonId)

        binding.sectionAb.edtDaerah.setText(tsApplication?.daerah)
        binding.sectionAb.edtParlimen.setText(tsApplication?.parlimen)
        binding.sectionAb.edtDun.setText(tsApplication?.dun)
        binding.sectionAb.edtMukim.setText(tsApplication?.mukim)
        binding.sectionAb.edtKampung.setText(tsApplication?.kampung)
    }

    private fun initC1Content() {
        binding.sectionC1.textviewLuasKawasanSiapDibersihkan.text = paramPendingVerify?.tebangJentera1.toString()
        binding.sectionC1.textviewJarakTanamanDanKepadatanPokokHektar.text = paramPendingVerify?.tebangJentera1.toString()
        binding.sectionC1.textviewLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih.text = paramPendingVerify?.luasTanam.toString()
        binding.sectionC1.textviewTarikhTanam.text = TimeUtils.displayDate(paramPendingVerify?.tkhTanam ?: 0)

        if (paramPendingVerify?.benihLuar?.trim() == "Ya") {
            binding.sectionC1.textviewKelulusanBorangTs29a.text = getString(R.string.ya)
        } else if (paramPendingVerify?.benihLuar?.trim() == "Tidak") {
            binding.sectionC1.textviewKelulusanBorangTs29a.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.klonMutu?.trim() == "Ya") {
            binding.sectionC1.textviewKlonBermutuTinggi.text = getString(R.string.ya)
        } else if (paramPendingVerify?.klonMutu?.trim() == "Tidak") {
            binding.sectionC1.textviewKlonBermutuTinggi.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.resitBenih?.trim() == "Ya") {
            binding.sectionC1.textviewResitPembelianBenih.text = getString(R.string.ya)
        } else if (paramPendingVerify?.resitBenih?.trim() == "Tidak") {
            binding.sectionC1.textviewResitPembelianBenih.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.sahKlon?.trim() == "Ya") {
            binding.sectionC1.textviewSijilPengesahan.text = getString(R.string.ya)
        } else if (paramPendingVerify?.sahKlon?.trim() == "Tidak") {
            binding.sectionC1.textviewSijilPengesahan.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.tapakSemaian?.trim() == "Ya") {
            binding.sectionC1.textviewTapakSemaian.text = getString(R.string.ya)
        } else if (paramPendingVerify?.tapakSemaian?.trim() == "Tidak") {
            binding.sectionC1.textviewTapakSemaian.text = getString(R.string.tidak)
        }

        binding.sectionC1.textviewBenihSawitNyatakanSumber.text = paramPendingVerify?.benihSawit.toString()

        if (paramPendingVerify?.gapSenggara?.trim() == "Ya") {
            binding.sectionC1.textviewAdakahTeres.text = getString(R.string.ya)
        } else if (paramPendingVerify?.gapSenggara?.trim() == "Tidak") {
            binding.sectionC1.textviewAdakahTeres.text = getString(R.string.tidak)
        }

        binding.sectionC1.textviewLainLainKenyataan.text = paramPendingVerify?.kenyataanLain.toString()

        binding.sectionC1.textviewJenisBenihGetahSilaNyatakan.text = paramPendingVerify?.jenisBenih.toString()
    }

    private fun initC2Content() {
        binding.sectionC2.edtLuasTanamanUtamaYangBerjaya.setText(paramPendingVerify?.luasBerjaya.toString())

        if (paramPendingVerify?.gapBaja?.trim() == "Ya") {
            binding.sectionC2.textviewAdakahMembaja.text = getString(R.string.ya)
        } else if (paramPendingVerify?.gapBaja?.trim() == "Tidak") {
            binding.sectionC2.textviewAdakahMembaja.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.gapCantasan?.trim() == "Ya") {
            binding.sectionC2.textviewAdakahCantasan.text = getString(R.string.ya)
        } else if (paramPendingVerify?.gapCantasan?.trim() == "Tidak") {
            binding.sectionC2.textviewAdakahCantasan.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.jenisCantasan?.trim() == "Pembetulan") {
            binding.sectionC2.textviewJikaYa.text = getString(R.string.pembetulan)
        } else if (paramPendingVerify?.jenisCantasan?.trim() == "Terkawal") {
            binding.sectionC2.textviewJikaYa.text = getString(R.string.terkawal)
        }

        if (paramPendingVerify?.gapGalakDahan?.trim() == "Ya") {
            binding.sectionC2.textviewAdakahGalakan.text = getString(R.string.ya)
        } else if (paramPendingVerify?.gapGalakDahan?.trim() == "Tidak") {
            binding.sectionC2.textviewAdakahGalakan.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.jenisGalakDahan?.trim() == "Dilaksanakan") {
            binding.sectionC2.textviewJikaYaNyatakan.text = getString(R.string.dilaksanakan)
        } else if (paramPendingVerify?.jenisGalakDahan?.trim() == "Tidak dilaksanakan") {
            binding.sectionC2.textviewJikaYaNyatakan.text = getString(R.string.tidak_dilaksanakan)
        }

        if (paramPendingVerify?.kawalPerosak?.trim() == "Ya") {
            binding.sectionC2.textviewAdakahKawalan.text = getString(R.string.ya)
        } else if (paramPendingVerify?.kawalPerosak?.trim() == "Tidak") {
            binding.sectionC2.textviewAdakahKawalan.text = getString(R.string.tidak)
        }

        binding.sectionC2.textviewRumpaiSiapDiselenggaraDenganMemuaskanDanLalang.text = paramPendingVerify?.rumpaiSenggara.toString()
        binding.sectionC2.textviewRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh.text = paramPendingVerify?.rumpaiTdkSenggara.toString()

        if (paramPendingVerify?.gapSenggara?.trim() == "Ya") {
            binding.sectionC2.textviewAdakahTeresPelantar.text = getString(R.string.ya)
        } else if (paramPendingVerify?.gapSenggara?.trim() == "Tidak") {
            binding.sectionC2.textviewAdakahTeresPelantar.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.kpbKontan?.trim() == "Ya") {
            binding.sectionC2.textviewAdakahKpb.text = getString(R.string.ya)
        } else if (paramPendingVerify?.kpbKontan?.trim() == "Tidak") {
            binding.sectionC2.textviewAdakahKpb.text = getString(R.string.tidak)
        }

        binding.sectionC2.textviewJikaYaLuasYangDiselenggara.text = paramPendingVerify?.luasKontan.toString()
        binding.sectionC2.textviewGetah.text = paramPendingVerify?.getahPkkHek.toString()
        binding.sectionC2.textviewTanamanLain.text = paramPendingVerify?.tlPkkHidup.toString()
        binding.sectionC2.textviewPurataUkurLilitPokokGetahYang.text = paramPendingVerify?.ukurLilit.toString()
        binding.sectionC2.textviewLainLainKenyataanJikaAda.text = paramPendingVerify?.kenyataanLain.toString()
    }

    private fun initAttachmentsContent() {
        binding.sectionUploadAttachment.cameraButton.visibility = View.GONE
        binding.sectionUploadAttachment.textviewMandatory.visibility = View.GONE

        GlobalScope.launch {
            callApiAndUpdateLiveData()
        }
    }


    private suspend fun callApiAndUpdateLiveData() {
        paramPendingVerify?.documents?.forEach {
            coroutineScope {
                async {
                    val result = it.id?.let { file -> viewModel.getFile(file) }
                }
            }.await()
        }

        lifecycleScope.launch {
            withContext(Dispatchers.Main) {
                viewModel.apiFileResponse.observe(this@Ts18aEFragment, { event ->
                    event.getContentIfNotHandled()?.let { response ->
                        when (response) {
                            is Resource.Success -> {
                                response.data?.let { response ->
                                    val bitmap =
                                        BitmapFactory.decodeStream(response.byteStream())

                                    val inflater =
                                        activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

                                    val attachmentView: View =
                                        inflater.inflate(R.layout.item_camera, null)
                                    val imageView =
                                        attachmentView.findViewById<ImageView>(R.id.image_view_attachment)
                                    imageView.setImageBitmap(bitmap)
                                    val deleteButton =
                                        attachmentView.findViewById<Button>(R.id.delete_button)
                                    deleteButton.visibility = View.GONE

                                    binding.sectionUploadAttachment.linearLayoutAttachment.addView(
                                        attachmentView
                                    )
                                }
                            }
                        }
                    }
                })
            }
        }
    }

    private fun initDContent() {
        /*** Section D default date ***/
        binding.sectionD.edtTarikhLawatan.setText(TimeUtils.displayTodayDate())

        /*** Time input picker ***/
        // This property is for Time picker usage
        val myCalendar: Calendar = Calendar.getInstance()
        val timeDari =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                myCalendar.set(Calendar.MINUTE, minute)

                val sdf = SimpleDateFormat("hh:mm a")
                binding.sectionD.edtMasaDari2.setText(sdf.format(myCalendar.getTime()))
            }
        binding.sectionD.edtMasaDari2.setOnClickListener { v ->
            TimePickerDialog(
                context!!,
                timeDari,
                myCalendar[Calendar.HOUR_OF_DAY],
                myCalendar[Calendar.MINUTE],
                false
            ).show()
        }

        val timeSampai =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                myCalendar.set(Calendar.MINUTE, minute)

                val sdf = SimpleDateFormat("hh:mm a")
                binding.sectionD.edtMasaSampai2.setText(sdf.format(myCalendar.getTime()))
            }
        binding.sectionD.edtMasaSampai2.setOnClickListener { v ->
            TimePickerDialog(
                context!!,
                timeSampai,
                myCalendar[Calendar.HOUR_OF_DAY],
                myCalendar[Calendar.MINUTE],
                false
            ).show()
        }

        /*** Sign listener ***/
        binding.sectionD.buttonSignHereD.setOnClickListener { v ->
            val dialogFragment = SignatureDialogFragment(this, "D")
            dialogFragment.show(activity!!.supportFragmentManager, "signature")
        }
    }

    private fun initFContent() {
        binding.sectionF.buttonNextF.visibility = View.GONE
        binding.sectionF.buttonSaveF.visibility = View.VISIBLE
    }

    private fun nextButtonListener() {
        binding.sectionAb.buttonNextAb.setOnClickListener { v ->
            scrollToTop()
            binding.sectionAb.sectionAb.visibility = View.GONE

            if (paramPendingVerify!!.getAnsuranNo?.toIntOrNull() == 1) {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_pendahuluan_semasa)
                binding.sectionC1.sectionC1.visibility = View.VISIBLE
            } else {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_berturut_ansuran)
                binding.sectionC2.sectionC2.visibility = View.VISIBLE
            }

            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionC1.buttonNextC1.setOnClickListener { v ->
            val edtLuasKawasanSiapDibersihkan =
                binding.sectionC1.edtLuasKawasanSiapDibersihkan2.text.toString().toDoubleOrNull() ?: 0.0
            val edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih =
                binding.sectionC1.edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih2.text.toString()
                    .toDoubleOrNull() ?: 0.0

            if (edtLuasKawasanSiapDibersihkan < 0.0
                || edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih < 0.0) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_hektar_negative),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            if(!sectionC1Mandatory()) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.mandatory),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionC1.sectionC1.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionC2.buttonNextC2.setOnClickListener { v ->
            val edtLuasTanamanUtamaYangBerjaya =
                binding.sectionC2.edtLuasTanamanUtamaYangBerjaya.text.toString().toDoubleOrNull() ?: 0.0
            val edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang =
                binding.sectionC2.edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang.text.toString()
                    .toDoubleOrNull() ?: 0.0
            val edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh =
                binding.sectionC2.edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh.text.toString()
                    .toDoubleOrNull() ?: 0.0
            val edtJikaYaLuasYangDiselenggara =
                binding.sectionC2.edtJikaYaLuasYangDiselenggara.text.toString().toDoubleOrNull() ?: 0.0
            val edtGetah =
                binding.sectionC2.edtGetah.text.toString().toIntOrNull() ?: 0
            val edtTanamanLain =
                binding.sectionC2.edtTanamanLain.text.toString().toIntOrNull() ?: 0
            val edtPurataUkurLilitPokokGetahYang =
                binding.sectionC2.edtPurataUkurLilitPokokGetahYang.text.toString().toDoubleOrNull()
                    ?: 0.0

            if (edtLuasTanamanUtamaYangBerjaya < 0.0
                || edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang < 0.0
                || edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh < 0.0
                || edtJikaYaLuasYangDiselenggara < 0.0
                || edtGetah < 0.0
                || edtTanamanLain < 0.0
                || edtPurataUkurLilitPokokGetahYang < 0.0) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_hektar_negative),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            if(!sectionC2Mandatory()) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.mandatory),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionC2.sectionC2.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionUploadAttachment.buttonNextUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionUploadAttachmentVerify.buttonNextUploadAttachment.setOnClickListener { v ->
            if (filePaths.size < 2 && !BuildConfig.DEBUG) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.attachements_desc),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.GONE
            binding.sectionD.sectionD.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.d_pengesahan_pegawai_penilai)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionD.buttonNextD.setOnClickListener { v ->
            val edtTarikhLawatan = binding.sectionD.edtTarikhLawatan.text.toString()
            val edtMasaDari = TimeUtils.parseDisplayTimeFormat(binding.sectionD.edtMasaDari2.text.toString())
            val edtMasaSampai = TimeUtils.parseDisplayTimeFormat(binding.sectionD.edtMasaSampai2.text.toString())
            val signPegawai = DisplayUtils.getBase64String(binding.sectionD.imageViewSignD)
            val edtCatatan2 = binding.sectionD.edtCatatan2.text.toString()

            if (edtTarikhLawatan.isEmpty() || edtMasaDari.isEmpty() || edtMasaSampai.isEmpty() || signPegawai.isEmpty() || edtCatatan2.isEmpty()) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_not_filled),
                    Snackbar.LENGTH_LONG
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionD.sectionD.visibility = View.GONE
            binding.sectionF.sectionF.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.f_peringatan)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

    }

    private fun previousButtonListener() {
        binding.sectionC1.buttonPreviousC1.setOnClickListener { v ->
            scrollToTop()
            binding.sectionAb.sectionAb.visibility = View.VISIBLE
            binding.sectionC1.sectionC1.visibility = View.GONE

            binding.textviewTitle.text = getString(R.string.a_b_profil_pemohon_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionC2.buttonPreviousC2.setOnClickListener { v ->
            scrollToTop()
            binding.sectionAb.sectionAb.visibility = View.VISIBLE
            binding.sectionC2.sectionC2.visibility = View.GONE

            binding.textviewTitle.text =
                getString(R.string.a_b_profil_pemohon_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionUploadAttachment.buttonPreviousUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            if (paramPendingVerify!!.getAnsuranNo?.toIntOrNull() == 1) {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_pendahuluan_semasa)
                binding.sectionC1.sectionC1.visibility = View.VISIBLE
            } else {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_berturut_ansuran)
                binding.sectionC2.sectionC2.visibility = View.VISIBLE
            }

            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionUploadAttachmentVerify.buttonPreviousUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.textviewTitle.text = getString(R.string.attachments)
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.GONE
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionD.buttonPreviousD.setOnClickListener { v ->
            scrollToTop()
            binding.textviewTitle.text =
                getString(R.string.attachments)
            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.VISIBLE

            binding.sectionD.sectionD.visibility = View.GONE
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionF.buttonPreviousF.setOnClickListener { v ->
            scrollToTop()
            binding.sectionD.sectionD.visibility = View.VISIBLE
            binding.sectionF.sectionF.visibility = View.GONE

            binding.textviewTitle.text = getString(R.string.d_pengesahan_pegawai_penilai)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }
    }

    private fun sectionC1Mandatory(): Boolean {
        val radiogroupKerjaMengukur =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupKerjaMengukur2.checkedRadioButtonId)?.text.toString())
        if (radiogroupKerjaMengukur.isEmpty()) return false
        val spinnerJarakTanamanDanKepadatanPokokHektar = binding.sectionC1.spinnerJarakTanaman.selectedItemPosition.toString()
        if (spinnerJarakTanamanDanKepadatanPokokHektar.isEmpty()) return false
        val edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih = binding.sectionC1.edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih2.text.toString()
        if (edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih.isEmpty()) return false
        val radiogroupJenisBenih =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupJenisBenih2.checkedRadioButtonId)?.text.toString())
        if (radiogroupJenisBenih.isEmpty()) return false
        val radiogroupKelulusanBorangTs29a =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupKelulusanBorangTs29a2.checkedRadioButtonId)?.text.toString())
        if (radiogroupKelulusanBorangTs29a.isEmpty()) return false
        val radiogroupKlonBermutuTinggi =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupKlonBermutuTinggi2.checkedRadioButtonId)?.text.toString())
        if (radiogroupKlonBermutuTinggi.isEmpty()) return false
        val radiogroupResitPembelianBenih =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupResitPembelianBenih2.checkedRadioButtonId)?.text.toString())
        if (radiogroupResitPembelianBenih.isEmpty()) return false
        val radiogroupSijilPengesahan =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupSijilPengesahan2.checkedRadioButtonId)?.text.toString())
        if (radiogroupSijilPengesahan.isEmpty()) return false
        val radiogroupTapakSemaian =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupTapakSemaian2.checkedRadioButtonId)?.text.toString())
        if (radiogroupTapakSemaian.isEmpty()) return false
        val radiogroupAdakahTeres =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupAdakahTeres2.checkedRadioButtonId)?.text.toString())
        if (radiogroupAdakahTeres.isEmpty()) return false

        return true
    }

    private fun sectionC2Mandatory(): Boolean {
        val edtLuasTanamanUtamaYangBerjaya = binding.sectionC2.edtLuasTanamanUtamaYangBerjaya.text.toString()
        if(edtLuasTanamanUtamaYangBerjaya.isEmpty()) return false
        val radiogroupAdakahCantasan =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahCantasan.checkedRadioButtonId)?.text.toString())
        if (radiogroupAdakahCantasan.isEmpty()) return false
        val radiogroupAdakahMembaja =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahMembaja.checkedRadioButtonId)?.text.toString())
        if (radiogroupAdakahMembaja.isEmpty()) return false
        val radiogroupAdakahGalakan =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahGalakan.checkedRadioButtonId)?.text.toString())
        if (radiogroupAdakahGalakan.isEmpty()) return false
        val radiogroupAdakahKawalan =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahKawalan.checkedRadioButtonId)?.text.toString())
        if (radiogroupAdakahKawalan.isEmpty()) return false
        val edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang = binding.sectionC2.edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang.text.toString()
        if(edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang.isEmpty()) return false
        val edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh = binding.sectionC2.edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh.text.toString()
        if(edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh.isEmpty()) return false
        val edtGetah = binding.sectionC2.edtGetah.text.toString()
        if(edtGetah.isEmpty()) return false
        val edtTanamanLain = binding.sectionC2.edtTanamanLain.text.toString()
        if(edtTanamanLain.isEmpty()) return false
        val edtPurataUkurLilitPokokGetahYang = binding.sectionC2.edtPurataUkurLilitPokokGetahYang.text.toString()
        if(edtPurataUkurLilitPokokGetahYang.isEmpty()) return false

        return true
    }

    private fun cameraButtonListener() {
        binding.sectionUploadAttachmentVerify.cameraButton.setOnClickListener {
            if (filePaths.size >= 4) {
                // not allow to add more than 4 pictures
                return@setOnClickListener
            }
            try {
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

                // Create the File where the photo should go
                var photoFile: File? = null
                try {
                    photoFile = context?.let { it1 -> PhotoUtils.createImageFile(it1, latitude, longitude) }
                    // Save a file: path for use with ACTION_VIEW intents
                    photoFile?.let { it1 -> filePaths.add(it1.absolutePath) }
                } catch (e: IOException) {
                    Log.e(TAG, e.printStackTrace().toString())
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    photoURI = FileProvider.getUriForFile(
                        context!!,
                        getString(R.string.authority),
                        photoFile
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, Constants.REQUEST_IMAGE_CAPTURE)
                }
            } catch (e: Exception) {
                Log.e(TAG, e.printStackTrace().toString())
                binding.textviewTitle.errorSnack(
                    e.message.toString(),
                    Snackbar.LENGTH_SHORT
                )
            }
        }
    }

    override fun onSignatureCaptured(bitmap: Bitmap, fileName: String) {
        when (fileName) {
            "D" -> {
                binding.sectionD.imageViewSignD.setImageBitmap(bitmap)
            }
        }
    }

    private fun saveForm() {
        viewModel.postTS18EForm(setupRequestBodies())

        viewModel.apiResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { response ->
                            binding.sectionF.buttonSaveF.isEnabled = false
                            if(!response.data.isNullOrEmpty()) {
                                response.data?.let {
                                    Event(saveImage(it))
                                }
                            } else {
                                binding.textviewTitle.showSnack(
                                    "Submitted",
                                    Snackbar.LENGTH_SHORT
                                )
                            }
                        }
                    }

                    is Resource.NoConnection -> {
                        // todo store as offline storage
//                        showPrintAlertDialog()
                    }

                    is Resource.Error -> {
                        response.message?.let { message ->
                            binding.textviewTitle.errorSnack(
                                message,
                                Snackbar.LENGTH_SHORT
                            )
                        }
                    }
                }
            }
        })
    }

    private fun setupRequestBodies(): RequestBodies.AddTS18EForm {
        val array = JsonArray()

        if (paramPendingVerify?.getAnsuranNo?.toIntOrNull() == 1) {
            /*** Section C1 ***/
            val radiogroupKerjaMengukur =
                activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupKerjaMengukur2.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("Perimeter", radiogroupKerjaMengukur))
            val edtLuasKawasanSiapDibersihkan =
                binding.sectionC1.edtLuasKawasanSiapDibersihkan2.text.toString().toDoubleOrNull() ?: 0.0
            array.add(appendJsonObject("Tebang_Jentera1", edtLuasKawasanSiapDibersihkan))
            val spinnerJarakTanamanDanKepadatanPokokHektar =
                binding.sectionC1.spinnerJarakTanaman.selectedItemPosition.toString()
            array.add(appendJsonObject("Jarak_Tanaman", spinnerJarakTanamanDanKepadatanPokokHektar))
            val edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih =
                binding.sectionC1.edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih2.text.toString()
                    .toDoubleOrNull() ?: 0.0
            array.add(appendJsonObject("Luas_Tanam", edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih))
            val edtTarikhTanam =
                binding.sectionC1.edtTarikhTanam2.text.toString()
            array.add(appendJsonObject("Tkh_Tanam", getTimestamp(edtTarikhTanam).toString()))
            val radiogroupJenisBenih =
                activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupJenisBenih2.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("Jenis_Benih", radiogroupJenisBenih))
            val radiogroupKelulusanBorangTs29a =
                activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupKelulusanBorangTs29a2.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("Benih_Luar", radiogroupKelulusanBorangTs29a))
            val radiogroupKlonBermutuTinggi =
                activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupKlonBermutuTinggi2.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("Klon_Mutu", radiogroupKlonBermutuTinggi))
            val radiogroupResitPembelianBenih =
                activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupResitPembelianBenih2.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("Resit_Benih", radiogroupResitPembelianBenih))
            val radiogroupSijilPengesahan =
                activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupSijilPengesahan2.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("Sah_Klon", radiogroupSijilPengesahan))
            val radiogroupTapakSemaian =
                activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupTapakSemaian2.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("Tapak_Semaian", radiogroupTapakSemaian))
            val edtBenihSawitNyatakanSumber =
                binding.sectionC1.edtBenihSawitNyatakanSumber2.text.toString()
            array.add(appendJsonObject("Benih_Sawit", edtBenihSawitNyatakanSumber))
            val radiogroupAdakahTeres =
                activity?.findViewById<RadioButton>(binding.sectionC1.radiogroupAdakahTeres2.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("GAP_Senggara", radiogroupAdakahTeres))
            val edtLainLainKenyataan =
                binding.sectionC1.edtLainLainKenyataan2.text.toString()
            array.add(appendJsonObject("Kenyataan_Lain", edtLainLainKenyataan))

        } else {
            /*** Section C2 ***/
            val edtLuasTanamanUtamaYangBerjaya =
                binding.sectionC2.edtLuasTanamanUtamaYangBerjaya.text.toString().toDoubleOrNull() ?: 0.0
            array.add(appendJsonObject("LuasBerjaya", edtLuasTanamanUtamaYangBerjaya))
            val radiogroupAdakahMembaja =
                activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahMembaja.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("GAP_Baja", radiogroupAdakahMembaja))
            val radiogroupAdakahCantasan =
                activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahCantasan.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("GAP_Cantasan", radiogroupAdakahCantasan))
            val radiogroupJikaYa =
                activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupJikaYa.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("Jenis_Cantasan", radiogroupJikaYa))
            val radiogroupAdakahGalakan =
                activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahGalakan.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("GAP_GalakDahan", radiogroupAdakahGalakan))
            val radiogroupJikaYaNyatakan =
                activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupJikaYaNyatakan.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("Jenis_GalakDahan", radiogroupJikaYaNyatakan))
            val radiogroupAdakahKawalan =
                activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahKawalan.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("Kawal_Perosak", radiogroupAdakahKawalan))
            val edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang =
                binding.sectionC2.edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang.text.toString()
                    .toDoubleOrNull() ?: 0.0
            array.add(appendJsonObject("Rumpai_Senggara", edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang))
            val edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh =
                binding.sectionC2.edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh.text.toString()
                    .toDoubleOrNull() ?: 0.0
            array.add(appendJsonObject("Rumpai_TdkSenggara", edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh))
            val radiogroupAdakahTeresPelantar =
                activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahTeresPelantar.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("GAP_Senggara", radiogroupAdakahTeresPelantar))
            val radiogroupAdakahKpb =
                activity?.findViewById<RadioButton>(binding.sectionC2.radiogroupAdakahKpb.checkedRadioButtonId)?.text.toString()
            array.add(appendJsonObject("KPB_Kontan", radiogroupAdakahKpb))
            val edtJikaYaLuasYangDiselenggara =
                binding.sectionC2.edtJikaYaLuasYangDiselenggara.text.toString().toDoubleOrNull() ?: 0.0
            array.add(appendJsonObject("Luas_Kontan", edtJikaYaLuasYangDiselenggara))

            val edtGetah =
                binding.sectionC2.edtGetah.text.toString().toIntOrNull() ?: 0
            array.add(appendJsonObject("Getah_PkkHek", edtGetah))
            val edtTanamanLain =
                binding.sectionC2.edtTanamanLain.text.toString().toIntOrNull() ?: 0
            array.add(appendJsonObject("TL_PkkHidup", edtTanamanLain))
            val edtPurataUkurLilitPokokGetahYang =
                binding.sectionC2.edtPurataUkurLilitPokokGetahYang.text.toString().toDoubleOrNull()
                    ?: 0.0
            array.add(appendJsonObject("Ukur_Lilit", edtPurataUkurLilitPokokGetahYang))
            val edtLainLainKenyataanJikaAda =
                binding.sectionC2.edtLainLainKenyataanJikaAda.text.toString()
            array.add(appendJsonObject("Kenyataan_Lain", edtLainLainKenyataanJikaAda))
        }

        /*** Section D ***/
        val edtTarikhLawatan = binding.sectionD.edtTarikhLawatan.text.toString()
        val edtMasaDari = binding.sectionD.edtMasaDari2.text.toString()
        val edtMasaSampai = binding.sectionD.edtMasaSampai2.text.toString()
        val signPegawai = DisplayUtils.getBase64String(binding.sectionD.imageViewSignD)
        val edtCatatan2 = binding.sectionD.edtCatatan2.text.toString()

        // todo save form to local phone
        val sessionManager = context?.let { SessionManager(it) }!!
        return RequestBodies.AddTS18EForm(
            jenisLE = Constants.Form.TS18A.type,
            noSiriLawatan = paramPendingVerify?.noSiriLB!!,
            ptCode = paramPendingVerify?.ptCode!!.trim(),
            tsMohonId = paramPendingVerify?.tsMohonId!!.trim(),
            tkhLawat = TimeUtils.getTimestamp(edtTarikhLawatan),
            mlawatMula = TimeUtils.getMasaTimestamp(edtMasaDari),
            mlawatTamat = TimeUtils.getMasaTimestamp(edtMasaSampai),
            catatan = edtCatatan2,
            pegLulus = sessionManager.fetchUsername(),
            signPegawai = signPegawai,
            items = Utils.getGson()?.toJsonTree(array.filter { !it.isJsonNull })!!.asJsonArray
        )
    }

    private fun appendJsonObject(key: String, value: Any): JsonObject? {
        if(value.toString().isNullOrEmpty() || value.toString() == "null") {
            return null
        }

        val jsonObject = JsonObject()
        jsonObject.addProperty(Constants.ITEM, key)
        jsonObject.addProperty(Constants.CATATAN, value.toString())

        return jsonObject
    }

    private fun scrollToTop() {
        binding.scrollview.fullScroll(View.FOCUS_UP)
        binding.scrollview.smoothScrollTo(0, 0)
    }

    private fun saveImage(noSeri: String? = null) {
        if (filePaths.size == 0) {
            return
        }

        filePaths.forEachIndexed{ index, it ->
            val imageFile = File(it) // Create a file using the absolute path of the image
            val reqBody: RequestBody =
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), imageFile)
            val latitude = Utils.getLatitude(activity!!).toString().replace('.',',')
            val longitude = Utils.getLongitude(activity!!).toString().replace('.',',')
            val pathImage: MultipartBody.Part =
                MultipartBody.Part.createFormData("file", "image${index}_${latitude}_${longitude}." + imageFile.extension, reqBody)
            viewModel.uploadFile(
                noSeri!!,
                pathImage
            )
        }

        viewModel.apiImageResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { response ->
                            response.message?.let {
                                binding.textviewTitle.showSnack(
                                    "Submitted",
                                    Snackbar.LENGTH_SHORT
                                )
                            }
                        }
                    }

                    is Resource.NoConnection -> {
                        // todo store as offline storage
                    }

                    is Resource.Error -> {
                        response.message?.let { message ->
                            binding.textviewTitle.errorSnack(
                                message,
                                Snackbar.LENGTH_SHORT
                            )
                        }
                    }
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.REQUEST_IMAGE_CAPTURE) {
            if (resultCode == Activity.RESULT_OK) {

                try {
                    val bitmap =
                        MediaStore.Images.Media.getBitmap(context!!.contentResolver, photoURI);

                    val inflater =
                        activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

                    val attachmentView: View = inflater.inflate(R.layout.item_camera, null)
                    attachmentView.id = filePaths.size

                    val imageView =
                        attachmentView.findViewById<ImageView>(R.id.image_view_attachment)
                    imageView.setImageBitmap(bitmap)
                    val deleteButton = attachmentView.findViewById<Button>(R.id.delete_button)
                    deleteButton.tag = filePaths.size
                    deleteButton.setOnClickListener {
                        try {
                            binding.sectionUploadAttachmentVerify.linearLayoutAttachment.removeViewAt(it.tag as Int - 1)
                            filePaths.removeAt(it.tag as Int - 1)
                            imageView.setImageBitmap(null)
                        } catch (e: Exception) {
                            Log.e(TAG, e.message.toString())
                        }
                    }
                    binding.sectionUploadAttachmentVerify.linearLayoutAttachment.addView(attachmentView)

                } catch (e: IOException) {
                    e.printStackTrace();
                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
                binding.sectionUploadAttachmentVerify.cameraButton.errorSnack(
                    getString(R.string.cancelled),
                    Snackbar.LENGTH_SHORT
                )
            }
        }
    }

    companion object {
        val TAG = Ts18aEFragment::class.java.simpleName!!

        @JvmStatic
        fun newInstance(
            paramPendingVerify: PendingVerify,
            paramTsApplication: TsApplication
        ) =
            Ts18aEFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM_PENDING_VERIFY, paramPendingVerify)
                    putParcelable(ARG_PARAM_TS_APPLICATION, paramTsApplication)
                }
            }

    }
}