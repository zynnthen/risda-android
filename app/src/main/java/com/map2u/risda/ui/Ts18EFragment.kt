package com.map2u.risda.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.map2u.risda.BuildConfig
import com.map2u.risda.R
import com.map2u.risda.databinding.FragmentTs18EBinding
import com.map2u.risda.model.appointment.PendingVerify
import com.map2u.risda.network.RequestBodies
import com.map2u.risda.network.SessionManager
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.ui.shared.OnSignedCaptureListener
import com.map2u.risda.ui.shared.SignatureDialogFragment
import com.map2u.risda.util.*
import com.map2u.risda.util.TimeUtils.getMasaTimestamp
import com.map2u.risda.util.TimeUtils.getTimestamp
import com.map2u.risda.util.display.DisplayUtils
import com.map2u.risda.viewmodel.Ts18EViewModel
import com.map2u.risda.viewmodel.ViewModelProviderFactory
import kotlinx.coroutines.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Use the [Ts18EFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Ts18EFragment : Fragment(), OnSignedCaptureListener {
    val ARG_PARAM_PENDING_VERIFY = "ARG_PARAM_PENDING_VERIFY"
    var paramPendingVerify: PendingVerify? = null

    private var _binding: FragmentTs18EBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private lateinit var viewModel: Ts18EViewModel

    // This property is for image upload
    private val filePaths: ArrayList<String> = ArrayList()

    lateinit var photoURI: Uri

    // This property is to get location
    var longitude: Double = 0.0
    var latitude: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            paramPendingVerify = it.getParcelable(ARG_PARAM_PENDING_VERIFY)
        }

        latitude = Utils.getLatitude(activity!!)
        longitude = Utils.getLongitude(activity!!)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTs18EBinding.inflate(inflater, container, false)
        val view = binding.root

        setupViewModel()

        /*** Disable edit text, radio button ***/
        val ll: LinearLayout = binding.rootLinearLayout
        for (view in ll!!.touchables) {
            if (view is EditText) {
                val editText = view
                editText.setTextColor(context!!.getColor(R.color.colorPrimaryText))
                val editTextIdName = editText.resources.getResourceName(editText.id)
                if (!editTextIdName.contains("_2")) {
                    editText.isEnabled = false
                    editText.isFocusable = false
                    editText.isFocusableInTouchMode = false
                    editText.background = null
                }
            } else if (view is RadioButton) {
                val radioButton = view
                val radioButtonIdName = radioButton.resources.getResourceName(radioButton.id)
                if (!radioButtonIdName.contains("_2")) {
                    radioButton.isEnabled = false
                    radioButton.buttonTintList =
                        ColorStateList.valueOf(context!!.getColor(R.color.colorPrimaryDark))
                    radioButton.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.md_grey_300)))
                }
            }
        }

        binding.sectionC.sectionC.visibility = View.GONE
        binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
        binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.GONE
        binding.sectionD.sectionD.visibility = View.GONE
        binding.sectionF.sectionF.visibility = View.GONE

        binding.seekBar.setOnTouchListener(View.OnTouchListener { v, event ->
            // True doesn't propagate the event
            // the user cannot move the indicator
            true
        })

        /*** Next button listener ***/
        nextButtonListener()

        /*** Previous button listener ***/
        previousButtonListener()

        /*** Camera button listener ***/
        cameraButtonListener()

        initAbContent()
        initCContent()
        initAttachmentsContent()
        initDContent()
        initFContent()

        /*** Section C if not economical radio button listener ***/
        binding.sectionC.rbKeadaanPokokTidakEkonomik2.setOnCheckedChangeListener { v, isChecked ->
            for (i in 0 until binding.sectionC.radiogroupHasil5002.getChildCount()) {
                binding.sectionC.radiogroupHasil5002.getChildAt(i).isEnabled = isChecked
                binding.sectionC.radiogroupHasil5002.clearCheck()
            }
            for (i in 0 until binding.sectionC.radiogroupHabisKulit2.getChildCount()) {
                binding.sectionC.radiogroupHabisKulit2.getChildAt(i).isEnabled = isChecked
                binding.sectionC.radiogroupHabisKulit2.clearCheck()
            }
            for (i in 0 until binding.sectionC.radiogroupPokokGetah2.getChildCount()) {
                binding.sectionC.radiogroupPokokGetah2.getChildAt(i).isEnabled = isChecked
                binding.sectionC.radiogroupPokokGetah2.clearCheck()
            }
            for (i in 0 until binding.sectionC.radiogroupLainLainSebab2.getChildCount()) {
                binding.sectionC.radiogroupLainLainSebab2.getChildAt(i).isEnabled = isChecked
                binding.sectionC.radiogroupLainLainSebab2.clearCheck()
            }

            if (isChecked) {
                binding.sectionC.tvHasil500.text = Html.fromHtml(
                    "<font color='#FF5733'>*</font> i) Hasil &lt;500kg/hek/thn Bilangan kurang 300 pokok/hektar",
                    Html.FROM_HTML_MODE_COMPACT
                )
                binding.sectionC.tvHabisKulitTapakTorehan.text = Html.fromHtml(
                    "<font color='#FF5733'>*</font> " + getString(R.string.habis_kulit_tapak_torehan),
                    Html.FROM_HTML_MODE_COMPACT
                )
                binding.sectionC.tvPokokGetahBertoreh.text = Html.fromHtml(
                    "<font color='#FF5733'>*</font> " + getString(R.string.pokok_getah_bertoreh),
                    Html.FROM_HTML_MODE_COMPACT
                )
                binding.sectionC.tvLainLain.text = Html.fromHtml(
                    "<font color='#FF5733'>*</font> " + getString(R.string.lain_lain),
                    Html.FROM_HTML_MODE_COMPACT
                )
            } else {
                binding.sectionC.tvHasil500.text = getString(R.string.hasil_500)
                binding.sectionC.tvHabisKulitTapakTorehan.text =
                    getString(R.string.habis_kulit_tapak_torehan)
                binding.sectionC.tvPokokGetahBertoreh.text =
                    getString(R.string.pokok_getah_bertoreh)
                binding.sectionC.tvLainLain.text = getString(R.string.lain_lain)
            }
        }

        binding.sectionC.checkboxMengemaskini2.setOnCheckedChangeListener { v, isChecked ->
            if (isChecked) {
                binding.sectionC.edtTanamanLain2.isEnabled = true
                binding.sectionC.edtHutanTanahKosong2.isEnabled = true
                binding.sectionC.edtRumahBangunan2.isEnabled = true
                binding.sectionC.edtLainLainSilaNyatakan2.isEnabled = true
                binding.sectionC.edtLainLainSilaNyatakanHek2.isEnabled = true

                val launchIntent =
                    context?.packageManager?.getLaunchIntentForPackage("com.esri.fieldmaps")
                if (launchIntent != null) {
                    startActivity(launchIntent)
                } else {
                    binding.textviewTitle.errorSnack(
                        getString(R.string.no_argis_app),
                        Snackbar.LENGTH_LONG
                    )
                }
            }
        }

        /*** Save form button listener ***/
        binding.sectionF.buttonSaveF.setOnClickListener { v ->
            if (!binding.sectionF.checkboxSetuju.isChecked) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_not_checked),
                    Snackbar.LENGTH_LONG
                )
                return@setOnClickListener
            }

            saveForm()
        }

        return view
    }

    private fun setupViewModel() {
        val repository = AppRepository()
        val factory = activity?.let { ViewModelProviderFactory(it.application, repository) }
        viewModel = factory?.let { ViewModelProvider(this, it).get(Ts18EViewModel::class.java) }!!
    }

    private fun initAbContent() {
        val address =
            "${paramPendingVerify?.tsApplication?.address1} ${paramPendingVerify?.tsApplication?.address2} ${paramPendingVerify?.tsApplication?.address3}"
        binding.sectionAb.edtNama.setText(paramPendingVerify?.tsApplication?.namaPemohon)
        binding.sectionAb.edtNoKpPolisTenteraSyarikat.setText(paramPendingVerify?.tsApplication?.icNoPemohon)
        binding.sectionAb.edtAlamat.setText(address)
        binding.sectionAb.edtPoskod.setText(paramPendingVerify?.tsApplication?.poskod)
        binding.sectionAb.edtNoTelRumah.setText(paramPendingVerify?.tsApplication?.noPhoneHouse)
        binding.sectionAb.edtNoTelBimbit.setText(paramPendingVerify?.tsApplication?.noPhoneMobile)
        binding.sectionAb.edtNoPermohonan.setText(paramPendingVerify?.tsMohonId)

        binding.sectionAb.edtDaerah.setText(paramPendingVerify?.tsApplication?.daerah)
        binding.sectionAb.edtParlimen.setText(paramPendingVerify?.tsApplication?.parlimen)
        binding.sectionAb.edtDun.setText(paramPendingVerify?.tsApplication?.dun)
        binding.sectionAb.edtMukim.setText(paramPendingVerify?.tsApplication?.mukim)
        binding.sectionAb.edtKampung.setText(paramPendingVerify?.tsApplication?.kampung)
        binding.sectionAb.edtNoGeran.setText(paramPendingVerify?.tsApplication?.lotDimilikiList?.get(0)?.noGeran)
        binding.sectionAb.edtNoLot.setText(paramPendingVerify?.tsApplication?.lotDimilikiList?.get(0)?.noLot)
        binding.sectionAb.edtLuasKebunHektar.setText(
            paramPendingVerify?.tsApplication?.lotDimilikiList?.get(
                0
            )?.luasLot.toString()
        )
        binding.sectionAb.edtLuasKebunHendakDitanamSemulaHektar.setText(
            paramPendingVerify?.tsApplication?.lotDimilikiList?.get(
                0
            )?.luasTs.toString()
        )

        if (paramPendingVerify != null) {
            binding.sectionAb.radiogroupMenerimaBantuan.visibility = View.GONE
            binding.sectionAb.textviewMenerimaBantuan.visibility = View.VISIBLE
            if (paramPendingVerify!!.bantuanLain?.trim() == "Ya") {
                binding.sectionAb.textviewMenerimaBantuan.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.bantuanLain?.trim() == "Tidak") {
                binding.sectionAb.textviewMenerimaBantuan.text = getString(R.string.tidak)
            }

            // jika ya, nyatakan
            binding.sectionAb.edtNamaAgensiPemberiBantuan.setText(
                paramPendingVerify!!.blAgensi
            )
            binding.sectionAb.edtLuasDilulus.setText(paramPendingVerify!!.blLuasLulus.toString())
            binding.sectionAb.edtJenisTanaman.setText(paramPendingVerify!!.blCrop)
        }
    }

    private fun initCContent() {
        if (paramPendingVerify != null) {
            binding.sectionC.textviewKeluasanTanamanGetahTuaSekarang.text = paramPendingVerify!!.luasGetahTua.toString()
            binding.sectionC.textviewTanamanLain.text = paramPendingVerify!!.luasTanamanLain.toString()
            binding.sectionC.textviewHutanTanahKosong.text = paramPendingVerify!!.luasHutan.toString()
            binding.sectionC.textviewRumahBangunan.text = paramPendingVerify!!.luasRumah.toString()
            binding.sectionC.textviewLainLainSilaNyatakan.text = paramPendingVerify!!.lainLain.toString()
            binding.sectionC.textviewLainLainLuasHektar.text = paramPendingVerify!!.luasLainLain.toString()

            if (paramPendingVerify!!.tunggulGetah?.trim() == "Ya") {
                binding.sectionC.textviewTunggulGetahTua.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.tunggulGetah?.trim() == "Tidak") {
                binding.sectionC.textviewTunggulGetahTua.text = getString(R.string.tidak)
            }

            if (paramPendingVerify!!.bukuLesen?.trim() == "Ya") {
                binding.sectionC.textviewBukuLesenGetah.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.bukuLesen?.trim() == "Tidak") {
                binding.sectionC.textviewBukuLesenGetah.text = getString(R.string.tidak)
            }

            if (paramPendingVerify!!.gambarSatelit?.trim() == "Ya") {
                binding.sectionC.textviewGambarSatelit.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.gambarSatelit?.trim() == "Tidak") {
                binding.sectionC.textviewGambarSatelit.text = getString(R.string.tidak)
            }

            if (paramPendingVerify!!.lesenGetah?.trim() == "Ya") {
                binding.sectionC.textviewLesenGetah.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.lesenGetah?.trim() == "Tidak") {
                binding.sectionC.textviewLesenGetah.text = getString(R.string.tidak)
            }

            if (paramPendingVerify!!.suratAgensi?.trim() == "Ya") {
                binding.sectionC.textviewSuratPengesahan.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.lesenGetah?.trim() == "Tidak") {
                binding.sectionC.textviewSuratPengesahan.text = getString(R.string.tidak)
            }

            if (paramPendingVerify!!.buktiLotTs?.trim() == "Ya") {
                binding.sectionC.textviewRekodYangMembukitkan.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.buktiLotTs?.trim() == "Tidak") {
                binding.sectionC.textviewRekodYangMembukitkan.text = getString(R.string.tidak)
            }

            if (paramPendingVerify!!.buktiLot?.trim() == "Ya") {
                binding.sectionC.textviewSilaSertakan.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.buktiLot?.trim() == "Tidak") {
                binding.sectionC.textviewSilaSertakan.text = getString(R.string.tidak)
            }

            if (paramPendingVerify!!.umurPokok?.trim() == "<20tahun") {
                binding.sectionC.textviewUmurPokok.text = getString(R.string.less_20)
            } else if (paramPendingVerify!!.umurPokok?.trim() == ">=20tahun") {
                binding.sectionC.textviewUmurPokok.text = getString(R.string.more_20)
            }

            if (paramPendingVerify!!.getahTua?.trim() == "Ekonomik") {
                binding.sectionC.textviewKeadaanPokok.text = getString(R.string.ekonomik)
            } else if (paramPendingVerify!!.getahTua?.trim() == "Tidak ekonomik") {
                binding.sectionC.textviewKeadaanPokok.text = getString(R.string.tidak_ekonomik)

                if (paramPendingVerify!!.pokokK300?.trim() == "Ya") {
                    binding.sectionC.textviewHasil500.text = getString(R.string.ya)
                } else if (paramPendingVerify!!.pokokK300?.trim() == "Tidak") {
                    binding.sectionC.textviewHasil500.text = getString(R.string.tidak)
                }

                if (paramPendingVerify!!.kulitTorehan?.trim() == "Ya") {
                    binding.sectionC.textviewHabisKulit.text = getString(R.string.ya)
                } else if (paramPendingVerify!!.kulitTorehan?.trim() == "Tidak") {
                    binding.sectionC.textviewHabisKulit.text = getString(R.string.tidak)
                }

                if (paramPendingVerify!!.hasilK500?.trim() == "Ya") {
                    binding.sectionC.textviewPokokGetah.text = getString(R.string.ya)
                } else if (paramPendingVerify!!.hasilK500?.trim() == "Tidak") {
                    binding.sectionC.textviewPokokGetah.text = getString(R.string.tidak)
                }

                if (paramPendingVerify!!.sebabLainPRN?.trim() == "Ya") {
                    binding.sectionC.textviewLainLainSebab.text = getString(R.string.ya)
                } else if (paramPendingVerify!!.sebabLainPRN?.trim() == "Tidak") {
                    binding.sectionC.textviewLainLainSebab.text = getString(R.string.tidak)
                }
            }
        }
    }

    private fun initAttachmentsContent() {
        binding.sectionUploadAttachment.cameraButton.visibility = View.GONE
        binding.sectionUploadAttachment.textviewMandatory.visibility = View.GONE

        GlobalScope.launch {
            callApiAndUpdateLiveData()
        }
    }

    private suspend fun callApiAndUpdateLiveData() {
        paramPendingVerify?.documents?.forEach {
            coroutineScope {
                async {
                    val result = it.id?.let { file -> viewModel.getFile(file) }
                }
            }.await()
        }

        var index = 0

        lifecycleScope.launch {
            withContext(Dispatchers.Main) {
                viewModel.apiFileResponse.observe(this@Ts18EFragment, { event ->
                    event.getContentIfNotHandled()?.let { response ->
                        when (response) {
                            is Resource.Success -> {
                                response.data?.let { response ->
                                    val bitmap =
                                        BitmapFactory.decodeStream(response.byteStream())

                                    val inflater =
                                        activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

                                    val attachmentView: View =
                                        inflater.inflate(R.layout.item_camera, null)
                                    val imageView =
                                        attachmentView.findViewById<ImageView>(R.id.image_view_attachment)
                                    imageView.setImageBitmap(bitmap)
                                    val imageName = attachmentView.findViewById<TextView>(R.id.tv_image_name)
                                    imageName.text = paramPendingVerify?.documents?.get(index)?.docName
                                    index++
                                    val deleteButton =
                                        attachmentView.findViewById<Button>(R.id.delete_button)
                                    deleteButton.visibility = View.GONE

                                    binding.sectionUploadAttachment.linearLayoutAttachment.addView(
                                        attachmentView
                                    )
                                }
                            }
                        }
                    }
                })
            }
        }
    }

    private fun initDContent() {
        /*** Section D default date ***/
        binding.sectionD.edtTarikhLawatan.setText(TimeUtils.displayTodayDate())

        /*** Time input picker ***/
        // This property is for Time picker usage
        val myCalendar: Calendar = Calendar.getInstance()
        val timeDari =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                myCalendar.set(Calendar.MINUTE, minute)

                val sdf = SimpleDateFormat("hh:mm a")
                binding.sectionD.edtMasaDari2.setText(sdf.format(myCalendar.getTime()))
            }
        binding.sectionD.edtMasaDari2.setOnClickListener { v ->
            TimePickerDialog(
                context!!,
                timeDari,
                myCalendar[Calendar.HOUR_OF_DAY],
                myCalendar[Calendar.MINUTE],
                false
            ).show()
        }

        val timeSampai =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                myCalendar.set(Calendar.MINUTE, minute)

                val sdf = SimpleDateFormat("hh:mm a")
                binding.sectionD.edtMasaSampai2.setText(sdf.format(myCalendar.getTime()))
            }
        binding.sectionD.edtMasaSampai2.setOnClickListener { v ->
            TimePickerDialog(
                context!!,
                timeSampai,
                myCalendar[Calendar.HOUR_OF_DAY],
                myCalendar[Calendar.MINUTE],
                false
            ).show()
        }

        /*** Sign listener ***/
        binding.sectionD.buttonSignHereD.setOnClickListener { v ->
            val dialogFragment = SignatureDialogFragment(this, "D")
            dialogFragment.show(activity!!.supportFragmentManager, "signature")
        }
    }

    private fun initFContent() {
        binding.sectionF.buttonNextF.visibility = View.GONE
        binding.sectionF.buttonSaveF.visibility = View.VISIBLE
    }

    private fun nextButtonListener() {
        binding.sectionAb.buttonAbNextAb.setOnClickListener { v ->
            scrollToTop()
            binding.sectionAb.sectionAb.visibility = View.GONE
            binding.sectionC.sectionC.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.c_laporan_pengesahan_lawatan_kebun)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionC.buttonNextC.setOnClickListener { v ->
            val lotTanamSemula = paramPendingVerify?.tsApplication?.lotDimilikiList?.get(
                0
            )?.luasTs

            val editTanamanGetahTua =
                binding.sectionC.edtKeluasanTanamanGetahTuaSekarang2.text.toString().toDoubleOrNull()
                    ?: 0.0

            val edtTanamanLain =
                binding.sectionC.edtTanamanLain2.text.toString().toDoubleOrNull() ?: 0.0
            val edtHutanTanahKosong =
                binding.sectionC.edtHutanTanahKosong2.text.toString().toDoubleOrNull() ?: 0.0
            val edtRumahBangunan =
                binding.sectionC.edtRumahBangunan2.text.toString().toDoubleOrNull() ?: 0.0
            val edtLainLainSilaNyatakanHek =
                binding.sectionC.edtLainLainSilaNyatakanHek2.text.toString().toDoubleOrNull() ?: 0.0
            val totalTanamanLain =
                edtTanamanLain + edtHutanTanahKosong + edtRumahBangunan + edtLainLainSilaNyatakanHek

            if (lotTanamSemula != null
                && editTanamanGetahTua > lotTanamSemula
            ) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_luas_hektar_sekarang),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            if (lotTanamSemula != null
                && totalTanamanLain > lotTanamSemula
            ) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_luas_hektar_lain_lain),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            if (edtTanamanLain < 0.0 || edtHutanTanahKosong < 0.0
                || edtRumahBangunan < 0.0 || edtLainLainSilaNyatakanHek < 0.0
            ) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_hektar_negative),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            if (!sectionCMandatoryField()) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.mandatory),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionC.sectionC.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionUploadAttachment.buttonNextUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionUploadAttachmentVerify.buttonNextUploadAttachment.setOnClickListener { v ->
            if (filePaths.size < 2 && !BuildConfig.DEBUG) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.attachements_desc),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.GONE
            binding.sectionD.sectionD.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.d_pengesahan_pegawai_penilai)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionD.buttonNextD.setOnClickListener { v ->
            val edtTarikhLawatan = binding.sectionD.edtTarikhLawatan.text.toString()
            val edtMasaDari = binding.sectionD.edtMasaDari2.text.toString()
            val edtMasaSampai = binding.sectionD.edtMasaSampai2.text.toString()
            val signPegawai = DisplayUtils.getBase64String(binding.sectionD.imageViewSignD)
            val edtCatatan2 = binding.sectionD.edtCatatan2.text.toString()

            if (edtTarikhLawatan.isEmpty() || edtMasaDari.isEmpty() || edtMasaSampai.isEmpty() || signPegawai.isEmpty() || edtCatatan2.isEmpty()) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_not_filled),
                    Snackbar.LENGTH_LONG
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionD.sectionD.visibility = View.GONE
            binding.sectionF.sectionF.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.f_peringatan)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

    }

    private fun previousButtonListener() {
        binding.sectionC.buttonPreviousC.setOnClickListener { v ->
            scrollToTop()
            binding.sectionC.sectionC.visibility = View.GONE
            binding.sectionAb.sectionAb.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.a_b_profil_pemohon_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionUploadAttachment.buttonPreviousUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.sectionC.sectionC.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.c_laporan_pengesahan_lawatan_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionUploadAttachmentVerify.buttonPreviousUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionD.buttonPreviousD.setOnClickListener { v ->
            scrollToTop()
            binding.sectionD.sectionD.visibility = View.GONE
            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionF.buttonPreviousF.setOnClickListener { v ->
            scrollToTop()
            binding.sectionF.sectionF.visibility = View.GONE
            binding.sectionD.sectionD.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.d_pengesahan_pegawai_penilai)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

    }

    private fun sectionCMandatoryField(): Boolean {

        val radiogroupTunggulGetahTua =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupTunggulGetahTua2.checkedRadioButtonId)?.text.toString())
        if (radiogroupTunggulGetahTua.isEmpty()) return false
        val radiogroupBukuLesenGetah =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupBukuLesenGetah2.checkedRadioButtonId)?.text.toString())
        if (radiogroupBukuLesenGetah.isEmpty()) return false
        val radiogroupGambarSatelit =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupGambarSatelit2.checkedRadioButtonId)?.text.toString())
        if (radiogroupGambarSatelit.isEmpty()) return false
        val radiogroupLesenGetah =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupLesenGetah2.checkedRadioButtonId)?.text.toString())
        if (radiogroupLesenGetah.isEmpty()) return false
        val radiogroupSuratPengesahan =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupSuratPengesahan2.checkedRadioButtonId)?.text.toString())
        if (radiogroupSuratPengesahan.isEmpty()) return false
        val radiogroupRekodYangMembukitkan =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupRekodYangMembukitkan2.checkedRadioButtonId)?.text.toString())
        if (radiogroupRekodYangMembukitkan.isEmpty()) return false
        val radiogroupSilaSertakan =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupSilaSertakan2.checkedRadioButtonId)?.text.toString())
        if (radiogroupSilaSertakan.isEmpty()) return false
        val radiogroupUmurPokok =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupUmurPokok2.checkedRadioButtonId)?.text.toString())
        if (radiogroupUmurPokok.isEmpty()) return false
        val radiogroupKeadaanPokok =
            StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupKeadaanPokok2.checkedRadioButtonId)?.text.toString())
        if (radiogroupKeadaanPokok.isEmpty()) return false

        if (binding.sectionC.rbKeadaanPokokTidakEkonomik2.isChecked) {
            val radiogroupHasil500 =
                StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupHasil5002.checkedRadioButtonId)?.text.toString())
            if (radiogroupHasil500.isEmpty()) return false
            val radiogroupHabisKulit =
                StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupHabisKulit2.checkedRadioButtonId)?.text.toString())
            if (radiogroupHabisKulit.isEmpty()) return false
            val radiogroupPokokGetah =
                StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupPokokGetah2.checkedRadioButtonId)?.text.toString())
            if (radiogroupPokokGetah.isEmpty()) return false
            val radiogroupLainLainSebab =
                StringUtils.replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupLainLainSebab2.checkedRadioButtonId)?.text.toString())
            if (radiogroupLainLainSebab.isEmpty()) return false
        }

        if(!binding.sectionC.checkboxMengemaskini2.isChecked) return false

        val edtKeluasanTanamanGetahTuaSekarang = binding.sectionC.edtKeluasanTanamanGetahTuaSekarang2.text.toString()
        if (edtKeluasanTanamanGetahTuaSekarang.isEmpty()) {
            return false
        }

        return true
    }

    private fun cameraButtonListener() {
        binding.sectionUploadAttachmentVerify.cameraButton.setOnClickListener {
            if (filePaths.size >= 4) {
                // not allow to add more than 4 pictures
                return@setOnClickListener
            }
            try {
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

                // Create the File where the photo should go
                var photoFile: File? = null
                try {
                    photoFile = context?.let { it1 -> PhotoUtils.createImageFile(it1, latitude, longitude) }
                    // Save a file: path for use with ACTION_VIEW intents
                    photoFile?.let { it1 -> filePaths.add(it1.absolutePath) }
                } catch (e: IOException) {
                    Log.e(TAG, e.printStackTrace().toString())
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    photoURI = FileProvider.getUriForFile(
                        context!!,
                        getString(R.string.authority),
                        photoFile
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, Constants.REQUEST_IMAGE_CAPTURE)
                }
            } catch (e: Exception) {
                Log.e(TAG, e.printStackTrace().toString())
                binding.textviewTitle.errorSnack(
                    e.message.toString(),
                    Snackbar.LENGTH_SHORT
                )
            }
        }
    }

    override fun onSignatureCaptured(bitmap: Bitmap, fileName: String) {
        when (fileName) {
            "D" -> {
                binding.sectionD.imageViewSignD.setImageBitmap(bitmap)
            }
        }
    }

    private fun saveForm() {
        viewModel.postTS18EForm(setupRequestBodies())

        viewModel.apiResponse.observe(this, { event ->
            event.getContentIfNotHandled().let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { response ->
                            binding.sectionF.buttonSaveF.isEnabled = false
                            if(!response.data.isNullOrEmpty()) {
                                response.data?.let {
                                    Event(saveImage(it))
                                }
                            } else {
                                binding.textviewTitle.showSnack(
                                    "Submitted",
                                    Snackbar.LENGTH_SHORT
                                )
                            }
                        }
                    }

                    is Resource.NoConnection -> {
                        // todo store as offline storage
//                        showPrintAlertDialog()
                    }

                    is Resource.Error -> {
                        response.message?.let { message ->
                            binding.textviewTitle.errorSnack(
                                message,
                                Snackbar.LENGTH_SHORT
                            )
                        }
                    }
                }
            }
        })
    }

    private fun setupRequestBodies(): RequestBodies.AddTS18EForm {
        /*** Section B ***/
        val radiogroupMenerimaBantuan =
            activity?.findViewById<RadioButton>(binding.sectionAb.radiogroupMenerimaBantuan.checkedRadioButtonId)?.text.toString()
        val edtNamaAgensiPemberiBantuan =
            binding.sectionAb.edtNamaAgensiPemberiBantuan.text.toString()
        val edtLuasDilulus = binding.sectionAb.edtLuasDilulus.text.toString().toDoubleOrNull()
            ?: 0.0
        val edtJenisTanaman = binding.sectionAb.edtJenisTanaman.text.toString()

        /*** Section C ***/
        val array = JsonArray()

        val edtKeluasanTanamanGetahTuaSekarang =
            binding.sectionC.edtKeluasanTanamanGetahTuaSekarang2.text.toString().toDoubleOrNull()
                ?: 0.0
        array.add(appendJsonObject("Luas_GetahTua", edtKeluasanTanamanGetahTuaSekarang))
        val radiogroupTunggulGetahTua =
            activity?.findViewById<RadioButton>(binding.sectionC.radiogroupTunggulGetahTua2.checkedRadioButtonId)?.text.toString()
        array.add(appendJsonObject("Tunggul_Getah", radiogroupTunggulGetahTua))
        val radiogroupBukuLesenGetah =
            activity?.findViewById<RadioButton>(binding.sectionC.radiogroupBukuLesenGetah2.checkedRadioButtonId)?.text.toString()
        array.add(appendJsonObject("Buku_Lesen", radiogroupBukuLesenGetah))
        val radiogroupGambarSatelit =
            activity?.findViewById<RadioButton>(binding.sectionC.radiogroupGambarSatelit2.checkedRadioButtonId)?.text.toString()
        array.add(appendJsonObject("Gambar_Satelit", radiogroupGambarSatelit))
        val radiogroupLesenGetah =
            activity?.findViewById<RadioButton>(binding.sectionC.radiogroupLesenGetah2.checkedRadioButtonId)?.text.toString()
        array.add(appendJsonObject("Lesen_Getah", radiogroupLesenGetah))
        val radiogroupSuratPengesahan =
            activity?.findViewById<RadioButton>(binding.sectionC.radiogroupSuratPengesahan2.checkedRadioButtonId)?.text.toString()
        array.add(appendJsonObject("Surat_Agensi", radiogroupSuratPengesahan))
        val radiogroupRekodYangMembukitkan =
            activity?.findViewById<RadioButton>(binding.sectionC.radiogroupRekodYangMembukitkan2.checkedRadioButtonId)?.text.toString()
        array.add(appendJsonObject("Bukti_Lot_TS", radiogroupRekodYangMembukitkan))
        val radiogroupSilaSertakan =
            activity?.findViewById<RadioButton>(binding.sectionC.radiogroupSilaSertakan2.checkedRadioButtonId)?.text.toString()
        array.add(appendJsonObject("Bukti_lot", radiogroupSilaSertakan))
        var radiogroupUmurPokok =
            activity?.findViewById<RadioButton>(binding.sectionC.radiogroupUmurPokok2.checkedRadioButtonId)?.text.toString()
        radiogroupUmurPokok = if(radiogroupUmurPokok == getString(R.string.less_20)){
            "Ya"
        } else {
            "Tidak"
        }
        array.add(appendJsonObject("Umur_Pokok", radiogroupUmurPokok))
        var radiogroupKeadaanPokok =
            activity?.findViewById<RadioButton>(binding.sectionC.radiogroupKeadaanPokok2.checkedRadioButtonId)?.text.toString()
        radiogroupKeadaanPokok = if(radiogroupKeadaanPokok == getString(R.string.ekonomik)){
            "Ya"
        } else {
            "Tidak"
        }
        array.add(appendJsonObject("Getah_Tua", radiogroupKeadaanPokok))
        val radiogroupHasil500 =
            activity?.findViewById<RadioButton>(binding.sectionC.radiogroupHasil5002.checkedRadioButtonId)?.text.toString()
        array.add(appendJsonObject("Pokok_K300", radiogroupHasil500))
        val radiogroupHabisKulit =
            activity?.findViewById<RadioButton>(binding.sectionC.radiogroupHabisKulit2.checkedRadioButtonId)?.text.toString()
        array.add(appendJsonObject("Kulit_Torehan", radiogroupHabisKulit))
        val radiogroupPokokGetah =
            activity?.findViewById<RadioButton>(binding.sectionC.radiogroupPokokGetah2.checkedRadioButtonId)?.text.toString()
        array.add(appendJsonObject("Hasil_K500", radiogroupPokokGetah))
        val radiogroupLainLainSebab =
            activity?.findViewById<RadioButton>(binding.sectionC.radiogroupLainLainSebab2.checkedRadioButtonId)?.text.toString()
        array.add(appendJsonObject("Sebab_Lain_PRN", radiogroupLainLainSebab))

        val edtTanamanLain = binding.sectionC.edtTanamanLain2.text.toString().toDoubleOrNull() ?: 0.0
        array.add(appendJsonObject("Luas_TanamanLain", edtTanamanLain))
        val edtHutanTanahKosong =
            binding.sectionC.edtHutanTanahKosong2.text.toString().toDoubleOrNull() ?: 0.0
        array.add(appendJsonObject("Luas_Hutan", edtHutanTanahKosong))
        val edtRumahBangunan =
            binding.sectionC.edtRumahBangunan2.text.toString().toDoubleOrNull() ?: 0.0
        array.add(appendJsonObject("Luas_Rumah", edtRumahBangunan))
        val edtLainLainSilaNyatakan = binding.sectionC.edtLainLainSilaNyatakan2.text.toString()
        array.add(appendJsonObject("Luas_Lain", edtLainLainSilaNyatakan))
        val edtLainLainSilaNyatakanHek =
            binding.sectionC.edtLainLainSilaNyatakanHek2.text.toString().toDoubleOrNull() ?: 0.0
        array.add(appendJsonObject("Luas_LainLain", edtLainLainSilaNyatakanHek))

        /*** Section D ***/
        val edtTarikhLawatan = binding.sectionD.edtTarikhLawatan.text.toString()
        val edtMasaDari = TimeUtils.parseDisplayTimeFormat(binding.sectionD.edtMasaDari2.text.toString())
        val edtMasaSampai = TimeUtils.parseDisplayTimeFormat(binding.sectionD.edtMasaSampai2.text.toString())
        val signPegawai = DisplayUtils.getBase64String(binding.sectionD.imageViewSignD)
        val edtCatatan2 = binding.sectionD.edtCatatan2.text.toString()

        // todo save form to local phone
        val sessionManager = context?.let { SessionManager(it) }!!
        return RequestBodies.AddTS18EForm(
            jenisLE = Constants.Form.TS18.type,
            noSiriLawatan = paramPendingVerify!!.noSiriLK!!,
            ptCode = paramPendingVerify!!.ptCode!!.trim(),
            tsMohonId = paramPendingVerify!!.tsMohonId!!.trim(),
            tkhLawat = getTimestamp(edtTarikhLawatan),
            mlawatMula = getMasaTimestamp(edtMasaDari),
            mlawatTamat = getMasaTimestamp(edtMasaSampai),
            catatan = edtCatatan2,
            pegLulus = sessionManager.fetchUsername(),
            signPegawai = signPegawai,
            items = Utils.getGson()?.toJsonTree(array.filter { !it.isJsonNull })!!.asJsonArray
        )
    }

    private fun appendJsonObject(key: String, value: Any): JsonObject? {
        if(value.toString().isNullOrEmpty() || value.toString() == "null") {
            return null
        }

        val jsonObject = JsonObject()
        jsonObject.addProperty(Constants.ITEM, key)
        jsonObject.addProperty(Constants.CATATAN, value.toString())

        return jsonObject
    }

    private fun scrollToTop() {
        binding.scrollview.fullScroll(View.FOCUS_UP)
        binding.scrollview.smoothScrollTo(0, 0)
    }

//    private fun showPrintAlertDialog() {
//        if (!PrintUtils.checkBluetoothStatus()) {
//            binding.textviewTitle.showSnack(
//                getString(R.string.ble_not_enabled),
//                Snackbar.LENGTH_SHORT
//            )
//            return
//        }
//
//        val printerDialogFragment =
//            PrinterDialogFragment(reportItem = null, appointmentItem = paramPendingVerify)
//        fragmentManager?.let { printerDialogFragment.show(it, TAG) }
//    }

    private fun saveImage(noSeri: String? = null) {
        if (filePaths.size == 0) {
            return
        }

        filePaths.forEachIndexed{ index, it ->
            val imageFile = File(it) // Create a file using the absolute path of the image
            val reqBody: RequestBody =
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), imageFile)
            val latitude = Utils.getLatitude(activity!!).toString().replace('.',',')
            val longitude = Utils.getLongitude(activity!!).toString().replace('.',',')
            val pathImage: MultipartBody.Part =
                MultipartBody.Part.createFormData("file", "image${index}_${latitude}_${longitude}." + imageFile.extension, reqBody)
            viewModel.uploadFile(
                noSeri!!,
                pathImage
            )
        }

        viewModel.apiImageResponse.observe(this, { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { response ->
                            response.message?.let {
//                                showPrintAlertDialog()
                                binding.textviewTitle.showSnack(
                                    "Submitted",
                                    Snackbar.LENGTH_SHORT
                                )
                            }
                        }
                    }

                    is Resource.NoConnection -> {
                        // todo store as offline storage
                    }

                    is Resource.Error -> {
                        response.message?.let { message ->
                            binding.textviewTitle.errorSnack(
                                message,
                                Snackbar.LENGTH_SHORT
                            )
                        }
                    }
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.REQUEST_IMAGE_CAPTURE) {
            if (resultCode == Activity.RESULT_OK) {

                try {
                    val bitmap =
                        MediaStore.Images.Media.getBitmap(context!!.contentResolver, photoURI);

                    val inflater =
                        activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

                    val attachmentView: View = inflater.inflate(R.layout.item_camera, null)
                    attachmentView.id = filePaths.size

                    val imageView =
                        attachmentView.findViewById<ImageView>(R.id.image_view_attachment)
                    imageView.setImageBitmap(bitmap)
                    val imageName = attachmentView.findViewById<TextView>(R.id.tv_image_name)
                    val imageFileName = "image${SimpleDateFormat("HHmmss").format(Date())}\n${latitude}, ${longitude}"
                    imageName.text = imageFileName
                    val deleteButton = attachmentView.findViewById<Button>(R.id.delete_button)
                    deleteButton.tag = filePaths.size
                    deleteButton.setOnClickListener {
                        try {
                            binding.sectionUploadAttachmentVerify.linearLayoutAttachment.removeViewAt(it.tag as Int - 1)
                            filePaths.removeAt(it.tag as Int - 1)
                            imageView.setImageBitmap(null)
                        } catch (e: Exception) {
                            Log.e(TAG, e.message.toString())
                        }
                    }
                    binding.sectionUploadAttachmentVerify.linearLayoutAttachment.addView(attachmentView)

                } catch (e: IOException) {
                    e.printStackTrace();
                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
                binding.sectionUploadAttachmentVerify.cameraButton.errorSnack(
                    getString(R.string.cancelled),
                    Snackbar.LENGTH_SHORT
                )
            }
        }
    }

    companion object {
        val TAG = Ts18EFragment::class.java.simpleName!!

        @JvmStatic
        fun newInstance(paramPendingVerify: PendingVerify) =
            Ts18EFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM_PENDING_VERIFY, paramPendingVerify)
                }
            }

    }
}