package com.map2u.risda.ui.readonly

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.map2u.risda.R
import com.map2u.risda.app.MyApplication
import com.map2u.risda.databinding.FragmentTs18aBinding
import com.map2u.risda.model.appointment.AppointmentItem
import com.map2u.risda.model.appointment.Form
import com.map2u.risda.model.appointment.TsApplication
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.util.Resource
import com.map2u.risda.util.TimeUtils
import com.map2u.risda.util.Utils
import com.map2u.risda.util.display.DisplayUtils
import com.map2u.risda.util.errorSnack
import com.map2u.risda.viewmodel.Ts18AViewModel
import com.map2u.risda.viewmodel.ViewModelProviderFactory
import kotlinx.coroutines.*
import java.io.File

class ReadOnlyTs18aIndividualFragment : Fragment() {
    val ARG_PARAM_APPT_ITEM = "ARG_PARAM_APPT_ITEM"
    val ARG_PARAM_TS_APPLICATION = "ARG_PARAM_TS_APPLICATION"
    val ARG_PARAM_FORM = "ARG_PARAM_FORM"
    var paramApptItem: AppointmentItem? = null
    var paramTsApplication: TsApplication? = null
    var paramForm: Form? = null

    private var _binding: FragmentTs18aBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private lateinit var viewModel: Ts18AViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            paramApptItem = it.getParcelable(ARG_PARAM_APPT_ITEM)
            paramTsApplication = it.getParcelable(ARG_PARAM_TS_APPLICATION)
            paramForm = it.getParcelable(ARG_PARAM_FORM)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTs18aBinding.inflate(inflater, container, false)
        val view = binding.root

        setupViewModel()

        /*** Disable edit text, radio button ***/
        val ll: LinearLayout? = binding.rootLinearLayout
        for (view in ll!!.touchables) {
            if (view is EditText) {
                val editText = view
                editText.setTextColor(context!!.getColor(R.color.colorPrimaryText))
                editText.isEnabled = false
                editText.isFocusable = false
                editText.isFocusableInTouchMode = false
            } else if (view is RadioButton) {
                val radioButton = view
                radioButton.isEnabled = false
                radioButton.buttonTintList =
                    ColorStateList.valueOf(context!!.getColor(R.color.colorPrimaryDark))
                radioButton.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.md_grey_300)))
            }
        }

        binding.sectionC1.sectionC1.visibility = View.GONE
        binding.sectionC2.sectionC2.visibility = View.GONE
        binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
        binding.sectionD.sectionD.visibility = View.GONE
        binding.sectionF.sectionF.visibility = View.GONE
        binding.sectionG.sectionG.visibility = View.GONE

        /*** Next button listener ***/
        nextButtonListener()

        /*** Previous button listener ***/
        previousButtonListener()

        initAbContent()
        initC1Content()
        initC2Content()
        initAttachmentsContent()
        initDContent()
        initFContent()
        initGContent()

        return view
    }

    private fun initAbContent() {
        val tsApplication: TsApplication? = paramTsApplication

        val address =
            "${tsApplication?.address1} ${tsApplication?.address2} ${tsApplication?.address3}"
        binding.sectionAb.edtNama.setText(tsApplication?.namaPemohon)
        binding.sectionAb.edtNoKpPolisTenteraSyarikat.setText(tsApplication?.icNoPemohon)
        binding.sectionAb.edtAlamat.setText(address)
        binding.sectionAb.edtPoskod.setText(tsApplication?.poskod)
        binding.sectionAb.edtNoTelRumah.setText(tsApplication?.noPhoneHouse)
        binding.sectionAb.edtNoTelBimbit.setText(tsApplication?.noPhoneMobile)
        binding.sectionAb.edtNoPermohonan.setText(tsApplication?.tsMohonId)

        binding.sectionAb.edtDaerah.setText(tsApplication?.daerah)
        binding.sectionAb.edtParlimen.setText(tsApplication?.parlimen)
        binding.sectionAb.edtDun.setText(tsApplication?.dun)
        binding.sectionAb.edtMukim.setText(tsApplication?.mukim)
        binding.sectionAb.edtKampung.setText(tsApplication?.kampung)

        binding.sectionAb.edtNoGeran.setText(tsApplication?.lotDimilikiList?.get(0)?.noGeran.toString())
        binding.sectionAb.edtNoLot.setText(tsApplication?.lotDimilikiList?.get(0)?.noLot.toString())
        binding.sectionAb.edtJenisTanaman.setText(tsApplication?.lotDimilikiList?.get(0)?.tsCrop.toString())

        binding.sectionAb.edtLuasKebunHektar.setText(tsApplication?.lotDimilikiList?.get(0)?.luasLot.toString())
        binding.sectionAb.edtLuasKebunHektarHendakDitanam.setText(tsApplication?.lotDimilikiList?.get(0)?.luasTs.toString())
    }

    private fun initC1Content() {
        if (paramForm == null) return

        binding.sectionC1.edtLuasKawasanSiapDibersihkan.setText(paramForm!!.tebangJentera1.toString())
        paramForm!!.jarakTanaman?.toInt()?.let {
            binding.sectionC1.spinnerJarakTanaman.setSelection(
                it
            )
        }
        binding.sectionC1.edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih.setText(paramForm!!.luasTanam.toString())
        binding.sectionC1.edtTarikhTanam.setText(TimeUtils.displayDate(paramForm!!.tkhTanam ?: 0))

        if (paramForm!!.perimeter?.trim() == "Ya") {
            binding.sectionC1.rbKerjaMengukurYa.isChecked = true
        } else if (paramForm!!.perimeter?.trim() == "Tidak") {
            binding.sectionC1.rbKerjaMengukurTidak.isChecked = true
        }

        if (paramForm!!.benihLuar?.trim() == "Ya") {
            binding.sectionC1.rbKelulusanBorangTs29aYa.isChecked = true
        } else if (paramForm!!.benihLuar?.trim() == "Tidak") {
            binding.sectionC1.rbKelulusanBorangTs29aTidak.isChecked = true
        }

        if (paramForm!!.klonMutu?.trim() == "Ya") {
            binding.sectionC1.rbKlonBermutuTinggiYa.isChecked = true
        } else if (paramForm!!.klonMutu?.trim() == "Tidak") {
            binding.sectionC1.rbKlonBermutuTinggiTidak.isChecked = true
        }

        if (paramForm!!.klonMutu?.trim() == "Ya") {
            binding.sectionC1.rbKlonBermutuTinggiYa.isChecked = true
        } else if (paramForm!!.klonMutu?.trim() == "Tidak") {
            binding.sectionC1.rbKlonBermutuTinggiTidak.isChecked = true
        }

        if (paramForm!!.resitBenih?.trim() == "Ya") {
            binding.sectionC1.rbResitPembelianBenihYa.isChecked = true
        } else if (paramForm!!.resitBenih?.trim() == "Tidak") {
            binding.sectionC1.rbResitPembelianBenihTidak.isChecked = true
        }

        if (paramForm!!.sahKlon?.trim() == "Ya") {
            binding.sectionC1.rbSijilPengesahanYa.isChecked = true
        } else if (paramForm!!.sahKlon?.trim() == "Tidak") {
            binding.sectionC1.rbSijilPengesahanTidak.isChecked = true
        }

        if (paramForm!!.tapakSemaian?.trim() == "Ya") {
            binding.sectionC1.rbTapakSemaianYa.isChecked = true
        } else if (paramForm!!.tapakSemaian?.trim() == "Tidak") {
            binding.sectionC1.rbTapakSemaianTidak.isChecked = true
        }

        if (paramForm!!.tapakSemaian?.trim() == "Ya") {
            binding.sectionC1.rbTapakSemaianYa.isChecked = true
        } else if (paramForm!!.tapakSemaian?.trim() == "Tidak") {
            binding.sectionC1.rbTapakSemaianTidak.isChecked = true
        }

        binding.sectionC1.edtBenihSawitNyatakanSumber.setText(paramForm!!.benihSawit.toString())

        if (paramForm!!.gapSenggara?.trim() == "Ya") {
            binding.sectionC1.rbAdakahTeresYa.isChecked = true
        } else if (paramForm!!.gapSenggara?.trim() == "Tidak") {
            binding.sectionC1.rbAdakahTeresTidak.isChecked = true
        }

        binding.sectionC1.edtLainLainKenyataan.setText(paramForm!!.kenyataanLain.toString())

        if (paramForm!!.jenisBenih?.trim() == "BCMP") {
            binding.sectionC1.rbJenisBenihBcmp.isChecked = true
        } else if (paramForm!!.jenisBenih?.trim() == "TCP") {
            binding.sectionC1.rbJenisBenihTcp.isChecked = true
        } else if (paramForm!!.jenisBenih?.trim() == "APM") {
            binding.sectionC1.rbJenisBenihApm.isChecked = true
        }

        binding.sectionC1.checkboxMengemaskini.isChecked = true
        binding.sectionC1.checkboxMengemaskini.isClickable = false
    }

    private fun initC2Content() {
        if (paramForm == null) return

        binding.sectionC2.edtLuasTanamanUtamaYangBerjaya.setText(paramForm!!.luasBerjaya.toString())

        if (paramForm!!.gapBaja?.trim() == "Ya") {
            binding.sectionC2.rbAdakahMembajaYa.isChecked = true
        } else if (paramForm!!.gapBaja?.trim() == "Tidak") {
            binding.sectionC2.rbAdakahMembajaTidak.isChecked = true
        }

        if (paramForm!!.gapCantasan?.trim() == "Ya") {
            binding.sectionC2.rbAdakahCantasanYa.isChecked = true
        } else if (paramForm!!.gapCantasan?.trim() == "Tidak") {
            binding.sectionC2.rbAdakahCantasanTidak.isChecked = true
        }

        if (paramForm!!.jenisCantasan?.trim() == "Pembetulan") {
            binding.sectionC2.rbJikaYaPembetulan.isChecked = true
        } else if (paramForm!!.jenisCantasan?.trim() == "Terkawal") {
            binding.sectionC2.rbJikaYaTerkawal.isChecked = true
        }

        if (paramForm!!.gapGalakDahan?.trim() == "Ya") {
            binding.sectionC2.rbAdakahGalakanYa.isChecked = true
        } else if (paramForm!!.gapGalakDahan?.trim() == "Tidak") {
            binding.sectionC2.rbAdakahGalakanTidak.isChecked = true
        }

        if (paramForm!!.jenisGalakDahan?.trim() == "Dilaksanakan") {
            binding.sectionC2.rbJikaYaNyatakanDilaksanakan.isChecked = true
        } else if (paramForm!!.jenisGalakDahan?.trim() == "Tidak dilaksanakan") {
            binding.sectionC2.rbJikaYaNyatakanTidakDilaksanakan.isChecked = true
        }

        if (paramForm!!.kawalPerosak?.trim() == "Ya") {
            binding.sectionC2.rbAdakahKawalanYa.isChecked = true
        } else if (paramForm!!.kawalPerosak?.trim() == "Tidak") {
            binding.sectionC2.rbAdakahKawalanTidak.isChecked = true
        }

        binding.sectionC2.edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang.setText(paramForm!!.rumpaiSenggara.toString())
        binding.sectionC2.edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh.setText(paramForm!!.rumpaiTdkSenggara.toString())

        if (paramForm!!.gapSenggara?.trim() == "Ya") {
            binding.sectionC2.rbAdakahTeresYa.isChecked = true
        } else if (paramForm!!.gapSenggara?.trim() == "Tidak") {
            binding.sectionC2.rbAdakahTeresTidak.isChecked = true
        }

        if (paramForm!!.kpbKontan?.trim() == "Ya") {
            binding.sectionC2.rbAdakahKpbYa.isChecked = true
        } else if (paramForm!!.kpbKontan?.trim() == "Tidak") {
            binding.sectionC2.rbAdakahKpbTidak.isChecked = true
        }

        binding.sectionC2.edtJikaYaLuasYangDiselenggara.setText(paramForm!!.luasKontan.toString())
        binding.sectionC2.edtGetah.setText(paramForm!!.getahPkkHek.toString())
        binding.sectionC2.edtTanamanLain.setText(paramForm!!.tlPkkHidup.toString())
        binding.sectionC2.edtPurataUkurLilitPokokGetahYang.setText(paramForm!!.ukurLilit.toString())
        binding.sectionC2.edtLainLainKenyataanJikaAda.setText(paramForm!!.kenyataanLain.toString())

        binding.sectionC2.checkboxMengemaskini.isChecked = true
        binding.sectionC2.checkboxMengemaskini.isClickable = false
    }

    private fun initAttachmentsContent() {
        binding.sectionUploadAttachment.cameraButton.visibility = View.GONE

        if (Utils.hasInternetConnection(activity!!.application as MyApplication)) {
            GlobalScope.launch {
                callApiAndUpdateLiveData()
            }
        } else {
            val filePaths = paramForm?.noSiriTemujanji?.let { viewModel.queryImageOffline(it) }

            filePaths?.forEach{
                val dir = it.substringBeforeLast('/').replace("\"", "")
                val fileName = it.substringAfterLast('/').replace("\"", "")
                val file = File(dir, fileName)
                if (file.exists()) {
                    val bitmap = BitmapFactory.decodeFile(file.absolutePath)

                    val inflater =
                        activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

                    val attachmentView: View = inflater.inflate(R.layout.item_camera, null)
                    val imageView = attachmentView.findViewById<ImageView>(R.id.image_view_attachment)
                    imageView.setImageBitmap(bitmap)

                    val imageName = attachmentView.findViewById<TextView>(R.id.tv_image_name)
                    val name = fileName.replace("_","\n")
                    imageName.text = name

                    val deleteButton = attachmentView.findViewById<Button>(R.id.delete_button)
                    deleteButton.visibility = View.GONE

                    binding.sectionUploadAttachment.linearLayoutAttachment.addView(
                        attachmentView
                    )
                }
            }
        }
    }

    private suspend fun callApiAndUpdateLiveData() {
        paramForm?.documents?.forEach {
            coroutineScope {
                async {
                    val result = it.id?.let { file -> viewModel.getFile(file) }
                }
            }.await()
        }

        lifecycleScope.launch {
            withContext(Dispatchers.Main) {
                viewModel.apiFileResponse.observe(this@ReadOnlyTs18aIndividualFragment, { event ->
                    event.getContentIfNotHandled()?.let { response ->
                        when (response) {
                            is Resource.Success -> {
                                val fileName = response.message
                                response.data?.let { response ->
                                    val bitmap =
                                        BitmapFactory.decodeStream(response.byteStream())

                                    val inflater =
                                        activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

                                    val attachmentView: View =
                                        inflater.inflate(R.layout.item_camera, null)
                                    val imageView =
                                        attachmentView.findViewById<ImageView>(R.id.image_view_attachment)
                                    imageView.setImageBitmap(bitmap)

                                    val imageName = attachmentView.findViewById<TextView>(R.id.tv_image_name)
                                    imageName.text = fileName

                                    val deleteButton =
                                        attachmentView.findViewById<Button>(R.id.delete_button)
                                    deleteButton.visibility = View.GONE

                                    binding.sectionUploadAttachment.linearLayoutAttachment.addView(
                                        attachmentView
                                    )
                                }
                            }
                        }
                    }
                })
            }
        }
    }

    private fun initDContent() {
        binding.sectionD.buttonSignHereD.visibility = View.GONE

        binding.sectionD.edtTarikhLawatan.setText(paramForm!!.tkhLawat?.let {
            TimeUtils.displayDate(
                it
            )
        })
        try {
            binding.sectionD.edtMasaDari.setText(TimeUtils.displayTime(paramForm!!.mlawatMula!!.toLong()))
            binding.sectionD.edtMasaSampai.setText(TimeUtils.displayTime(paramForm!!.mlawatTamat!!.toLong()))
        } catch (e: Exception) {
            Log.e(TAG, e.message.toString())
        }

        binding.sectionD.imageViewSignD.setImageBitmap(paramForm!!.signPegawai?.let {
            DisplayUtils.getBitmapFromBase64String(
                it
            )
        })
    }

    private fun initFContent() {
        binding.sectionF.checkboxSetuju.isClickable = false
        binding.sectionF.checkboxSetuju.isChecked = true
    }

    private fun initGContent() {
        binding.sectionG.buttonSignHereG.visibility = View.GONE
        binding.sectionG.buttonSaveG.visibility = View.GONE

        if (paramForm!!.hadirSendiri?.trim() == "Ya") {
            binding.sectionG.rbPemohonHadirSendiriYa.isChecked = true
        } else if (paramForm!!.hadirSendiri?.trim() == "Tidak") {
            binding.sectionG.rbPemohonHadirSendiriTidak.isChecked = true
        }

        binding.sectionG.edtNama.setText(paramForm!!.namaPerakuan)
        binding.sectionG.edtKpPemohon.setText(paramForm!!.noKpPerakuan)
        binding.sectionG.imageViewSignG.setImageBitmap(paramForm!!.signPerakuan?.let {
            DisplayUtils.getBitmapFromBase64String(
                it
            )
        })
    }

    private fun nextButtonListener() {
        binding.sectionAb.buttonNextAb.setOnClickListener { v ->
            scrollToTop()
            binding.sectionAb.sectionAb.visibility = View.GONE

            if (paramApptItem!!.getAnsuranNo?.toIntOrNull() == 1) {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_pendahuluan_semasa)
                binding.sectionC1.sectionC1.visibility = View.VISIBLE
            } else {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_berturut_ansuran)
                binding.sectionC2.sectionC2.visibility = View.VISIBLE
            }

            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionC1.buttonNextC1.setOnClickListener { v ->
            scrollToTop()
            binding.sectionC1.sectionC1.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionC2.buttonNextC2.setOnClickListener { v ->
            scrollToTop()
            binding.sectionC2.sectionC2.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionUploadAttachment.buttonNextUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.sectionD.sectionD.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.d_perakuan_pegawai_pemeriksa_kebun)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionD.buttonNextD.setOnClickListener { v ->
            val edtTarikhLawatan = binding.sectionD.edtTarikhLawatan.text.toString()
            val edtMasaDari = binding.sectionD.edtMasaDari.text.toString()
            val edtMasaSampai = binding.sectionD.edtMasaSampai.text.toString()
            val signPegawai = DisplayUtils.getBase64String(binding.sectionD.imageViewSignD)

            if (edtTarikhLawatan.isEmpty() || edtMasaDari.isEmpty() || edtMasaSampai.isEmpty() || signPegawai.isEmpty()) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_not_filled),
                    Snackbar.LENGTH_LONG
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionD.sectionD.visibility = View.GONE
            binding.sectionF.sectionF.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.f_peringatan)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionF.buttonNextF.setOnClickListener { v ->
            if (!binding.sectionF.checkboxSetuju.isChecked) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_not_checked),
                    Snackbar.LENGTH_LONG
                )
                return@setOnClickListener
            }
            scrollToTop()
            binding.sectionF.sectionF.visibility = View.GONE
            binding.sectionG.sectionG.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.g_perakuan_pemohon_wakilnya)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }
    }

    private fun previousButtonListener() {
        binding.sectionC1.buttonPreviousC1.setOnClickListener { v ->
            scrollToTop()
            binding.sectionAb.sectionAb.visibility = View.VISIBLE
            binding.sectionC1.sectionC1.visibility = View.GONE

            binding.textviewTitle.text = getString(R.string.a_b_profil_pemohon_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionC2.buttonPreviousC2.setOnClickListener { v ->
            scrollToTop()
            binding.sectionAb.sectionAb.visibility = View.VISIBLE
            binding.sectionC2.sectionC2.visibility = View.GONE

            binding.textviewTitle.text =
                getString(R.string.a_b_profil_pemohon_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionUploadAttachment.buttonPreviousUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            if (paramApptItem!!.getAnsuranNo?.toIntOrNull() == 1) {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_pendahuluan_semasa)
                binding.sectionC1.sectionC1.visibility = View.VISIBLE
            } else {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_berturut_ansuran)
                binding.sectionC2.sectionC2.visibility = View.VISIBLE
            }

            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionD.buttonPreviousD.setOnClickListener { v ->
            scrollToTop()
            binding.textviewTitle.text =
                getString(R.string.attachments)
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.sectionD.sectionD.visibility = View.GONE
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionF.buttonPreviousF.setOnClickListener { v ->
            scrollToTop()
            binding.sectionD.sectionD.visibility = View.VISIBLE
            binding.sectionF.sectionF.visibility = View.GONE

            binding.textviewTitle.text = getString(R.string.d_perakuan_pegawai_pemeriksa_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionG.buttonPreviousG.setOnClickListener { v ->
            scrollToTop()
            binding.sectionF.sectionF.visibility = View.VISIBLE
            binding.sectionG.sectionG.visibility = View.GONE

            binding.textviewTitle.text = getString(R.string.f_peringatan)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }
    }

    private fun scrollToTop() {
        binding.scrollview.fullScroll(View.FOCUS_UP)
        binding.scrollview.smoothScrollTo(0, 0)
    }

    private fun setupViewModel() {
        val repository = AppRepository()
        val factory = activity?.let { ViewModelProviderFactory(it.application, repository) }
        viewModel = factory?.let { ViewModelProvider(this, it).get(Ts18AViewModel::class.java) }!!
    }

    companion object {
        val TAG = ReadOnlyTs18aIndividualFragment::class.java.simpleName!!

        @JvmStatic
        fun newInstance(
            paramApptItem: AppointmentItem,
            paramTsApplication: TsApplication,
            paramForm: Form?
        ) =
            ReadOnlyTs18aIndividualFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM_APPT_ITEM, paramApptItem)
                    putParcelable(ARG_PARAM_TS_APPLICATION, paramTsApplication)
                    putParcelable(ARG_PARAM_FORM, paramForm)
                }
            }
    }
}