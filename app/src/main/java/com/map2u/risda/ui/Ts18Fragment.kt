package com.map2u.risda.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.map2u.risda.BuildConfig
import com.map2u.risda.R
import com.map2u.risda.databinding.FragmentTs18Binding
import com.map2u.risda.model.appointment.AppointmentItem
import com.map2u.risda.model.appointment.Form
import com.map2u.risda.model.appointment.ReportItem
import com.map2u.risda.model.appointment.TsApplication
import com.map2u.risda.network.RequestBodies
import com.map2u.risda.network.SessionManager
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.ui.shared.OnSignedCaptureListener
import com.map2u.risda.ui.shared.PrinterDialogFragment
import com.map2u.risda.ui.shared.SignatureDialogFragment
import com.map2u.risda.util.*
import com.map2u.risda.util.StringUtils.replaceNullToEmptyString
import com.map2u.risda.util.TimeUtils.displayTodayDate
import com.map2u.risda.util.TimeUtils.getCurrentTimestamp
import com.map2u.risda.util.TimeUtils.getTimestamp
import com.map2u.risda.util.Utils.getLatitude
import com.map2u.risda.util.Utils.getLongitude
import com.map2u.risda.util.display.DisplayUtils
import com.map2u.risda.util.display.PrintUtils
import com.map2u.risda.viewmodel.Ts18ViewModel
import com.map2u.risda.viewmodel.ViewModelProviderFactory
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [Ts18Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Ts18Fragment : Fragment(), OnSignedCaptureListener {
    val ARG_PARAM_APPT_ITEM = "ARG_PARAM_APPT_ITEM"
    val ARG_PARAM_FORM_INDEX = "ARG_PARAM_FORM_INDEX"
    var paramApptItem: AppointmentItem? = null
    var paramFormIndex: Int? = null

    private var _binding: FragmentTs18Binding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private lateinit var viewModel: Ts18ViewModel

    // This property is to get username
    private lateinit var sessionManager: SessionManager

    // This property is for image upload
    private val filePaths: ArrayList<String> = ArrayList()

    // This property is to get location
    var longitude: Double = 0.0
    var latitude: Double = 0.0

    lateinit var photoURI: Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            paramApptItem = it.getParcelable(ARG_PARAM_APPT_ITEM)
            paramFormIndex = it.getInt(ARG_PARAM_FORM_INDEX)
        }

        latitude = getLatitude(activity!!)
        longitude = getLongitude(activity!!)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTs18Binding.inflate(inflater, container, false)
        val view = binding.root

        setupViewModel()

        binding.sectionC.sectionC.visibility = View.GONE
        binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
        binding.sectionD.sectionD.visibility = View.GONE
        binding.sectionF.sectionF.visibility = View.GONE
        binding.sectionG.sectionG.visibility = View.GONE

        binding.seekBar.setOnTouchListener(OnTouchListener { v, event ->
            // True doesn't propagate the event
            // the user cannot move the indicator
            true
        })

        /*** Next button listener ***/
        nextButtonListener()

        /*** Previous button listener ***/
        previousButtonListener()

        /*** Camera button listener ***/
        cameraButtonListener()

        /*** Section C if not economical radio button listener ***/
        binding.sectionC.rbKeadaanPokokTidakEkonomik.setOnCheckedChangeListener { v, isChecked ->
            for (i in 0 until binding.sectionC.radiogroupHasil500.getChildCount()) {
                binding.sectionC.radiogroupHasil500.getChildAt(i).isEnabled = isChecked
                binding.sectionC.radiogroupHasil500.clearCheck()
            }
            for (i in 0 until binding.sectionC.radiogroupHabisKulit.getChildCount()) {
                binding.sectionC.radiogroupHabisKulit.getChildAt(i).isEnabled = isChecked
                binding.sectionC.radiogroupHabisKulit.clearCheck()
            }
            for (i in 0 until binding.sectionC.radiogroupPokokGetah.getChildCount()) {
                binding.sectionC.radiogroupPokokGetah.getChildAt(i).isEnabled = isChecked
                binding.sectionC.radiogroupPokokGetah.clearCheck()
            }
            for (i in 0 until binding.sectionC.radiogroupLainLainSebab.getChildCount()) {
                binding.sectionC.radiogroupLainLainSebab.getChildAt(i).isEnabled = isChecked
                binding.sectionC.radiogroupLainLainSebab.clearCheck()
            }

            if (isChecked) {
                binding.sectionC.tvHasil500.text = Html.fromHtml(
                    "<font color='#FF5733'>*</font> i) Hasil &lt;500kg/hek/thn Bilangan kurang 300 pokok/hektar",
                    Html.FROM_HTML_MODE_COMPACT
                )
                binding.sectionC.tvHabisKulitTapakTorehan.text = Html.fromHtml(
                    "<font color='#FF5733'>*</font> " + getString(R.string.habis_kulit_tapak_torehan),
                    Html.FROM_HTML_MODE_COMPACT
                )
                binding.sectionC.tvPokokGetahBertoreh.text = Html.fromHtml(
                    "<font color='#FF5733'>*</font> " + getString(R.string.pokok_getah_bertoreh),
                    Html.FROM_HTML_MODE_COMPACT
                )
                binding.sectionC.tvLainLain.text = Html.fromHtml(
                    "<font color='#FF5733'>*</font> " + getString(R.string.lain_lain),
                    Html.FROM_HTML_MODE_COMPACT
                )
            } else {
                binding.sectionC.tvHasil500.text = getString(R.string.hasil_500)
                binding.sectionC.tvHabisKulitTapakTorehan.text =
                    getString(R.string.habis_kulit_tapak_torehan)
                binding.sectionC.tvPokokGetahBertoreh.text =
                    getString(R.string.pokok_getah_bertoreh)
                binding.sectionC.tvLainLain.text = getString(R.string.lain_lain)
            }
        }

        binding.sectionC.checkboxMengemaskini.setOnCheckedChangeListener { v, isChecked ->
            if (isChecked) {
                binding.sectionC.edtTanamanLain.isEnabled = true
                binding.sectionC.edtHutanTanahKosong.isEnabled = true
                binding.sectionC.edtRumahBangunan.isEnabled = true
                binding.sectionC.edtLainLainSilaNyatakan.isEnabled = true
                binding.sectionC.edtLainLainSilaNyatakanHek.isEnabled = true

                val launchIntent =
                    context?.packageManager?.getLaunchIntentForPackage("com.esri.fieldmaps")
                if (launchIntent != null) {
                    startActivity(launchIntent)
                } else {
                    binding.textviewTitle.errorSnack(
                        getString(R.string.no_argis_app),
                        Snackbar.LENGTH_LONG
                    )
                }
            }
        }

        /*** Section D default date ***/
        binding.sectionD.edtTarikhLawatan.setText(displayTodayDate())

        /*** Time input picker ***/
        // This property is for Time picker usage
        val myCalendar: Calendar = Calendar.getInstance()
        val timeDari =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                myCalendar.set(Calendar.MINUTE, minute)

                val sdf = SimpleDateFormat("hh:mm a")
                binding.sectionD.edtMasaDari.setText(sdf.format(myCalendar.getTime()))
            }
        binding.sectionD.edtMasaDari.setOnClickListener { v ->
            TimePickerDialog(
                context!!,
                timeDari,
                myCalendar[Calendar.HOUR_OF_DAY],
                myCalendar[Calendar.MINUTE],
                false
            ).show()
        }

        val timeSampai =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                myCalendar.set(Calendar.MINUTE, minute)

                val sdf = SimpleDateFormat("hh:mm a")
                binding.sectionD.edtMasaSampai.setText(sdf.format(myCalendar.getTime()))
            }
        binding.sectionD.edtMasaSampai.setOnClickListener { v ->
            TimePickerDialog(
                context!!,
                timeSampai,
                myCalendar[Calendar.HOUR_OF_DAY],
                myCalendar[Calendar.MINUTE],
                false
            ).show()
        }

        /*** Section G listener ***/
        binding.sectionG.rbPemohonHadirSendiriYa.setOnCheckedChangeListener { v, isChecked ->
            if (isChecked) {
                val tsApplication: TsApplication? = paramApptItem!!.tsApplication
                binding.sectionG.edtNama.setText(tsApplication?.namaPemohon)
                binding.sectionG.edtKpPemohon.setText(tsApplication?.icNoPemohon)
            } else {
                binding.sectionG.edtNama.setText("")
                binding.sectionG.edtKpPemohon.setText("")
            }
        }

        /*** Sign listener ***/
        binding.sectionD.buttonSignHereD.setOnClickListener { v ->
            val dialogFragment = SignatureDialogFragment(this, "D")
            dialogFragment.show(activity!!.supportFragmentManager, "signature")
        }
        binding.sectionG.buttonSignHereG.setOnClickListener { v ->
            val dialogFragment = SignatureDialogFragment(this, "G")
            dialogFragment.show(activity!!.supportFragmentManager, "signature")
        }

        /*** Save form button listener ***/
        binding.sectionG.buttonSaveG.setOnClickListener { v ->
            val signPerakuan = DisplayUtils.getBase64String(binding.sectionG.imageViewSignG)
            val edtNama = binding.sectionG.edtNama.text.toString()
            val edtKpPemohon = binding.sectionG.edtKpPemohon.text.toString()

            if (signPerakuan.isEmpty() || edtNama.isEmpty() || edtKpPemohon.isEmpty()) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_not_filled),
                    Snackbar.LENGTH_LONG
                )
                return@setOnClickListener
            }

            saveForm()
            updateAppointmentStatus()
        }

        initAbContent()

        return view
    }

    private fun setupViewModel() {
        val repository = AppRepository()
        val factory = activity?.let { ViewModelProviderFactory(it.application, repository) }
        viewModel = factory?.let { ViewModelProvider(this, it).get(Ts18ViewModel::class.java) }!!
    }

    private fun saveForm() {
        val form: RequestBodies.AddTS18Form?
        if (paramFormIndex == -1) {
            form = setupRequestBodies("POST")
            viewModel.postTS18Form(setupRequestBodies("POST"))
        } else {
            form = setupRequestBodies("PUT")
            viewModel.putTS18Form(setupRequestBodies("PUT"))
        }

        viewModel.apiResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { response ->
                            if (!response.data.isNullOrEmpty()) {
                                response.data?.let {
                                    Event(saveImage(it))
                                }
                            } else {
                                showPrintAlertDialog()
                            }
                        }
                    }

                    is Resource.NoConnection -> {
                        viewModel.updateAppointmentOffline(
                            noSiriTemujanji = paramApptItem?.noSiriTemujanji!!,
                            form
                        )
                        viewModel.updateImageOffline(
                            noSiriTemujanji = paramApptItem?.noSiriTemujanji!!,
                            filePaths
                        )
                        showPrintAlertDialog()
                    }

                    is Resource.Error -> {
                        response.message?.let { message ->
                            binding.textviewTitle.errorSnack(
                                message,
                                Snackbar.LENGTH_SHORT
                            )
                        }
                    }
                }
            }
        })
    }

    private fun saveImage(noSeri: String? = null) {
        if (filePaths.size == 0) {
            return
        }

        filePaths.forEachIndexed { index, it ->
            val imageFile = File(it) // Create a file using the absolute path of the image
            val reqBody: RequestBody =
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), imageFile)
            val latitude = getLatitude(activity!!).toString().replace('.', ',')
            val longitude = getLongitude(activity!!).toString().replace('.', ',')
            val pathImage: MultipartBody.Part =
                MultipartBody.Part.createFormData(
                    "file",
                    "image${index}_${latitude}_${longitude}." + imageFile.extension,
                    reqBody
                )
            if (StringUtils.isNullOrEmpty(noSeri)) {
                viewModel.uploadFile(
                    paramApptItem!!.formList?.get(paramFormIndex!!)?.getNoSiriLK.toString(),
                    pathImage
                )
            } else {
                viewModel.uploadFile(
                    noSeri!!,
                    pathImage
                )
            }
        }

        viewModel.apiImageResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { response ->
                            response.message?.let {
                                showPrintAlertDialog()
                            }
                        }
                    }

                    is Resource.Error -> {
                        response.message?.let { message ->
                            binding.textviewTitle.errorSnack(
                                message,
                                Snackbar.LENGTH_SHORT
                            )
                            showPrintAlertDialog()
                        }
                    }
                }
            }
        })
    }

    private fun updateAppointmentStatus() {
        val body = RequestBodies.UpdateAppointmentStatus(
            noSiriTemujanji = paramApptItem!!.noSiriTemujanji!!,
        )
        viewModel.putAppointmentStatus(body)
        viewModel.apiApptStatusResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { response ->
                            response.message?.let {

                            }
                        }
                    }

                    is Resource.Error -> {
                        response.message?.let { message ->
                            binding.textviewTitle.errorSnack(
                                message,
                                Snackbar.LENGTH_SHORT
                            )
                        }
                    }
                }
            }
        })
    }

    private fun setupRequestBodies(apiType: String): RequestBodies.AddTS18Form {
        /*** Section B ***/
        val radiogroupMenerimaBantuan =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionAb.radiogroupMenerimaBantuan.checkedRadioButtonId)?.text.toString())
        val edtNamaAgensiPemberiBantuan =
            binding.sectionAb.edtNamaAgensiPemberiBantuan.text.toString()
        val edtLuasDilulus = binding.sectionAb.edtLuasDilulus.text.toString().toDoubleOrNull()
            ?: 0.0
        val edtJenisTanaman = binding.sectionAb.edtJenisTanaman.text.toString()

        /*** Section C ***/
        val edtKeluasanTanamanGetahTuaSekarang =
            binding.sectionC.edtKeluasanTanamanGetahTuaSekarang.text.toString().toDoubleOrNull()
                ?: 0.0
        val radiogroupTunggulGetahTua =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupTunggulGetahTua.checkedRadioButtonId)?.text.toString())
        val radiogroupBukuLesenGetah =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupBukuLesenGetah.checkedRadioButtonId)?.text.toString())
        val radiogroupGambarSatelit =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupGambarSatelit.checkedRadioButtonId)?.text.toString())
        val radiogroupLesenGetah =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupLesenGetah.checkedRadioButtonId)?.text.toString())
        val radiogroupSuratPengesahan =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupSuratPengesahan.checkedRadioButtonId)?.text.toString())
        val radiogroupRekodYangMembukitkan =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupRekodYangMembukitkan.checkedRadioButtonId)?.text.toString())
        val radiogroupSilaSertakan =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupSilaSertakan.checkedRadioButtonId)?.text.toString())
        var radiogroupUmurPokok =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupUmurPokok.checkedRadioButtonId)?.text.toString())
        radiogroupUmurPokok = if(radiogroupUmurPokok == getString(R.string.less_20)){
            "Ya"
        } else {
            "Tidak"
        }
        var radiogroupKeadaanPokok =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupKeadaanPokok.checkedRadioButtonId)?.text.toString())
        radiogroupKeadaanPokok = if(radiogroupKeadaanPokok == getString(R.string.ekonomik)){
            "Ya"
        } else {
            "Tidak"
        }
        val radiogroupHasil500 =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupHasil500.checkedRadioButtonId)?.text.toString())
        val radiogroupHabisKulit =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupHabisKulit.checkedRadioButtonId)?.text.toString())
        val radiogroupPokokGetah =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupPokokGetah.checkedRadioButtonId)?.text.toString())
        val radiogroupLainLainSebab =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupLainLainSebab.checkedRadioButtonId)?.text.toString())
        val edtTanamanLain = binding.sectionC.edtTanamanLain.text.toString().toDoubleOrNull() ?: 0.0
        val edtHutanTanahKosong =
            binding.sectionC.edtHutanTanahKosong.text.toString().toDoubleOrNull() ?: 0.0
        val edtRumahBangunan =
            binding.sectionC.edtRumahBangunan.text.toString().toDoubleOrNull() ?: 0.0
        val edtLainLainSilaNyatakan = binding.sectionC.edtLainLainSilaNyatakan.text.toString()
        val edtLainLainSilaNyatakanHek =
            binding.sectionC.edtLainLainSilaNyatakanHek.text.toString().toDoubleOrNull() ?: 0.0

        /*** Section D ***/
        val edtTarikhLawatan = binding.sectionD.edtTarikhLawatan.text.toString()
        val edtMasaDari = TimeUtils.parseDisplayTimeFormat(binding.sectionD.edtMasaDari.text.toString())
        val edtMasaSampai = TimeUtils.parseDisplayTimeFormat(binding.sectionD.edtMasaSampai.text.toString())
        val signPegawai = DisplayUtils.getBase64String(binding.sectionD.imageViewSignD)

        /*** Section G ***/
        val signPerakuan = DisplayUtils.getBase64String(binding.sectionG.imageViewSignG)
        val edtNama = binding.sectionG.edtNama.text.toString()
        val edtKpPemohon = binding.sectionG.edtKpPemohon.text.toString()
        val radiogroupPemohonHadir =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionG.radiogroupPemohonHadirSendiri.checkedRadioButtonId)?.text.toString())

        sessionManager = context?.let { SessionManager(it) }!!

        return RequestBodies.AddTS18Form(
            alasanLulus = null,
            alasanNo = null,
            bantuanLain = radiogroupMenerimaBantuan,
            bilLawatan = null,
            blAgensi = edtNamaAgensiPemberiBantuan,
            blCrop = edtJenisTanaman,
            blLuasHabisByr = null,
            blLuasLulus = edtLuasDilulus,
            blNoTs = "",
            buktiLot = radiogroupSilaSertakan,
            buktiLotTs = radiogroupRekodYangMembukitkan,
            bukuLesen = radiogroupBukuLesenGetah,
            catatan = "",
            catatanLulus = "",
            gambarSatelit = radiogroupGambarSatelit,
            getahTua = radiogroupKeadaanPokok,
            hasilK500 = radiogroupPokokGetah,
            jawPegLulus = sessionManager.fetchUsername(),
            jawPemeriksa = sessionManager.fetchDesignation(),
            jenisLK = paramApptItem!!.jenisLK,
            kulitTorehan = radiogroupHabisKulit,
            lainLain = edtLainLainSilaNyatakan,
            lesenGetah = radiogroupLesenGetah,
            luasGetahTua = edtKeluasanTanamanGetahTuaSekarang,
            luasHutan = edtHutanTanahKosong,
            luasLain = null,
            luasLainLain = edtLainLainSilaNyatakanHek,
            luasLulus = null,
            luasRumah = edtRumahBangunan,
            luasTanamanLain = edtTanamanLain,
            mlawatMula = edtMasaDari,
            mlawatTamat = edtMasaSampai,
            namaPerakuan = edtNama,
            noKpPerakuan = edtKpPemohon,
            noKpPpk = sessionManager.fetchUsername(),
            noSiriLK = if (apiType == "POST") null else paramApptItem!!.formList?.get(paramFormIndex!!)?.getNoSiriLK.toString(),
            noSiriTemujanji = paramApptItem!!.noSiriTemujanji!!.trim(),
            pegKemaskini = sessionManager.fetchUsername(),
            pegLulus = "",
            pegPemeriksa = sessionManager.fetchName(),
            pelanTanah = "",
            pkHadir = radiogroupPemohonHadir,
            pokokK300 = radiogroupHasil500,
            ptCode = paramApptItem!!.ptCode!!.trim(),
            sebabLainPRN = radiogroupLainLainSebab,
            signPegawai = signPegawai,
            signPerakuan = signPerakuan,
            statusLulus = "",
            suratAgensi = radiogroupSuratPengesahan,
            tkhKemaskini = getCurrentTimestamp(),
            tkhLawat = getTimestamp(edtTarikhLawatan),
            tkhLulus = getCurrentTimestamp(),
            tsMohonId = paramApptItem!!.tsMohonId!!.trim(),
            tunggulGetah = radiogroupTunggulGetahTua,
            umurPokok = radiogroupUmurPokok,
            latitude = latitude,
            longitude = longitude,
            hadir = radiogroupPemohonHadir
        )
    }

    override fun onSignatureCaptured(bitmap: Bitmap, fileName: String) {
        when (fileName) {
            "D" -> {
                binding.sectionD.imageViewSignD.setImageBitmap(bitmap)
            }
            "G" -> {
                binding.sectionG.imageViewSignG.setImageBitmap(bitmap)
            }
        }
    }

    private fun initAbContent() {
        val address =
            "${paramApptItem?.tsApplication?.address1} ${paramApptItem?.tsApplication?.address2} ${paramApptItem?.tsApplication?.address3}"
        binding.sectionAb.edtNama.setText(paramApptItem?.tsApplication?.namaPemohon)
        binding.sectionAb.edtNoKpPolisTenteraSyarikat.setText(paramApptItem?.tsApplication?.icNoPemohon)
        binding.sectionAb.edtAlamat.setText(address)
        binding.sectionAb.edtPoskod.setText(paramApptItem?.tsApplication?.poskod)
        binding.sectionAb.edtNoTelRumah.setText(paramApptItem?.tsApplication?.noPhoneHouse)
        binding.sectionAb.edtNoTelBimbit.setText(paramApptItem?.tsApplication?.noPhoneMobile)
        binding.sectionAb.edtNoPermohonan.setText(paramApptItem?.tsMohonId)

        binding.sectionAb.edtDaerah.setText(paramApptItem?.tsApplication?.daerah)
        binding.sectionAb.edtParlimen.setText(paramApptItem?.tsApplication?.parlimen)
        binding.sectionAb.edtDun.setText(paramApptItem?.tsApplication?.dun)
        binding.sectionAb.edtMukim.setText(paramApptItem?.tsApplication?.mukim)
        binding.sectionAb.edtKampung.setText(paramApptItem?.tsApplication?.kampung)
        binding.sectionAb.edtNoGeran.setText(paramApptItem?.tsApplication?.lotDimilikiList?.get(0)?.noGeran)
        binding.sectionAb.edtNoLot.setText(paramApptItem?.tsApplication?.lotDimilikiList?.get(0)?.noLot)

        binding.sectionAb.edtLuasKebunHektar.setText(
            paramApptItem?.tsApplication?.lotDimilikiList?.get(
                0
            )?.luasLot.toString()
        )
        binding.sectionAb.edtLuasKebunHendakDitanamSemulaHektar.setText(
            paramApptItem?.tsApplication?.lotDimilikiList?.get(
                0
            )?.luasTs.toString()
        )
    }

    private fun nextButtonListener() {
        binding.sectionAb.buttonAbNextAb.setOnClickListener { v ->
            scrollToTop()
            binding.sectionAb.sectionAb.visibility = View.GONE
            binding.sectionC.sectionC.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.c_laporan_pengesahan_lawatan_kebun)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionC.buttonNextC.setOnClickListener { v ->
            val lotTanamSemula = paramApptItem?.tsApplication?.lotDimilikiList?.get(0)?.luasTs

            val editTanamanGetahTua =
                binding.sectionC.edtKeluasanTanamanGetahTuaSekarang.text.toString().toDoubleOrNull()
                    ?: 0.0

            val edtTanamanLain =
                binding.sectionC.edtTanamanLain.text.toString().toDoubleOrNull() ?: 0.0
            val edtHutanTanahKosong =
                binding.sectionC.edtHutanTanahKosong.text.toString().toDoubleOrNull() ?: 0.0
            val edtRumahBangunan =
                binding.sectionC.edtRumahBangunan.text.toString().toDoubleOrNull() ?: 0.0
            val edtLainLainSilaNyatakanHek =
                binding.sectionC.edtLainLainSilaNyatakanHek.text.toString().toDoubleOrNull() ?: 0.0
            val totalTanamanLain =
                edtTanamanLain + edtHutanTanahKosong + edtRumahBangunan + edtLainLainSilaNyatakanHek

            if (lotTanamSemula != null
                && editTanamanGetahTua > lotTanamSemula
            ) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_luas_hektar_sekarang),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            if (lotTanamSemula != null
                && totalTanamanLain > lotTanamSemula
            ) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_luas_hektar_lain_lain),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            if (edtTanamanLain < 0.0 || edtHutanTanahKosong < 0.0
                || edtRumahBangunan < 0.0 || edtLainLainSilaNyatakanHek < 0.0
            ) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_hektar_negative),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            if (!sectionCMandatoryField()) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.mandatory),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionC.sectionC.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionUploadAttachment.buttonNextUploadAttachment.setOnClickListener { v ->
            if (filePaths.size < 2 && !BuildConfig.DEBUG) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.attachements_desc),
                    Snackbar.LENGTH_SHORT
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.sectionD.sectionD.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.d_perakuan_pegawai_pemeriksa_kebun)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionD.buttonNextD.setOnClickListener { v ->
            val edtTarikhLawatan = binding.sectionD.edtTarikhLawatan.text.toString()
            val edtMasaDari = binding.sectionD.edtMasaDari.text.toString()
            val edtMasaSampai = binding.sectionD.edtMasaSampai.text.toString()
            val signPegawai = DisplayUtils.getBase64String(binding.sectionD.imageViewSignD)
            if (edtTarikhLawatan.isEmpty() || edtMasaDari.isEmpty() || edtMasaSampai.isEmpty() || signPegawai.isEmpty()) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_not_filled),
                    Snackbar.LENGTH_LONG
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionD.sectionD.visibility = View.GONE
            binding.sectionF.sectionF.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.f_peringatan)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionF.buttonNextF.setOnClickListener { v ->
            if (!binding.sectionF.checkboxSetuju.isChecked) {
                binding.textviewTitle.errorSnack(
                    getString(R.string.error_not_checked),
                    Snackbar.LENGTH_LONG
                )
                return@setOnClickListener
            }

            scrollToTop()
            binding.sectionF.sectionF.visibility = View.GONE
            binding.sectionG.sectionG.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.g_perakuan_pemohon_wakilnya)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }
    }

    private fun previousButtonListener() {
        binding.sectionC.buttonPreviousC.setOnClickListener { v ->
            scrollToTop()
            binding.sectionC.sectionC.visibility = View.GONE
            binding.sectionAb.sectionAb.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.a_b_profil_pemohon_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionUploadAttachment.buttonPreviousUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.sectionC.sectionC.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.c_laporan_pengesahan_lawatan_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionD.buttonPreviousD.setOnClickListener { v ->
            scrollToTop()
            binding.sectionD.sectionD.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionF.buttonPreviousF.setOnClickListener { v ->
            scrollToTop()
            binding.sectionF.sectionF.visibility = View.GONE
            binding.sectionD.sectionD.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.d_perakuan_pegawai_pemeriksa_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionG.buttonPreviousG.setOnClickListener { v ->
            scrollToTop()
            binding.sectionG.sectionG.visibility = View.GONE
            binding.sectionF.sectionF.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.f_peringatan)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }
    }

    private fun sectionCMandatoryField(): Boolean {

        val radiogroupTunggulGetahTua =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupTunggulGetahTua.checkedRadioButtonId)?.text.toString())
        if (radiogroupTunggulGetahTua.isEmpty()) return false
        val radiogroupBukuLesenGetah =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupBukuLesenGetah.checkedRadioButtonId)?.text.toString())
        if (radiogroupBukuLesenGetah.isEmpty()) return false
        val radiogroupGambarSatelit =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupGambarSatelit.checkedRadioButtonId)?.text.toString())
        if (radiogroupGambarSatelit.isEmpty()) return false
        val radiogroupLesenGetah =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupLesenGetah.checkedRadioButtonId)?.text.toString())
        if (radiogroupLesenGetah.isEmpty()) return false
        val radiogroupSuratPengesahan =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupSuratPengesahan.checkedRadioButtonId)?.text.toString())
        if (radiogroupSuratPengesahan.isEmpty()) return false
        val radiogroupRekodYangMembukitkan =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupRekodYangMembukitkan.checkedRadioButtonId)?.text.toString())
        if (radiogroupRekodYangMembukitkan.isEmpty()) return false
        val radiogroupSilaSertakan =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupSilaSertakan.checkedRadioButtonId)?.text.toString())
        if (radiogroupSilaSertakan.isEmpty()) return false
        val radiogroupUmurPokok =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupUmurPokok.checkedRadioButtonId)?.text.toString())
        if (radiogroupUmurPokok.isEmpty()) return false
        val radiogroupKeadaanPokok =
            replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupKeadaanPokok.checkedRadioButtonId)?.text.toString())
        if (radiogroupKeadaanPokok.isEmpty()) return false

        if (binding.sectionC.rbKeadaanPokokTidakEkonomik.isChecked) {
            val radiogroupHasil500 =
                replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupHasil500.checkedRadioButtonId)?.text.toString())
            if (radiogroupHasil500.isEmpty()) return false
            val radiogroupHabisKulit =
                replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupHabisKulit.checkedRadioButtonId)?.text.toString())
            if (radiogroupHabisKulit.isEmpty()) return false
            val radiogroupPokokGetah =
                replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupPokokGetah.checkedRadioButtonId)?.text.toString())
            if (radiogroupPokokGetah.isEmpty()) return false
            val radiogroupLainLainSebab =
                replaceNullToEmptyString(activity?.findViewById<RadioButton>(binding.sectionC.radiogroupLainLainSebab.checkedRadioButtonId)?.text.toString())
            if (radiogroupLainLainSebab.isEmpty()) return false
        }

        if(!binding.sectionC.checkboxMengemaskini.isChecked) return false

        val edtKeluasanTanamanGetahTuaSekarang = binding.sectionC.edtKeluasanTanamanGetahTuaSekarang.text.toString()
        if (edtKeluasanTanamanGetahTuaSekarang.isEmpty()) {
            return false
        }

        return true
    }

    private fun cameraButtonListener() {
        binding.sectionUploadAttachment.cameraButton.setOnClickListener {
            if (filePaths.size >= 4) {
                // not allow to add more than 4 pictures
                return@setOnClickListener
            }
            try {
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

                // Create the File where the photo should go
                var photoFile: File? = null
                try {
                    photoFile =
                        context?.let { it1 -> PhotoUtils.createImageFile(it1, latitude, longitude) }
                    // Save a file: path for use with ACTION_VIEW intents
                    photoFile?.let { it1 -> filePaths.add(it1.absolutePath) }
                } catch (e: IOException) {
                    Log.e(TAG, e.printStackTrace().toString())
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    photoURI = FileProvider.getUriForFile(
                        context!!,
                        getString(R.string.authority),
                        photoFile
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, Constants.REQUEST_IMAGE_CAPTURE)
                }
            } catch (e: Exception) {
                Log.e(TAG, e.printStackTrace().toString())
                binding.textviewTitle.errorSnack(
                    e.message.toString(),
                    Snackbar.LENGTH_SHORT
                )
            }
        }
    }

    private fun scrollToTop() {
        binding.scrollview.fullScroll(View.FOCUS_UP)
        binding.scrollview.smoothScrollTo(0, 0)
    }

    private fun showPrintAlertDialog() {
        binding.sectionG.buttonSaveG.isEnabled = false

        if (!PrintUtils.checkBluetoothStatus()) {
            binding.textviewTitle.showSnack(
                getString(R.string.ble_not_enabled),
                Snackbar.LENGTH_SHORT
            )
            return
        }

        val edtTanamanLain = binding.sectionC.edtTanamanLain.text.toString().toDoubleOrNull() ?: 0.0
        val edtHutanTanahKosong =
            binding.sectionC.edtHutanTanahKosong.text.toString().toDoubleOrNull() ?: 0.0
        val edtRumahBangunan =
            binding.sectionC.edtRumahBangunan.text.toString().toDoubleOrNull() ?: 0.0
        val edtLainLainSilaNyatakanHek =
            binding.sectionC.edtLainLainSilaNyatakanHek.text.toString().toDoubleOrNull() ?: 0.0

        val form = Form()
        form.luasTanamanLain = edtTanamanLain.toString()
        form.luasHutan = edtHutanTanahKosong.toString()
        form.luasRumah = edtRumahBangunan.toString()
        form.luasLainLain = edtLainLainSilaNyatakanHek.toString()

        val reportItem = ReportItem(paramApptItem, form)

        val printerDialogFragment =
            PrinterDialogFragment(reportItem = reportItem, appointmentItem = paramApptItem)
        fragmentManager?.let { printerDialogFragment.show(it, TAG) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.REQUEST_IMAGE_CAPTURE) {
            if (resultCode == Activity.RESULT_OK) {

                try {
                    val bitmap =
                        MediaStore.Images.Media.getBitmap(context!!.contentResolver, photoURI);

                    val inflater =
                        activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

                    val attachmentView: View = inflater.inflate(R.layout.item_camera, null)
                    attachmentView.id = filePaths.size

                    val imageView =
                        attachmentView.findViewById<ImageView>(R.id.image_view_attachment)
                    imageView.setImageBitmap(bitmap)
                    val imageName = attachmentView.findViewById<TextView>(R.id.tv_image_name)
                    val imageFileName = "image${SimpleDateFormat("HHmmss").format(Date())}\n${latitude}, ${longitude}"
                    imageName.setText(imageFileName)
                    val deleteButton = attachmentView.findViewById<Button>(R.id.delete_button)
                    deleteButton.tag = filePaths.size
                    deleteButton.setOnClickListener {
                        try {
                            binding.sectionUploadAttachment.linearLayoutAttachment.removeViewAt(it.tag as Int - 1)
                            filePaths.removeAt(it.tag as Int - 1)
                            imageView.setImageBitmap(null)
                        } catch (e: Exception) {
                            Log.e(TAG, e.message.toString())
                        }
                    }
                    binding.sectionUploadAttachment.linearLayoutAttachment.addView(attachmentView)

                } catch (e: IOException) {
                    e.printStackTrace();
                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
                binding.sectionUploadAttachment.cameraButton.errorSnack(
                    getString(R.string.cancelled),
                    Snackbar.LENGTH_SHORT
                )
            }
        }
    }

    companion object {
        val TAG = Ts18Fragment::class.java.simpleName!!

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param paramApptItem Parameter 1.
         * @return A new instance of fragment Ts18Fragment.
         */
        @JvmStatic
        fun newInstance(paramApptItem: AppointmentItem, paramFormIndex: Int) =
            Ts18Fragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM_APPT_ITEM, paramApptItem)
                    putInt(ARG_PARAM_FORM_INDEX, paramFormIndex)
                }
            }

    }
}