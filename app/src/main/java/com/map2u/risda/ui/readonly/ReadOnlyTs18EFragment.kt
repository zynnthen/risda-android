package com.map2u.risda.ui.readonly

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.map2u.risda.R
import com.map2u.risda.databinding.FragmentTs18EBinding
import com.map2u.risda.model.appointment.PendingVerify
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.ui.shared.OnSignedCaptureListener
import com.map2u.risda.util.*
import com.map2u.risda.util.display.DisplayUtils
import com.map2u.risda.viewmodel.Ts18EViewModel
import com.map2u.risda.viewmodel.ViewModelProviderFactory
import kotlinx.coroutines.*
import java.io.ByteArrayOutputStream
import java.util.*

class ReadOnlyTs18EFragment : Fragment(), OnSignedCaptureListener {
    val ARG_PARAM_PENDING_VERIFY = "ARG_PARAM_PENDING_VERIFY"
    var paramPendingVerify: PendingVerify? = null

    private var _binding: FragmentTs18EBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private lateinit var viewModel: Ts18EViewModel

    // This property is for image upload
    private val filePaths: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            paramPendingVerify = it.getParcelable(ARG_PARAM_PENDING_VERIFY)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTs18EBinding.inflate(inflater, container, false)
        val view = binding.root

        setupViewModel()

        /*** Disable edit text, radio button ***/
        val ll: LinearLayout? = binding.rootLinearLayout
        for (view in ll!!.touchables) {
            if (view is EditText) {
                val editText = view
                editText.setTextColor(context!!.getColor(R.color.colorPrimaryText))
                editText.isEnabled = false
                editText.isFocusable = false
                editText.isFocusableInTouchMode = false

            } else if (view is RadioButton) {
                val radioButton = view
                radioButton.isEnabled = false
                radioButton.buttonTintList =
                    ColorStateList.valueOf(context!!.getColor(R.color.colorPrimaryDark))
                radioButton.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.md_grey_300)))

            }
        }

        binding.sectionC.sectionC.visibility = View.GONE
        binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
        binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.GONE
        binding.sectionD.sectionD.visibility = View.GONE
        binding.sectionF.sectionF.visibility = View.GONE

        binding.seekBar.setOnTouchListener(View.OnTouchListener { v, event ->
            // True doesn't propagate the event
            // the user cannot move the indicator
            true
        })

        /*** Next button listener ***/
        nextButtonListener()

        /*** Previous button listener ***/
        previousButtonListener()

        initAbContent()
        initCContent()
        initAttachmentsContent()
        initDContent()
        initFContent()

        /*** Section C if not economical radio button listener ***/
        binding.sectionC.rbKeadaanPokokTidakEkonomik2.setOnCheckedChangeListener { v, isChecked ->
            for (i in 0 until binding.sectionC.radiogroupHasil5002.getChildCount()) {
                binding.sectionC.radiogroupHasil5002.getChildAt(i).isEnabled = isChecked
                binding.sectionC.radiogroupHasil5002.clearCheck()
            }
            for (i in 0 until binding.sectionC.radiogroupHabisKulit2.getChildCount()) {
                binding.sectionC.radiogroupHabisKulit2.getChildAt(i).isEnabled = isChecked
                binding.sectionC.radiogroupHabisKulit2.clearCheck()
            }
            for (i in 0 until binding.sectionC.radiogroupPokokGetah2.getChildCount()) {
                binding.sectionC.radiogroupPokokGetah2.getChildAt(i).isEnabled = isChecked
                binding.sectionC.radiogroupPokokGetah2.clearCheck()
            }
            for (i in 0 until binding.sectionC.radiogroupLainLainSebab2.getChildCount()) {
                binding.sectionC.radiogroupLainLainSebab2.getChildAt(i).isEnabled = isChecked
                binding.sectionC.radiogroupLainLainSebab2.clearCheck()
            }
        }

        return view
    }

    private fun setupViewModel() {
        val repository = AppRepository()
        val factory = activity?.let { ViewModelProviderFactory(it.application, repository) }
        viewModel = factory?.let { ViewModelProvider(this, it).get(Ts18EViewModel::class.java) }!!
    }

    private fun initAbContent() {
        val address =
            "${paramPendingVerify?.tsApplication?.address1} ${paramPendingVerify?.tsApplication?.address2} ${paramPendingVerify?.tsApplication?.address3}"
        binding.sectionAb.edtNama.setText(paramPendingVerify?.tsApplication?.namaPemohon)
        binding.sectionAb.edtNoKpPolisTenteraSyarikat.setText(paramPendingVerify?.tsApplication?.icNoPemohon)
        binding.sectionAb.edtAlamat.setText(address)
        binding.sectionAb.edtPoskod.setText(paramPendingVerify?.tsApplication?.poskod)
        binding.sectionAb.edtNoTelRumah.setText(paramPendingVerify?.tsApplication?.noPhoneHouse)
        binding.sectionAb.edtNoTelBimbit.setText(paramPendingVerify?.tsApplication?.noPhoneMobile)
        binding.sectionAb.edtNoPermohonan.setText(paramPendingVerify?.tsMohonId)

        binding.sectionAb.edtDaerah.setText(paramPendingVerify?.tsApplication?.daerah)
        binding.sectionAb.edtParlimen.setText(paramPendingVerify?.tsApplication?.parlimen)
        binding.sectionAb.edtDun.setText(paramPendingVerify?.tsApplication?.dun)
        binding.sectionAb.edtMukim.setText(paramPendingVerify?.tsApplication?.mukim)
        binding.sectionAb.edtKampung.setText(paramPendingVerify?.tsApplication?.kampung)
        binding.sectionAb.edtNoGeran.setText(
            paramPendingVerify?.tsApplication?.lotDimilikiList?.get(
                0
            )?.noGeran
        )
        binding.sectionAb.edtNoLot.setText(paramPendingVerify?.tsApplication?.lotDimilikiList?.get(0)?.noLot)
        binding.sectionAb.edtLuasKebunHektar.setText(
            paramPendingVerify?.tsApplication?.lotDimilikiList?.get(
                0
            )?.luasLot.toString()
        )
        binding.sectionAb.edtLuasKebunHendakDitanamSemulaHektar.setText(
            paramPendingVerify?.tsApplication?.lotDimilikiList?.get(
                0
            )?.luasTs.toString()
        )

        if (paramPendingVerify != null) {
            binding.sectionAb.radiogroupMenerimaBantuan.visibility = View.GONE
            binding.sectionAb.textviewMenerimaBantuan.visibility = View.VISIBLE

            if (paramPendingVerify!!.bantuanLain?.trim() == "Ya") {
                binding.sectionAb.textviewMenerimaBantuan.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.bantuanLain?.trim() == "Tidak") {
                binding.sectionAb.textviewMenerimaBantuan.text = getString(R.string.tidak)
            }

            // jika ya, nyatakan
            binding.sectionAb.edtNamaAgensiPemberiBantuan.setText(
                paramPendingVerify!!.blAgensi
            )
            binding.sectionAb.edtLuasDilulus.setText(paramPendingVerify!!.blLuasLulus.toString())
            binding.sectionAb.edtJenisTanaman.setText(paramPendingVerify!!.blCrop)
        }

    }

    private fun initCContent() {
        if (paramPendingVerify != null) {
            binding.sectionC.textviewKeluasanTanamanGetahTuaSekarang.text = paramPendingVerify!!.luasGetahTua.toString()
            binding.sectionC.textviewTanamanLain.text = paramPendingVerify!!.luasTanamanLain.toString()
            binding.sectionC.textviewHutanTanahKosong.text = paramPendingVerify!!.luasHutan.toString()
            binding.sectionC.textviewRumahBangunan.text = paramPendingVerify!!.luasRumah.toString()
            binding.sectionC.textviewLainLainSilaNyatakan.text = paramPendingVerify!!.lainLain.toString()
            binding.sectionC.textviewLainLainLuasHektar.text = paramPendingVerify!!.luasLainLain.toString()

            if (paramPendingVerify!!.tunggulGetah?.trim() == "Ya") {
                binding.sectionC.textviewTunggulGetahTua.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.tunggulGetah?.trim() == "Tidak") {
                binding.sectionC.textviewTunggulGetahTua.text = getString(R.string.tidak)
            }

            if (paramPendingVerify!!.bukuLesen?.trim() == "Ya") {
                binding.sectionC.textviewBukuLesenGetah.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.bukuLesen?.trim() == "Tidak") {
                binding.sectionC.textviewBukuLesenGetah.text = getString(R.string.tidak)
            }

            if (paramPendingVerify!!.gambarSatelit?.trim() == "Ya") {
                binding.sectionC.textviewGambarSatelit.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.gambarSatelit?.trim() == "Tidak") {
                binding.sectionC.textviewGambarSatelit.text = getString(R.string.tidak)
            }

            if (paramPendingVerify!!.lesenGetah?.trim() == "Ya") {
                binding.sectionC.textviewLesenGetah.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.lesenGetah?.trim() == "Tidak") {
                binding.sectionC.textviewLesenGetah.text = getString(R.string.tidak)
            }

            if (paramPendingVerify!!.suratAgensi?.trim() == "Ya") {
                binding.sectionC.textviewSuratPengesahan.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.lesenGetah?.trim() == "Tidak") {
                binding.sectionC.textviewSuratPengesahan.text = getString(R.string.tidak)
            }

            if (paramPendingVerify!!.buktiLotTs?.trim() == "Ya") {
                binding.sectionC.textviewRekodYangMembukitkan.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.buktiLotTs?.trim() == "Tidak") {
                binding.sectionC.textviewRekodYangMembukitkan.text = getString(R.string.tidak)
            }

            if (paramPendingVerify!!.buktiLot?.trim() == "Ya") {
                binding.sectionC.textviewSilaSertakan.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.buktiLot?.trim() == "Tidak") {
                binding.sectionC.textviewSilaSertakan.text = getString(R.string.tidak)
            }

            if (paramPendingVerify!!.umurPokok?.trim() == "<20tahun") {
                binding.sectionC.textviewUmurPokok.text = getString(R.string.ya)
            } else if (paramPendingVerify!!.umurPokok?.trim() == ">=20tahun") {
                binding.sectionC.textviewUmurPokok.text = getString(R.string.tidak)
            }

            if (paramPendingVerify!!.getahTua?.trim() == "Ekonomik") {
                binding.sectionC.textviewKeadaanPokok.text = getString(R.string.ekonomik)
            } else if (paramPendingVerify!!.getahTua?.trim() == "Tidak ekonomik") {
                binding.sectionC.textviewKeadaanPokok.text = getString(R.string.tidak_ekonomik)

                if (paramPendingVerify!!.pokokK300?.trim() == "Ya") {
                    binding.sectionC.textviewHasil500.text = getString(R.string.ya)
                } else if (paramPendingVerify!!.pokokK300?.trim() == "Tidak") {
                    binding.sectionC.textviewHasil500.text = getString(R.string.tidak)
                }

                if (paramPendingVerify!!.kulitTorehan?.trim() == "Ya") {
                    binding.sectionC.textviewHabisKulit.text = getString(R.string.ya)
                } else if (paramPendingVerify!!.kulitTorehan?.trim() == "Tidak") {
                    binding.sectionC.textviewHabisKulit.text = getString(R.string.tidak)
                }

                if (paramPendingVerify!!.hasilK500?.trim() == "Ya") {
                    binding.sectionC.textviewPokokGetah.text = getString(R.string.ya)
                } else if (paramPendingVerify!!.hasilK500?.trim() == "Tidak") {
                    binding.sectionC.textviewPokokGetah.text = getString(R.string.tidak)
                }

                if (paramPendingVerify!!.sebabLainPRN?.trim() == "Ya") {
                    binding.sectionC.textviewLainLainSebab.text = getString(R.string.ya)
                } else if (paramPendingVerify!!.sebabLainPRN?.trim() == "Tidak") {
                    binding.sectionC.textviewLainLainSebab.text = getString(R.string.tidak)
                }
            }

            val items = paramPendingVerify?.ts18e?.items
            binding.sectionC.edtKeluasanTanamanGetahTuaSekarang2.setText(items?.find { it.item == "Luas_GetahTua" }?.catatan)
            binding.sectionC.edtTanamanLain2.setText(items?.find { it.item == "Luas_TanamanLain" }?.catatan)
            binding.sectionC.edtHutanTanahKosong2.setText(items?.find { it.item == "Luas_Hutan" }?.catatan)
            binding.sectionC.edtRumahBangunan2.setText(items?.find { it.item == "Luas_Rumah" }?.catatan)
            binding.sectionC.edtLainLainSilaNyatakan2.setText(items?.find { it.item == "Luas_Lain" }?.catatan)
            binding.sectionC.edtLainLainSilaNyatakanHek2.setText(items?.find { it.item == "Luas_LainLain" }?.catatan)

            if (items?.find { it.item == "Tunggul_Getah" }?.catatan?.trim() == "Ya") {
                binding.sectionC.rbTunggulGetahTuaYa2.isChecked = true
            } else if (items?.find { it.item == "Tunggul_Getah" }?.catatan?.trim() == "Tidak") {
                binding.sectionC.rbTunggulGetahTuaTidak2.isChecked = true
            }

            if (items?.find { it.item == "Buku_Lesen" }?.catatan?.trim() == "Ya") {
                binding.sectionC.rbBukuLesenGetahYa2.isChecked = true
            } else if (items?.find { it.item == "Buku_Lesen" }?.catatan?.trim() == "Tidak") {
                binding.sectionC.rbBukuLesenGetahTidak2.isChecked = true
            }

            if (items?.find { it.item == "Gambar_Satelit" }?.catatan?.trim() == "Ya") {
                binding.sectionC.rbGambarSatelitYa2.isChecked = true
            } else if (items?.find { it.item == "Gambar_Satelit" }?.catatan?.trim() == "Tidak") {
                binding.sectionC.rbGambarSatelitTidak2.isChecked = true
            }

            if (items?.find { it.item == "Lesen_Getah" }?.catatan?.trim() == "Ya") {
                binding.sectionC.rbLesenGetahYa2.isChecked = true
            } else if (items?.find { it.item == "Lesen_Getah" }?.catatan?.trim() == "Tidak") {
                binding.sectionC.rbLesenGetahTidak2.isChecked = true
            }

            if (items?.find { it.item == "Surat_Agensi" }?.catatan?.trim() == "Ya") {
                binding.sectionC.rbSuratPengesahanYa2.isChecked = true
            } else if (items?.find { it.item == "Surat_Agensi" }?.catatan?.trim() == "Tidak") {
                binding.sectionC.rbSuratPengesahanTidak2.isChecked = true
            }

            if (items?.find { it.item == "Bukti_Lot_TS" }?.catatan?.trim() == "Ya") {
                binding.sectionC.rbRekodYangMembukitkanYa2.isChecked = true
            } else if (items?.find { it.item == "Bukti_Lot_TS" }?.catatan?.trim() == "Tidak") {
                binding.sectionC.rbRekodYangMembukitkanTidak2.isChecked = true
            }

            if (items?.find { it.item == "Bukti_lot" }?.catatan?.trim() == "Ya") {
                binding.sectionC.rbSilaSertakanYa2.isChecked = true
            } else if (items?.find { it.item == "Bukti_lot" }?.catatan?.trim() == "Tidak") {
                binding.sectionC.rbSilaSertakanTidak2.isChecked = true
            }

            if (items?.find { it.item == "Umur_Pokok" }?.catatan?.trim() == "<20tahun") {
                binding.sectionC.rbUmurPokok202.isChecked = true
            } else if (items?.find { it.item == "Umur_Pokok" }?.catatan?.trim() == ">=20tahun") {
                binding.sectionC.rbUmurPokokMore202.isChecked = true
            }

            if (items?.find { it.item == "Getah_Tua" }?.catatan?.trim() == "Ekonomik") {
                binding.sectionC.rbKeadaanPokokEkonomik2.isChecked = true
            } else if (items?.find { it.item == "Getah_Tua" }?.catatan?.trim() == "Tidak ekonomik") {
                binding.sectionC.rbKeadaanPokokTidakEkonomik2.isChecked = true
                val color = ColorStateList.valueOf(context!!.getColor(R.color.colorPrimaryDark))

                binding.sectionC.rbHasil500Ya2.buttonTintList = color
                binding.sectionC.rbHasil500Tidak2.buttonTintList = color
                if (items?.find { it.item == "Pokok_K300" }?.catatan?.trim() == "Ya") {
                    binding.sectionC.rbHasil500Ya2.isChecked = true
                } else if (items?.find { it.item == "Pokok_K300" }?.catatan?.trim() == "Tidak") {
                    binding.sectionC.rbHasil500Tidak2.isChecked = true
                }

                binding.sectionC.rbHabisKulitYa2.buttonTintList = color
                binding.sectionC.rbHabisKulitTidak2.buttonTintList = color
                if (items?.find { it.item == "Kulit_Torehan" }?.catatan?.trim() == "Ya") {
                    binding.sectionC.rbHabisKulitYa2.isChecked = true
                } else if (items?.find { it.item == "Kulit_Torehan" }?.catatan?.trim() == "Tidak") {
                    binding.sectionC.rbHabisKulitTidak2.isChecked = true
                }

                binding.sectionC.rbPokokGetahYa2.buttonTintList = color
                binding.sectionC.rbPokokGetahTidak2.buttonTintList = color
                if (items?.find { it.item == "Hasil_K500" }?.catatan?.trim() == "Ya") {
                    binding.sectionC.rbPokokGetahYa2.isChecked = true
                } else if (items?.find { it.item == "Hasil_K500" }?.catatan?.trim() == "Tidak") {
                    binding.sectionC.rbPokokGetahTidak2.isChecked = true
                }

                binding.sectionC.rbLainLainSebabYa2.buttonTintList = color
                binding.sectionC.rbLainLainSebabTidak2.buttonTintList = color
                if (items?.find { it.item == "Sebab_Lain_PRN" }?.catatan?.trim() == "Ya") {
                    binding.sectionC.rbLainLainSebabYa2.isChecked = true
                } else if (items?.find { it.item == "Sebab_Lain_PRN" }?.catatan?.trim() == "Tidak") {
                    binding.sectionC.rbLainLainSebabTidak2.isChecked = true
                }
            }
        }
    }

    private fun initAttachmentsContent() {
        binding.sectionUploadAttachment.cameraButton.visibility = View.GONE
        binding.sectionUploadAttachment.textviewMandatory.visibility = View.GONE

        binding.sectionUploadAttachmentVerify.cameraButton.visibility = View.GONE
        binding.sectionUploadAttachmentVerify.textviewMandatory.visibility = View.GONE

        GlobalScope.launch {
            callApiAndUpdateLiveData()
        }
    }

    private suspend fun callApiAndUpdateLiveData() {
        paramPendingVerify?.documents?.forEach {
            coroutineScope {
                async {
                    val result = it.id?.let { file -> viewModel.getFile(file) }
                }
            }.await()
        }

        paramPendingVerify?.ts18e?.documents?.forEach {
            coroutineScope {
                async {
                    val result = it.id?.let { file -> viewModel.getVerifyFile(file) }
                }
            }.await()
        }

        lifecycleScope.launch {
            withContext(Dispatchers.Main) {
                viewModel.apiFileResponse.observe(this@ReadOnlyTs18EFragment, { event ->
                    event.getContentIfNotHandled()?.let { response ->
                        when (response) {
                            is Resource.Success -> {
                                response.data?.let { response ->
                                    val bitmap =
                                        BitmapFactory.decodeStream(response.byteStream())

                                    val inflater =
                                        activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

                                    val attachmentView: View =
                                        inflater.inflate(R.layout.item_camera, null)
                                    val imageView =
                                        attachmentView.findViewById<ImageView>(R.id.image_view_attachment)
                                    imageView.setImageBitmap(bitmap)
                                    val deleteButton =
                                        attachmentView.findViewById<Button>(R.id.delete_button)
                                    deleteButton.visibility = View.GONE

                                    binding.sectionUploadAttachment.linearLayoutAttachment.addView(
                                        attachmentView
                                    )
                                }
                            }
                        }
                    }
                })

                viewModel.apiVerifyFileResponse.observe(this@ReadOnlyTs18EFragment, { event ->
                    event.getContentIfNotHandled()?.let { response ->
                        when (response) {
                            is Resource.Success -> {
                                response.data?.let { response ->
                                    val bitmap =
                                        BitmapFactory.decodeStream(response.byteStream())

                                    val inflater =
                                        activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

                                    val attachmentView: View =
                                        inflater.inflate(R.layout.item_camera, null)
                                    val imageView =
                                        attachmentView.findViewById<ImageView>(R.id.image_view_attachment)
                                    imageView.setImageBitmap(bitmap)
                                    val deleteButton =
                                        attachmentView.findViewById<Button>(R.id.delete_button)
                                    deleteButton.visibility = View.GONE

                                    binding.sectionUploadAttachmentVerify.linearLayoutAttachment.addView(
                                        attachmentView
                                    )
                                }
                            }
                        }
                    }
                })
            }
        }
    }

    private fun initDContent() {
        binding.sectionD.buttonSignHereD.visibility = View.GONE

        binding.sectionD.edtTarikhLawatan.setText(TimeUtils.displayDate(paramPendingVerify?.ts18e?.tkhLawat ?: 0))
        binding.sectionD.edtMasaDari2.setText(TimeUtils.displayTime(paramPendingVerify?.ts18e?.mlawatMula ?: 0))
        binding.sectionD.edtMasaSampai2.setText(TimeUtils.displayTime(paramPendingVerify?.ts18e?.mlawatTamat ?: 0))
        binding.sectionD.imageViewSignD.setImageBitmap(paramPendingVerify!!.ts18e?.signPegawai.let {
            it?.let { it1 ->
                DisplayUtils.getBitmapFromBase64String(
                    it1
                )
            }
        })
        binding.sectionD.edtCatatan2.setText(paramPendingVerify?.ts18e?.catatan)
    }

    private fun initFContent() {
        binding.sectionF.checkboxSetuju.isClickable = false
        binding.sectionF.checkboxSetuju.isChecked = true
        binding.sectionF.buttonSaveF.visibility = View.GONE
        binding.sectionF.buttonNextF.visibility = View.GONE
    }

    private fun nextButtonListener() {
        binding.sectionAb.buttonAbNextAb.setOnClickListener { v ->
            scrollToTop()
            binding.sectionAb.sectionAb.visibility = View.GONE
            binding.sectionC.sectionC.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.c_laporan_pengesahan_lawatan_kebun)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionC.buttonNextC.setOnClickListener { v ->
            scrollToTop()
            binding.sectionC.sectionC.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionUploadAttachment.buttonNextUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments_pengesahan)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionUploadAttachmentVerify.buttonNextUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.GONE
            binding.sectionD.sectionD.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.d_pengesahan_pegawai_penilai)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionD.buttonNextD.setOnClickListener { v ->
            scrollToTop()
            binding.sectionD.sectionD.visibility = View.GONE
            binding.sectionF.sectionF.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.f_peringatan)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

    }

    private fun previousButtonListener() {
        binding.sectionC.buttonPreviousC.setOnClickListener { v ->
            scrollToTop()
            binding.sectionC.sectionC.visibility = View.GONE
            binding.sectionAb.sectionAb.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.a_b_profil_pemohon_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionUploadAttachment.buttonPreviousUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.sectionC.sectionC.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.c_laporan_pengesahan_lawatan_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionUploadAttachmentVerify.buttonPreviousUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionD.buttonPreviousD.setOnClickListener { v ->
            scrollToTop()
            binding.sectionD.sectionD.visibility = View.GONE
            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments_pengesahan)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionF.buttonPreviousF.setOnClickListener { v ->
            scrollToTop()
            binding.sectionF.sectionF.visibility = View.GONE
            binding.sectionD.sectionD.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.d_pengesahan_pegawai_penilai)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

    }

    override fun onSignatureCaptured(bitmap: Bitmap, fileName: String) {
        when (fileName) {
            "D" -> {
                binding.sectionD.imageViewSignD.setImageBitmap(bitmap)
            }
        }
    }

    private fun scrollToTop() {
        binding.scrollview.fullScroll(View.FOCUS_UP)
        binding.scrollview.smoothScrollTo(0, 0)
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes)
        val path: String =
            MediaStore.Images.Media.insertImage(
                inContext.contentResolver,
                inImage,
                TimeUtils.getCurrentTimestamp().toString(),
                null
            )
        return Uri.parse(path)
    }

    fun getRealPathFromURI(uri: Uri?): String? {
        var path = ""
        if (activity!!.contentResolver != null) {
            val cursor: Cursor? =
                uri?.let { activity?.contentResolver?.query(it, null, null, null, null) }
            if (cursor != null) {
                cursor.moveToFirst()
                val idx: Int = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()
            }
        }
        return path
    }

    companion object {
        val TAG = ReadOnlyTs18EFragment::class.java.simpleName!!

        @JvmStatic
        fun newInstance(paramPendingVerify: PendingVerify) =
            ReadOnlyTs18EFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM_PENDING_VERIFY, paramPendingVerify)
                }
            }

    }
}