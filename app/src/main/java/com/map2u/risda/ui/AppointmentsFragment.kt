package com.map2u.risda.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.map2u.risda.R
import com.map2u.risda.adapter.AppointmentsAdapter
import com.map2u.risda.databinding.FragmentAppointmentsBinding
import com.map2u.risda.model.appointment.AppointmentItem
import com.map2u.risda.model.appointment.Form
import com.map2u.risda.model.appointment.Participant
import com.map2u.risda.network.RequestBodies
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.ui.shared.SearchDialogFragment
import com.map2u.risda.util.*
import com.map2u.risda.util.Constants.TARGET_FRAGMENT_REQUEST_CODE
import com.map2u.risda.util.display.DisplayUtils
import com.map2u.risda.viewmodel.AppointmentViewModel
import com.map2u.risda.viewmodel.ViewModelProviderFactory
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [AppointmentsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AppointmentsFragment : Fragment() {

    lateinit var viewModel: AppointmentViewModel
    lateinit var appointmentsAdapter: AppointmentsAdapter

    private var _binding: FragmentAppointmentsBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    // This variables is to handle search counts
    private var searchFieldsCount = 0
    lateinit var searchBadgeTextView: TextView

    var noPermohonanPlaceholder: String? = null
    var tarikhTemujanjiPlaceholder: String? = null
    var kpPemohonPlaceholder: String? = null
    var namaPemohonPlaceholder: String? = null
    var noLotPlaceholder: String? = null
    var noGeranPlaceholder: String? = null

    // This variables is to handle number of appointment's day selected
    var selectedApptDays: Int = 5

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAppointmentsBinding.inflate(inflater, container, false)
        val view = binding.root

        init()

        return view
    }

    private fun init() {
        appointmentsAdapter = AppointmentsAdapter() { appointmentItem: AppointmentItem ->
            if(appointmentItem.tkhTJanji!! <= TimeUtils.get1159pmTimestamp()) {
                navigateToForm(
                    appointmentItem
                )
            } else {
                binding.progressbarAppointments.errorSnack(
                    getString(R.string.error_tarikh),
                    Snackbar.LENGTH_SHORT
                )
            }
        }
        setupViewModel()
    }

    private fun setupViewModel() {
        val repository = AppRepository()
        val factory = activity?.let { ViewModelProviderFactory(it.application, repository) }
        viewModel =
            factory?.let { ViewModelProvider(this, it).get(AppointmentViewModel::class.java) }!!
        getAppointments()
    }

    private fun getAppointments(numberOfDays: Int = 5, isDownloadOffline: Boolean = false) {
        viewModel.getLatestAppointments(
            numberOfDays = numberOfDays,
            tsMohonId = noPermohonanPlaceholder,
            noKpPemohon = kpPemohonPlaceholder,
            namaPemohon = namaPemohonPlaceholder,
            noLot = noLotPlaceholder,
            noGeran = noGeranPlaceholder,
            date = tarikhTemujanjiPlaceholder,
            isDownloadOffline = isDownloadOffline
        )
        viewModel.apiResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        hideProgressBar()
                        response.data?.let { response ->
                            if (response.datums?.size == 0) {
                                val noAppt = getString(R.string.no_appt_available, selectedApptDays)
                                binding.textviewNoAppt.text = noAppt
                                binding.textviewNoAppt.visibility = View.VISIBLE
                            }
                            val sortedList = response.datums?.sortedWith(
                                compareBy({ it.tkhTJanji },
                                    { it.masaTJanji })
                            )?.toMutableList()
                            appointmentsAdapter.differ.submitList(sortedList)
                            binding.rvAppointments.adapter = appointmentsAdapter

                            if(response.isDownloadOffline) {
                                binding.progressbarAppointments.showSnack(
                                    getString(R.string.download_completed),
                                    Snackbar.LENGTH_SHORT
                                )
                            }
                        }
                    }

                    is Resource.NoConnection -> {
                        hideProgressBar()
                        val appointments = viewModel.queryAppointmentOffline(Constants.BARU)
                        if (appointments?.size ?: 0 == 0) {
                            val noAppt = getString(R.string.no_appt_available, selectedApptDays)
                            binding.textviewNoAppt.text = noAppt
                            binding.textviewNoAppt.visibility = View.VISIBLE
                        } else {
                            val sortedList = appointments?.sortedWith(
                                compareBy({ it.tkhTJanji },
                                    { it.masaTJanji })
                            )?.toMutableList()
                            appointmentsAdapter.differ.submitList(sortedList)
                            binding.rvAppointments.adapter = appointmentsAdapter
                        }
                    }

                    is Resource.Error -> {
                        hideProgressBar()
                        response.message?.let { message ->
                            binding.progressbarAppointments.errorSnack(
                                message,
                                Snackbar.LENGTH_SHORT
                            )
                        }
                    }

                    is Resource.Loading -> {
                        showProgressBar()
                    }
                }
            }
        })
    }

    private fun navigateToForm(appointmentItem: AppointmentItem) {
        val jenisBorang = DisplayUtils.getJenisBorang(
            appointmentItem.jenisLK,
            appointmentItem.moduleOwner,
            appointmentItem.tsPendekatan
        )

        if (jenisBorang.contains("TS18A") &&
            appointmentItem.jenisLK?.equals("ts18a", true) == true
        ) {
            selectParticipant(
                jenisBorang,
                appointmentItem,
                appointmentItem.participantList!!,
                appointmentItem.formList!!
            )
        } else {
            openForm(jenisBorang, appointmentItem, null)
        }

    }

    private fun selectParticipant(
        jenisBorang: String,
        appointmentItem: AppointmentItem,
        participantList: ArrayList<Participant>,
        formList: ArrayList<Form>,
    ) {
        // setup the alert builder
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle(getString(R.string.choose_form))

        val formListTsMohonIds = formList.map { it.tsMohonId }
        val result = participantList.filter { it.tsmohonID !in formListTsMohonIds }

        // add a list
        val names = result.map { it.tsApplication?.namaPemohon + " (" + it.tsmohonID + ")" }
            .toTypedArray()
        builder.setItems(
            names
        ) { dialog, which ->
            openForm(jenisBorang, appointmentItem, result[which])
        }
        if (names.isEmpty()) {
            builder.setMessage(getString(R.string.temujanji_telah_diselesaikan))
        }

        builder.setNeutralButton(
            R.string.set_temujanji_selesai
        ) { dialog, message ->
            updateAppointmentStatus(appointmentItem)
        }

        // create and show the alert dialog
        val dialog = builder.create()
        dialog.show()
    }

    private fun openForm(
        jenisBorang: String,
        appointmentItem: AppointmentItem,
        selectedParticipant: Participant?
    ) {
        showProgressBar()

        if (jenisBorang.contains("TS18A") &&
            appointmentItem.jenisLK?.equals("bayar", true) == true
        ) {
            val form =
                appointmentItem.formList?.find { it.tsMohonId == appointmentItem.tsApplication?.tsMohonId }

            activity!!.supportFragmentManager
                .beginTransaction()
                .addToBackStack(tag)
                .replace(
                    R.id.content_frame,
                    Ts18aIndividualFragment.newInstance(
                        appointmentItem,
                        appointmentItem?.tsApplication,
                        form,
                        jenisBorang
                    ),
                    tag
                )
                .commit()
        } else if (jenisBorang.contains("TS18A") &&
            appointmentItem.jenisLK?.equals("ts18a", true) == true
        ) {
            val form =
                appointmentItem.formList?.find { it.tsMohonId == selectedParticipant?.tsmohonID }

            activity!!.supportFragmentManager
                .beginTransaction()
                .addToBackStack(tag)
                .replace(
                    R.id.content_frame,
                    Ts18aFragment.newInstance(appointmentItem, selectedParticipant!!, form, jenisBorang),
                    tag
                )
                .commit()

        } else {
            var formIndex = 0
            if (appointmentItem.formList?.size == 0 || appointmentItem.formList?.get(0) == null) {
                formIndex = -1
            }

            activity!!.supportFragmentManager
                .beginTransaction()
                .addToBackStack(tag)
                .replace(
                    R.id.content_frame,
                    Ts18Fragment.newInstance(
                        appointmentItem,
                        formIndex
                    ), // form index as -1 to use POST method
                    tag
                )
                .commit()
        }
    }

    private fun hideProgressBar() {
        binding.progressbarAppointments.visibility = View.GONE
    }

    private fun showProgressBar() {
        binding.progressbarAppointments.visibility = View.VISIBLE
        binding.textviewNoAppt.visibility = View.GONE
    }

    private fun updateAppointmentStatus(appointmentItem: AppointmentItem) {
        val body = RequestBodies.UpdateAppointmentStatus(
            noSiriTemujanji = appointmentItem.noSiriTemujanji!!,
        )
        viewModel.putAppointmentStatus(body)
        viewModel.apiApptStatusResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { response ->
                            response.message?.let {
                                getAppointments(5)
                            }
                        }
                    }

                    is Resource.NoConnection -> {
                        viewModel.updateAppointmentOffline(noSiriTemujanji = appointmentItem?.noSiriTemujanji!!)
                        getAppointments(selectedApptDays)
                    }

                    is Resource.Error -> {
                        response.message?.let { message ->
                            binding.progressbarAppointments.errorSnack(
                                message,
                                Snackbar.LENGTH_SHORT
                            )
                        }
                    }
                }
            }
        })
    }

    /*** Start of search menu ***/
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.appointment_menu, menu)

        val menuItem = menu.findItem(R.id.search_menu)
        val actionView: View = menuItem.actionView
        searchBadgeTextView = actionView.findViewById(R.id.textview_search_badge)
        setupBadge()

        actionView.setOnClickListener { onOptionsItemSelected(menuItem) }
    }

    private fun setupBadge() {
        if (this::searchBadgeTextView.isInitialized) {
            if (searchFieldsCount == 0) {
                searchBadgeTextView.visibility = View.GONE
            } else {
                searchBadgeTextView.text = searchFieldsCount.toString()
                searchBadgeTextView.visibility = View.VISIBLE
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.search_menu -> {
                onSearchClick()
                return true
            }
            R.id.save_offline_menu -> {
                onSaveOfflineClick()
                return true
            }
            R.id.option_5 -> {
                selectedApptDays = 5
                getAppointments(5)
                return true
            }
            R.id.option_7 -> {
                selectedApptDays = 7
                getAppointments(7)
                return true
            }
            R.id.option_10 -> {
                selectedApptDays = 10
                getAppointments(10)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun onSearchClick() {
        val searchDialogFragment = SearchDialogFragment(
            searchFieldsCount = searchFieldsCount,
            noPermohonanPlaceholder = noPermohonanPlaceholder,
            tarikhTemujanjiPlaceholder = tarikhTemujanjiPlaceholder,
            kpPemohonPlaceholder = kpPemohonPlaceholder,
            namaPemohonPlaceholder = namaPemohonPlaceholder,
            noLotPlaceholder = noLotPlaceholder,
            noGeranPlaceholder = noGeranPlaceholder,
        )
        searchDialogFragment.setTargetFragment(this, TARGET_FRAGMENT_REQUEST_CODE);
        fragmentManager?.let { searchDialogFragment.show(it, TAG) }
    }

    private fun onSaveOfflineClick() {
        getAppointments(selectedApptDays, isDownloadOffline = true)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) {
            return
        }
        if (requestCode == TARGET_FRAGMENT_REQUEST_CODE) {
            when (data?.getStringExtra(EXTRA_SEARCH_ACTION)) {
                Constants.SearchAction.CLEAR_ALL.action -> {
                    searchFieldsCount = 0

                    noPermohonanPlaceholder = null
                    tarikhTemujanjiPlaceholder = null
                    kpPemohonPlaceholder = null
                    namaPemohonPlaceholder = null
                    noLotPlaceholder = null
                    noGeranPlaceholder = null
                }
                Constants.SearchAction.OK.action -> {
                    searchFieldsCount = data?.getIntExtra("searchFieldsCount", 0)
                    noPermohonanPlaceholder = data?.getStringExtra("noPermohonanPlaceholder")
                    tarikhTemujanjiPlaceholder = data?.getStringExtra("tarikhTemujanjiPlaceholder")
                    kpPemohonPlaceholder = data?.getStringExtra("kpPemohonPlaceholder")
                    namaPemohonPlaceholder = data?.getStringExtra("namaPemohonPlaceholder")
                    noLotPlaceholder = data?.getStringExtra("noLotPlaceholder")
                    noGeranPlaceholder = data?.getStringExtra("noGeranPlaceholder")
                }
            }
            setupBadge()
            getAppointments()
        }
    }

    companion object {
        val TAG = AppointmentsFragment::class.java.simpleName!!
        val EXTRA_SEARCH_ACTION = "search_action"

        fun newInstance(): AppointmentsFragment {
            return AppointmentsFragment()
        }

        fun newIntent(
            action: String,
            searchFieldsCount: Int? = null,
            noPermohonanPlaceholder: String? = null,
            tarikhTemujanjiPlaceholder: String? = null,
            kpPemohonPlaceholder: String? = null,
            namaPemohonPlaceholder: String? = null,
            noLotPlaceholder: String? = null,
            noGeranPlaceholder: String? = null
        ): Intent {
            val intent = Intent()
            intent.putExtra(EXTRA_SEARCH_ACTION, action)
            if (action == Constants.SearchAction.OK.action) {
                intent.putExtra("searchFieldsCount", searchFieldsCount)
                intent.putExtra("noPermohonanPlaceholder", noPermohonanPlaceholder)
                intent.putExtra("tarikhTemujanjiPlaceholder", tarikhTemujanjiPlaceholder)
                intent.putExtra("kpPemohonPlaceholder", kpPemohonPlaceholder)
                intent.putExtra("namaPemohonPlaceholder", namaPemohonPlaceholder)
                intent.putExtra("noLotPlaceholder", noLotPlaceholder)
                intent.putExtra("noGeranPlaceholder", noGeranPlaceholder)
            }
            return intent
        }
    }
    /*** End of search menu ***/
}