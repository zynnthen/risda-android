package com.map2u.risda.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.map2u.risda.R
import com.map2u.risda.adapter.ReportsAdapter
import com.map2u.risda.databinding.FragmentReportsBinding
import com.map2u.risda.model.appointment.AppointmentItem
import com.map2u.risda.model.appointment.Form
import com.map2u.risda.model.appointment.Participant
import com.map2u.risda.model.appointment.ReportItem
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.ui.readonly.ReadOnlyTs18Fragment
import com.map2u.risda.ui.readonly.ReadOnlyTs18aFragment
import com.map2u.risda.ui.readonly.ReadOnlyTs18aIndividualFragment
import com.map2u.risda.ui.shared.SearchDialogFragment
import com.map2u.risda.util.Constants
import com.map2u.risda.util.Resource
import com.map2u.risda.util.display.DisplayUtils
import com.map2u.risda.util.errorSnack
import com.map2u.risda.viewmodel.ReportViewModel
import com.map2u.risda.viewmodel.ViewModelProviderFactory


/**
 * A simple [Fragment] subclass.
 * Use the [ReportsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ReportsFragment : Fragment() {

    private lateinit var viewModel: ReportViewModel
    lateinit var reportsAdapter: ReportsAdapter

    private var _binding: FragmentReportsBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private var pageNumber = 0
    private var isLoading = false

    private var _isSearch = false

    // This variables is to handle search counts
    private var searchFieldsCount = 0
    lateinit var searchBadgeTextView: TextView

    var noPermohonanPlaceholder: String? = null
    var tarikhTemujanjiPlaceholder: String? = null
    var kpPemohonPlaceholder: String? = null
    var namaPemohonPlaceholder: String? = null
    var noLotPlaceholder: String? = null
    var noGeranPlaceholder: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentReportsBinding.inflate(inflater, container, false)
        val view = binding.root

        init()

        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun init() {
        reportsAdapter = ReportsAdapter() { reportItem: ReportItem ->
            navigateToForm(reportItem)
        }
        setupViewModel()
        setupRvListener()
    }

    private fun navigateToForm(reportItem: ReportItem) {
        val jenisBorang = DisplayUtils.getJenisBorang(
            reportItem.appointmentItem?.jenisLK,
            reportItem.appointmentItem?.moduleOwner,
            reportItem.appointmentItem?.tsPendekatan
        )

        openForm(
            jenisBorang,
            reportItem.appointmentItem!!,
            reportItem.form!!,
            reportItem.participant
        )
    }

    private fun openForm(
        jenisBorang: String,
        appointmentItem: AppointmentItem,
        form: Form,
        participant: Participant?
    ) {
        showProgressBar()

        pageNumber = 0
        if (jenisBorang.contains("TS18A") &&
            appointmentItem.jenisLK?.equals("bayar", true) == true
        ) {
            activity!!.supportFragmentManager
                .beginTransaction()
                .addToBackStack(tag)
                .replace(
                    R.id.content_frame,
                    ReadOnlyTs18aIndividualFragment.newInstance(
                        appointmentItem,
                        appointmentItem.tsApplication!!, form
                    ),
                    tag
                )
                .commit()

        } else if (jenisBorang.contains("TS18A") &&
            appointmentItem.jenisLK?.equals("ts18a", true) == true
        ) {
            activity!!.supportFragmentManager
                .beginTransaction()
                .addToBackStack(tag)
                .replace(
                    R.id.content_frame,
                    ReadOnlyTs18aFragment.newInstance(appointmentItem, participant!!, form),
                    tag
                )
                .commit()

        } else {
            activity!!.supportFragmentManager
                .beginTransaction()
                .addToBackStack(tag)
                .replace(
                    R.id.content_frame,
                    ReadOnlyTs18Fragment.newInstance(appointmentItem, form),
                    tag
                )
                .commit()
        }
    }

    private fun setupViewModel() {
        val repository = AppRepository()
        val factory = activity?.let { ViewModelProviderFactory(it.application, repository) }
        viewModel = factory?.let { ViewModelProvider(this, it).get(ReportViewModel::class.java) }!!

        getAppointments()
    }

    private fun setupRvListener() {
        binding.rvReports.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE && !isLoading) {
                    getAppointments()
                }
            }
        })
    }

    private fun getAppointments(isSearch: Boolean = false) {
        viewModel.getListOfAppointments(
            pageNumber = pageNumber,
            tsMohonId = noPermohonanPlaceholder,
            noKpPemohon = kpPemohonPlaceholder,
            namaPemohon = namaPemohonPlaceholder,
            noLot = noLotPlaceholder,
            noGeran = noGeranPlaceholder,
            date = tarikhTemujanjiPlaceholder
        )
        _isSearch = isSearch
        viewModel.apiResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        isLoading(false)
                        response.data?.let { response ->
                            binding.rvReports.adapter = reportsAdapter
                            if (response.datums?.size == 0) {
                                hideProgressBar()
                                if (_isSearch) {
                                    reportsAdapter.differ.submitList(null)
                                }
                                if (reportsAdapter.itemCount == 0) {
                                    binding.textviewNoData.visibility = View.VISIBLE
                                }
                            } else {
                                val reportItems: ArrayList<ReportItem> = ArrayList()
                                response.datums!!.forEachIndexed { index, appointmentItem ->
                                    appointmentItem.formList?.forEachIndexed { formIndex, form ->
                                        val participant =
                                            appointmentItem.participantList?.find { it.tsmohonID == form.tsMohonId }
                                        val reportItem = ReportItem(
                                            appointmentItem = appointmentItem,
                                            form = form,
                                            participant = participant
                                        )
                                        reportItems.add(reportItem)
                                    }
                                }
                                reportsAdapter.differ.submitList(reportItems)
                                pageNumber += 1
                                hideProgressBar()
                            }
                        }
                    }

                    is Resource.NoConnection -> {
                        isLoading(false)
                        hideProgressBar()

                        val appointments =
                            viewModel.queryAppointmentOffline(Constants.SELESAI + "','" + Constants.BATAL)
                        val reportItems: ArrayList<ReportItem> = ArrayList()
                        appointments!!.forEachIndexed { index, appointmentItem ->
                            appointmentItem.formList?.forEachIndexed { formIndex, form ->
                                val participant =
                                    appointmentItem.participantList?.find { it.tsmohonID == form.tsMohonId }
                                val reportItem = ReportItem(
                                    appointmentItem = appointmentItem,
                                    form = form,
                                    participant = participant
                                )
                                reportItems.add(reportItem)
                            }
                        }

                        if (reportItems.size == 0) {
                            binding.textviewNoData.text = getString(R.string.no_data_available)
                            binding.textviewNoData.visibility = View.VISIBLE
                        } else {
                            reportsAdapter.differ.submitList(reportItems)
                            binding.rvReports.adapter = reportsAdapter
                        }
                    }

                    is Resource.Error -> {
                        isLoading(false)
                        hideProgressBar()
                        response.message?.let { message ->
                            binding.progressbar.errorSnack(message, Snackbar.LENGTH_SHORT)
                        }
                    }

                    is Resource.Loading -> {
                        isLoading(true)
                        showProgressBar()
                    }
                }
            }
        })
    }

    private fun hideProgressBar() {
        binding.progressbar.visibility = View.GONE
    }

    private fun showProgressBar() {
        binding.progressbar.visibility = View.VISIBLE
        binding.textviewNoData.visibility = View.GONE
    }

    private fun isLoading(state: Boolean) {
        isLoading = state
    }

    /*** Start of search menu ***/
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.report_menu, menu)

        val menuItem = menu.findItem(R.id.search_menu)
        val actionView: View = menuItem.actionView
        searchBadgeTextView = actionView.findViewById(R.id.textview_search_badge)
        setupBadge()

        actionView.setOnClickListener { onOptionsItemSelected(menuItem) }
    }

    private fun setupBadge() {
        if (this::searchBadgeTextView.isInitialized) {
            if (searchFieldsCount == 0) {
                searchBadgeTextView.visibility = View.GONE
            } else {
                searchBadgeTextView.text = searchFieldsCount.toString()
                searchBadgeTextView.visibility = View.VISIBLE
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.search_menu -> {
                onSearchClick()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun onSearchClick() {
        val searchDialogFragment = SearchDialogFragment(
            searchFieldsCount = searchFieldsCount,
            noPermohonanPlaceholder = noPermohonanPlaceholder,
            tarikhTemujanjiPlaceholder = tarikhTemujanjiPlaceholder,
            kpPemohonPlaceholder = kpPemohonPlaceholder,
            namaPemohonPlaceholder = namaPemohonPlaceholder,
            noLotPlaceholder = noLotPlaceholder,
            noGeranPlaceholder = noGeranPlaceholder,
        )
        searchDialogFragment.setTargetFragment(this, Constants.TARGET_FRAGMENT_REQUEST_CODE);
        fragmentManager?.let { searchDialogFragment.show(it, TAG) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) {
            return
        }
        if (requestCode == Constants.TARGET_FRAGMENT_REQUEST_CODE) {
            when (data?.getStringExtra(AppointmentsFragment.EXTRA_SEARCH_ACTION)) {
                Constants.SearchAction.CLEAR_ALL.action -> {
                    pageNumber = 0
                    searchFieldsCount = 0

                    noPermohonanPlaceholder = null
                    tarikhTemujanjiPlaceholder = null
                    kpPemohonPlaceholder = null
                    namaPemohonPlaceholder = null
                    noLotPlaceholder = null
                    noGeranPlaceholder = null
                }
                Constants.SearchAction.OK.action -> {
                    pageNumber = 0

                    searchFieldsCount = data?.getIntExtra("searchFieldsCount", 0)
                    noPermohonanPlaceholder = data?.getStringExtra("noPermohonanPlaceholder")
                    tarikhTemujanjiPlaceholder = data?.getStringExtra("tarikhTemujanjiPlaceholder")
                    kpPemohonPlaceholder = data?.getStringExtra("kpPemohonPlaceholder")
                    namaPemohonPlaceholder = data?.getStringExtra("namaPemohonPlaceholder")
                    noLotPlaceholder = data?.getStringExtra("noLotPlaceholder")
                    noGeranPlaceholder = data?.getStringExtra("noGeranPlaceholder")
                }
            }
            setupBadge()
            getAppointments(isSearch = true)
        }
    }

    companion object {
        val TAG = ReportsFragment::class.java.simpleName!!
        val EXTRA_SEARCH_ACTION = "search_action"

        @JvmStatic
        fun newInstance() =
            ReportsFragment().apply {
            }

        fun newIntent(
            action: String,
            searchFieldsCount: Int? = null,
            noPermohonanPlaceholder: String? = null,
            tarikhTemujanjiPlaceholder: String? = null,
            kpPemohonPlaceholder: String? = null,
            namaPemohonPlaceholder: String? = null,
            noLotPlaceholder: String? = null,
            noGeranPlaceholder: String? = null
        ): Intent {
            val intent = Intent()
            intent.putExtra(EXTRA_SEARCH_ACTION, action)
            if (action == Constants.SearchAction.OK.action) {
                intent.putExtra("searchFieldsCount", searchFieldsCount)
                intent.putExtra("noPermohonanPlaceholder", noPermohonanPlaceholder)
                intent.putExtra("tarikhTemujanjiPlaceholder", tarikhTemujanjiPlaceholder)
                intent.putExtra("kpPemohonPlaceholder", kpPemohonPlaceholder)
                intent.putExtra("namaPemohonPlaceholder", namaPemohonPlaceholder)
                intent.putExtra("noLotPlaceholder", noLotPlaceholder)
                intent.putExtra("noGeranPlaceholder", noGeranPlaceholder)
            }
            return intent
        }
    }
    /*** End of search menu ***/
}