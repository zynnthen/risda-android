package com.map2u.risda.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.map2u.risda.BuildConfig
import com.map2u.risda.R
import com.map2u.risda.databinding.ActivityLoginBinding
import com.map2u.risda.network.RequestBodies
import com.map2u.risda.network.SessionManager
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.util.*
import com.map2u.risda.util.StringUtils.isNullOrEmpty
import com.map2u.risda.util.Utils.hideKeyboard
import com.map2u.risda.viewmodel.LoginViewModel
import com.map2u.risda.viewmodel.ViewModelProviderFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var sessionManager: SessionManager
    lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        init()

        sessionManager = SessionManager(this)
        if (!isNullOrEmpty(sessionManager.fetchRefreshToken())) {
            onRefreshToken()
        }

        /*** Fix username and password for debug mode only ***/
        if (BuildConfig.DEBUG) {
            binding.edtUsername.setText(TestConstants.TEST_USERNAME)
            binding.edtPass.setText(TestConstants.TEST_PSS)
        }

        supportActionBar?.hide()
    }

    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        loginViewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)
    }

    fun onLoginClick(view: View) {

        var username = binding.edtUsername.text.toString()
        val password = binding.edtPass.text.toString()

        if (username.isNotEmpty() && password.isNotEmpty()) {
            hideKeyboard()

            val body = RequestBodies.LoginBody(
                username,
                password
            )

            loginViewModel.loginUser(body)
            loginViewModel.loginResponse.observe(this, Observer { event ->
                event.getContentIfNotHandled()?.let { response ->
                    when (response) {
                        is Resource.Success -> {
                            hideProgressBar()
                            response.data?.let { loginResponse ->
                                val role = loginResponse.datum!!.user?.userRole?.id
                                sessionManager.saveIdToken(loginResponse.datum!!.tokens?.idToken)
                                sessionManager.saveRefreshToken(loginResponse.datum!!.tokens?.refreshToken)
                                sessionManager.saveUsername(username)
                                sessionManager.saveName(loginResponse.datum!!.user?.name.toString())
                                sessionManager.saveDesignation(loginResponse.datum!!.user?.designation.toString())
                                sessionManager.savePtCode(loginResponse.datum!!.user?.ptCode.toString())
                                sessionManager.saveRole(role ?: 0)
                                sessionManager.saveLastLogin(TimeUtils.getCurrentTimestamp())

                                if (role == Constants.UserRole.KETUA_UNIT.role || role == Constants.UserRole.PEGAWAI_PEMERIKSA_KEBUN.role) {
                                    MainActivity().navigateToMainScreen(this@LoginActivity)
                                } else {
                                    binding.progress.errorSnack(
                                        getString(R.string.error_role),
                                        Snackbar.LENGTH_LONG
                                    )
                                }
                            }
                        }

                        is Resource.Error -> {
                            hideProgressBar()
                            response.message?.let { message ->
                                binding.progress.errorSnack(message, Snackbar.LENGTH_LONG)
                            }
                        }

                        is Resource.Loading -> {
                            showProgressBar()
                        }
                    }
                }
            })
        }
    }

    private fun onRefreshToken() {
        val body = RequestBodies.RefreshTokenBody(
            sessionManager.fetchRefreshToken().toString()
        )
        loginViewModel.refreshTokenUser(body)

        loginViewModel.loginResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        hideProgressBar()
                        response.data?.let { loginResponse ->
                            sessionManager.saveIdToken(loginResponse.datum!!.tokens?.idToken)
                            sessionManager.saveLastLogin(TimeUtils.getCurrentTimestamp())
                            MainActivity().navigateToMainScreen(this@LoginActivity)
                        }
                    }

                    is Resource.NoConnection -> {
                        hideProgressBar()
                        response.message?.let { message ->
                            binding.progress.errorSnack(message, Snackbar.LENGTH_SHORT)
                        }
                        GlobalScope.launch(context = Dispatchers.Main) {
                            MainActivity().navigateToMainScreen(this@LoginActivity)
                        }
                    }

                    is Resource.Error -> {
                        hideProgressBar()
                        response.message?.let { message ->
                            binding.progress.errorSnack(message, Snackbar.LENGTH_LONG)
                        }
                    }

                    is Resource.Loading -> {
                        showProgressBar()
                    }
                }
            }
        })
    }

    private fun hideProgressBar() {
        binding.progress.visibility = View.GONE
    }

    private fun showProgressBar() {
        binding.progress.visibility = View.VISIBLE
    }

    fun navigateToLoginScreen(context: Context) {
        val intent = Intent(context, LoginActivity::class.java)
        context.startActivity(intent)
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
    }
}