package com.map2u.risda.ui.shared

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.google.android.material.textfield.TextInputEditText
import com.map2u.risda.R
import com.map2u.risda.ui.AppointmentsFragment
import com.map2u.risda.ui.PrintFragment
import com.map2u.risda.ui.ReportsFragment
import com.map2u.risda.util.Constants
import com.map2u.risda.util.StringUtils
import java.text.SimpleDateFormat
import java.util.*


class SearchDialogFragment(
    private var searchFieldsCount: Int,
    private var noPermohonanPlaceholder: String?,
    private var tarikhTemujanjiPlaceholder: String?,
    private var kpPemohonPlaceholder: String?,
    private var namaPemohonPlaceholder: String?,
    private var noLotPlaceholder: String?,
    private var noGeranPlaceholder: String?
) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity!!, R.style.AlertDialogTheme)
        val alertDialogView: View =
            activity!!.layoutInflater.inflate(R.layout.dialog_search_appointment, null)

        val noPermohonan =
            alertDialogView.findViewById<TextInputEditText>(R.id.edt_no_permohonan)
        val tarikhTemujanji =
            alertDialogView.findViewById<TextInputEditText>(R.id.edt_tarikh_temujanji)
        val kpPemohon =
            alertDialogView.findViewById<TextInputEditText>(R.id.edt_kp_pemohon)
        val namaPemohon =
            alertDialogView.findViewById<TextInputEditText>(R.id.edt_nama_pemohon)
        val noLot =
            alertDialogView.findViewById<TextInputEditText>(R.id.edt_no_lot)
        val noGeran =
            alertDialogView.findViewById<TextInputEditText>(R.id.edt_no_geran)
        noPermohonan.setText(noPermohonanPlaceholder)
        tarikhTemujanji.setText(tarikhTemujanjiPlaceholder)
        kpPemohon.setText(kpPemohonPlaceholder)
        namaPemohon.setText(namaPemohonPlaceholder)
        noLot.setText(noLotPlaceholder)
        noGeran.setText(noGeranPlaceholder)

        /*** Date input picker ***/
        val myCalendar: Calendar = Calendar.getInstance()
        val date =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val sdf = SimpleDateFormat("dd-MM-yyyy")
                tarikhTemujanji.setText(sdf.format(myCalendar.getTime()))
            }
        tarikhTemujanji.setOnClickListener { v ->
            DatePickerDialog(
                context!!, date, myCalendar[Calendar.YEAR], myCalendar[Calendar.MONTH],
                myCalendar[Calendar.DAY_OF_MONTH]
            ).show()
        }

        builder.setView(alertDialogView);

        builder.setTitle(getString(R.string.search))

        builder.setPositiveButton(
            getString(android.R.string.ok)
        ) { dialog, message ->
            // to check how many filter applied
            var count = 0
            if (!StringUtils.isNullOrEmpty(noPermohonan.text.toString())) {
                noPermohonanPlaceholder = noPermohonan.text.toString()
                count++
            }
            if (!StringUtils.isNullOrEmpty(tarikhTemujanji.text.toString())) {
                tarikhTemujanjiPlaceholder = tarikhTemujanji.text.toString()
                count++
            }
            if (!StringUtils.isNullOrEmpty(kpPemohon.text.toString())) {
                kpPemohonPlaceholder = kpPemohon.text.toString()
                count++
            }
            if (!StringUtils.isNullOrEmpty(namaPemohon.text.toString())) {
                namaPemohonPlaceholder = namaPemohon.text.toString()
                count++
            }
            if (!StringUtils.isNullOrEmpty(noLot.text.toString())) {
                noLotPlaceholder = noLot.text.toString()
                count++
            }
            if (!StringUtils.isNullOrEmpty(noGeran.text.toString())) {
                noGeranPlaceholder = noGeran.text.toString()
                count++
            }
            searchFieldsCount = count

            sendOk(
                searchFieldsCount = searchFieldsCount,
                noPermohonanPlaceholder = noPermohonanPlaceholder,
                tarikhTemujanjiPlaceholder = tarikhTemujanjiPlaceholder,
                kpPemohonPlaceholder = kpPemohonPlaceholder,
                namaPemohonPlaceholder = namaPemohonPlaceholder,
                noLotPlaceholder = noLotPlaceholder,
                noGeranPlaceholder = noGeranPlaceholder
            )
        }

        builder.setNeutralButton(
            R.string.clear_all
        ) { dialog, message ->
            clearAll()
        }

        builder.setNegativeButton(
            R.string.cancel
        ) { dialog, message -> dialog.dismiss() }

        return builder.create()
    }

    private fun clearAll() {
        if (targetFragment == null) {
            return
        }
        val intent: Intent = AppointmentsFragment.newIntent(Constants.SearchAction.CLEAR_ALL.action)
        targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
        dismiss()
    }

    private fun sendOk(
        searchFieldsCount: Int,
        noPermohonanPlaceholder: String?,
        tarikhTemujanjiPlaceholder: String?,
        kpPemohonPlaceholder: String?,
        namaPemohonPlaceholder: String?,
        noLotPlaceholder: String?,
        noGeranPlaceholder: String?
    ) {
        if (targetFragment == null) {
            return
        }

        when(targetFragment!!.javaClass.simpleName) {
            AppointmentsFragment.TAG -> {
                val intent: Intent = AppointmentsFragment.newIntent(
                    Constants.SearchAction.OK.action,
                    searchFieldsCount = searchFieldsCount,
                    noPermohonanPlaceholder = noPermohonanPlaceholder,
                    tarikhTemujanjiPlaceholder = tarikhTemujanjiPlaceholder,
                    kpPemohonPlaceholder = kpPemohonPlaceholder,
                    namaPemohonPlaceholder = namaPemohonPlaceholder,
                    noLotPlaceholder = noLotPlaceholder,
                    noGeranPlaceholder = noGeranPlaceholder
                )
                targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
            }
            ReportsFragment.TAG -> {
                val intent: Intent = ReportsFragment.newIntent(
                    Constants.SearchAction.OK.action,
                    searchFieldsCount = searchFieldsCount,
                    noPermohonanPlaceholder = noPermohonanPlaceholder,
                    tarikhTemujanjiPlaceholder = tarikhTemujanjiPlaceholder,
                    kpPemohonPlaceholder = kpPemohonPlaceholder,
                    namaPemohonPlaceholder = namaPemohonPlaceholder,
                    noLotPlaceholder = noLotPlaceholder,
                    noGeranPlaceholder = noGeranPlaceholder
                )
                targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
            }
            PrintFragment.TAG -> {
                val intent: Intent = PrintFragment.newIntent(
                    Constants.SearchAction.OK.action,
                    searchFieldsCount = searchFieldsCount,
                    noPermohonanPlaceholder = noPermohonanPlaceholder,
                    tarikhTemujanjiPlaceholder = tarikhTemujanjiPlaceholder,
                    kpPemohonPlaceholder = kpPemohonPlaceholder,
                    namaPemohonPlaceholder = namaPemohonPlaceholder,
                    noLotPlaceholder = noLotPlaceholder,
                    noGeranPlaceholder = noGeranPlaceholder
                )
                targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
            }
        }

        dismiss()
    }
}