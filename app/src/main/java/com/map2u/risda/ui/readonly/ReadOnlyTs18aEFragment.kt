package com.map2u.risda.ui.readonly

import android.content.Context
import android.content.res.ColorStateList
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.map2u.risda.R
import com.map2u.risda.databinding.FragmentTs18aEBinding
import com.map2u.risda.model.appointment.PendingVerify
import com.map2u.risda.model.appointment.TsApplication
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.ui.shared.OnSignedCaptureListener
import com.map2u.risda.util.*
import com.map2u.risda.util.display.DisplayUtils
import com.map2u.risda.viewmodel.Ts18EViewModel
import com.map2u.risda.viewmodel.ViewModelProviderFactory
import kotlinx.coroutines.*
import java.io.ByteArrayOutputStream
import java.util.*


class ReadOnlyTs18aEFragment : Fragment(), OnSignedCaptureListener {
    val ARG_PARAM_PENDING_VERIFY = "ARG_PARAM_PENDING_VERIFY"
    val ARG_PARAM_TS_APPLICATION = "ARG_PARAM_TS_APPLICATION"

    var paramPendingVerify: PendingVerify? = null
    var paramTsApplication: TsApplication? = null

    private var _binding: FragmentTs18aEBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private lateinit var viewModel: Ts18EViewModel

    // This property is for image upload
    private val filePaths: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            paramPendingVerify = it.getParcelable(ARG_PARAM_PENDING_VERIFY)
            paramTsApplication = it.getParcelable(ARG_PARAM_TS_APPLICATION)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTs18aEBinding.inflate(inflater, container, false)
        val view = binding.root

        setupViewModel()

        /*** Disable edit text, radio button ***/
        val ll: LinearLayout? = binding.rootLinearLayout
        for (view in ll!!.touchables) {
            if (view is EditText) {
                val editText = view
                editText.setTextColor(context!!.getColor(R.color.colorPrimaryText))
                editText.isEnabled = false
                editText.isFocusable = false
                editText.isFocusableInTouchMode = false

            } else if (view is RadioButton) {
                val radioButton = view
                radioButton.isEnabled = false
                radioButton.buttonTintList =
                    ColorStateList.valueOf(context!!.getColor(R.color.colorPrimaryDark))
                radioButton.setBackgroundTintList(ColorStateList.valueOf(resources.getColor(R.color.md_grey_300)))

            }
        }

        binding.sectionC1.sectionC1.visibility = View.GONE
        binding.sectionC2.sectionC2.visibility = View.GONE
        binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
        binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.GONE
        binding.sectionD.sectionD.visibility = View.GONE
        binding.sectionF.sectionF.visibility = View.GONE

        /*** Next button listener ***/
        nextButtonListener()

        /*** Previous button listener ***/
        previousButtonListener()

        initAbContent()
        initC1Content()
        initC2Content()
        initAttachmentsContent()
        initDContent()
        initFContent()

        return view
    }

    private fun setupViewModel() {
        val repository = AppRepository()
        val factory = activity?.let { ViewModelProviderFactory(it.application, repository) }
        viewModel = factory?.let { ViewModelProvider(this, it).get(Ts18EViewModel::class.java) }!!
    }

    private fun initAbContent() {
        val tsApplication: TsApplication? = paramTsApplication

        val address =
            "${tsApplication?.address1} ${tsApplication?.address2} ${tsApplication?.address3}"
        binding.sectionAb.edtNama.setText(tsApplication?.namaPemohon)
        binding.sectionAb.edtNoKpPolisTenteraSyarikat.setText(tsApplication?.icNoPemohon)
        binding.sectionAb.edtAlamat.setText(address)
        binding.sectionAb.edtPoskod.setText(tsApplication?.poskod)
        binding.sectionAb.edtNoTelRumah.setText(tsApplication?.noPhoneHouse)
        binding.sectionAb.edtNoTelBimbit.setText(tsApplication?.noPhoneMobile)
        binding.sectionAb.edtNoPermohonan.setText(tsApplication?.tsMohonId)

        binding.sectionAb.edtDaerah.setText(tsApplication?.daerah)
        binding.sectionAb.edtParlimen.setText(tsApplication?.parlimen)
        binding.sectionAb.edtDun.setText(tsApplication?.dun)
        binding.sectionAb.edtMukim.setText(tsApplication?.mukim)
        binding.sectionAb.edtKampung.setText(tsApplication?.kampung)

        binding.sectionAb.edtNoGeran.setText(tsApplication?.lotDimilikiList?.get(0)?.noGeran.toString())
        binding.sectionAb.edtNoLot.setText(tsApplication?.lotDimilikiList?.get(0)?.noLot.toString())
        binding.sectionAb.edtJenisTanaman.setText(tsApplication?.lotDimilikiList?.get(0)?.noLot.toString())
    }

    private fun initC1Content() {
        binding.sectionC1.textviewLuasKawasanSiapDibersihkan.text = paramPendingVerify?.tebangJentera1.toString()
        binding.sectionC1.textviewJarakTanamanDanKepadatanPokokHektar.text = paramPendingVerify?.jarakTanaman.toString()
        binding.sectionC1.textviewLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih.text = paramPendingVerify?.luasTanam.toString()
        binding.sectionC1.textviewTarikhTanam.text = TimeUtils.displayDate(paramPendingVerify?.tkhTanam ?: 0)

        if (paramPendingVerify!!.perimeter?.trim() == "Ya") {
            binding.sectionC1.rbKerjaMengukurYa2.isChecked = true
        } else if (paramPendingVerify!!.perimeter?.trim() == "Tidak") {
            binding.sectionC1.rbKerjaMengukurTidak2.isChecked = true
        }

        if (paramPendingVerify?.benihLuar?.trim() == "Ya") {
            binding.sectionC1.textviewKelulusanBorangTs29a.text = getString(R.string.ya)
        } else if (paramPendingVerify?.benihLuar?.trim() == "Tidak") {
            binding.sectionC1.textviewKelulusanBorangTs29a.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.klonMutu?.trim() == "Ya") {
            binding.sectionC1.textviewKlonBermutuTinggi.text = getString(R.string.ya)
        } else if (paramPendingVerify?.klonMutu?.trim() == "Tidak") {
            binding.sectionC1.textviewKlonBermutuTinggi.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.resitBenih?.trim() == "Ya") {
            binding.sectionC1.textviewResitPembelianBenih.text = getString(R.string.ya)
        } else if (paramPendingVerify?.resitBenih?.trim() == "Tidak") {
            binding.sectionC1.textviewResitPembelianBenih.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.sahKlon?.trim() == "Ya") {
            binding.sectionC1.textviewSijilPengesahan.text = getString(R.string.ya)
        } else if (paramPendingVerify?.sahKlon?.trim() == "Tidak") {
            binding.sectionC1.textviewSijilPengesahan.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.tapakSemaian?.trim() == "Ya") {
            binding.sectionC1.textviewTapakSemaian.text = getString(R.string.ya)
        } else if (paramPendingVerify?.tapakSemaian?.trim() == "Tidak") {
            binding.sectionC1.textviewTapakSemaian.text = getString(R.string.tidak)
        }

        binding.sectionC1.textviewBenihSawitNyatakanSumber.text = paramPendingVerify?.benihSawit.toString()

        if (paramPendingVerify?.gapSenggara?.trim() == "Ya") {
            binding.sectionC1.textviewAdakahTeres.text = getString(R.string.ya)
        } else if (paramPendingVerify?.gapSenggara?.trim() == "Tidak") {
            binding.sectionC1.textviewAdakahTeres.text = getString(R.string.tidak)
        }

        binding.sectionC1.textviewLainLainKenyataan.text = paramPendingVerify?.kenyataanLain.toString()

        binding.sectionC1.textviewJenisBenihGetahSilaNyatakan.text = paramPendingVerify?.jenisBenih.toString()

        // ts18a-e submitted
        val items = paramPendingVerify?.ts18e?.items
        binding.sectionC1.edtLuasKawasanSiapDibersihkan2.setText(items?.find { it.item == "Tebang_Jentera1" }?.catatan)
        items?.find { it.item == "Jarak_Tanaman" }?.catatan?.toInt()?.let {
            binding.sectionC1.spinnerJarakTanaman.setSelection(
                it
            )
        }
        binding.sectionC1.edtLuasKawasanSiapDitanamDenganBaikDanKebunBerkeadaanBersih2.setText(items?.find { it.item == "Luas_Tanam" }?.catatan)
        binding.sectionC1.edtTarikhTanam2.setText(TimeUtils.displayDate(items?.find { it.item == "Tkh_Tanam" }?.catatan?.toLong() ?: 0))

        if (items?.find { it.item == "Jenis_Benih" }?.catatan == "BCMP") {
            binding.sectionC1.rbJenisBenihBcmp2.isChecked = true
        } else if (items?.find { it.item == "Jenis_Benih" }?.catatan == "TCP") {
            binding.sectionC1.rbJenisBenihTcp2.isChecked = true
        } else if (items?.find { it.item == "Jenis_Benih" }?.catatan == "APM") {
            binding.sectionC1.rbJenisBenihApm2.isChecked = true
        }

        if (items?.find { it.item == "Benih_Luar" }?.catatan?.trim() == "Ya") {
            binding.sectionC1.rbKelulusanBorangTs29aYa2.isChecked = true
        } else if (items?.find { it.item == "Benih_Luar" }?.catatan?.trim() == "Tidak") {
            binding.sectionC1.rbKelulusanBorangTs29aTidak2.isChecked = true
        }

        if (items?.find { it.item == "Klon_Mutu" }?.catatan?.trim() == "Ya") {
            binding.sectionC1.rbKlonBermutuTinggiYa2.isChecked = true
        } else if (items?.find { it.item == "Klon_Mutu" }?.catatan?.trim() == "Tidak") {
            binding.sectionC1.rbKlonBermutuTinggiTidak2.isChecked = true
        }

        if (items?.find { it.item == "Resit_Benih" }?.catatan?.trim() == "Ya") {
            binding.sectionC1.rbResitPembelianBenihYa2.isChecked = true
        } else if (items?.find { it.item == "Resit_Benih" }?.catatan?.trim() == "Tidak") {
            binding.sectionC1.rbResitPembelianBenihTidak2.isChecked = true
        }

        if (items?.find { it.item == "Sah_Klon" }?.catatan?.trim() == "Ya") {
            binding.sectionC1.rbSijilPengesahanYa2.isChecked = true
        } else if (items?.find { it.item == "Sah_Klon" }?.catatan?.trim() == "Tidak") {
            binding.sectionC1.rbSijilPengesahanTidak2.isChecked = true
        }

        if (items?.find { it.item == "Tapak_Semaian" }?.catatan?.trim() == "Ya") {
            binding.sectionC1.rbTapakSemaianYa2.isChecked = true
        } else if (items?.find { it.item == "Tapak_Semaian" }?.catatan?.trim() == "Tidak") {
            binding.sectionC1.rbTapakSemaianTidak2.isChecked = true
        }

        binding.sectionC1.edtBenihSawitNyatakanSumber2.setText(items?.find { it.item == "Benih_Sawit" }?.catatan)

        if (items?.find { it.item == "GAP_Senggara" }?.catatan?.trim() == "Ya") {
            binding.sectionC1.rbAdakahTeresYa2.isChecked = true
        } else if (items?.find { it.item == "GAP_Senggara" }?.catatan?.trim() == "Tidak") {
            binding.sectionC1.rbAdakahTeresTidak2.isChecked = true
        }

        binding.sectionC1.edtLainLainKenyataan2.setText(items?.find { it.item == "Kenyataan_Lain" }?.catatan)
    }

    private fun initC2Content() {
        binding.sectionC2.edtLuasTanamanUtamaYangBerjaya.setText(paramPendingVerify?.luasBerjaya.toString())

        if (paramPendingVerify?.gapBaja?.trim() == "Ya") {
            binding.sectionC2.textviewAdakahMembaja.text = getString(R.string.ya)
        } else if (paramPendingVerify?.gapBaja?.trim() == "Tidak") {
            binding.sectionC2.textviewAdakahMembaja.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.gapCantasan?.trim() == "Ya") {
            binding.sectionC2.textviewAdakahCantasan.text = getString(R.string.ya)
        } else if (paramPendingVerify?.gapCantasan?.trim() == "Tidak") {
            binding.sectionC2.textviewAdakahCantasan.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.jenisCantasan?.trim() == "Pembetulan") {
            binding.sectionC2.textviewJikaYa.text = getString(R.string.pembetulan)
        } else if (paramPendingVerify?.jenisCantasan?.trim() == "Terkawal") {
            binding.sectionC2.textviewJikaYa.text = getString(R.string.terkawal)
        }

        if (paramPendingVerify?.gapGalakDahan?.trim() == "Ya") {
            binding.sectionC2.textviewAdakahGalakan.text = getString(R.string.ya)
        } else if (paramPendingVerify?.gapGalakDahan?.trim() == "Tidak") {
            binding.sectionC2.textviewAdakahGalakan.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.jenisGalakDahan?.trim() == "Dilaksanakan") {
            binding.sectionC2.textviewJikaYaNyatakan.text = getString(R.string.dilaksanakan)
        } else if (paramPendingVerify?.jenisGalakDahan?.trim() == "Tidak dilaksanakan") {
            binding.sectionC2.textviewJikaYaNyatakan.text = getString(R.string.tidak_dilaksanakan)
        }

        if (paramPendingVerify?.kawalPerosak?.trim() == "Ya") {
            binding.sectionC2.textviewAdakahKawalan.text = getString(R.string.ya)
        } else if (paramPendingVerify?.kawalPerosak?.trim() == "Tidak") {
            binding.sectionC2.textviewAdakahKawalan.text = getString(R.string.tidak)
        }

        binding.sectionC2.textviewRumpaiSiapDiselenggaraDenganMemuaskanDanLalang.text = paramPendingVerify?.rumpaiSenggara.toString()
        binding.sectionC2.textviewRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh.text = paramPendingVerify?.rumpaiTdkSenggara.toString()

        if (paramPendingVerify?.gapSenggara?.trim() == "Ya") {
            binding.sectionC2.textviewAdakahTeresPelantar.text = getString(R.string.ya)
        } else if (paramPendingVerify?.gapSenggara?.trim() == "Tidak") {
            binding.sectionC2.textviewAdakahTeresPelantar.text = getString(R.string.tidak)
        }

        if (paramPendingVerify?.kpbKontan?.trim() == "Ya") {
            binding.sectionC2.textviewAdakahKpb.text = getString(R.string.ya)
        } else if (paramPendingVerify?.kpbKontan?.trim() == "Tidak") {
            binding.sectionC2.textviewAdakahKpb.text = getString(R.string.tidak)
        }

        binding.sectionC2.textviewJikaYaLuasYangDiselenggara.text = paramPendingVerify?.luasKontan.toString()
        binding.sectionC2.textviewGetah.text = paramPendingVerify?.getahPkkHek.toString()
        binding.sectionC2.textviewTanamanLain.text = paramPendingVerify?.tlPkkHidup.toString()
        binding.sectionC2.textviewPurataUkurLilitPokokGetahYang.text = paramPendingVerify?.ukurLilit.toString()
        binding.sectionC2.textviewLainLainKenyataanJikaAda.text = paramPendingVerify?.kenyataanLain.toString()

        // ts18a-e submitted
        val items = paramPendingVerify?.ts18e?.items
        binding.sectionC2.edtLuasTanamanUtamaYangBerjaya.setText(items?.find { it.item == "LuasBerjaya" }?.catatan)

        if (items?.find { it.item == "GAP_Baja" }?.catatan?.trim() == "Ya") {
            binding.sectionC2.rbAdakahMembajaYa.isChecked = true
        } else if (items?.find { it.item == "GAP_Baja" }?.catatan?.trim() == "Tidak") {
            binding.sectionC2.rbAdakahMembajaTidak.isChecked = true
        }

        if (items?.find { it.item == "GAP_Cantasan" }?.catatan?.trim() == "Ya") {
            binding.sectionC2.rbAdakahCantasanYa.isChecked = true
        } else if (items?.find { it.item == "GAP_Cantasan" }?.catatan?.trim() == "Tidak") {
            binding.sectionC2.rbAdakahCantasanTidak.isChecked = true
        }

        if (items?.find { it.item == "Jenis_Cantasan" }?.catatan?.trim() == "Pembetulan") {
            binding.sectionC2.rbJikaYaPembetulan.isChecked = true
        } else if (items?.find { it.item == "Jenis_Cantasan" }?.catatan?.trim() == "Terkawal") {
            binding.sectionC2.rbJikaYaTerkawal.isChecked = true
        }

        if (items?.find { it.item == "GAP_GalakDahan" }?.catatan?.trim() == "Ya") {
            binding.sectionC2.rbAdakahGalakanYa.isChecked = true
        } else if (items?.find { it.item == "GAP_GalakDahan" }?.catatan?.trim() == "Tidak") {
            binding.sectionC2.rbAdakahGalakanTidak.isChecked = true
        }

        if (items?.find { it.item == "Jenis_GalakDahan" }?.catatan?.trim() == "Dilaksanakan") {
            binding.sectionC2.rbJikaYaNyatakanDilaksanakan.isChecked = true
        } else if (items?.find { it.item == "Jenis_GalakDahan" }?.catatan?.trim() == "Tidak dilaksanakan") {
            binding.sectionC2.rbJikaYaNyatakanTidakDilaksanakan.isChecked = true
        }

        if (items?.find { it.item == "Kawal_Perosak" }?.catatan?.trim() == "Ya") {
            binding.sectionC2.rbAdakahKawalanYa.isChecked = true
        } else if (items?.find { it.item == "Kawal_Perosak" }?.catatan?.trim() == "Tidak") {
            binding.sectionC2.rbAdakahKawalanTidak.isChecked = true
        }

        binding.sectionC2.edtRumpaiSiapDiselenggaraDenganMemuaskanDanLalang.setText(items?.find { it.item == "Rumpai_Senggara" }?.catatan)
        binding.sectionC2.edtRumpaiTidakDiselenggaraAtauTerdapatLalangYangTumbuh.setText(items?.find { it.item == "Rumpai_TdkSenggara" }?.catatan)

        if (items?.find { it.item == "GAP_Senggara" }?.catatan?.trim() == "Ya") {
            binding.sectionC2.rbAdakahTeresYa.isChecked = true
        } else if (items?.find { it.item == "GAP_Senggara" }?.catatan?.trim() == "Tidak") {
            binding.sectionC2.rbAdakahTeresTidak.isChecked = true
        }

        binding.sectionC2.edtJikaYaLuasYangDiselenggara.setText(items?.find { it.item == "Luas_Kontan" }?.catatan)

        binding.sectionC2.edtGetah.setText(items?.find { it.item == "Getah_PkkHek" }?.catatan)
        binding.sectionC2.edtTanamanLain.setText(items?.find { it.item == "TL_PkkHidup" }?.catatan)
        binding.sectionC2.edtPurataUkurLilitPokokGetahYang.setText(items?.find { it.item == "Ukur_Lilit" }?.catatan)
        binding.sectionC2.edtLainLainKenyataanJikaAda.setText(items?.find { it.item == "Kenyataan_Lain" }?.catatan)

    }

    private fun initAttachmentsContent() {
        binding.sectionUploadAttachment.cameraButton.visibility = View.GONE
        binding.sectionUploadAttachment.textviewMandatory.visibility = View.GONE

        binding.sectionUploadAttachmentVerify.cameraButton.visibility = View.GONE
        binding.sectionUploadAttachmentVerify.textviewMandatory.visibility = View.GONE

        GlobalScope.launch {
            callApiAndUpdateLiveData()
        }
    }

    private suspend fun callApiAndUpdateLiveData() {
        paramPendingVerify?.documents?.forEach {
            coroutineScope {
                async {
                    val result = it.id?.let { file -> viewModel.getFile(file) }
                }
            }.await()
        }

        paramPendingVerify?.ts18e?.documents?.forEach {
            coroutineScope {
                async {
                    val result = it.id?.let { file -> viewModel.getVerifyFile(file) }
                }
            }.await()
        }

        lifecycleScope.launch {
            withContext(Dispatchers.Main) {
                viewModel.apiFileResponse.observe(this@ReadOnlyTs18aEFragment, { event ->
                    event.getContentIfNotHandled()?.let { response ->
                        when (response) {
                            is Resource.Success -> {
                                response.data?.let { response ->
                                    val bitmap =
                                        BitmapFactory.decodeStream(response.byteStream())

                                    val inflater =
                                        activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

                                    val attachmentView: View =
                                        inflater.inflate(R.layout.item_camera, null)
                                    val imageView =
                                        attachmentView.findViewById<ImageView>(R.id.image_view_attachment)
                                    imageView.setImageBitmap(bitmap)
                                    val deleteButton =
                                        attachmentView.findViewById<Button>(R.id.delete_button)
                                    deleteButton.visibility = View.GONE

                                    binding.sectionUploadAttachment.linearLayoutAttachment.addView(
                                        attachmentView
                                    )
                                }
                            }
                        }
                    }
                })

                viewModel.apiVerifyFileResponse.observe(this@ReadOnlyTs18aEFragment, { event ->
                    event.getContentIfNotHandled()?.let { response ->
                        when (response) {
                            is Resource.Success -> {
                                response.data?.let { response ->
                                    val bitmap =
                                        BitmapFactory.decodeStream(response.byteStream())

                                    val inflater =
                                        activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

                                    val attachmentView: View =
                                        inflater.inflate(R.layout.item_camera, null)
                                    val imageView =
                                        attachmentView.findViewById<ImageView>(R.id.image_view_attachment)
                                    imageView.setImageBitmap(bitmap)
                                    val deleteButton =
                                        attachmentView.findViewById<Button>(R.id.delete_button)
                                    deleteButton.visibility = View.GONE

                                    binding.sectionUploadAttachmentVerify.linearLayoutAttachment.addView(
                                        attachmentView
                                    )
                                }
                            }
                        }
                    }
                })
            }
        }
    }

    private fun initDContent() {
        binding.sectionD.buttonSignHereD.visibility = View.GONE

        binding.sectionD.edtTarikhLawatan.setText(TimeUtils.displayDate(paramPendingVerify?.ts18e?.tkhLawat ?: 0))
        binding.sectionD.edtMasaDari2.setText(TimeUtils.displayTime(paramPendingVerify?.ts18e?.mlawatMula ?: 0))
        binding.sectionD.edtMasaSampai2.setText(TimeUtils.displayTime(paramPendingVerify?.ts18e?.mlawatTamat ?: 0))
        binding.sectionD.imageViewSignD.setImageBitmap(paramPendingVerify!!.ts18e?.signPegawai.let {
            it?.let { it1 ->
                DisplayUtils.getBitmapFromBase64String(
                    it1
                )
            }
        })
        binding.sectionD.edtCatatan2.setText(paramPendingVerify?.ts18e?.catatan)
    }

    private fun initFContent() {
        binding.sectionF.checkboxSetuju.isClickable = false
        binding.sectionF.checkboxSetuju.isChecked = true
        binding.sectionF.buttonSaveF.visibility = View.GONE
        binding.sectionF.buttonNextF.visibility = View.GONE
    }

    private fun nextButtonListener() {
        binding.sectionAb.buttonNextAb.setOnClickListener { v ->
            scrollToTop()
            binding.sectionAb.sectionAb.visibility = View.GONE

            if (paramPendingVerify!!.getAnsuranNo?.toIntOrNull() == 1) {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_pendahuluan_semasa)
                binding.sectionC1.sectionC1.visibility = View.VISIBLE
            } else {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_berturut_ansuran)
                binding.sectionC2.sectionC2.visibility = View.VISIBLE
            }

            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionC1.buttonNextC1.setOnClickListener { v ->
            scrollToTop()
            binding.sectionC1.sectionC1.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionC2.buttonNextC2.setOnClickListener { v ->
            scrollToTop()
            binding.sectionC2.sectionC2.visibility = View.GONE
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionUploadAttachment.buttonNextUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.attachments_pengesahan)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionUploadAttachmentVerify.buttonNextUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.GONE
            binding.sectionD.sectionD.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.d_pengesahan_pegawai_penilai)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

        binding.sectionD.buttonNextD.setOnClickListener { v ->
            scrollToTop()
            binding.sectionD.sectionD.visibility = View.GONE
            binding.sectionF.sectionF.visibility = View.VISIBLE

            binding.textviewTitle.text = getString(R.string.f_peringatan)
            binding.seekBar.progress = binding.seekBar.progress + 1
        }

    }

    private fun previousButtonListener() {
        binding.sectionC1.buttonPreviousC1.setOnClickListener { v ->
            scrollToTop()
            binding.sectionAb.sectionAb.visibility = View.VISIBLE
            binding.sectionC1.sectionC1.visibility = View.GONE

            binding.textviewTitle.text = getString(R.string.a_b_profil_pemohon_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionC2.buttonPreviousC2.setOnClickListener { v ->
            scrollToTop()
            binding.sectionAb.sectionAb.visibility = View.VISIBLE
            binding.sectionC2.sectionC2.visibility = View.GONE

            binding.textviewTitle.text =
                getString(R.string.a_b_profil_pemohon_kebun)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionUploadAttachment.buttonPreviousUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            if (paramPendingVerify!!.getAnsuranNo?.toIntOrNull() == 1) {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_pendahuluan_semasa)
                binding.sectionC1.sectionC1.visibility = View.VISIBLE
            } else {
                binding.textviewTitle.text =
                    getString(R.string.c_laporan_pengesahan_lawatan_kebun_berturut_ansuran)
                binding.sectionC2.sectionC2.visibility = View.VISIBLE
            }

            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.GONE
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionUploadAttachmentVerify.buttonPreviousUploadAttachment.setOnClickListener { v ->
            scrollToTop()
            binding.textviewTitle.text = getString(R.string.attachments)
            binding.sectionUploadAttachment.sectionUploadAttachment.visibility = View.VISIBLE

            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.GONE
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionD.buttonPreviousD.setOnClickListener { v ->
            scrollToTop()
            binding.textviewTitle.text =
                getString(R.string.attachments_pengesahan)
            binding.sectionUploadAttachmentVerify.sectionUploadAttachment.visibility = View.VISIBLE

            binding.sectionD.sectionD.visibility = View.GONE
            binding.seekBar.progress = binding.seekBar.progress - 1
        }

        binding.sectionF.buttonPreviousF.setOnClickListener { v ->
            scrollToTop()
            binding.sectionD.sectionD.visibility = View.VISIBLE
            binding.sectionF.sectionF.visibility = View.GONE

            binding.textviewTitle.text = getString(R.string.d_pengesahan_pegawai_penilai)
            binding.seekBar.progress = binding.seekBar.progress - 1
        }
    }

    override fun onSignatureCaptured(bitmap: Bitmap, fileName: String) {
        when (fileName) {
            "D" -> {
                binding.sectionD.imageViewSignD.setImageBitmap(bitmap)
            }
        }
    }

    private fun scrollToTop() {
        binding.scrollview.fullScroll(View.FOCUS_UP)
        binding.scrollview.smoothScrollTo(0, 0)
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes)
        val path: String =
            MediaStore.Images.Media.insertImage(
                inContext.contentResolver,
                inImage,
                TimeUtils.getCurrentTimestamp().toString(),
                null
            )
        return Uri.parse(path)
    }

    fun getRealPathFromURI(uri: Uri?): String? {
        var path = ""
        if (activity!!.contentResolver != null) {
            val cursor: Cursor? =
                uri?.let { activity?.contentResolver?.query(it, null, null, null, null) }
            if (cursor != null) {
                cursor.moveToFirst()
                val idx: Int = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()
            }
        }
        return path
    }

    companion object {
        val TAG = ReadOnlyTs18aEFragment::class.java.simpleName!!

        @JvmStatic
        fun newInstance(
            paramPendingVerify: PendingVerify,
            paramTsApplication: TsApplication
        ) =
            ReadOnlyTs18aEFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM_PENDING_VERIFY, paramPendingVerify)
                    putParcelable(ARG_PARAM_TS_APPLICATION, paramTsApplication)
                }
            }

    }
}