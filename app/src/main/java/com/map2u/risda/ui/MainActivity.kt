package com.map2u.risda.ui

import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.map2u.risda.R
import com.map2u.risda.network.SessionManager
import com.map2u.risda.util.*


class MainActivity : AppCompatActivity() {
    // Initialise the DrawerLayout, NavigationView and ToggleBar
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var actionBarToggle: ActionBarDrawerToggle
    private lateinit var navigationView: NavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val permissions = arrayOf(
            "android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.ACCESS_FINE_LOCATION",
            "android.permission.ACCESS_COARSE_LOCATION",
            "android.permission.ACCESS_BACKGROUND_LOCATION",
            "android.permission.READ_PHONE_STATE",
            "android.permission.SYSTEM_ALERT_WINDOW",
            "android.permission.CAMERA"
        )

        val requestCode = 200
        requestPermissions(permissions, requestCode)

        // By default the app will go to Home screen
        val fragmentManager = supportFragmentManager
        fragmentManager.addOnBackStackChangedListener { this }
        fragmentManager.beginTransaction()
            .add(R.id.content_frame, HomeFragment.newInstance(), HomeFragment.TAG)
            .commitNow()

        val sessionManager = SessionManager(this)

        // Call findViewById on the DrawerLayout
        drawerLayout = findViewById(R.id.drawer_layout)
        title = getString(R.string.menu_utama)

        // Pass the ActionBarToggle action into the drawerListener
        actionBarToggle = ActionBarDrawerToggle(this, drawerLayout, 0, 0)
        drawerLayout.addDrawerListener(actionBarToggle)
        // Display the hamburger icon to launch the drawer
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//        supportActionBar?.setDisplayUseLogoEnabled(true)
//        supportActionBar?.setDisplayShowHomeEnabled(true)
//        supportActionBar?.setIcon(R.drawable.ic_launcher_none)
        // Call syncState() on the action bar so it'll automatically change to the back button when the drawer layout is open
        actionBarToggle.syncState()

        // Call findViewById on the NavigationView
        navigationView = findViewById(R.id.navigationView)
        navigationView.itemIconTintList = null

        val role = sessionManager.fetchRole()
        if(role != Constants.UserRole.KETUA_UNIT.role) {
            navigationView.menu.findItem(R.id.menu_pengesahan).isVisible = false
            navigationView.menu.findItem(R.id.menu_pengesahan_selesai).isVisible = false
        }

        // Call setNavigationItemSelectedListener on the NavigationView to detect when items are clicked
        navigationView.setNavigationItemSelectedListener { menuItem ->
            var fragment: Fragment? =
                this.supportFragmentManager.findFragmentById(R.id.content_frame)
            val tag: String?
            when (menuItem.itemId) {
                R.id.menu_home -> {
                    if (fragment !is HomeFragment) {
                        popFragmentStack()
                        fragment = HomeFragment.newInstance()
                        tag = HomeFragment.TAG

                        this.supportFragmentManager
                            .beginTransaction()
                            .addToBackStack(tag)
                            .replace(R.id.content_frame, fragment, tag)
                            .commit()
                    }
                    title = getString(R.string.menu_utama);
                    drawerLayout.closeDrawer(GravityCompat.START)
                    true
                }
                R.id.menu_temujanji -> {
                    if (fragment !is AppointmentsFragment) {
                        popFragmentStack()
                        fragment = AppointmentsFragment.newInstance()
                        tag = AppointmentsFragment.TAG

                        this.supportFragmentManager
                            .beginTransaction()
                            .addToBackStack(tag)
                            .replace(R.id.content_frame, fragment, tag)
                            .commit()
                    }
                    title = getString(R.string.senarai_temujanji);
                    drawerLayout.closeDrawer(GravityCompat.START)
                    true
                }
                R.id.menu_temujanji_selesai -> {
                    if (fragment !is ReportsFragment) {
                        popFragmentStack()
                        fragment = ReportsFragment.newInstance()
                        tag = ReportsFragment.TAG

                        this.supportFragmentManager
                            .beginTransaction()
                            .addToBackStack(tag)
                            .replace(R.id.content_frame, fragment, tag)
                            .commit()
                    }
                    title = getString(R.string.temujanji_selesai);
                    drawerLayout.closeDrawer(GravityCompat.START)
                    true
                }
                R.id.menu_cetakan -> {
                    if (fragment !is PrintFragment) {
                        popFragmentStack()
                        fragment = PrintFragment.newInstance()
                        tag = PrintFragment.TAG

                        this.supportFragmentManager
                            .beginTransaction()
                            .addToBackStack(tag)
                            .replace(R.id.content_frame, fragment, tag)
                            .commit()
                    }
                    title = getString(R.string.cetakan);
                    drawerLayout.closeDrawer(GravityCompat.START)
                    true
                }
                R.id.menu_pengesahan -> {
                    if (fragment !is VerifyFragment) {
                        popFragmentStack()
                        fragment = VerifyFragment.newInstance()
                        tag = VerifyFragment.TAG

                        this.supportFragmentManager
                            .beginTransaction()
                            .addToBackStack(tag)
                            .replace(R.id.content_frame, fragment, tag)
                            .commit()
                    }
                    title = getString(R.string.pengesahan);
                    drawerLayout.closeDrawer(GravityCompat.START)
                    true
                }
                R.id.menu_pengesahan_selesai -> {
                    if (fragment !is VerifyCompleteFragment) {
                        popFragmentStack()
                        fragment = VerifyCompleteFragment.newInstance()
                        tag = VerifyCompleteFragment.TAG

                        this.supportFragmentManager
                            .beginTransaction()
                            .addToBackStack(tag)
                            .replace(R.id.content_frame, fragment, tag)
                            .commit()
                    }
                    title = getString(R.string.pengesahan_selesai);
                    drawerLayout.closeDrawer(GravityCompat.START)
                    true
                }
                R.id.menu_pelan_tanah -> {
                    val launchIntent =
                        packageManager.getLaunchIntentForPackage("com.esri.fieldmaps")
                    if (launchIntent != null) {
                        startActivity(launchIntent)
                    } else {
                        drawerLayout.errorSnack(
                            getString(R.string.no_argis_app),
                            Snackbar.LENGTH_LONG
                        )
                    }
                    true
                }
                R.id.menu_paparan_peta -> {
                    val url =
                        "https://gisportal.risda.gov.my/arcgis/apps/opsdashboard/index.html#/97c7352a7daa4552bb2a9e09e19233f1"
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(url)
                    startActivity(intent)
                    true
                }
                R.id.menu_about -> {
                    onAboutClick()
                    true
                }
                R.id.menu_logout -> {
                    // revoke authorization by clearing shared preference
                    sessionManager.removeAll()

                    // Destroy main activity and back to login activity
                    LoginActivity().navigateToLoginScreen(this@MainActivity)
                    finish()
                    true
                }
                else -> {
                    title = "";
                    false
                }
            }
        }

        // check location setting
        val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        if (!lm!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            // notify user
            AlertDialog.Builder(this)
                .setTitle(R.string.open_location_settings)
                .setPositiveButton(android.R.string.ok
                ) { paramDialogInterface, paramInt ->
                    startActivity(
                        Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    )
                }
                .setNegativeButton(R.string.cancel, null)
                .show()
        }
    }

    private fun onAboutClick() {
        try {
            val packageInfo: PackageInfo =
                this.packageManager.getPackageInfo(this.packageName, 0)
            val versionName = packageInfo.versionName
            val versionCode = packageInfo.versionCode.toString()
            val alertDialog: AlertDialog.Builder =
                AlertDialog.Builder(this, R.style.AlertDialogTheme)
            alertDialog.setTitle(getString(R.string.info) + " " + getString(R.string.app_name))

            val inflater = this.layoutInflater
            val alertDialogView: View = inflater.inflate(R.layout.dialog_about, null)
            val versionNameTextView =
                alertDialogView.findViewById<TextView>(R.id.textview_version_name)
            val versionCodeTextView =
                alertDialogView.findViewById<TextView>(R.id.textview_version_code)
            versionNameTextView.text = versionName
            versionCodeTextView.text = versionCode
            alertDialog.setView(alertDialogView)
            alertDialog.setNeutralButton(
                android.R.string.ok
            ) { dialog, _ -> dialog.dismiss() }
            alertDialog.show()
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

    private fun popFragmentStack() {
        // pop out all other screens before inflate the selected option
        while (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStackImmediate()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        drawerLayout.openDrawer(navigationView)
        return true
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
        title = ""
    }

    fun navigateToMainScreen(context: Context) {
        val intent = Intent(context, MainActivity::class.java)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        context.startActivity(intent)
    }

}