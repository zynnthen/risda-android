package com.map2u.risda.ui.shared

import android.app.Dialog
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.dascom.print.ESCPOS
import com.dascom.print.connection.BluetoothConnection
import com.dascom.print.connection.IConnection
import com.dascom.print.utils.BluetoothUtils
import com.map2u.risda.R
import com.map2u.risda.model.appointment.AppointmentItem
import com.map2u.risda.model.appointment.Form
import com.map2u.risda.model.appointment.Participant
import com.map2u.risda.model.appointment.ReportItem
import com.map2u.risda.network.SessionManager
import com.map2u.risda.util.TimeUtils
import com.map2u.risda.util.display.DisplayUtils
import com.map2u.risda.util.display.PrintUtils
import java.util.concurrent.Executors


class PrinterDialogFragment(
    private val reportItem: ReportItem?,
    private val appointmentItem: AppointmentItem?,
    private val participant: Participant? = null,
    private val form: Form? = null
) : DialogFragment() {

    // This property is for printing usage
    val mExecutorService = Executors.newSingleThreadExecutor()
    var mIConnection: IConnection? = null
    var mESCPOS: ESCPOS? = null

    val DISPLAY_TEXT = "Sila Pilih Printer"

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity!!, R.style.AlertDialogTheme)
        val alertDialogView: View =
            activity!!.layoutInflater.inflate(R.layout.dialog_print_receipt, null)

        val statusPrinter =
            alertDialogView.findViewById<TextView>(R.id.textview_printerStatus)
        val spinnerBle =
            alertDialogView.findViewById<Spinner>(R.id.spinner_ble)
        val connectPrinter =
            alertDialogView.findViewById<TextView>(R.id.textview_connect_printer)

        val namaPemohon =
            alertDialogView.findViewById<TextView>(R.id.textview_namaPemohon)
        val noTs =
            alertDialogView.findViewById<TextView>(R.id.textview_noTs)
        val noAnsuran =
            alertDialogView.findViewById<TextView>(R.id.textview_noAnsuran)
        val luasLulusTs =
            alertDialogView.findViewById<TextView>(R.id.textview_luasLulusTs)
        val namaPengesah =
            alertDialogView.findViewById<TextView>(R.id.textview_namaPengesah)
        val jawatanPengesah =
            alertDialogView.findViewById<TextView>(R.id.textview_jawatanPengesah)
        val tarikhLawatan =
            alertDialogView.findViewById<TextView>(R.id.textview_tarikhLawatan)

        val devices: ArrayList<String> = ArrayList()
        devices.add(DISPLAY_TEXT)
        val bondedDevices = BluetoothUtils.getBondedDevices()
        for (device in bondedDevices) {
            devices.add("${device.name} ${device.address}")
        }

        val adapter = ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, devices)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerBle.adapter = adapter
        spinnerBle.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                // do nothing
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (adapter.getItem(position)!! == DISPLAY_TEXT) {
                    return
                }
                mExecutorService.execute {
                    val macAddr = adapter.getItem(position)
                    val mac = macAddr?.substring(macAddr.length - 17, macAddr.length)
                    mIConnection = BluetoothConnection(
                        BluetoothUtils.getBluetoothDevice(mac)
                    )
                }
            }
        }

        connectPrinter.setOnClickListener {
            if (mIConnection != null) {
                mExecutorService.execute {
                    if (mIConnection!!.connect()) {
                        mESCPOS = ESCPOS(mIConnection)

                        activity?.runOnUiThread {
                            statusPrinter.text = getString(R.string.connected)
                        }
                    } else {
                        activity?.runOnUiThread {
                            statusPrinter.text = getString(R.string.failed_to_connect_printer)
                        }
                    }
                }
            } else {
                statusPrinter.text = ""
            }
        }

        val jenisBorang = DisplayUtils.getJenisBorang(
            reportItem?.form?.jenisLK,
            reportItem?.appointmentItem?.moduleOwner,
            reportItem?.appointmentItem?.tsPendekatan
        )

        var lotDimilikiLuasTs = 0.0
        var deductedValue = 0.0

        if (reportItem != null) {
            val luasTanamanLain = reportItem.form?.luasTanamanLain?.toDouble() ?: 0.0
            val luasHutan = reportItem.form?.luasHutan?.toDouble() ?: 0.0
            val luasRumah = reportItem.form?.luasRumah?.toDouble() ?: 0.0
            val luasLainLain = reportItem.form?.luasLainLain?.toDouble() ?: 0.0

            deductedValue = luasTanamanLain + luasHutan + luasRumah + luasLainLain
            lotDimilikiLuasTs = if(jenisBorang.contains("TS18A")) {
                reportItem.participant?.tsApplication?.lotDimilikiList?.get(0)?.luasTs ?: 0.0
            } else {
                reportItem.appointmentItem?.tsApplication?.lotDimilikiList?.get(0)?.luasTs ?: 0.0
            }

            namaPemohon.text = reportItem.appointmentItem?.tsApplication?.namaPemohon
            if(jenisBorang.contains("TS18A")) {
                noTs.text = reportItem.appointmentItem?.tsApplication?.tsMohonNo
            } else {
                noTs.text = reportItem.appointmentItem?.tsMohonId
            }
            noAnsuran.text = reportItem.appointmentItem?.getAnsuranNo
            jawatanPengesah.text = reportItem.form?.jawPemeriksa
            if (reportItem.form?.tkhLawat != null) {
                tarikhLawatan.text = TimeUtils.displayDate(timestamp = reportItem.form?.tkhLawat!!)
            }
        }
        if (appointmentItem != null) {
            namaPemohon.text = appointmentItem.tsApplication?.namaPemohon
            if(jenisBorang.contains("TS18A")) {
                noTs.text = appointmentItem.tsApplication?.tsMohonNo
            } else {
                noTs.text = appointmentItem.tsMohonId
            }
            noAnsuran.text = appointmentItem.getAnsuranNo

        }
        if (participant != null) {
            namaPemohon.text = participant.tsApplication?.namaPemohon
            noTs.text = participant.tsApplication?.tsMohonNo

            lotDimilikiLuasTs = participant.tsApplication?.lotDimilikiList?.get(0)?.luasTs ?: 0.0
        }
        if (form != null) {
            val luasTanamanLain = form.luasTanamanLain?.toDouble() ?: 0.0
            val luasHutan = form.luasHutan?.toDouble() ?: 0.0
            val luasRumah = form.luasRumah?.toDouble() ?: 0.0
            val luasLainLain = form.luasLainLain?.toDouble() ?: 0.0
            deductedValue = luasTanamanLain + luasHutan + luasRumah + luasLainLain

            jawatanPengesah.text = form.jawPemeriksa
            if (form.tkhLawat != null) {
                tarikhLawatan.text = TimeUtils.displayDate(timestamp = form.tkhLawat!!)
            }
        }

        val sessionManager = context?.let { SessionManager(it) }!!
        namaPengesah.text = sessionManager.fetchName()
        if(jenisBorang.contains("TS18A")) {
            luasLulusTs.text = lotDimilikiLuasTs.toString()
        } else {
            luasLulusTs.text = String.format("%.4f", (lotDimilikiLuasTs - deductedValue))
        }


        if(jawatanPengesah.text.isNullOrEmpty()) {
            jawatanPengesah.text = sessionManager.fetchDesignation()
        }
        if(tarikhLawatan.text.isNullOrEmpty()) {
            tarikhLawatan.text = TimeUtils.displayDate(timestamp = TimeUtils.getCurrentTimestamp())
        }

        builder.setView(alertDialogView);

        builder.setTitle(getString(R.string.resit_tanam_semula))

        val risdaLogoBitmap = BitmapFactory.decodeResource(resources, R.drawable.risda_mini)
        builder.setPositiveButton(
            getString(R.string.print)
        ) { dialog, message ->
            mExecutorService.execute {
                try {
                    val displayCurrentTime = "${TimeUtils.displayDate(timestamp = TimeUtils.getCurrentTimestamp())} ${TimeUtils.displayTime(timestamp = TimeUtils.getCurrentTimestamp())}"
//                    mESCPOS!!.printBitmap(risdaLogoBitmap, PrintUtils.inchToPoint(0.9f), 0, false)
                    mESCPOS!!.printText("Pihak Berkuasa Kemajuan\n", PrintUtils.inchToPoint(0.7f), 0)
                    mESCPOS!!.printText("Pekebun Kecil Perusahaan Getah", PrintUtils.inchToPoint(0.5f), 0)
                    mESCPOS!!.printText("(RISDA)", PrintUtils.inchToPoint(1.2f), 0)
                    mESCPOS!!.printText(
                        "\n\n",
                        PrintUtils.inchToPoint(0.1f), 0
                    )
                    mESCPOS!!.printText(
                        "Nama Pemohon: ${namaPemohon.text}\n",
                        PrintUtils.inchToPoint(0.1f), 0
                    )
                    mESCPOS!!.printText(
                        "No TS: ${noTs.text}\n",
                        PrintUtils.inchToPoint(0.1f), 0
                    )
                    mESCPOS!!.printText(
                        "No Ansuran: ${noAnsuran.text}\n",
                        PrintUtils.inchToPoint(0.1f), 0
                    )
                    mESCPOS!!.printText("Luas Kebun TS (Hektar): ${luasLulusTs.text}\n", PrintUtils.inchToPoint(0.1f), 0)
                    mESCPOS!!.printText(
                        "Nama Pemeriksa: ${namaPengesah.text}\n",
                        PrintUtils.inchToPoint(0.1f), 0
                    )
                    mESCPOS!!.printText(
                        "Tarikh Lawatan: ${tarikhLawatan.text}",
                        PrintUtils.inchToPoint(0.1f), 0
                    )
                    mESCPOS!!.printText(
                        "\n\n\n",
                        PrintUtils.inchToPoint(0.1f), 0
                    )
                    mESCPOS!!.printText(
                        "Resit ini dijana oleh Aplikasi RISDA Smartmap\n",
                        PrintUtils.inchToPoint(0.1f), 0
                    )
                    mESCPOS!!.printText(
                        "Cetak pada: ${displayCurrentTime}\n",
                        PrintUtils.inchToPoint(0.1f), 0
                    )
                    mESCPOS!!.printText(
                        "\n\n\n",
                        PrintUtils.inchToPoint(0.1f), 0
                    )

                    try {
                        Thread.sleep(300) // wait for the Bluetooth to completely disconnect
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }

                } catch (e: Exception) {
                    Log.e("PrinterDialogFragment", e.toString())
                    activity?.runOnUiThread {
                        val toast = Toast.makeText(activity, e.toString(), Toast.LENGTH_SHORT)
                        toast.show()
                    }
                }
            }
        }

        builder.setNegativeButton(
            R.string.cancel
        ) { dialog, message -> dialog.dismiss() }

        return builder.create()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mIConnection != null) {
            mExecutorService.execute {
                mIConnection!!.disconnect()
            }
        }
    }
}
