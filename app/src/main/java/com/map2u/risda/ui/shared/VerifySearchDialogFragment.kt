package com.map2u.risda.ui.shared

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Spinner
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.DialogFragment
import com.google.android.material.textfield.TextInputEditText
import com.map2u.risda.R
import com.map2u.risda.ui.AppointmentsFragment
import com.map2u.risda.ui.VerifyCompleteFragment
import com.map2u.risda.ui.VerifyFragment
import com.map2u.risda.util.Constants
import com.map2u.risda.util.StringUtils
import com.map2u.risda.util.TimeUtils.getTimestamp
import com.map2u.risda.util.toast
import java.text.SimpleDateFormat
import java.util.*


class VerifySearchDialogFragment(
    private var searchFieldsCount: Int,
    private var tsMohonIdPlaceholder: String?,
    private var tarikhStartPlaceholder: String,
    private var tarikhEndPlaceholder: String,
    private var kpPpkPlaceholder: String?,
    private var jenisBorangPlaceholder: String?
) : DialogFragment() {

    var isValidTimeGap = true

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity!!, R.style.AlertDialogTheme)
        val alertDialogView: View =
            activity!!.layoutInflater.inflate(R.layout.dialog_search_verify, null)

        val tsMohonId =
            alertDialogView.findViewById<TextInputEditText>(R.id.edt_ts_mohon_id)
        val tarikhStart =
            alertDialogView.findViewById<TextInputEditText>(R.id.edt_tarikh_start)
        val tarikhEnd =
            alertDialogView.findViewById<TextInputEditText>(R.id.edt_tarikh_end)
        val kpPpk =
            alertDialogView.findViewById<TextInputEditText>(R.id.edt_kp_ppk)
        val jenisBorangSpinner =
            alertDialogView.findViewById<Spinner>(R.id.spinner_jenis_borang)
        tsMohonId.setText(tsMohonIdPlaceholder)
        tarikhStart.setText(tarikhStartPlaceholder)
        tarikhEnd.setText(tarikhEndPlaceholder)
        kpPpk.setText(kpPpkPlaceholder)
        if(jenisBorangPlaceholder == "TS18") {
            jenisBorangSpinner.setSelection(1)
        } else if (jenisBorangPlaceholder == "TS18A") {
            jenisBorangSpinner.setSelection(2)
        }

        /*** Date input picker ***/
        val myCalendar1: Calendar = Calendar.getInstance()

        val date1 =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                myCalendar1.set(Calendar.YEAR, year)
                myCalendar1.set(Calendar.MONTH, monthOfYear)
                myCalendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val sdf = SimpleDateFormat("dd-MM-yyyy")
                tarikhStart.setText(sdf.format(myCalendar1.getTime()))
            }
        tarikhStart.setOnClickListener { v ->
            DatePickerDialog(
                context!!, date1, myCalendar1[Calendar.YEAR], myCalendar1[Calendar.MONTH],
                myCalendar1[Calendar.DAY_OF_MONTH]
            ).show()
        }

        val myCalendar2: Calendar = Calendar.getInstance()
        val date2 =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                myCalendar2.set(Calendar.YEAR, year)
                myCalendar2.set(Calendar.MONTH, monthOfYear)
                myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val sdf = SimpleDateFormat("dd-MM-yyyy")
                tarikhEnd.setText(sdf.format(myCalendar2.getTime()))
            }
        tarikhEnd.setOnClickListener { v ->
            DatePickerDialog(
                context!!, date2, myCalendar2[Calendar.YEAR], myCalendar2[Calendar.MONTH],
                myCalendar2[Calendar.DAY_OF_MONTH]
            ).show()
        }

        tarikhStart.doAfterTextChanged { checkTimeGap(tarikhStart.text.toString(), tarikhEnd.text.toString()) }
        tarikhEnd.doAfterTextChanged { checkTimeGap(tarikhStart.text.toString(), tarikhEnd.text.toString()) }

        builder.setView(alertDialogView);

        builder.setTitle(getString(R.string.search))

        builder.setPositiveButton(
            getString(android.R.string.ok)
        ) { dialog, message ->
            // to check how many filter applied
            if(isValidTimeGap) {
                var count = 0
                if (!StringUtils.isNullOrEmpty(tsMohonId.text.toString())) {
                    tsMohonIdPlaceholder = tsMohonId.text.toString()
                    count++
                }
                if (!StringUtils.isNullOrEmpty(tarikhStart.text.toString())) {
                    tarikhStartPlaceholder = tarikhStart.text.toString()
                    count++
                }
                if (!StringUtils.isNullOrEmpty(tarikhEnd.text.toString())) {
                    tarikhEndPlaceholder = tarikhEnd.text.toString()
                    count++
                }
                if (!StringUtils.isNullOrEmpty(kpPpk.text.toString())) {
                    kpPpkPlaceholder = kpPpk.text.toString()
                    count++
                }
                if (jenisBorangSpinner.selectedItemPosition != 0) {
                    jenisBorangPlaceholder = jenisBorangSpinner.selectedItem.toString()
                    count++
                } else {
                    jenisBorangPlaceholder = null
                }
                searchFieldsCount = count

                sendOk(
                    searchFieldsCount = searchFieldsCount,
                    tsMohonIdPlaceholder = tsMohonIdPlaceholder,
                    tarikhStartPlaceholder = tarikhStartPlaceholder,
                    tarikhEndPlaceholder = tarikhEndPlaceholder,
                    kpPpkPlaceholder = kpPpkPlaceholder,
                    jenisBorangPlaceholder = jenisBorangPlaceholder
                )
            }
        }

        builder.setNeutralButton(
            R.string.clear_all
        ) { dialog, message ->
            clearAll()
        }

        builder.setNegativeButton(
            R.string.cancel
        ) { dialog, message -> dialog.dismiss() }

        return builder.create()
    }

    private fun clearAll() {
        if (targetFragment == null) {
            return
        }
        val intent: Intent = AppointmentsFragment.newIntent(Constants.SearchAction.CLEAR_ALL.action)
        targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
        dismiss()
    }

    private fun sendOk(
        searchFieldsCount: Int,
        tsMohonIdPlaceholder: String?,
        tarikhStartPlaceholder: String,
        tarikhEndPlaceholder: String,
        kpPpkPlaceholder: String?,
        jenisBorangPlaceholder: String?
    ) {
        if (targetFragment == null) {
            return
        }

        when(targetFragment!!.javaClass.simpleName) {
            VerifyFragment.TAG -> {
                val intent: Intent = VerifyFragment.newIntent(
                    Constants.SearchAction.OK.action,
                    searchFieldsCount = searchFieldsCount,
                    tsMohonIdPlaceholder = tsMohonIdPlaceholder,
                    tarikhStartPlaceholder = tarikhStartPlaceholder,
                    tarikhEndPlaceholder = tarikhEndPlaceholder,
                    kpPpkPlaceholder = kpPpkPlaceholder,
                    jenisBorangPlaceholder = jenisBorangPlaceholder
                )
                targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
            }
            VerifyCompleteFragment.TAG -> {
                val intent: Intent = VerifyCompleteFragment.newIntent(
                    Constants.SearchAction.OK.action,
                    searchFieldsCount = searchFieldsCount,
                    tsMohonIdPlaceholder = tsMohonIdPlaceholder,
                    tarikhStartPlaceholder = tarikhStartPlaceholder,
                    tarikhEndPlaceholder = tarikhEndPlaceholder,
                    kpPpkPlaceholder = kpPpkPlaceholder,
                    jenisBorangPlaceholder = jenisBorangPlaceholder
                )
                targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
            }
        }

        dismiss()
    }

    private fun checkTimeGap(start: String, end: String) {
        val startTimestamp = getTimestamp(start)
        val endTimestamp = getTimestamp(end)

        // more than one month timestamp 2629743000
        if(startTimestamp > endTimestamp || endTimestamp - startTimestamp > 2629743000) {
            context?.toast("Invalid time range, max 1 month data.")

            isValidTimeGap = false
            return
        }
        isValidTimeGap = true
    }
}