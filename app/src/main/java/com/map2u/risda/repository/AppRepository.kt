package com.map2u.risda.repository

import com.map2u.risda.network.RequestBodies
import com.map2u.risda.network.RetrofitInstance
import com.map2u.risda.util.Constants
import com.map2u.risda.util.TimeUtils
import okhttp3.MultipartBody

class AppRepository {
    /*** Auth service ***/
    suspend fun loginUser(body: RequestBodies.LoginBody) = RetrofitInstance.authApi.loginUser(body)
    suspend fun refreshToken(body: RequestBodies.RefreshTokenBody) =
        RetrofitInstance.authApi.refreshToken(body)

    /*** Appointment service ***/
    suspend fun getAppointmentsByNumberOfDays(
        idToken: String,
        days: Int = 7,
        tsMohonId: String?,
        noKpPemohon: String?,
        namaPemohon: String?,
        noLot: String?,
        noGeran: String?,
        date: String?,
        pegKemaskini: String?
    ) =
        RetrofitInstance.appointmentApi.getLatestAppointmentsByNumberOfDays(
            idToken,
            days,
            tsMohonId,
            noKpPemohon,
            namaPemohon,
            noLot,
            noGeran,
            date,
            pegKemaskini
        )

    suspend fun getListOfAppointments(
        idToken: String,
        page: Int,
        tsMohonId: String?,
        noKpPemohon: String?,
        namaPemohon: String?,
        noLot: String?,
        noGeran: String?,
        date: String?,
        pegKemaskini: String?,
        ptCode: String? = null
    ) =
        RetrofitInstance.appointmentApi.getListOfAppointments(
            idToken,
            page,
            Constants.PAGINATION_LIMIT,
            tsMohonId,
            noKpPemohon,
            namaPemohon,
            noLot,
            noGeran,
            date,
            pegKemaskini,
            ptCode
        )

    suspend fun addTS18Form(
        idToken: String,
        body: RequestBodies.AddTS18Form
    ) =
        RetrofitInstance.appointmentApi.addTS18Form(
            idToken,
            body
        )

    suspend fun updateTS18Form(
        idToken: String,
        body: RequestBodies.AddTS18Form
    ) =
        RetrofitInstance.appointmentApi.updateTS18Form(
            idToken,
            body
        )

    suspend fun addTS18AForm(
        idToken: String,
        body: RequestBodies.AddTS18AForm
    ) =
        RetrofitInstance.appointmentApi.addTS18AForm(
            idToken,
            body
        )

    suspend fun updateTS18AForm(
        idToken: String,
        body: RequestBodies.AddTS18AForm
    ) =
        RetrofitInstance.appointmentApi.updateTS18AForm(
            idToken,
            body
        )

    suspend fun updateAppointmentStatus(
        idToken: String,
        body: RequestBodies.UpdateAppointmentStatus
    ) =
        RetrofitInstance.appointmentApi.updateAppointmentStatus(
            idToken,
            body
        )

    suspend fun uploadFile(
        idToken: String,
        formId: String,
        docType: String,
        file: MultipartBody.Part
    ) =
        RetrofitInstance.appointmentApi.uploadFile(
            idToken,
            formId,
            docType,
            file
        )

    suspend fun getFile(
        idToken: String,
        fileId: String
    ) =
        RetrofitInstance.appointmentApi.getFile(
            idToken,
            fileId
        )

    suspend fun addTS18EForm(
        idToken: String,
        body: RequestBodies.AddTS18EForm
    ) =
        RetrofitInstance.appointmentApi.addTS18EForm(
            idToken,
            body
        )

    suspend fun getListOfFormsPendingValidation(
        idToken: String,
        ptCode: String? = null,
        start: Long = TimeUtils.get1JanTimestamp(),
        end: Long = TimeUtils.getCurrentTimestamp(),
        noKpPpk: String?,
        jenisLK: String?,
        tsMohonId: String?
    ) =
        RetrofitInstance.appointmentApi.getFormsPendingValidation(
            idToken,
            ptCode,
            start,
            end,
            noKpPpk,
            jenisLK,
            tsMohonId
        )

    suspend fun getFormsCompleteValidation(
        idToken: String,
        page: Int,
        ptCode: String? = null,
        start: Long = TimeUtils.get1JanTimestamp(),
        end: Long = TimeUtils.getCurrentTimestamp(),
        noKpPpk: String?,
        jenisLK: String?,
        tsMohonId: String?
    ) =
        RetrofitInstance.appointmentApi.getFormsCompleteValidation(
            idToken,
            page,
            Constants.PAGINATION_LIMIT,
            ptCode,
            start,
            end,
            noKpPpk,
            jenisLK,
            tsMohonId
        )
}