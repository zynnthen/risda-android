package com.map2u.risda.network

import com.map2u.risda.model.appointment.response.LatestAppointmentsResponse
import com.map2u.risda.model.appointment.response.ListOfAppointmentResponse
import com.map2u.risda.model.appointment.response.ListOfCompletedFormResponse
import com.map2u.risda.model.appointment.response.ListOfPendingFormResponse
import com.map2u.risda.model.form.response.UpdateAppointmentStatusResponse
import com.map2u.risda.model.form.response.UpdateFormResponse
import com.map2u.risda.util.Constants
import com.map2u.risda.util.TestConstants
import com.map2u.risda.util.TimeUtils.get1JanTimestamp
import com.map2u.risda.util.TimeUtils.getCurrentTimestamp
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface AppointmentService {
    @GET("${TestConstants.ENDPOINT}/api/appointment/list/latest/days/{days}")
    suspend fun getLatestAppointmentsByNumberOfDays(
        @Header("Authorization") idToken: String,
        @Path("days") days: Int = 7,
        @Query("tsMohonId") tsMohonId: String?,
        @Query("noKpPemohon") noKpPemohon: String?,
        @Query("namaPemohon") namaPemohon: String?,
        @Query("noLot") noLot: String?,
        @Query("noGeran") noGeran: String?,
        @Query("date") date: String?,
        @Query("pegKemaskini") pegKemaskini: String?,
        @Query("statusTJanji") statusTJanji: String = Constants.BARU,
        @Query("tkhTJanji") tkhTJanji: String = "ASC"
    ): Response<LatestAppointmentsResponse>

    @GET("${TestConstants.ENDPOINT}/api/appointment/list/{page}/{limit}")
    suspend fun getListOfAppointments(
        @Header("Authorization") idToken: String,
        @Path("page") page: Int,
        @Path("limit") limit: Int = Constants.PAGINATION_LIMIT,
        @Query("tsMohonId") tsMohonId: String?,
        @Query("noKpPemohon") noKpPemohon: String?,
        @Query("namaPemohon") namaPemohon: String?,
        @Query("noLot") noLot: String?,
        @Query("noGeran") noGeran: String?,
        @Query("date") date: String?,
//        @Query("start") start: Long?,
//        @Query("end") end: Long?,
        @Query("pegKemaskini") pegKemaskini: String?,
        @Query("ptCode") ptCode: String?,
        @Query("statusTJanji") statusTJanji: String = Constants.SELESAI,
        @Query("tkhTJanji") tkhTJanji: String? = "DESC",
    ): Response<ListOfAppointmentResponse>

    @PUT("${TestConstants.ENDPOINT}/api/appointment/ts18/update")
    suspend fun updateTS18Form(
        @Header("Authorization") idToken: String,
        @Body body: RequestBodies.AddTS18Form
    ): Response<UpdateFormResponse>

    @PUT("${TestConstants.ENDPOINT}/api/appointment/ts18a/update")
    suspend fun updateTS18AForm(
        @Header("Authorization") idToken: String,
        @Body body: RequestBodies.AddTS18AForm
    ): Response<UpdateFormResponse>

    @POST("${TestConstants.ENDPOINT}/api/appointment/ts18/save")
    suspend fun addTS18Form(
        @Header("Authorization") idToken: String,
        @Body body: RequestBodies.AddTS18Form
    ): Response<UpdateFormResponse>

    @POST("${TestConstants.ENDPOINT}/api/appointment/ts18a/save")
    suspend fun addTS18AForm(
        @Header("Authorization") idToken: String,
        @Body body: RequestBodies.AddTS18AForm
    ): Response<UpdateFormResponse>

    @PUT("${TestConstants.ENDPOINT}/api/appointment/status/update")
    suspend fun updateAppointmentStatus(
        @Header("Authorization") idToken: String,
        @Body body: RequestBodies.UpdateAppointmentStatus
    ): Response<UpdateAppointmentStatusResponse>

    @Multipart
    @POST("${TestConstants.ENDPOINT}/api/appointment/file/{formId}/{docType}")
    suspend fun uploadFile(
        @Header("Authorization") idToken: String,
        @Path("formId") formId: String,
        @Path("docType") docType: String,
        @Part file: MultipartBody.Part
    ): Response<UpdateFormResponse>

    @GET("${TestConstants.ENDPOINT}/api/appointment/file/{fileId}")
    suspend fun getFile(
        @Header("Authorization") idToken: String,
        @Path("fileId") fileId: String
    ): Response<ResponseBody>

    @POST("${TestConstants.ENDPOINT}/api/appointment/ts18e/save")
    suspend fun addTS18EForm(
        @Header("Authorization") idToken: String,
        @Body body: RequestBodies.AddTS18EForm
    ): Response<UpdateFormResponse>

    @GET("${TestConstants.ENDPOINT}/api/appointment/list/forms/pending/validation")
    suspend fun getFormsPendingValidation(
        @Header("Authorization") idToken: String,
        @Query("ptCode") ptCode: String?,
        @Query("start") start: Long = get1JanTimestamp(),
        @Query("end") end: Long = getCurrentTimestamp(),
        @Query("noKpPpk") noKpPpk: String?,
        @Query("jenisLK") jenisLK: String?,
        @Query("tsMohonId") tsMohonId: String?
    ): Response<ListOfPendingFormResponse>

    @GET("${TestConstants.ENDPOINT}/api/appointment/list/forms/complete/validation/{page}/{limit}")
    suspend fun getFormsCompleteValidation(
        @Header("Authorization") idToken: String,
        @Path("page") page: Int,
        @Path("limit") limit: Int = Constants.PAGINATION_LIMIT,
        @Query("ptCode") ptCode: String?,
        @Query("start") start: Long = get1JanTimestamp(),
        @Query("end") end: Long = getCurrentTimestamp(),
        @Query("noKpPpk") noKpPpk: String?,
        @Query("jenisLK") jenisLK: String?,
        @Query("tsMohonId") tsMohonId: String?
    ): Response<ListOfCompletedFormResponse>
}