package com.map2u.risda.network

import com.map2u.risda.model.login.LoginResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthService {
    @POST("api/auth/login")
    suspend fun loginUser(@Body body:RequestBodies.LoginBody): Response<LoginResponse>

    @POST("api/auth/refresh")
    suspend fun refreshToken(@Body body:RequestBodies.RefreshTokenBody): Response<LoginResponse>
}