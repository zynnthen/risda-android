package com.map2u.risda.network

import android.content.Context
import android.content.SharedPreferences
import com.map2u.risda.R

class SessionManager (context: Context){
    private var prefs: SharedPreferences = context.getSharedPreferences(context.getString(R.string.pref_file_name), Context.MODE_PRIVATE)

    companion object {
        const val REFRESH_TOKEN = "refresh_token"
        const val ID_TOKEN = "id_token"
        const val USERNAME = "username"
        const val NAME = "name"
        const val DESIGNATION = "designation"
        const val PT_CODE = "pt_code"
        const val ROLE = "role"
        const val LAST_LOGIN = "last_login"
    }

    /**
     * Function to save refresh token
     */
    fun saveRefreshToken(token: String?) {
        val editor = prefs.edit()
        editor.putString(REFRESH_TOKEN, token)
        editor.apply()
    }

    /**
     * Function to fetch refresh token
     */
    fun fetchRefreshToken(): String? {
        return prefs.getString(REFRESH_TOKEN, null)
    }

    /**
     * Function to save id token
     */
    fun saveIdToken(token: String?) {
        val editor = prefs.edit()
        editor.putString(ID_TOKEN, token)
        editor.apply()
    }

    /**
     * Function to fetch id token
     */
    fun fetchIdToken(): String? {
        return prefs.getString(ID_TOKEN, null)
    }

    /**
     * Function to save, fetch username
     */
    fun saveUsername(username: String?) {
        val editor = prefs.edit()
        editor.putString(USERNAME, username)
        editor.apply()
    }

    fun fetchUsername(): String? {
        return prefs.getString(USERNAME, null)
    }

    /**
     * Function to save, fetch name
     */
    fun saveName(name: String?) {
        val editor = prefs.edit()
        editor.putString(NAME, name)
        editor.apply()
    }

    fun fetchName(): String? {
        return prefs.getString(NAME, null)
    }

    /**
     * Function to save, fetch designation
     */
    fun saveDesignation(designation: String) {
        val editor = prefs.edit()
        editor.putString(DESIGNATION, designation)
        editor.apply()
    }

    fun fetchDesignation(): String? {
        return prefs.getString(DESIGNATION, "")
    }

    /**
     * Function to save, fetch ptCode
     */
    fun savePtCode(ptCode: String) {
        val editor = prefs.edit()
        editor.putString(PT_CODE, ptCode)
        editor.apply()
    }

    fun fetchPtCode(): String? {
        return prefs.getString(PT_CODE, "")
    }

    /**
     * Function to save, fetch, user role
     */
    fun saveRole(role: Int) {
        val editor = prefs.edit()
        editor.putInt(ROLE, role)
        editor.apply()
    }

    fun fetchRole(): Int? {
        return prefs.getInt(ROLE, 0)
    }

    /**
     * Function to save, fetch last login
     */
    fun saveLastLogin(lastLogin: Long) {
        val editor = prefs.edit()
        editor.putLong(LAST_LOGIN, lastLogin)
        editor.apply()
    }

    fun fetchLastLogin(): Long? {
        return prefs.getLong(LAST_LOGIN, 0)
    }

    /**
     * Function to clear shared preference
     */
    fun removeAll() {
        val editor = prefs.edit()
        editor.clear()
        editor.apply()
    }
}