package com.map2u.risda.network

import com.google.gson.GsonBuilder
import com.map2u.risda.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitInstance {
    companion object {

        private val retrofit by lazy {
            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build()
            val gsonBuilder = GsonBuilder().serializeNulls().create()
            Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder))
                .client(client)
                .build()
        }

        val authApi by lazy {
            retrofit.create(AuthService::class.java)
        }

        val appointmentApi by lazy {
            retrofit.create(AppointmentService::class.java)
        }

        private fun getBaseUrl(): String {
            return BuildConfig.BASE_URL
        }
    }
}