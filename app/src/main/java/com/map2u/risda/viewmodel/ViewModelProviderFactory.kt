package com.map2u.risda.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.map2u.risda.repository.AppRepository

class ViewModelProviderFactory(
    val app: Application,
    val appRepository: AppRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(AppointmentViewModel::class.java)) {
            return AppointmentViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(ReportViewModel::class.java)) {
            return ReportViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(VerifyViewModel::class.java)) {
            return VerifyViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(VerifyCompleteViewModel::class.java)) {
            return VerifyCompleteViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(Ts18ViewModel::class.java)) {
            return Ts18ViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(Ts18AViewModel::class.java)) {
            return Ts18AViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(Ts18EViewModel::class.java)) {
            return Ts18EViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
            return HomeViewModel(app, appRepository) as T
        }

        throw IllegalArgumentException("Unknown class name")
    }
}