package com.map2u.risda.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.map2u.risda.R
import com.map2u.risda.app.MyApplication
import com.map2u.risda.model.ErrorResponse
import com.map2u.risda.model.appointment.response.ListOfPendingFormResponse
import com.map2u.risda.network.SessionManager
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.util.Event
import com.map2u.risda.util.Resource
import com.map2u.risda.util.Utils
import kotlinx.coroutines.launch
import retrofit2.Response

class VerifyViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {

    private val _apiResponse = MutableLiveData<Event<Resource<ListOfPendingFormResponse>>>()
    val apiResponse: LiveData<Event<Resource<ListOfPendingFormResponse>>> = _apiResponse

    private var sessionManager: SessionManager = SessionManager(app.applicationContext)

    fun getFormsPendingValidation(
        start: Long,
        end: Long,
        noKpPpk: String?,
        jenisLK: String?,
        tsMohonId: String?
    ) = viewModelScope.launch {
        fetchLostOfFormsPendingValidation(
            start,
            end,
            noKpPpk,
            jenisLK,
            tsMohonId
        )
    }

    private suspend fun fetchLostOfFormsPendingValidation(
        start: Long,
        end: Long,
        noKpPpk: String?,
        jenisBorang: String?,
        tsMohonId: String?
    ) {
        _apiResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.getListOfFormsPendingValidation(
                    idToken = "${sessionManager.fetchIdToken()}",
                    ptCode = sessionManager.fetchPtCode(),
                    start = start,
                    end = end,
                    noKpPpk = noKpPpk,
                    jenisLK = jenisBorang,
                    tsMohonId = tsMohonId
                )
                _apiResponse.postValue(handleResponse(response))
            } else {
                _apiResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(VerifyViewModel::class.simpleName, t.message!!)
            _apiResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    private fun handleResponse(response: Response<ListOfPendingFormResponse>): Event<Resource<ListOfPendingFormResponse>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        val gson = Gson()
        val type = object : TypeToken<ErrorResponse>() {}.type
        var errorResponse: ErrorResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
        return Event(Resource.Error("Error ${response.code()}: ${errorResponse?.message}"))
    }

}