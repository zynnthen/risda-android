package com.map2u.risda.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.map2u.risda.R
import com.map2u.risda.app.MyApplication
import com.map2u.risda.db.DbConstants
import com.map2u.risda.db.DbHelper
import com.map2u.risda.model.ErrorResponse
import com.map2u.risda.model.form.response.UpdateAppointmentStatusResponse
import com.map2u.risda.model.form.response.UpdateFormResponse
import com.map2u.risda.module.AppointmentDbHelper
import com.map2u.risda.module.FormDbHelper
import com.map2u.risda.network.RequestBodies
import com.map2u.risda.network.SessionManager
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.util.Event
import com.map2u.risda.util.Resource
import com.map2u.risda.util.Utils
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import java.util.regex.Matcher
import java.util.regex.Pattern


class Ts18ViewModel(
    val app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {
    private val _apiResponse = MutableLiveData<Event<Resource<UpdateFormResponse>>>()
    val apiResponse: LiveData<Event<Resource<UpdateFormResponse>>> = _apiResponse

    private val _apiImageResponse = MutableLiveData<Event<Resource<UpdateFormResponse>>>()
    val apiImageResponse: LiveData<Event<Resource<UpdateFormResponse>>> = _apiImageResponse

    private val _apiApptStatusResponse =
        MutableLiveData<Event<Resource<UpdateAppointmentStatusResponse>>>()
    val apiApptStatusResponse: LiveData<Event<Resource<UpdateAppointmentStatusResponse>>> =
        _apiApptStatusResponse

    private val _apiFileResponse = MutableLiveData<Event<Resource<ResponseBody>>>()
    val apiFileResponse: LiveData<Event<Resource<ResponseBody>>> = _apiFileResponse

    private var sessionManager: SessionManager = SessionManager(app.applicationContext)

    fun postTS18Form(
        body: RequestBodies.AddTS18Form
    ) = viewModelScope.launch {
        addTS18Form(body)
    }

    fun putAppointmentStatus(
        body: RequestBodies.UpdateAppointmentStatus
    ) = viewModelScope.launch {
        updateAppointmentStatus(body)
    }

    fun putTS18Form(
        body: RequestBodies.AddTS18Form
    ) = viewModelScope.launch {
        updateTS18Form(body)
    }

    fun uploadFile(
        formId: String,
        file: MultipartBody.Part
    ) = viewModelScope.launch {
        uploadFormImage(formId, file)
    }

    fun getFile(
        fileId: String
    ) = viewModelScope.launch {
        getFileFromServer(fileId)
    }

    fun updateAppointmentOffline(noSiriTemujanji: String, form: RequestBodies.AddTS18Form) {
        val dbHelper = DbHelper(app.applicationContext)
        FormDbHelper.updateAppointment(dbHelper, noSiriTemujanji)

        val addTS18FormTableName = FormDbHelper.getAddTS18FormTableName()

        FormDbHelper.createTable(
            dbHelper,
            DbConstants.ADD_FORM_TS18_COLUMN_NAMES.toList(),
            addTS18FormTableName
        )
        val jsonArray = JsonArray()
        jsonArray.add(Utils.getGson()?.toJsonTree(form)?.asJsonObject)

        DbHelper.insertAsJSON(dbHelper, addTS18FormTableName, jsonArray)
    }

    fun updateImageOffline(noSiriTemujanji: String, filePaths: ArrayList<String>) {
        val dbHelper = DbHelper(app.applicationContext)

        val addTS18FormImageTableName = FormDbHelper.getAddTS18FormImageTableName()

        FormDbHelper.createTable(
            dbHelper,
            DbConstants.ADD_FORM_TS18_IMAGE_COLUMN_NAMES.toList(),
            addTS18FormImageTableName
        )
        val jsonArray = JsonArray()
        filePaths.forEach{
            val jsonObj = JsonObject()
            jsonObj.addProperty("noSiriTemujanji", noSiriTemujanji)
            jsonObj.addProperty("filePath", it)

            jsonArray.add(jsonObj)
        }

        DbHelper.insertAsJSON(dbHelper, addTS18FormImageTableName, jsonArray)
    }

    fun queryImageOffline(noSiriTemujanji: String): ArrayList<String> {
        val dbHelper = DbHelper(app.applicationContext)
        val imagePaths = AppointmentDbHelper.queryTs18Image(dbHelper, noSiriTemujanji)

        val filePaths: ArrayList<String> = ArrayList()
        imagePaths?.forEach{
            filePaths.add(it.get("filePath").toString())
        }

        return filePaths
    }

    private suspend fun addTS18Form(
        body: RequestBodies.AddTS18Form
    ) {
        _apiResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.addTS18Form(
                    idToken = "${sessionManager.fetchIdToken()}",
                    body = body
                )
                _apiResponse.postValue(handleResponse(response))
            } else {
                _apiResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(Ts18ViewModel::class.simpleName, t.message!!)
            _apiResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    private suspend fun updateAppointmentStatus(
        body: RequestBodies.UpdateAppointmentStatus
    ) {
        _apiApptStatusResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.updateAppointmentStatus(
                    idToken = "${sessionManager.fetchIdToken()}",
                    body = body
                )
                _apiApptStatusResponse.postValue(handleAppointmentStatusResponse(response))
            } else {
                _apiApptStatusResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(Ts18ViewModel::class.simpleName, t.message!!)
            _apiResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    private suspend fun updateTS18Form(
        body: RequestBodies.AddTS18Form
    ) {
        _apiResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.updateTS18Form(
                    idToken = "${sessionManager.fetchIdToken()}",
                    body = body
                )
                _apiResponse.postValue(handleResponse(response))
            } else {
                _apiResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(Ts18ViewModel::class.simpleName, t.message!!)
            _apiResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    private suspend fun uploadFormImage(
        formId: String,
        file: MultipartBody.Part
    ) {
        _apiImageResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.uploadFile(
                    idToken = "${sessionManager.fetchIdToken()}",
                    formId = formId,
                    docType = "image",
                    file = file
                )
                _apiImageResponse.postValue(handleResponse(response))
            } else {
                _apiImageResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(Ts18ViewModel::class.simpleName, t.message!!)
            _apiResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    private suspend fun getFileFromServer(
        fileId: String
    ) {
        _apiFileResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.getFile(
                    idToken = "${sessionManager.fetchIdToken()}",
                    fileId = fileId
                )
                _apiFileResponse.postValue(handleFileResponse(response))
            } else {
                _apiFileResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(Ts18ViewModel::class.simpleName, t.message!!)
            _apiFileResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    private fun handleResponse(response: Response<UpdateFormResponse>): Event<Resource<UpdateFormResponse>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        val gson = Gson()
        val type = object : TypeToken<ErrorResponse>() {}.type
        var errorResponse: ErrorResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
        return Event(Resource.Error("Error ${response.code()}: ${errorResponse?.message}"))
    }

    private fun handleAppointmentStatusResponse(response: Response<UpdateAppointmentStatusResponse>): Event<Resource<UpdateAppointmentStatusResponse>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        val gson = Gson()
        val type = object : TypeToken<ErrorResponse>() {}.type
        var errorResponse: ErrorResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
        return Event(Resource.Error("Error ${response.code()}: ${errorResponse?.message}"))
    }

    private fun handleFileResponse(response: Response<ResponseBody>): Event<Resource<ResponseBody>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                val contentDisposition = response.headers()["Content-Disposition"]
                val pattern: Pattern = Pattern.compile("filename=['\"]?([^'\"\\s]+)['\"]?")
                val matcher: Matcher = pattern.matcher(contentDisposition)
                var fileName = ""
                if (matcher.find()) {
                    fileName = matcher.group(0)
                    fileName = fileName.substring(fileName.indexOf("\"") + 1, fileName.lastIndexOf("\""))
                }

                return Event(Resource.Success(resultResponse, fileName))
            }
        }
        val gson = Gson()
        val type = object : TypeToken<ErrorResponse>() {}.type
        var errorResponse: ErrorResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
        return Event(Resource.Error("Error ${response.code()}: ${errorResponse?.message}"))
    }
}