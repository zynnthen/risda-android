package com.map2u.risda.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.map2u.risda.R
import com.map2u.risda.app.MyApplication
import com.map2u.risda.db.DbConstants
import com.map2u.risda.db.DbHelper
import com.map2u.risda.model.ErrorResponse
import com.map2u.risda.model.form.response.UpdateAppointmentStatusResponse
import com.map2u.risda.model.form.response.UpdateFormResponse
import com.map2u.risda.module.AppointmentDbHelper
import com.map2u.risda.module.DataProcessDbHelper
import com.map2u.risda.module.FormDbHelper
import com.map2u.risda.network.RequestBodies
import com.map2u.risda.network.SessionManager
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.util.Event
import com.map2u.risda.util.Resource
import com.map2u.risda.util.TimeUtils
import com.map2u.risda.util.Utils
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import retrofit2.Response

class HomeViewModel(
    val app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {

    private val _apiResponse = MutableLiveData<Event<Resource<UpdateFormResponse>>>()
    val apiResponse: LiveData<Event<Resource<UpdateFormResponse>>> = _apiResponse

    private val _apiImageResponse = MutableLiveData<Event<Resource<UpdateFormResponse>>>()
    val apiImageResponse: LiveData<Event<Resource<UpdateFormResponse>>> = _apiImageResponse

    private val _apiApptStatusResponse =
        MutableLiveData<Event<Resource<UpdateAppointmentStatusResponse>>>()
    val apiApptStatusResponse: LiveData<Event<Resource<UpdateAppointmentStatusResponse>>> =
        _apiApptStatusResponse

    private var sessionManager: SessionManager = SessionManager(app.applicationContext)

    fun isSyncRequired(): Boolean {
        val dbHelper = DbHelper(app.applicationContext)
        return dbHelper.isTableExist(DbConstants.APPOINTMENT_TABLE_NAME)
    }

    fun syncAppointmentStatus(): Int {
        val dbHelper = DbHelper(app.applicationContext)
        FormDbHelper.updateMissedAppointment(dbHelper, TimeUtils.get12amTimestamp())
        val noSiriTemujanjiList = AppointmentDbHelper.queryNoSiriTemujanji(dbHelper)

        noSiriTemujanjiList?.forEach {
            val updateAppointmentStatus = RequestBodies.UpdateAppointmentStatus(
                it.get("noSiriTemujanji").asString,
                it.get("statusTJanji").asString
            )
            putAppointmentStatus(updateAppointmentStatus)
        }

        return noSiriTemujanjiList?.size ?: 0
    }

    fun syncTs18From(): Int {
        val dbHelper = DbHelper(app.applicationContext)
        val forms = FormDbHelper.queryCompletedTs18Forms(dbHelper)

        val requestBodies =
            forms?.let { DataProcessDbHelper.massageTs18Data(it) }

        requestBodies?.forEachIndexed {index, it ->
            val filePaths =
                forms.get(index).get("filePaths").toString().replace("[", "").replace("]", "")
                    .replace("""[$"]""".toRegex(), "")

            postTS18Form(it, filePaths.split(",") )
        }

        return requestBodies?.size ?: 0
    }

    fun syncTs18aFrom(): Int {
        val dbHelper = DbHelper(app.applicationContext)
        val forms = FormDbHelper.queryCompletedTs18aForms(dbHelper)

        val requestBodies =
            forms?.let { DataProcessDbHelper.massageTs18aData(it) }

        requestBodies?.forEachIndexed {index, it ->
            val filePaths =
                forms.get(index).get("filePaths").toString().replace("[", "").replace("]", "")
                    .replace("""[$"]""".toRegex(), "")

            postTS18aForm(it, filePaths.split(",") )
        }

        return requestBodies?.size ?: 0
    }

    fun dropAllTables(
    ) {
        val dbHelper = DbHelper(app.applicationContext)
        dbHelper.deleteAllTables()
    }

    /*** TS18 form ***/
    private fun postTS18Form(
        body: RequestBodies.AddTS18Form,
        images: List<String>
    ) = viewModelScope.launch {
        addTS18Form(body, images)
    }

    private suspend fun addTS18Form(
        body: RequestBodies.AddTS18Form,
        images: List<String>
    ) {
        _apiResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.addTS18Form(
                    idToken = "${sessionManager.fetchIdToken()}",
                    body = body
                )
                _apiResponse.postValue(handleResponse(response, images))
            } else {
                _apiResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(Ts18ViewModel::class.simpleName, t.message!!)
            _apiResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    /*** TS18A form ***/
    private fun postTS18aForm(
        body: RequestBodies.AddTS18AForm,
        images: List<String>
    ) = viewModelScope.launch {
        if (body.ansuranNo == "TIADA") {
            body.ansuranNo = null
        }
        addTS18AForm(body, images)
    }

    private suspend fun addTS18AForm(
        body: RequestBodies.AddTS18AForm,
        images: List<String>
    ) {
        _apiResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.addTS18AForm(
                    idToken = "${sessionManager.fetchIdToken()}",
                    body = body
                )
                _apiResponse.postValue(handleResponse(response, images))
            } else {
                _apiResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(Ts18AViewModel::class.simpleName, t.message!!)
            _apiResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    /*** Save image ***/
    fun uploadFile(
        formId: String,
        file: MultipartBody.Part
    ) = viewModelScope.launch {
        uploadFormImage(formId, file)
    }

    private suspend fun uploadFormImage(
        formId: String,
        file: MultipartBody.Part
    ) {
        _apiImageResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.uploadFile(
                    idToken = "${sessionManager.fetchIdToken()}",
                    formId = formId,
                    docType = "image",
                    file = file
                )
                _apiImageResponse.postValue(handleResponse(response))
            } else {
                _apiImageResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(Ts18ViewModel::class.simpleName, t.message!!)
            _apiResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    private fun handleResponse(response: Response<UpdateFormResponse>, images: List<String>? = null): Event<Resource<UpdateFormResponse>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                resultResponse.images = images
                return Event(Resource.Success(resultResponse))
            }
        }
        val gson = Gson()
        val type = object : TypeToken<ErrorResponse>() {}.type
        var errorResponse: ErrorResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
        return Event(Resource.Error("Error ${response.code()}: ${errorResponse?.message}"))
    }

    /*** Appointment status ***/
    private fun putAppointmentStatus(
        body: RequestBodies.UpdateAppointmentStatus
    ) = viewModelScope.launch {
        updateAppointmentStatus(body)
    }

    private suspend fun updateAppointmentStatus(
        body: RequestBodies.UpdateAppointmentStatus
    ) {
        _apiApptStatusResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.updateAppointmentStatus(
                    idToken = "${sessionManager.fetchIdToken()}",
                    body = body
                )
                _apiApptStatusResponse.postValue(handleAppointmentStatusResponse(response))
            } else {
                _apiApptStatusResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(HomeViewModel::class.simpleName, t.message!!)
            _apiApptStatusResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    private fun handleAppointmentStatusResponse(response: Response<UpdateAppointmentStatusResponse>): Event<Resource<UpdateAppointmentStatusResponse>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        val gson = Gson()
        val type = object : TypeToken<ErrorResponse>() {}.type
        var errorResponse: ErrorResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
        return Event(Resource.Error("Error ${response.code()}: ${errorResponse?.message}"))
    }
}