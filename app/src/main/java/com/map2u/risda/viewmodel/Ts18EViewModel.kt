package com.map2u.risda.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.map2u.risda.R
import com.map2u.risda.app.MyApplication
import com.map2u.risda.model.ErrorResponse
import com.map2u.risda.model.form.response.UpdateFormResponse
import com.map2u.risda.network.RequestBodies
import com.map2u.risda.network.SessionManager
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.util.Event
import com.map2u.risda.util.Resource
import com.map2u.risda.util.Utils
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response

class Ts18EViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {
    private val _apiResponse = MutableLiveData<Event<Resource<UpdateFormResponse>>>()
    val apiResponse: LiveData<Event<Resource<UpdateFormResponse>>> = _apiResponse

    private val _apiImageResponse = MutableLiveData<Event<Resource<UpdateFormResponse>>>()
    val apiImageResponse: LiveData<Event<Resource<UpdateFormResponse>>> = _apiImageResponse

    private val _apiFileResponse = MutableLiveData<Event<Resource<ResponseBody>>>()
    val apiFileResponse: LiveData<Event<Resource<ResponseBody>>> = _apiFileResponse

    private val _apiVerifyFileResponse = MutableLiveData<Event<Resource<ResponseBody>>>()
    val apiVerifyFileResponse: LiveData<Event<Resource<ResponseBody>>> = _apiVerifyFileResponse

    private var sessionManager: SessionManager = SessionManager(app.applicationContext)

    fun postTS18EForm(
        body: RequestBodies.AddTS18EForm
    ) = viewModelScope.launch {
        addTS18EForm(body)
    }

    fun getFile(
        fileId: String
    ) = viewModelScope.launch {
        getFileFromServer(fileId)
    }

    fun getVerifyFile(
        fileId: String
    ) = viewModelScope.launch {
        getVerifyFileFromServer(fileId)
    }

    fun uploadFile(
        formId: String,
        file : MultipartBody.Part
    ) = viewModelScope.launch {
        uploadFormImage(formId, file)
    }

    private suspend fun addTS18EForm(
        body: RequestBodies.AddTS18EForm
    ) {
        _apiResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.addTS18EForm(
                    idToken = "${sessionManager.fetchIdToken()}",
                    body = body
                )
                _apiResponse.postValue(handleResponse(response))
            } else {
                _apiResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(Ts18ViewModel::class.simpleName, t.message!!)
            _apiResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    private suspend fun uploadFormImage(
        formId: String,
        file: MultipartBody.Part
    ) {
        _apiImageResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.uploadFile(
                    idToken = "${sessionManager.fetchIdToken()}",
                    formId = formId,
                    docType = "image",
                    file = file
                )
                _apiImageResponse.postValue(handleResponse(response))
            } else {
                _apiImageResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(Ts18ViewModel::class.simpleName, t.message!!)
            _apiResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    private suspend fun getFileFromServer(
        fileId: String
    ) {
        _apiFileResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.getFile(
                    idToken = "${sessionManager.fetchIdToken()}",
                    fileId = fileId
                )
                _apiFileResponse.postValue(handleFileResponse(response))
            } else {
                _apiFileResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(Ts18EViewModel::class.simpleName, t.message!!)
            _apiFileResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    private suspend fun getVerifyFileFromServer(
        fileId: String
    ) {
        _apiVerifyFileResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.getFile(
                    idToken = "${sessionManager.fetchIdToken()}",
                    fileId = fileId
                )
                _apiVerifyFileResponse.postValue(handleFileResponse(response))
            } else {
                _apiVerifyFileResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(Ts18EViewModel::class.simpleName, t.message!!)
            _apiVerifyFileResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    private fun handleResponse(response: Response<UpdateFormResponse>): Event<Resource<UpdateFormResponse>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        val gson = Gson()
        val type = object : TypeToken<ErrorResponse>() {}.type
        var errorResponse: ErrorResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
        return Event(Resource.Error("Error ${response.code()}: ${errorResponse?.message}"))
    }

    private fun handleFileResponse(response: Response<ResponseBody>): Event<Resource<ResponseBody>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        val gson = Gson()
        val type = object : TypeToken<ErrorResponse>() {}.type
        var errorResponse: ErrorResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
        return Event(Resource.Error("Error ${response.code()}: ${errorResponse?.message}"))
    }
}