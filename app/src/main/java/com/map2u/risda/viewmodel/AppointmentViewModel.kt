package com.map2u.risda.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.reflect.TypeToken
import com.map2u.risda.R
import com.map2u.risda.app.MyApplication
import com.map2u.risda.db.DbConstants.APPOINTMENT_TABLE_COLUMN_NAMES
import com.map2u.risda.db.DbConstants.FARM_BUDGET_TABLE_COLUMN_NAMES
import com.map2u.risda.db.DbConstants.FORM_TABLE_COLUMN_NAMES
import com.map2u.risda.db.DbConstants.KUMPULAN_TS_TABLE_COLUMN_NAMES
import com.map2u.risda.db.DbConstants.LOT_DIMILIKI_TABLE_COLUMN_NAMES
import com.map2u.risda.db.DbConstants.PARTICIPANT_TABLE_COLUMN_NAMES
import com.map2u.risda.db.DbConstants.TS_APPLICATION_TABLE_COLUMN_NAMES
import com.map2u.risda.db.DbHelper
import com.map2u.risda.model.ErrorResponse
import com.map2u.risda.model.appointment.AppointmentItem
import com.map2u.risda.model.appointment.response.LatestAppointmentsResponse
import com.map2u.risda.model.form.response.UpdateAppointmentStatusResponse
import com.map2u.risda.module.AppointmentDbHelper
import com.map2u.risda.module.DataProcessDbHelper
import com.map2u.risda.module.FormDbHelper
import com.map2u.risda.network.RequestBodies
import com.map2u.risda.network.SessionManager
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.util.Event
import com.map2u.risda.util.Resource
import com.map2u.risda.util.TimeUtils.get12amTimestamp
import com.map2u.risda.util.Utils
import kotlinx.coroutines.launch
import retrofit2.Response


class AppointmentViewModel(
    val app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {

    private val _apiResponse = MutableLiveData<Event<Resource<LatestAppointmentsResponse>>>()
    val apiResponse: LiveData<Event<Resource<LatestAppointmentsResponse>>> = _apiResponse

    private val _apiApptStatusResponse =
        MutableLiveData<Event<Resource<UpdateAppointmentStatusResponse>>>()
    val apiApptStatusResponse: LiveData<Event<Resource<UpdateAppointmentStatusResponse>>> =
        _apiApptStatusResponse

    private var sessionManager: SessionManager = SessionManager(app.applicationContext)

    fun getLatestAppointments(
        numberOfDays: Int, tsMohonId: String?,
        noKpPemohon: String?,
        namaPemohon: String?,
        noLot: String?,
        noGeran: String?,
        date: String?,
        isDownloadOffline: Boolean = false
    ) = viewModelScope.launch {
        fetchLatestAppointments(
            numberOfDays, tsMohonId,
            noKpPemohon,
            namaPemohon,
            noLot,
            noGeran,
            date,
            isDownloadOffline
        )
    }

    fun putAppointmentStatus(
        body: RequestBodies.UpdateAppointmentStatus
    ) = viewModelScope.launch {
        updateAppointmentStatus(body)
    }

    fun queryAppointmentOffline(type: String): ArrayList<AppointmentItem>? {
        val dbHelper = DbHelper(app.applicationContext)
        FormDbHelper.updateMissedAppointment(dbHelper, get12amTimestamp())
        val list =
            AppointmentDbHelper.queryAppointment(dbHelper, type, ">=", get12amTimestamp())

        return DataProcessDbHelper.massageAppointmentsData(app.applicationContext, list)
    }

    fun updateAppointmentOffline(noSiriTemujanji: String) {
        val dbHelper = DbHelper(app.applicationContext)
        FormDbHelper.updateAppointment(dbHelper, noSiriTemujanji)
    }

    private suspend fun fetchLatestAppointments(
        numberOfDays: Int,
        tsMohonId: String?,
        noKpPemohon: String?,
        namaPemohon: String?,
        noLot: String?,
        noGeran: String?,
        date: String?,
        isDownloadOffline: Boolean = false
    ) {
        _apiResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.getAppointmentsByNumberOfDays(
                    idToken = "${sessionManager.fetchIdToken()}",
                    days = numberOfDays,
                    tsMohonId = tsMohonId,
                    noKpPemohon = noKpPemohon,
                    namaPemohon = namaPemohon,
                    noLot = noLot,
                    noGeran = noGeran,
                    date = date,
                    pegKemaskini = sessionManager.fetchUsername()
                )
                _apiResponse.postValue(handleResponse(response, isDownloadOffline))
            } else {
                _apiResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(AppointmentViewModel::class.simpleName, t.message!!)
            _apiResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    private suspend fun updateAppointmentStatus(
        body: RequestBodies.UpdateAppointmentStatus
    ) {
        _apiApptStatusResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.updateAppointmentStatus(
                    idToken = "${sessionManager.fetchIdToken()}",
                    body = body
                )
                _apiApptStatusResponse.postValue(handleAppointmentStatusResponse(response))
            } else {
                _apiApptStatusResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(AppointmentViewModel::class.simpleName, t.message!!)
            _apiApptStatusResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    private fun handleResponse(response: Response<LatestAppointmentsResponse>, isDownloadOffline: Boolean): Event<Resource<LatestAppointmentsResponse>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if(isDownloadOffline) {
                    /*** start of insert offline db ***/
                    val dbHelper = DbHelper(app.applicationContext)
                    val appointmentTableName = AppointmentDbHelper.getAppointmentTableName()
                    val tsApplicationTableName = AppointmentDbHelper.getTsApplicationTableName()
                    val lotDimilikiTableName = AppointmentDbHelper.getLotDimilikiTableName()
                    val participantTableName = AppointmentDbHelper.getParticipantTableName()
                    val kumpulanTSTableName = AppointmentDbHelper.getKumpulanTSTableName()
                    val farmBudgetTableName = AppointmentDbHelper.getFarmBudgetTableName()
                    val formTableName = AppointmentDbHelper.getFormTableName()

                    val arrayAppointments =
                        Utils.getGson()?.toJsonTree(resultResponse.datums)?.asJsonArray

                    val tsApplications = resultResponse.datums?.map { it.tsApplication }
                    val arrayTsApplication =
                        Utils.getGson()?.toJsonTree(tsApplications?.filterNotNull())?.asJsonArray

                    val lotDimilikiList = tsApplications?.map { it?.lotDimilikiList?.get(0) }
                    val arrayLotDimiliki = Utils.getGson()?.toJsonTree(lotDimilikiList?.filterNotNull())?.asJsonArray

                    val arrayParticipants = JsonArray()
                    val arrayKumpulans = JsonArray()
                    resultResponse.datums?.forEach {
                        it.participantList?.forEach{ item -> item.lejer.toString() }
                        arrayParticipants.addAll(Utils.getGson()?.toJsonTree(it.participantList?.toList())?.asJsonArray)

                        val ts18aTsApplication = it.participantList?.map { item -> item.tsApplication }
                        arrayTsApplication?.addAll(
                            Utils.getGson()
                                ?.toJsonTree(ts18aTsApplication?.filterNotNull())?.asJsonArray
                        )

                        val ts18aLotDimilikiList = ts18aTsApplication?.map { item -> item?.lotDimilikiList?.get(0) }
                        arrayLotDimiliki?.addAll(
                            Utils.getGson()
                                ?.toJsonTree(ts18aLotDimilikiList)?.asJsonArray
                        )

                        val ts18aKumpulan = it.participantList?.map { item -> item.kumpulanTS }
                        if(ts18aKumpulan?.size ?: 0 > 0) {
                            arrayKumpulans.add(Utils.getGson()?.toJsonTree(ts18aKumpulan?.filterNotNull()?.get(0)))
                        }
                    }

                    val arrayFarmBudgetItems = JsonArray()
                    resultResponse.datums?.forEach {
                        if (it.farmBudgetItems?.size ?: 0 > 0) {
                            it.farmBudgetItems?.forEach { item ->
                                item.noSiriTemujanji = it.noSiriTemujanji
                            }
                            arrayFarmBudgetItems.addAll(
                                Utils.getGson()?.toJsonTree(it.farmBudgetItems)?.asJsonArray
                            )
                        }
                    }

                    val arrayForms = JsonArray()
                    resultResponse.datums?.forEach {
                        if (it.formList?.size ?: 0 > 0) {
                            arrayForms.addAll(
                                Utils.getGson()?.toJsonTree(it.formList)?.asJsonArray
                            )
                        }
                    }

                    AppointmentDbHelper.createTable(dbHelper, APPOINTMENT_TABLE_COLUMN_NAMES.toList(), appointmentTableName)
                    DbHelper.insertAsJSON(dbHelper, appointmentTableName, arrayAppointments)

                    AppointmentDbHelper.createTable(dbHelper, TS_APPLICATION_TABLE_COLUMN_NAMES.toList(), AppointmentDbHelper.getTsApplicationTableName())
                    DbHelper.insertAsJSON(dbHelper, tsApplicationTableName, arrayTsApplication)

                    AppointmentDbHelper.createTable(dbHelper, LOT_DIMILIKI_TABLE_COLUMN_NAMES.toList(), AppointmentDbHelper.getLotDimilikiTableName())
                    DbHelper.insertAsJSON(dbHelper, lotDimilikiTableName, arrayLotDimiliki)

                    AppointmentDbHelper.createTable(dbHelper, PARTICIPANT_TABLE_COLUMN_NAMES.toList(), participantTableName)
                    DbHelper.insertAsJSON(dbHelper, participantTableName, arrayParticipants)

                    AppointmentDbHelper.createTable(dbHelper, KUMPULAN_TS_TABLE_COLUMN_NAMES.toList(), kumpulanTSTableName)
                    DbHelper.insertAsJSON(dbHelper, kumpulanTSTableName, arrayKumpulans)

                    AppointmentDbHelper.createTable(dbHelper, FARM_BUDGET_TABLE_COLUMN_NAMES.toList(), farmBudgetTableName)
                    DbHelper.insertAsJSON(dbHelper, farmBudgetTableName, arrayFarmBudgetItems)

                    AppointmentDbHelper.createTable(dbHelper, FORM_TABLE_COLUMN_NAMES.toList(), formTableName)
                    DbHelper.insertAsJSON(dbHelper, formTableName, arrayForms)
                    /*** end of insert offline db ***/
                }

                resultResponse.isDownloadOffline = isDownloadOffline

                return Event(Resource.Success(resultResponse))
            }
        }
        val gson = Gson()
        val type = object : TypeToken<ErrorResponse>() {}.type
        var errorResponse: ErrorResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
        return Event(Resource.Error("Error ${response.code()}: ${errorResponse?.message}"))
    }

    private fun handleAppointmentStatusResponse(response: Response<UpdateAppointmentStatusResponse>): Event<Resource<UpdateAppointmentStatusResponse>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        val gson = Gson()
        val type = object : TypeToken<ErrorResponse>() {}.type
        var errorResponse: ErrorResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
        return Event(Resource.Error("Error ${response.code()}: ${errorResponse?.message}"))
    }
}