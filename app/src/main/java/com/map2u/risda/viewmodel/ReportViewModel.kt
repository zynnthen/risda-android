package com.map2u.risda.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.map2u.risda.R
import com.map2u.risda.app.MyApplication
import com.map2u.risda.db.DbHelper
import com.map2u.risda.model.ErrorResponse
import com.map2u.risda.model.appointment.AppointmentItem
import com.map2u.risda.model.appointment.response.ListOfAppointmentResponse
import com.map2u.risda.module.AppointmentDbHelper
import com.map2u.risda.module.DataProcessDbHelper
import com.map2u.risda.network.SessionManager
import com.map2u.risda.repository.AppRepository
import com.map2u.risda.util.Event
import com.map2u.risda.util.Resource
import com.map2u.risda.util.TimeUtils
import com.map2u.risda.util.Utils
import kotlinx.coroutines.launch
import retrofit2.Response

class ReportViewModel(
    val app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {

    private val _apiResponse = MutableLiveData<Event<Resource<ListOfAppointmentResponse>>>()
    val apiResponse: LiveData<Event<Resource<ListOfAppointmentResponse>>> = _apiResponse

    private var sessionManager: SessionManager = SessionManager(app.applicationContext)

    fun getListOfAppointments(
        pageNumber: Int,
        tsMohonId: String?,
        noKpPemohon: String?,
        namaPemohon: String?,
        noLot: String?,
        noGeran: String?,
        date: String?
    ) = viewModelScope.launch {
        fetchListOfAppointments(
            pageNumber,
            tsMohonId,
            noKpPemohon,
            namaPemohon,
            noLot,
            noGeran,
            date
        )
    }

    fun queryAppointmentOffline(type: String): ArrayList<AppointmentItem>? {
        val dbHelper = DbHelper(app.applicationContext)
        val list =
            AppointmentDbHelper.queryOfflineCompletedAppointment(dbHelper, type, "<=", TimeUtils.get1159pmTimestamp())

        return DataProcessDbHelper.massageAppointmentsData(app.applicationContext, list)
    }

    private suspend fun fetchListOfAppointments(
        pageNumber: Int,
        tsMohonId: String?,
        noKpPemohon: String?,
        namaPemohon: String?,
        noLot: String?,
        noGeran: String?,
        date: String?
    ) {
        _apiResponse.postValue(Event(Resource.Loading()))
        try {
            if (Utils.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.getListOfAppointments(
                    idToken = "${sessionManager.fetchIdToken()}",
                    page = pageNumber,
                    tsMohonId = tsMohonId,
                    noKpPemohon = noKpPemohon,
                    namaPemohon = namaPemohon,
                    noLot = noLot,
                    noGeran = noGeran,
                    date = date,
                    pegKemaskini = sessionManager.fetchUsername()
                )
                _apiResponse.postValue(handleResponse(response))
            } else {
                _apiResponse.postValue(
                    Event(
                        Resource.NoConnection(
                            getApplication<MyApplication>().getString(
                                R.string.no_internet_connection
                            )
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            Log.e(ReportViewModel::class.simpleName, t.message!!)
            _apiResponse.postValue(
                Event(
                    Resource.Error(
                        t.message!!
                    )
                )
            )
        }
    }

    private fun handleResponse(response: Response<ListOfAppointmentResponse>): Event<Resource<ListOfAppointmentResponse>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        val gson = Gson()
        val type = object : TypeToken<ErrorResponse>() {}.type
        var errorResponse: ErrorResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
        return Event(Resource.Error("Error ${response.code()}: ${errorResponse?.message}"))
    }

}