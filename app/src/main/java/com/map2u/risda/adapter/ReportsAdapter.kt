package com.map2u.risda.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.map2u.risda.R
import com.map2u.risda.databinding.ItemReportsBinding
import com.map2u.risda.model.appointment.ReportItem
import com.map2u.risda.util.TimeUtils
import com.map2u.risda.util.display.DisplayUtils.getJenisBorang

class ReportsAdapter(private val clickListener: (ReportItem) -> Unit) :
    RecyclerView.Adapter<ReportsAdapter.ReportsViewHolder>() {

    inner class ReportsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemReportsBinding.bind(itemView)
    }

    private val differCallback = object : DiffUtil.ItemCallback<ReportItem>() {
        override fun areItemsTheSame(oldItem: ReportItem, newItem: ReportItem): Boolean {
            return oldItem.form?.jenisLK == newItem.form?.jenisLK
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(
            oldItem: ReportItem,
            newItem: ReportItem
        ): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    /**
     * Called when RecyclerView needs a new [ViewHolder] of the given type to represent
     * an item.
     *
     *
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     *
     *
     * The new ViewHolder will be used to display items of the adapter using
     * [.onBindViewHolder]. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary [View.findViewById] calls.
     *
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     * an adapter position.
     * @param viewType The view type of the new View.
     *
     * @return A new ViewHolder that holds a View of the given view type.
     * @see .getItemViewType
     * @see .onBindViewHolder
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_reports, parent, false)
        return ReportsViewHolder(view)
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the [ViewHolder.itemView] to reflect the item at the given
     * position.
     *
     *
     * Note that unlike [android.widget.ListView], RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the `position` parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use [ViewHolder.getAdapterPosition] which will
     * have the updated adapter position.
     *
     * Override [.onBindViewHolder] instead if Adapter can
     * handle efficient partial bind.
     *
     * @param holder The ViewHolder which should be updated to represent the contents of the
     * item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    override fun onBindViewHolder(holder: ReportsViewHolder, position: Int) {
        val item = differ.currentList[position]
        var jenisBorang = ""
        with(holder) {
            var masa = "Tarikh lawat: "
            if (item.form != null) {
                masa += "${item.form!!.tkhLawat ?.let { TimeUtils.displayDate(it) }}"

                jenisBorang = getJenisBorang(
                    item.form?.jenisLK,
                    item.appointmentItem?.moduleOwner,
                    item.appointmentItem?.tsPendekatan
                )
            }

            val ansuranNo = "Ansuran No: ${item.appointmentItem?.getAnsuranNo}"
            var noPermohonan = "No Permohonan: ${item.appointmentItem?.tsApplication?.tsMohonId}"

            if(jenisBorang == "TS18A (Berkumpulan)") {
                binding.textviewNamaPemohon.visibility = View.VISIBLE
                binding.textviewKpPemohon.visibility = View.VISIBLE
                binding.textviewKumpulan.visibility = View.VISIBLE

                val kumpulan = "Nama Kumpulan: ${item.participant?.kumpulanTS?.namaKumpulan}"
                binding.textviewKumpulan.text = kumpulan

                var namaPemohon = "Nama Pemohon: ${item.participant?.tsApplication?.namaPemohon}"
                var kpPemohon = "No KP Pemohon: ${item.participant?.tsApplication?.icNoPemohon}"
                noPermohonan = "No Permohonan: ${item.participant?.tsApplication?.tsMohonId}"

                binding.textviewNamaPemohon.text = namaPemohon
                binding.textviewKpPemohon.text = kpPemohon

            } else {
                binding.textviewNamaPemohon.visibility = View.VISIBLE
                binding.textviewKpPemohon.visibility = View.VISIBLE
                binding.textviewKumpulan.visibility = View.GONE

                var namaPemohon = "Nama Pemohon: ${item.appointmentItem?.tsApplication?.namaPemohon}"
                var kpPemohon = "No KP Pemohon: ${item.appointmentItem?.tsApplication?.icNoPemohon}"

                if (jenisBorang.contains("TS18A") && item.participant != null) {
                    namaPemohon = "Nama Pemohon: ${item.participant?.tsApplication?.namaPemohon}"
                    kpPemohon = "No KP Pemohon: ${item.participant?.tsApplication?.icNoPemohon}"
                }

                binding.textviewNamaPemohon.text = namaPemohon
                binding.textviewKpPemohon.text = kpPemohon
            }

            binding.textviewMasa.text = masa
            binding.textviewNoPermohonan.text = noPermohonan
            binding.textviewJenisBorang.text = jenisBorang
            binding.textviewAnsuran.text = ansuranNo

        }

        if(jenisBorang.isNullOrEmpty()) {
            holder.itemView.isClickable = false
        } else {
            holder.itemView.isClickable = true
            holder.itemView.setOnClickListener { clickListener(item) }
        }
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    override fun getItemCount() = differ.currentList.size
}