package com.map2u.risda.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.map2u.risda.R
import com.map2u.risda.databinding.ItemAppointmentsBinding
import com.map2u.risda.model.appointment.AppointmentItem
import com.map2u.risda.util.TimeUtils.displayDate
import com.map2u.risda.util.TimeUtils.displayTime
import com.map2u.risda.util.display.DisplayUtils.getJenisBorang
import java.util.*

class AppointmentsAdapter(private val clickListener: (AppointmentItem) -> Unit) :
    RecyclerView.Adapter<AppointmentsAdapter.AppointmentsViewHolder>() {

    inner class AppointmentsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemAppointmentsBinding.bind(itemView)
    }

    private val differCallback = object : DiffUtil.ItemCallback<AppointmentItem>() {
        override fun areItemsTheSame(oldItem: AppointmentItem, newItem: AppointmentItem): Boolean {
            return oldItem.noSiriTemujanji == newItem.noSiriTemujanji
        }

        override fun areContentsTheSame(
            oldItem: AppointmentItem,
            newItem: AppointmentItem
        ): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    /**
     * Called when RecyclerView needs a new [ViewHolder] of the given type to represent
     * an item.
     *
     *
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     *
     *
     * The new ViewHolder will be used to display items of the adapter using
     * [.onBindViewHolder]. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary [View.findViewById] calls.
     *
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     * an adapter position.
     * @param viewType The view type of the new View.
     *
     * @return A new ViewHolder that holds a View of the given view type.
     * @see .getItemViewType
     * @see .onBindViewHolder
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppointmentsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_appointments, parent, false)
        return AppointmentsViewHolder(view)
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the [ViewHolder.itemView] to reflect the item at the given
     * position.
     *
     *
     * Note that unlike [android.widget.ListView], RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the `position` parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use [ViewHolder.getAdapterPosition] which will
     * have the updated adapter position.
     *
     * Override [.onBindViewHolder] instead if Adapter can
     * handle efficient partial bind.
     *
     * @param holder The ViewHolder which should be updated to represent the contents of the
     * item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    override fun onBindViewHolder(holder: AppointmentsViewHolder, position: Int) {
        val item = differ.currentList[position]
        val jenisBorang = getJenisBorang(
            item.jenisLK,
            item.moduleOwner,
            item.tsPendekatan
        )

        with(holder) {
            val masa = "${item.tkhTJanji?.let { displayDate(it) }} ${
                item.masaTJanji?.let {
                    displayTime(it + item.tkhTJanji!!)
                }
            }"
            val noSiriTemujanji = "No Siri Temujanji: ${item.noSiriTemujanji}"
            val namaPemohon = "Nama Pemohon: ${item.tsApplication?.namaPemohon}"
            val kpPemohon = "No KP Pemohon: ${item.tsApplication?.icNoPemohon}"
            val tempat = "Tempat Temujanji: ${item.tempatTJanji}"
            val ansuranNo = "Ansuran No: ${item.getAnsuranNo}"

            if(jenisBorang == "TS18A (Berkumpulan)") {
                binding.textviewNamaPemohon.visibility = View.GONE
                binding.textviewKpPemohon.visibility = View.GONE
                binding.textviewKumpulan.visibility = View.VISIBLE

                try {
                    val kumpulan = "Name Kumpulan: ${item.participantList?.get(0)?.kumpulanTS?.namaKumpulan}"
                    binding.textviewKumpulan.text = kumpulan
                } catch (e: Exception) {
                    Log.e("AppointmentsAdapter", e.toString())
                }

            } else {
                binding.textviewNamaPemohon.visibility = View.VISIBLE
                binding.textviewKpPemohon.visibility = View.VISIBLE
                binding.textviewKumpulan.visibility = View.GONE

                binding.textviewNamaPemohon.text = namaPemohon
                binding.textviewKpPemohon.text = kpPemohon
            }

            binding.textviewMasa.text = masa
            binding.textviewNoSiriTemujanji.text = noSiriTemujanji
            binding.textviewTempat.text = tempat

            binding.textviewJenisBorang.text = jenisBorang
            binding.textviewAnsuran.text = ansuranNo
        }
        if(jenisBorang.isNullOrEmpty()) {
            holder.itemView.isClickable = false
        } else {
            holder.itemView.isClickable = true
            holder.itemView.setOnClickListener { clickListener(item) }
        }
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    override fun getItemCount() = differ.currentList.size
}
