package com.map2u.risda.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.map2u.risda.R
import com.map2u.risda.databinding.ItemPendingVerifyBinding
import com.map2u.risda.model.appointment.PendingVerify
import com.map2u.risda.util.TimeUtils

class PendingVerifyAdapter(private val clickListener: (PendingVerify) -> Unit) :
    RecyclerView.Adapter<PendingVerifyAdapter.PendingVerifyViewHolder>() {

    inner class PendingVerifyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemPendingVerifyBinding.bind(itemView)
    }

    private val differCallback = object : DiffUtil.ItemCallback<PendingVerify>() {
        override fun areItemsTheSame(oldItem: PendingVerify, newItem: PendingVerify): Boolean {
            return oldItem.noSiriTemujanji == newItem.noSiriTemujanji
        }

        override fun areContentsTheSame(
            oldItem: PendingVerify,
            newItem: PendingVerify
        ): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PendingVerifyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_pending_verify, parent, false)
        return PendingVerifyViewHolder(view)
    }

    override fun onBindViewHolder(holder: PendingVerifyViewHolder, position: Int) {
        val item = differ.currentList[position]
        with(holder) {
            var masa = "Tarikh lawat: ${item.tkhLawat ?.let { TimeUtils.displayDate(it) }}"

            val ansuranNo = "Ansuran No: ${item.getAnsuranNo}"
            var noPermohonan = "No Permohonan: ${item.tsMohonId}"
            if(item.jenisLK == "TS18A") {
                noPermohonan = "No Permohonan: ${item.tsApplication?.tsMohonNo}"
            }

            if(item.kumpulanId != null) {
                binding.textviewNamaPemohon.visibility = View.VISIBLE
                binding.textviewKpPemohon.visibility = View.VISIBLE
                binding.textviewKumpulan.visibility = View.VISIBLE

                val kumpulan = "Nama Kumpulan: ${item.namaKumpulan}"
                binding.textviewKumpulan.text = kumpulan

                var namaPemohon = "Nama Pemohon: ${item.tsApplication?.namaPemohon}"
                var kpPemohon = "No KP Pemohon: ${item.tsApplication?.icNoPemohon}"

                binding.textviewNamaPemohon.text = namaPemohon
                binding.textviewKpPemohon.text = kpPemohon

            } else {
                binding.textviewNamaPemohon.visibility = View.VISIBLE
                binding.textviewKpPemohon.visibility = View.VISIBLE
                binding.textviewKumpulan.visibility = View.GONE

                val namaPemohon = "Nama Pemohon: ${item.tsApplication?.namaPemohon}"
                val kpPemohon = "No KP Pemohon: ${item.tsApplication?.icNoPemohon}"

                binding.textviewNamaPemohon.text = namaPemohon
                binding.textviewKpPemohon.text = kpPemohon
            }

            binding.textviewMasa.text = masa
            binding.textviewNoPermohonan.text = noPermohonan
            binding.textviewJenisBorang.text = item.jenisLK
            binding.textviewAnsuran.text = ansuranNo
        }

        holder.itemView.setOnClickListener { clickListener(item) }
    }

    override fun getItemCount() = differ.currentList.size
}